using System;

public struct Vector2
{
	public float x;
	public float y;
	public float Magnitude
	{
		get
		{
			return (float) Math.Sqrt(x * x + y * y);
		}
	}
	public float SqrMagnitude
	{
		get
		{
			return x * x + y * y;
		}
	}
	public static Vector2 One
	{
		get
		{
			return new Vector2(1, 1);
		}
	}

	public Vector2 (float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2 Normalized ()
	{
		return this / Magnitude;
	}

	public static Vector2 Random (Random random)
	{
		return FromFacingAngle(MathExtensions.Random(0, 360, random));
	}
		
	public static Vector2 FromFacingAngle (float angle)
	{
		angle *= MathExtensions.DEGREES_PER_RADIAN;
		return new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle)).Normalized();
	}

	public static Vector2 FromNormalizedPoint (Vector2 center, Vector2 size, Vector2 normalizedPoint)
	{
		return center + size * normalizedPoint;
	}

	public static Vector2 ToNormalizedPosition (Vector2 center, Vector2 size, Vector2 point)
	{
		return One / size * (point - center);
	}

	public static float GetDifference (Vector2 v1, Vector2 v2)
	{
		float dX = Math.Abs(v1.x - v2.x);
		float dY = Math.Abs(v1.y - v2.y);
		return (dX + dY) / 2;
	}

	public static Vector2 operator+ (Vector2 v, Vector2 v2)
	{
		return new Vector2(v.x + v2.x, v.y + v2.y);
	}

	public static Vector2 operator- (Vector2 v, Vector2 v2)
	{
		return new Vector2(v.x - v2.x, v.y - v2.y);
	}

	public static Vector2 operator* (Vector2 v, Vector2 v2)
	{
		return new Vector2(v.x * v2.x, v.y * v2.y);
	}

	public static Vector2 operator/ (Vector2 v, Vector2 v2)
	{
		return new Vector2(v.x / v2.x, v.y / v2.y);
	}

	public static Vector2 operator* (Vector2 v, float f)
	{
		return new Vector2(v.x * f, v.y * f);
	}

	public static Vector2 operator/ (Vector2 v, float f)
	{
		return new Vector2(v.x / f, v.y / f);
	}
}