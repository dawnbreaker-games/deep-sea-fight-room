using PlayerIO.GameLibrary;

public class Player : BasePlayer
{
	public Vector2 position;
	public float rotation;
	public bool id;
	public int score;
	public Vector3 color;
}