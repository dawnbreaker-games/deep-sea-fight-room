using PlayerIO.GameLibrary;

[RoomType("Duel")]
public class Duel : Game<Player>
{
	Match match;

	public override void GameStarted ()
	{
        match = new Match(this);
	}

	public override void UserJoined (Player player)
	{
        match.UserJoined (player);
	}

	public override void UserLeft (Player player)
	{
        match.UserLeft (player);
	}

	public override void GotMessage (Player player, Message message)
	{
        match.GotMessage (player, message);
	}
}