using System;
using PlayerIO.GameLibrary;
using System.Collections.Generic;

public class Match
{
	bool ranked;
	byte maxScore;
	Game<Player> game;
	List<bool> unusedPlayerIds = new List<bool>();
	Dictionary<bool, Player> playersDict = new Dictionary<bool, Player>();
	List<Player> allPlayers = new List<Player>();
	bool hasEnded;
	Random random = new Random();
	const uint MAX_COLOR_TRIES = 10;
	const float MIN_COLOR_SIMULARITY = .3f;

	public Match (Game<Player> game)
	{
		this.game = game;
		unusedPlayerIds.Add(true);
		unusedPlayerIds.Add(false);
		ranked = bool.Parse(game.RoomData["ranked"]);
		maxScore = byte.Parse(game.RoomData["maxScore"]);
		if (ranked)
			game.PreloadPlayerObjects = true;
	}

	public void UserJoined (Player player)
	{
		bool playerId = unusedPlayerIds[0];
		allPlayers.Add(player);
		Vector3 teamColor = new Vector3();
		float leastColorSimularity = float.MaxValue;
		Vector3 leastSimularColor = new Vector3();
		bool isValidColor = true;
		for (int i = 0; i < MAX_COLOR_TRIES; i ++)
		{
			isValidColor = true;
			teamColor = new Vector3((float) random.NextDouble(), (float) random.NextDouble(), (float) random.NextDouble());
			foreach (KeyValuePair<bool, Player> keyValuePair in playersDict)
			{
				float colorSimularity = 1f - Vector3.GetDifference(keyValuePair.Value.color, teamColor);
				if (colorSimularity < MIN_COLOR_SIMULARITY)
				{
					if (colorSimularity < leastColorSimularity)
					{
						leastColorSimularity = colorSimularity;
						leastSimularColor = teamColor;
					}
					isValidColor = false;
					break;
				}
			}
			if (isValidColor)
				break;
		}
		if (!isValidColor)
			teamColor = leastSimularColor;
		player.color = teamColor;
		Message spawnLocalPlayerMessage = Message.Create("Spawn Player", teamColor.x, teamColor.y, teamColor.z);
		player.Send(spawnLocalPlayerMessage);
		if (!playerId)
		{
			Player firstPlayer = playersDict[!playerId];
			Message spawnFirstPlayerMessage = Message.Create("Spawn Player", firstPlayer.color.x, firstPlayer.color.y, firstPlayer.color.z);
			firstPlayer.Send(spawnLocalPlayerMessage);
			player.Send(spawnFirstPlayerMessage);
		}
		unusedPlayerIds.RemoveAt(0);
		player.id = playerId;
		playersDict.Add(playerId, player);
	}

	public void UserLeft (Player player)
	{
		playersDict.Remove(player.id);
		if (ranked && playersDict.Count == 1 && !hasEnded)
		{
			playersDict[!player.id].Send("Remove Player");
			End (playersDict[!player.id]);
		}
		else if (playersDict.Count == 0)
			game.Visible = false;
		unusedPlayerIds.Add(player.id);
	}

	public void GotMessage (Player player, Message message)
	{
		if (message.Type == "Move Player")
			player.position = new Vector2(message.GetFloat(0), message.GetFloat(1));
		else if (message.Type == "Rotate Player")
			player.rotation = message.GetFloat(0);
		else if (message.Type == "Kill Player")
		{
			bool killerId = !player.id;
			Player killer = playersDict[killerId];
			if (killer != player)
			{
				killer.score ++;
				if (killer.score >= maxScore && !hasEnded)
					End (killer);
			}
			else
				player.score --;
			if (ranked)
			{
				DatabaseObject victimDBObject = player.PlayerObject;
				victimDBObject.Set("duelDeaths", victimDBObject.GetUInt("duelDeaths") + 1);
				victimDBObject.Set("duelDeathsInCurrentSeason", victimDBObject.GetUInt("duelDeathsInCurrentSeason") + 1);
				victimDBObject.Save();
				DatabaseObject killerDBObject = killer.PlayerObject;
				killerDBObject.Set("duelKills", killerDBObject.GetUInt("duelKills") + 1);
				killerDBObject.Set("duelKillsinCurrentSeason", killerDBObject.GetUInt("duelKillsinCurrentSeason") + 1);
				killerDBObject.Save();
			}
		}
		playersDict[!player.id].Send(message);
	}

	void End (Player winner)
	{
		hasEnded = true;
		if (ranked)
		{
			List<Player> losingPlayers = new List<Player>(allPlayers);
			losingPlayers.Remove(winner);
            DatabaseObject winnerDBObject = winner.PlayerObject;
            winnerDBObject.Set("duelWins", winnerDBObject.GetUInt("duelWins") + 1);
            winnerDBObject.Set("duelWinsInCurrentSeason", winnerDBObject.GetUInt("duelWinsInCurrentSeason") + 1);
            winnerDBObject.Set("duelSkill", winnerDBObject.GetFloat("duelSkill") + 1);
            winnerDBObject.Set("duelSkillInCurrentSeason", winnerDBObject.GetFloat("duelSkillInCurrentSeason") + 1);
            winnerDBObject.Save();
            for (int i = 0; i < losingPlayers.Count; i ++)
			{
				Player loser = losingPlayers[i];
				DatabaseObject loserDBObject = loser.PlayerObject;
                loserDBObject.Set("duelLosses", loserDBObject.GetUInt("dueLosses") + 1);
                loserDBObject.Set("duelLossesInCurrentSeason", loserDBObject.GetUInt("dueLossesInCurrentSeason") + 1);
				float skillChange;
				if (loser.score >= 0)
					skillChange = 1f - ((float) loser.score / maxScore);
				else
					skillChange = 1f + ((float) Math.Abs(loser.score) / maxScore);
                loserDBObject.Set("duelSkill", loserDBObject.GetFloat("duelSkill") - skillChange);
                loserDBObject.Set("duelSkillInCurrentSeason", loserDBObject.GetFloat("duelSkillInCurrentSeason") - skillChange);
				loserDBObject.Save();
			}
		}
		DisconnectPlayers ();
	}

	void DisconnectPlayers ()
	{
		List<Player> players = new List<Player>(playersDict.Values);
		for (int i = 0; i < players.Count; i ++)
		{
			Player player = players[i];
			player.Disconnect();
		}
		game.Visible = false;
	}
}