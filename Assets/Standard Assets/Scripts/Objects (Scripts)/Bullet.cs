﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class Bullet : Hazard
	{
		public float range;
		public float duration;
		public Rigidbody2D rigid;
		public float moveSpeed;
		public AutoDespawnMode autoDespawnMode;
		public ObjectPool.RangedDespawn rangedDespawn;
		public ObjectPool.DelayedDespawn delayedDespawn;
		public new Collider2D collider;
		public byte hitsTillDespawn;
		public LayerMask whatReducesHits;
		[HideInInspector]
		public Vector2 velocity;
		[HideInInspector]
		public Vector2 extraVelocity;
		public Action onDisable;
		public List<EventManager.Event> events = new List<EventManager.Event>();
		[HideInInspector]
		public Vector2 previousFacing;
		[HideInInspector]
		public Entity shooter;
		public new static List<Bullet> instances = new List<Bullet>();
		uint hitsTillDespawnRemaining;
		
		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (collider == null)
					collider = GetComponentInChildren<Collider2D>();
				return;
			}
#endif
			base.OnEnable ();
			hitsTillDespawnRemaining = hitsTillDespawn;
			if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
				rangedDespawn = ObjectPool.instance.RangeDespawn(prefabIndex, gameObject, trs, range);
			else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
				delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, duration);
			if (moveSpeed > 0)
			{
				velocity = trs.up * moveSpeed;
				rigid.linearVelocity = velocity + extraVelocity;
			}
			instances.Add(this);
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			for (int i = 0; i < events.Count; i ++)
			{
				EventManager.Event _event = events[i];
				EventManager.RemoveEvent (_event);
			}
			if (ObjectPool.Instance != null && !_SceneManager.isLoading)
			{
				if (onDisable != null)
				{
					onDisable ();
					onDisable = null;
				}
				if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
					ObjectPool.instance.CancelRangedDespawn (rangedDespawn);
				else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
					ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			}
			instances.Remove(this);
		}

		public override void OnCollisionEnter2D (Collision2D coll)
		{
			Entity entity = coll.gameObject.GetComponentInParent<Entity>();
			if (entity != null && entity.enabled)
			{
				ContactPoint2D contactPoint = coll.GetContact(0);
				if (Player.instance != entity || !Player.instance.invulnerable)
					Instantiate(entity.bloodTrsPrefab, contactPoint.point, Quaternion.LookRotation(Vector3.forward, -contactPoint.normal));
			}
			OnTriggerEnter2D (coll.collider);
		}

		public override void OnTriggerEnter2D (Collider2D other)
		{
			if (other.GetComponent<ShieldPet>() != null)
			{
				ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
				return;
			}
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null)
			{
				if (destructable is Player)
					destructable.TakeDamage (damage, shooter);
				else
					Player.instance.ApplyDamage (destructable, damage);
			}
			if ((!Level.instance.type.HasFlag(Level.Type.Teleport) || other.gameObject.layer != LayerMask.NameToLayer("Wall")) && whatReducesHits.ContainsLayer(other.gameObject.layer))
			{
				hitsTillDespawnRemaining --;
				if (hitsTillDespawnRemaining == 0)
					ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
			}
		}

		public EventManager.Event AddEvent (Action<object> action, float timeUntilEvent = 0)
		{
			EventManager.Event _event = EventManager.AddEvent(action, timeUntilEvent, this);
			events.Add(_event);
			return _event;
		}

		public void AddEvent (EventManager.Event _event)
		{
			AddEvent (_event.onEvent, _event.timeUntilEvent);
		}

		public enum AutoDespawnMode
		{
			DontAutoDespawn,
			RangedAutoDespawn,
			DelayedAutoDespawn
		}

		public struct Snapshot
		{
			public int prefabIndex;
			public Bullet bullet;
			public Vector2 velocity;
			public Vector2 position;
			public float rotation;
			public float rangeRemaining;
			public float durationRemaining;
			public EventManager.Event[] events;

			public Snapshot (int prefabIndex, Bullet bullet, Vector2 velocity, Vector2 position, float rotation, float rangeRemaining, float durationRemaining, EventManager.Event[] events)
			{
				this.prefabIndex = prefabIndex;
				this.bullet = bullet;
				this.velocity = velocity;
				this.position = position;
				this.rotation = rotation;
				this.rangeRemaining = rangeRemaining;
				this.durationRemaining = durationRemaining;
				this.events = events;
			}

			public Snapshot (Bullet bullet) : this (bullet.prefabIndex, bullet, bullet.velocity, bullet.trs.position, bullet.trs.eulerAngles.z, Mathf.Infinity, Mathf.Infinity, null)
			{
				if (bullet.rangedDespawn != null)
					rangeRemaining = bullet.rangedDespawn.rangeRemaining;
				else if (bullet.delayedDespawn != null)
					durationRemaining = bullet.delayedDespawn.timeRemaining;
				events = new EventManager.Event[bullet.events.Count];
				for (int i = 0; i < events.Length; i ++)
					events[i] = new EventManager.Event(bullet.events[i]);
			}

			public Bullet Apply (bool useEvents = true)
			{
				if (bullet == null)
					bullet = ObjectPool.instance.SpawnComponent<Bullet>(prefabIndex, position, Quaternion.Euler(Vector3.forward * rotation));
				else
				{
					bullet.trs.position = position;
					bullet.trs.eulerAngles = Vector3.forward * rotation;
				}
				bullet.velocity = velocity;
				bullet.rigid.linearVelocity = bullet.velocity + bullet.extraVelocity;
				if (rangeRemaining != Mathf.Infinity)
				{
					bullet.rangedDespawn.previousPosition = position;
					bullet.rangedDespawn.rangeRemaining = rangeRemaining;
				}
				else if (durationRemaining != Mathf.Infinity)
				{
					bullet.delayedDespawn.timeRemaining = durationRemaining;
					BombBullet bombBullet = bullet as BombBullet;
					if (bombBullet != null)
						bombBullet.durationRemaining = durationRemaining;
				}
				bullet.events.Clear();
				if (useEvents)
				{
					for (int i = 0; i < events.Length; i ++)
					{
						EventManager.Event _event = events[i];
						_event.arg = bullet;
						bullet.AddEvent (_event);
					}
				}
				return bullet;
			}
		}
	}
}