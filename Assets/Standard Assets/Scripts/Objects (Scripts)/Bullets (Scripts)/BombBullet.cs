using System;
using Extensions;
using UnityEngine;
using System.Collections;

namespace FightRoom
{
	public class BombBullet : Bullet, IUpdatable
	{
		public bool playExplodeDelayAnimEntryOnHit;
		public AnimationEntry explodeDelayAnimEntry;
		public Explosion explosionPrefab;
		public bool explodeOnHit;
		public bool explodeOnDisable;
		[HideInInspector]
		public float durationRemaining;
		bool exploded;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			durationRemaining = duration;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			durationRemaining -= Time.deltaTime;
			if (durationRemaining <= 0)
				ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
		}

		public override void OnTriggerEnter2D (Collider2D other)
		{
			base.OnTriggerEnter2D (other);
			if (!explodeOnHit || Level.instance.type.HasFlag(Level.Type.Teleport))
				return;
			if (playExplodeDelayAnimEntryOnHit)
				explodeDelayAnimEntry.Play ();
			else
				ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (!exploded)
				OnDisable ();
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			exploded = true;
			GameManager.updatables = GameManager.updatables.Remove(this);
			base.OnDisable ();
			if (!explodeOnDisable || _SceneManager.isLoading || GameManager.isQuitting)
				return;
			Explosion explosion = ObjectPool.instance.SpawnComponent<Explosion>(explosionPrefab.prefabIndex, trs.position, trs.rotation);
			explosion.maxHits = hitsTillDespawn;
		}
	}
}