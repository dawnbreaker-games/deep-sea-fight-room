using UnityEngine;
using System.Collections.Generic;

public class PortalWall : _SpriteShapeController
{
	public Collider2D collider;
	const float CHECK_INTERVAL = .01f;

	void OnCollisionEnter2D (Collision2D coll)
	{
		Collider2D otherCollider = coll.collider;
		Rigidbody2D rigid = coll.rigidbody;
		Vector2 moveDirection = coll.relativeVelocity;
		Transform otherTrs = rigid.GetComponent<Transform>();
		ContactFilter2D contactFilter = new ContactFilter2D();
		contactFilter.SetLayerMask(LayerMask.GetMask(LayerMask.LayerToName(gameObject.layer)));
		rigid.simulated = false;
		otherTrs.position += (Vector3) moveDirection * Physics2D.defaultContactOffset * 2;
		while (Physics2D.OverlapCollider(otherCollider, contactFilter, new Collider2D[1]) > 0)
		{
			otherTrs.position += (Vector3) moveDirection * CHECK_INTERVAL;
			Physics2D.SyncTransforms();
		}
		rigid.simulated = true;
	}
}