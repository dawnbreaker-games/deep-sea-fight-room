using UnityEngine;

namespace FightRoom
{
	public class SmartTurret : Turret
	{
		public float searchRange;
		public LayerMask whatIsEnemy;

		public override void Trigger ()
		{
			base.Trigger ();
			Collider2D[] hits = Physics2D.OverlapCircleAll(trs.position, searchRange, whatIsEnemy);
			Vector3 closestHitPosition = new Vector3();
			float closestHitDistanceSqr = Mathf.Infinity;
			for (int i = 0; i < hits.Length; i ++)
			{
				Vector3 hitPosition = hits[i].bounds.center;
				float hitDistanceSqr = (hitPosition - trs.position).sqrMagnitude;
				if (hitDistanceSqr < closestHitDistanceSqr)
				{
					closestHitDistanceSqr = hitDistanceSqr;
					closestHitPosition = hitPosition;
				}
			}
			trs.up = closestHitPosition - trs.position;
		}
	}
}