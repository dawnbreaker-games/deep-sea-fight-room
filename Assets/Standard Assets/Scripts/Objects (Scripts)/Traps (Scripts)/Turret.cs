using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class Turret : Trap
	{
		public BulletPatternEntry bulletPaternEntry;
		public Level level;
#if UNITY_EDITOR
		public LineRenderer lineRenderer;
		public Transform colliderTrs;
		public bool updateLineRenderersAndColliderTransforms;
		public LayerMask whatBlocksBullets;
		public int maxBulletTeleports;
		[HideInInspector]
		public GameObject[] extraColliderGos = new GameObject[0];

		void OnValidate ()
		{
			if (level == null)
				level = GetComponentInParent<Level>();
			float bulletRadius = bulletPaternEntry.bulletPrefab.radius;
			if (updateLineRenderersAndColliderTransforms)
			{
				updateLineRenderersAndColliderTransforms = false;
				if (!colliderTrs.gameObject.activeSelf)
					return;
				RaycastHit2D hit = Physics2D.CircleCast(colliderTrs.position, bulletPaternEntry.bulletPrefab.radius, colliderTrs.up, Mathf.Infinity, whatBlocksBullets);
				lineRenderer.widthMultiplier = bulletRadius * 2;
				colliderTrs.localScale = new Vector2(bulletRadius * 2, ((Vector2) colliderTrs.position - hit.point).magnitude) / trs.lossyScale.x;
				if (level.type.HasFlag(Level.Type.Teleport))
				{
					List<GameObject> extraColliderGos = new List<GameObject>();
					for (int i = 0; i < this.extraColliderGos.Length; i ++)
					{
						GameObject oldColliderGo = this.extraColliderGos[i];
						if (oldColliderGo != null)
							GameManager.DestroyOnNextEditorUpdate (oldColliderGo);
					}
					int bulletTeleportsRemaining = maxBulletTeleports;
					while (true)
					{
						Vector2 point = level.GetPositionToTeleportTo(hit.point, bulletRadius);
						hit = Physics2D.CircleCast(point, bulletRadius, colliderTrs.up, Mathf.Infinity, whatBlocksBullets);
						Quaternion rotation = Quaternion.LookRotation(Vector3.forward, hit.point - point);
						Transform newColliderTrs = Instantiate(colliderTrs, point, rotation, trs);
						newColliderTrs.localScale = new Vector2(bulletRadius * 2, ((Vector2) point - hit.point).magnitude) / trs.lossyScale.x;
						extraColliderGos.Add(newColliderTrs.gameObject);
						bulletTeleportsRemaining --;
						if (bulletTeleportsRemaining <= 0 || hit.collider.gameObject.layer == LayerMask.NameToLayer("Wall"))
							break;
					}
					this.extraColliderGos = extraColliderGos.ToArray();
				}
			}
		}
#endif

		public void Shoot ()
		{
			if (level.enabled)
				bulletPaternEntry.Shoot ();
		}
	}
}