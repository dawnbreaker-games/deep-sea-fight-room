using UnityEngine;

namespace FightRoom
{
	public class VentTrap : SpikeTrap
	{
		public float intervalBetweenGrowAndShrink;
		public bool useSeperateDurationsForGrowAndShrink;
		public float timeUntilGrow;
		public float timeUntilShrink;
		public bool isGrowing;
		public float timeOffset;

		void OnEnable ()
		{
			isGrowing = !isGrowing;
			ChangeBetweenGrowAndShrink (timeOffset);
		}

		void ChangeBetweenGrowAndShrink (float timeOffset = 0)
		{
			isGrowing = !isGrowing;
			if (isGrowing)
			{
				StartGrowingMask ();
				if (useSeperateDurationsForGrowAndShrink)
				{
					EventManager.AddEvent ((object obj) => { ChangeBetweenGrowAndShrink (); }, timeUntilShrink - timeOffset);
					return;
				}
			}
			else
			{
				StartShrinkingMask ();
				if (useSeperateDurationsForGrowAndShrink)
				{
					EventManager.AddEvent ((object obj) => { ChangeBetweenGrowAndShrink (); }, timeUntilGrow - timeOffset);
					return;
				}
			}
			EventManager.AddEvent ((object obj) => { ChangeBetweenGrowAndShrink (); }, intervalBetweenGrowAndShrink - timeOffset);
		}
		
		public override void OnTriggerEnter2D (Collider2D other)
		{
			OnTriggerStay2D (other);
		}

		public override void OnTriggerExit2D (Collider2D other)
		{
		}
	}
}