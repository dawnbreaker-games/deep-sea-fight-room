using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class SpikeTrap : Trap
	{
		public Transform maskTrs;
		public float maskGrowRate;
		public float maskShrinkRate;
		public MaskUpdater maskUpdater;
		public float damageRate;
		public LayerMask whatIDamage;
		[HideInInspector]
		public float maxMaskSize;
#if UNITY_EDITOR
		public _SpriteShapeController spriteShapeController;
#endif
		float changeBetweenGrowAndShrinkTimer;
		static uint entitiesInsideCount;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (spriteShapeController == null)
				spriteShapeController = GetComponent<_SpriteShapeController>();
			Shape2D shape = Shape2D.FromSpline(spriteShapeController.spline).Transform(GetComponent<Transform>());
			maxMaskSize = ((Vector2) maskTrs.position - VectorExtensions.GetFurthestPoint(maskTrs.position, shape.corners)).magnitude * 2;
		}
#endif

		void OnDestroy ()
		{
			entitiesInsideCount = 0;
			GameManager.updatables = GameManager.updatables.Remove(maskUpdater);
		}

		public override void OnTriggerEnter2D (Collider2D other)
		{
			if (other.GetComponentInParent<Entity>() == null)
				return;
			entitiesInsideCount ++;
			if (entitiesInsideCount == 1)
			{
				for (int i = 0; i < triggerTraps.Length; i ++)
				{
					Trap trap = triggerTraps[i];
					SpikeTrap spikeTrap = trap as SpikeTrap;
					if (spikeTrap != null)
						spikeTrap.StartGrowingMask ();
				}
			}
		}

		public override void OnTriggerStay2D (Collider2D other)
		{
			Collider2D[] hitColliders = Physics2D.OverlapCircleAll(maskTrs.position, maskTrs.localScale.x / 2, whatIDamage);
			for (int i = 0; i < hitColliders.Length; i ++)
			{
				Collider2D hitCollider = hitColliders[i];
				if (hitCollider == other)
				{
					IDestructable destructable = other.GetComponentInParent<IDestructable>();
					destructable.TakeDamage (damageRate * Time.deltaTime);
					return;
				}
			}
		}

		public virtual void OnTriggerExit2D (Collider2D other)
		{
			if (other.GetComponentInParent<Entity>() == null)
				return;
			entitiesInsideCount --;
			if (entitiesInsideCount == 0)
			{
				for (int i = 0; i < triggerTraps.Length; i ++)
				{
					Trap trap = triggerTraps[i];
					SpikeTrap spikeTrap = trap as SpikeTrap;
					if (spikeTrap != null)
						spikeTrap.StartShrinkingMask ();
				}
			}
		}

		public void StartGrowingMask ()
		{
			GameManager.updatables = GameManager.updatables.Remove(maskUpdater);
			if (maskGrowRate == Mathf.Infinity)
			{
				maskTrs.localScale = Vector3.one * maxMaskSize;
				return;
			}
			maskUpdater = new MaskUpdater(this, maskGrowRate);
			GameManager.updatables = GameManager.updatables.Add(maskUpdater);
		}

		public void StartShrinkingMask ()
		{
			GameManager.updatables = GameManager.updatables.Remove(maskUpdater);
			if (maskShrinkRate == Mathf.Infinity)
			{
				maskTrs.localScale = Vector3.zero;
				return;
			}
			maskUpdater = new MaskUpdater(this, -maskShrinkRate);
			GameManager.updatables = GameManager.updatables.Add(maskUpdater);
		}

		public class MaskUpdater : IUpdatable
		{
			SpikeTrap spikeTrap;
			float growRate;

			public MaskUpdater (SpikeTrap spikeTrap, float growRate)
			{
				this.spikeTrap = spikeTrap;
				this.growRate = growRate;
			}

			public void DoUpdate ()
			{
				spikeTrap.maskTrs.localScale += Vector3.one * growRate * Time.deltaTime;
				if (growRate > 0)
				{
					if (spikeTrap.maskTrs.localScale.x >= spikeTrap.maxMaskSize)
					{
						spikeTrap.maskTrs.localScale = Vector3.one * spikeTrap.maxMaskSize;
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
				else if (growRate < 0)
				{
					if (spikeTrap.maskTrs.localScale.x <= 0)
					{
						spikeTrap.maskTrs.localScale = Vector3.zero;
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
			}
		}
	}
}