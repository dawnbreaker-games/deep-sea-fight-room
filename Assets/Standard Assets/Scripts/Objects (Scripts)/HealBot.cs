using UnityEngine;

namespace FightRoom
{
	public class HealBot : UpdateWhileEnabled
	{
		public Transform trs;
		public float makeHealAreaInterval;
		public HealArea healAreaPrefab;
		public float followDistance;
		float makeHealAreaTimer;

		public override void DoUpdate ()
		{
			Vector2 fromPlayer = trs.position - Player.instance.trs.position;
			if (fromPlayer.sqrMagnitude > followDistance * followDistance)
				trs.position = Player.instance.trs.position + ((Vector3) fromPlayer).normalized * followDistance;
			makeHealAreaTimer -= Time.deltaTime;
			if (makeHealAreaTimer <= 0)
			{
				makeHealAreaTimer += makeHealAreaInterval;
				Instantiate(healAreaPrefab, trs.position, Quaternion.identity);
			}
		}
	}
}