using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class TurretPet : SmartTurret
	{
		public FollowWaypoints followWaypoints;
		static TurretPet[] instances = new TurretPet[0];

		void Start ()
		{
			if (instances.Length == 0)
			{
				instances = FindObjectsByType<TurretPet>(FindObjectsSortMode.None);
				Rect2D pathRect = new Rect2D(Level.instance.trs.position, Vector2.one * Level.instance.innerSize);
				float perimeter = pathRect.GetPerimeter();
				for (int i = 0; i < instances.Length; i ++)
				{
					TurretPet instance = instances[i];
					float distanceAlongPerimeter = perimeter / instances.Length * i;
					Vector2 initPoint = pathRect.GetPointOnPerimeter(distanceAlongPerimeter);
					instance.trs.position = initPoint;
					for (int i2 = 0; i2 < 4; i2 ++)
					{
						if (distanceAlongPerimeter >= perimeter / 4 * i2)
							instance.followWaypoints.currentWaypointIndex = i2;
						else
							break;
                    }
                    instance.followWaypoints.waypoints[0].trs.position = Level.instance.trs.position + new Vector3(-1, -1) * Level.instance.innerSize / 2;
                    instance.followWaypoints.waypoints[1].trs.position = Level.instance.trs.position + new Vector3(-1, 1) * Level.instance.innerSize / 2;
                    instance.followWaypoints.waypoints[2].trs.position = Level.instance.trs.position + new Vector3(1, 1) * Level.instance.innerSize / 2;
                    instance.followWaypoints.waypoints[3].trs.position = Level.instance.trs.position + new Vector3(1, -1) * Level.instance.innerSize / 2;
                    instance.followWaypoints.OnEnable();
                }
            }
        }

		void OnDisable ()
		{
			instances = new TurretPet[0];
		}
	}
}