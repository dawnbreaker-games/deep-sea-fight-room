using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace FightRoom
{
	public class Player : Entity
	{
		public string displayName;
		public SortedList<string, BulletPatternEntry> bulletPatternEntriesSortedList = new SortedList<string, BulletPatternEntry>();
		public Transform itemsParent;
		public Item[] items = new Item[0];
		public Weapon[] weapons = new Weapon[0];
		public UseableItem[] useableItems = new UseableItem[0];
		public bool unlocked;
		public Achievement unlockOnCompleteAchievement;
		public GameObject lockedIndicatorGo;
		public GameObject untriedIndicatorGo;
		public float money;
		public Text moneyText;
		public LifeSteal lifeSteal;
		public Dodge dodge;
		public Armor armor;
		public Harvesting harvesting;
		public HpRegen hpRegen;
		public Materialistic materialistic;
		public FreeRerolls freeRerolls;
		public Haggling haggling;
		public Damage damage;
		public AttackSpeed attackSpeed;
		public Piercing piercing;
		public MoneyDamage moneyDamage;
		public PickupRange pickupRange;
		public MoveSpeed _moveSpeed;
		public MaxHp _maxHp;
		public Rage rage;
		public Mastery mastery;
		public EnemyMoney enemyMoney;
		public Polarized polarized;
		public Unbalanced unbalanced;
		public Stealth stealth;
		public CriticalChance criticalChance;
		public Size size;
		public Text hpText;
        //public Transform hpBarTrs;
        public Image hpBarImage;
		public Transform pickupRangeTrs;
		public float negativePolarityDuration;
		public float rageDuration;
		public MasteryTier[] masteryTiers = new MasteryTier[0];
		public RageUpdater rageUpdater;
		public Transform shieldPetsParent;
		public bool Enabled
		{
			set
			{
				enabled = value;
				if (!enabled)
				{
					rigid.linearVelocity = Vector2.zero;
					GameManager.Instance.defaultMenusCursorEntry.Apply ();
				}
			}
		}
		[Header("Aim assistance")]
		public Transform aimIndicatorTrs;
		public float aimAssistDistanceMultiplier;
		public float aimAssistAngleMultiplier;
		[HideInInspector]
		public int nextMasteryTierIndex;
		[HideInInspector]
		public MasteryTier nextMasteryTier;
		[HideInInspector]
		public float polarityTimer;
		[HideInInspector]
		public Stat[] stats;
		[HideInInspector]
		public bool invulnerable;
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>(true);
				return instance;
			}
		}
		public static float damageHealed;
		bool initialized;

//#if UNITY_EDITOR
//		void OnValidate ()
//		{
//			if (Application.isPlaying)
//				return;
//            stats = new Stat[] { _maxHp, mastery, rage, freeRerolls, piercing, hpRegen, damage, attackSpeed, lifeSteal, armor, dodge, harvesting, haggling, materialistic, moneyDamage, enemyMoney, pickupRange, _moveSpeed, stealth, criticalChance, size, polarized, unbalanced };
//            for (int i = 0; i < stats.Length; i++)
//            {
//                Stat stat = stats[i];
//                stat.indicator = StatsMenu.Instance.statIndicators[i];
//            }
//        }
//#endif

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (StatsMenu.Instance != null)
			{
				stats = new Stat[] { _maxHp, mastery, rage, freeRerolls, piercing, hpRegen, damage, attackSpeed, lifeSteal, armor, dodge, harvesting, haggling, materialistic, moneyDamage, enemyMoney, pickupRange, _moveSpeed, stealth, criticalChance, size, polarized, unbalanced };
				for (int i = 0; i < stats.Length; i++)
				{
					Stat stat = stats[i];
					stat.indicator = StatsMenu.instance.statIndicators[i];
				}
			}
            base.Awake ();
			items = itemsParent.GetComponentsInChildren<Item>();
			useableItems = itemsParent.GetComponentsInChildren<UseableItem>();
			SetWeapons ();
			SaveAndLoadManager.Init ();
			if (unlockOnCompleteAchievement == null || unlockOnCompleteAchievement.complete || unlockOnCompleteAchievement.ShouldBeComplete())
			{
				unlocked = true;
				lockedIndicatorGo.SetActive(false);
				untriedIndicatorGo.SetActive(!SaveAndLoadManager.saveData.triedPlayers.Contains(name.Replace("(Clone)", "")));
			}
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (initialized)
			{
				base.OnEnable ();
				return;
			}
			initialized = true;
			instance = this;
			untriedIndicatorGo.SetActive(false);
			for (int i = 0; i < items.Length; i ++)
			{
				Item item = items[i];
				if (item.gameObject.activeSelf)
					item.OnGain (this);
			}
			aimIndicatorTrs.gameObject.SetActive(SettingsMenu.ShowAimDirection);
			nextMasteryTier = masteryTiers[0];
			if (hpText != null)
			{
				stats = new Stat[] { _maxHp, mastery, rage, freeRerolls, piercing, hpRegen, damage, attackSpeed, lifeSteal, armor, dodge, harvesting, haggling, materialistic, moneyDamage, enemyMoney, pickupRange, _moveSpeed, stealth, criticalChance, size, polarized, unbalanced };
				for (int i = 0; i < stats.Length; i ++)
				{
					Stat stat = stats[i];
					stat.Init ();
					stat.AddToValue (0);
				}
			}
			damageHealed = 0;
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			base.DoUpdate ();
			if (polarized.value != 0)
				HandlePolarity (Time.unscaledDeltaTime);
			if (weapons.Length > 0)
				HandleShooting ();
			HandleAbilities ();
			TakeDamage (hpRegen.GetValue());
		}

		public override void HandleRotating ()
		{
			Vector2 aimInput = InputManager.AimInput;
			if (aimInput != Vector2.zero)
				trs.rotation = Quaternion.LookRotation(Vector3.forward, aimInput);
			else if (InputManager.instance.inputDevice == InputManager.InputDevice.KeyboardAndMouse)
				trs.rotation = Quaternion.LookRotation(Vector3.forward, (Vector2) GameCamera.Instance.camera.ScreenToWorldPoint((Vector2) InputManager.MousePosition) - (Vector2) trs.position);
			if (SettingsMenu.AimAssist && Enemy.instances.Count > 0)
			{
				Enemy enemyToShootAt = null;
				float highestEnemyAttractiveness = 0;
				for (int i = 0; i < Enemy.instances.Count; i ++)
				{
					Enemy enemy = Enemy.instances[i];
					float enemyAttractiveness = 180f - Vector2.Angle(enemy.trs.position - aimIndicatorTrs.position, trs.up) / 180 * aimAssistAngleMultiplier + 1f / ((enemy.trs.position - aimIndicatorTrs.position).magnitude * aimAssistDistanceMultiplier);
					if (enemyAttractiveness > highestEnemyAttractiveness)
					{
						enemyToShootAt = enemy;
						highestEnemyAttractiveness = enemyAttractiveness;
					}
				}
				aimIndicatorTrs.up = enemyToShootAt.trs.position - aimIndicatorTrs.position;
			}
			else
				aimIndicatorTrs.up = trs.up;
			shieldPetsParent.eulerAngles = Vector3.zero;
		}

		public override void HandleMoving ()
		{
			Move (InputManager.MoveInput * moveSpeed);
		}

		public void HandlePolarity (float deltaTime)
		{
			polarityTimer -= deltaTime;
			if (polarityTimer <= -negativePolarityDuration)
			{
				polarized.AddToValue (-polarized.value * 2);
				polarityTimer += negativePolarityDuration + unbalanced.Apply(negativePolarityDuration);
			}
			else if (polarityTimer <= 0 && polarized.value > 0)
				polarized.AddToValue (-polarized.value * 2);
		}

		void HandleAbilities ()
		{
			for (int i = 0; i < useableItems.Length; i ++)
			{
				UseableItem useableItem = useableItems[i];
				if (useableItem.useAction.ReadValue<float>() == 1)
					useableItem.TryToUse ();
			}
		}
		
		public virtual void ShootBulletPatternEntry (string name)
		{
			if (GameManager.paused)
				return;
			bulletPatternEntriesSortedList[name].Shoot ();
		}

		public void AddMoney (float amount)
		{
			money += amount;
			moneyText.text = "Money: " + (uint) money;
			if (amount > 0)
				TakeDamage (materialistic.Apply(amount));
			if (moneyDamage.value > 0 && Enemy.instances.Count > 0)
			{
				Enemy enemy = Enemy.instances[Random.Range(0, Enemy.instances.Count)];
				ApplyDamage (enemy, moneyDamage.Apply(amount));
			}
		}
		
		public override void TakeDamage (float amount, Entity attacker = null)
		{
			if (dodge.value != 0 && Random.value < dodge.GetValue())
			{
				if (attacker != null)
				{
					float damage = this.damage.Apply(Riposte.DAMAGE * Riposte.ownedCount);
					ApplyDamage (attacker, damage);
				}
				return;
			}
			if (amount > 0)
			{
				if (invulnerable)
					return;
				if (Tartigrade.hitsBlocked < Tartigrade.ownedCount)
				{
					Tartigrade.hitsBlocked ++;
					return;
				}
				if (attacker != null && rage.GetValue() != 0)
				{
					if (rageUpdater != null)
						rageUpdater.End ();
					rageUpdater = new RageUpdater();
					GameManager.updatables = GameManager.updatables.Add(rageUpdater);
				}
				base.TakeDamage (armor.Apply(amount));
			}
			else
			{
				float previousHp = hp;
				base.TakeDamage (amount);
				damageHealed += hp - previousHp;
			}
			if (hpText != null)
			{
				hpText.text = "" + hp.ToString("F0") + " / " + maxHp;
				//hpBarTrs.localScale = hpBarTrs.localScale.SetX(hp / maxHp);
				hpBarImage.fillAmount = hp / maxHp;
			}
		}

		public override void Death ()
		{
			Level.Instance.End ();
			dead = false;
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			if (rageUpdater != null)
				rageUpdater.End ();
		}

		public void SetWeapons ()
		{
			weapons = itemsParent.GetComponentsInChildren<Weapon>();
		}

		public void ApplyDamage (IDestructable destructable, float amount)
		{
			float damage = this.damage.Apply(amount);
			if (criticalChance.value != 0 && Random.value <= criticalChance.GetValue())
				damage *= 2;
			destructable.TakeDamage (damage);
			if (lifeSteal.GetValue() > 0)
				TakeDamage (lifeSteal.Apply(damage));
		}

		void HandleShooting ()
		{
			bool shootInput = InputManager.ShootInput;
			if (shootInput)
			{
				for (int i = 0; i < weapons.Length; i ++)
				{
					Weapon weapon = weapons[i];
					if (weapon.animationEntry.animator != null)
						weapon.animationEntry.Play ();
				}
				if (Physics2D.OverlapPoint(GameCamera.instance.camera.ScreenToWorldPoint((Vector2) InputManager.MousePosition), LayerMask.GetMask("Enemy")) != null)
					GameManager.instance.shootingAndMousedOverEnemyCursorEntry.Apply ();
				else
					GameManager.instance.shootingCursorEntry.Apply ();
			}
			else if (Physics2D.OverlapPoint(GameCamera.instance.camera.ScreenToWorldPoint((Vector2) InputManager.MousePosition), LayerMask.GetMask("Enemy")) != null)
				GameManager.instance.mousedOverEnemyCusrorEntry.Apply ();
			else
				GameManager.instance.defaultGameplayCursorEntry.Apply ();
		}

		public void Move (Vector2 move)
		{
			move = Vector2.ClampMagnitude(move, moveSpeed);
			if (Level.instance.type.HasFlag(Level.Type.Wind))
				move += ((Vector2) (trs.position - Level.instance.trs.position)).Rotate270().normalized * Level.instance.windSpeed;
			rigid.linearVelocity = move;
		}

		public static void SetEnabled (bool enable)
		{
			instance.Enabled = enable;
		}

		public class RageUpdater : IUpdatable
		{
			int rage;
			float timer;

			public RageUpdater ()
			{
				rage = (int) instance.rage.GetValue();
				timer = instance.rageDuration;
				instance.damage.AddToValue (rage);
				instance.attackSpeed.AddToValue (rage);
			}

			public void DoUpdate ()
			{
				if (GameManager.paused)
					return;
				timer -= Time.unscaledDeltaTime;
				if (timer <= 0)
					End ();
			}

			public void End ()
			{
				instance.damage.AddToValue (-rage);
				instance.attackSpeed.AddToValue (-rage);
				GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}

		[Serializable]
		public class MasteryTier
		{
			public uint masteryRequired;
			public float abilityCooldown;
			public BulletPatternEntry bulletPatternEntry;

			public void Apply ()
			{
				UseableItem useableItem = instance.useableItems[0];
				useableItem.cooldown = instance.nextMasteryTier.abilityCooldown;
				BulletPatternEntry bulletPatternEntry = instance.nextMasteryTier.bulletPatternEntry;
				if (bulletPatternEntry != null)
					instance.bulletPatternEntriesSortedList[bulletPatternEntry.name] = bulletPatternEntry;
			}
		}
	}
}