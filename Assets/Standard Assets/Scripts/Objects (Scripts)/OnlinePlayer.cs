using UnityEngine;

namespace FightRoom
{
	public class OnlinePlayer : Player
	{
		public bool id;
		public Duel.Team owner;
		public float minMoveDistanceToSync;
		public float minTimeToSyncPosition;
		public float minRotateAngleToSync;
		public float minTimeToSyncRotation;
		[HideInInspector]
		public Vector2 initPosition;
		Vector2 syncedPosition;
		float lastSyncPositionTime;
		float syncedRotation;
		float lastSyncRotationTime;

		public override void DoUpdate ()
		{
			if (!ShopMenu.Instance.gameObject.activeSelf)
			{
				GameManager.paused = false;
				Duel.isLocalPlayerReady = false;
				Duel.isEnemyPlayerReady = false;
			}
			base.DoUpdate ();
			if ((Time.time - lastSyncPositionTime > minTimeToSyncPosition && syncedPosition != (Vector2) trs.position) || (syncedPosition - (Vector2) trs.position).sqrMagnitude >= minMoveDistanceToSync * minMoveDistanceToSync)
				SyncPosition ();
			if ((Time.time - lastSyncRotationTime > minTimeToSyncRotation && syncedRotation != trs.eulerAngles.z) || Mathf.DeltaAngle(syncedRotation, trs.eulerAngles.z) >= minRotateAngleToSync)
				SyncRotation ();
		}
		
		public override void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesSortedList[name].Shoot ();
			SyncPosition ();
			SyncRotation ();
			NetworkManager.connection.Send("Shoot");
		}

		public override void Death ()
		{
			dead = false;
			if (enabled || Duel.instance.virtualEnemy)
			{
				OnlinePlayer enemyPlayer = Duel.playersDict[!id];
				Duel.instance.OnPlayerDied (this, enemyPlayer);
				hp = maxHp;
				hpText.text = "" + hp.ToString("F0") + " / " + maxHp;
				//hpBarTrs.localScale = Vector3.one;
				hpBarImage.fillAmount = 1;
			}
			if (enabled && !Duel.instance.virtualEnemy)
				NetworkManager.connection.Send("Kill Player");
		}

		void SyncPosition ()
		{
			NetworkManager.connection.Send("Move Player", trs.position.x, trs.position.y);
			syncedPosition = trs.position;
			lastSyncPositionTime = Time.time;
		}

		void SyncRotation ()
		{
			NetworkManager.connection.Send("Rotate Player", trs.eulerAngles.z);
			syncedRotation = trs.eulerAngles.z;
			lastSyncRotationTime = Time.time;
		}
	}
}