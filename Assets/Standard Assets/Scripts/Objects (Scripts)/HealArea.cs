﻿using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class HealArea : MonoBehaviour
	{
		public float healRate;
		public float duration;

		void OnEnable ()
		{
			ObjectPool.instance.DelayDespawn (gameObject, GetComponent<Transform>(), duration);
		}

		void OnTriggerStay2D (Collider2D other)
		{
			Player.instance.TakeDamage (-healRate * Time.deltaTime);
		}
	}
}