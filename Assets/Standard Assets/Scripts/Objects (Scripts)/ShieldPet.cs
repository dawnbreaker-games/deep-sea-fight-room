using UnityEngine;

namespace FightRoom
{
	public class ShieldPet : MonoBehaviour
	{
		public Transform trs;
		static ShieldPet[] instances = new ShieldPet[0];

		void OnEnable ()
		{
			if (instances.Length == 0)
			{
				instances = FindObjectsByType<ShieldPet>(FindObjectsSortMode.None);
				trs.SetParent(Player.instance.shieldPetsParent);
				trs.localPosition = Vector3.zero;
				for (int i = 0; i < instances.Length; i ++)
				{
					ShieldPet instance = instances[i];
					instance.trs.eulerAngles = Vector3.forward * 360f / instances.Length * i;
				}
			}
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			Bullet bullet = other.GetComponentInParent<Bullet>();
			ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
		}

		void OnDisaable ()
		{
			instances = new ShieldPet[0];
		}
	}
}