﻿using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class Money : MonoBehaviour
	{
		public uint amount;
		public static List<Money> instances = new List<Money>();

		void OnEnable ()
		{
			instances.Add(this);
		}

		void OnDisable ()
		{
			instances.Remove(this);
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			Player.instance.AddMoney (amount);
			Destroy(gameObject);
			GameManager.instance.StartTutorialIfShould (GameManager.instance.tutorialOnGotMoney);
		}
	}
}