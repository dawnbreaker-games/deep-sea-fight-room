﻿namespace FightRoom
{
	public interface IDestructable
	{
		float Hp { get; set; }
		float MaxHp { get; set; }
		
		void TakeDamage (float amount, Entity attacker = null);
		void Death ();
	}
}