using System;
using FightRoom;
using UnityEngine;
using PlayerIOClient;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class DatabaseExtensions
	{
		public static int IndexOf (this DatabaseArray dbArray, string find)
		{
			for (int i = 0; i < dbArray.Count; i ++)
			{
				object value = dbArray.GetValue(i);
				if (value.Equals(find))
					return i;
			}
			return -1; 
		}
		
		public static T GetOrMakeValue<T> (this DatabaseObject dbObj, string valueName, T defaultValue = default(T))
		{
			object value = dbObj.GetValue(valueName);
			if (value == null)
			{
				dbObj.SetValue (valueName, defaultValue);
				return defaultValue;
			}
			else
				return (T) value;
		}
		
		public static object GetOrMakeValue (this DatabaseObject dbObj, string valueName, object defaultValue)
		{
			object value = dbObj.GetValue(valueName);
			if (value == null)
			{
				dbObj.SetValue (valueName, defaultValue);
				return defaultValue;
			}
			else
				return value;
		}

		public static void SetValue (this DatabaseObject dbObj, string valueName, object value)
		{
			bool? b = value as bool?;
			if (b != null)
				dbObj.Set(valueName, (bool) b);
			else
			{
				uint? u = value as uint?;
				if (u != null)
					dbObj.Set(valueName, (uint) u);
				else
				{
					int? i = value as int?;
					if (i != null)
						dbObj.Set(valueName, (int) i);
					else
					{
						long? l = value as long?;
						if (l != null)
							dbObj.Set(valueName, (long) l);
						else
						{
							float? f = value as float?;
							if (f != null)
								dbObj.Set(valueName, (float) f);
							else
							{
								double? d = value as double?;
								if (d != null)
									dbObj.Set(valueName, (double) d);
								else
								{
									string s = value as string;
									if (s != null)
										dbObj.Set(valueName, s);
									else
									{
										DatabaseArray dA = value as DatabaseArray;
										if (dA != null)
											dbObj.Set(valueName, dA);
										else
										{
											DatabaseObject dO = value as DatabaseObject;
											if (dO != null)
												dbObj.Set(valueName, dO);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}