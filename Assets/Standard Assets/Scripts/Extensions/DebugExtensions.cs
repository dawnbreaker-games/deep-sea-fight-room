using System;
using FightRoom;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class DebugExtensions
	{
		public static void DrawPoint (Vector3 point, float radius, Color color, float duration, Quaternion rotation = default(Quaternion))
		{
			Debug.DrawLine(point + rotation * (Vector3.right * radius), point + rotation * (Vector3.left * radius), color, duration);
			Debug.DrawLine(point + rotation * (Vector3.up * radius), point + rotation * (Vector3.down * radius), color, duration);
			Debug.DrawLine(point + rotation * (Vector3.forward * radius), point + rotation * (Vector3.back * radius), color, duration);
		}

		public static void DrawRect (Rect rect, Color color, float duration, Quaternion rotation = default(Quaternion))
		{
			Debug.DrawLine(rotation * rect.min, rotation * new Vector2(rect.xMin, rect.yMax), color, duration);
			Debug.DrawLine(rotation * new Vector2(rect.xMin, rect.yMax), rotation * rect.max, color, duration);
			Debug.DrawLine(rotation * rect.max, rotation * new Vector2(rect.xMax, rect.yMin), color, duration);
			Debug.DrawLine(rotation * new Vector2(rect.xMax, rect.yMin), rotation * rect.min, color, duration);
		}

		public static void DrawBounds (Bounds bounds, Color color, float duration, Quaternion rotation = default(Quaternion))
		{
			LineSegment3D[] sides = bounds.GetSides();
			for (int i = 0; i < sides.Length; i ++)
			{
				LineSegment3D side = sides[i];
				Debug.DrawLine(side.start.Rotate(bounds.center, rotation), side.end.Rotate(bounds.center, rotation), color, duration);
			}
		}
	}
}