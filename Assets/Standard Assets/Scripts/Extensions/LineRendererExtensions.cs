using System;
using FightRoom;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Extensions
{
	public static class LineRendererExtensions
	{
		public static void SetLineRenderersToBoundsSides (Bounds bounds, LineRenderer[] lineRenderers)
		{
			LineSegment3D[] sides = bounds.GetSides();
			for (int i = 0; i < 12; i ++)
			{
				LineSegment3D side = sides[i];
				lineRenderers[i].SetPositions(new Vector3[2] { side.start, side.end });
			}
		}

		public static LineRenderer AddLineRendererToGameObjectOrMakeNew (GameObject go = null)
		{
			if (go == null)
				go = new GameObject();
			else if (go.GetComponent<LineRenderer>() != null)
			{
				Transform trs = go.GetComponent<Transform>();
				go = new GameObject();
				go.GetComponent<Transform>().SetParent(trs);
			}
			return go.AddComponent<LineRenderer>();
		}

		public static LineRenderer AddLineRendererToGameObjectOrMakeNew (GameObject go, Style style)
		{
			LineRenderer lineRenderer = AddLineRendererToGameObjectOrMakeNew(go);
			SetStyle (lineRenderer, style);
			return lineRenderer;
		}

		public static LineRenderer AddLineRendererToGameObject (GameObject go, Style style)
		{
			LineRenderer lineRenderer = go.AddComponent<LineRenderer>();
			SetStyle (lineRenderer, style);
			return lineRenderer;
		}

		public static void SetStyle (this LineRenderer lineRenderer, Style style)
		{
			lineRenderer.material = style.material;
			lineRenderer.startColor = style.color;
			lineRenderer.endColor = style.color;
			lineRenderer.startWidth = style.width;
			lineRenderer.endWidth = style.width;
			lineRenderer.sortingLayerName = style.sortingLayerName;
			lineRenderer.sortingOrder = Mathf.Clamp(style.sortingOrder, -32768, 32767);
			lineRenderer.numCapVertices = style.endCapVertices;
			lineRenderer.castShadows = style.castShadows;
			lineRenderer.motionVectorGenerationMode = style.motionVectorGenerationMode;
		}

		public static void RemoveLineRendererAndGameObjectIfEmpty (this LineRenderer lineRenderer)
		{
			Object destroyObject = lineRenderer;
			if (lineRenderer.GetComponents<Component>().Length == 2)
				destroyObject = lineRenderer.gameObject;
			GameManager.DestroyImmediate (lineRenderer);
		}

		public static void SetUseWorldSpace (this LineRenderer lineRenderer, bool useWorldSpace)
		{
			SetUseWorldSpace (lineRenderer, lineRenderer.GetComponent<Transform>(), useWorldSpace);
		}

		public static void SetUseWorldSpace (this LineRenderer lineRenderer, Transform trs, bool useWorldSpace)
		{
			lineRenderer.useWorldSpace = useWorldSpace;
			Vector3[] positions = new Vector3[lineRenderer.positionCount];
			lineRenderer.GetPositions(positions);
			for (int i = 0; i < lineRenderer.positionCount; i ++)
			{
				if (useWorldSpace)
					positions[i] = trs.TransformPoint(positions[i]);
				else
					positions[i] = trs.InverseTransformPoint(positions[i]);
			}
			lineRenderer.SetPositions(positions);
		}

#if UNITY_EDITOR
		public static void RemoveLineRendererAndGameObjectIfEmpty (this LineRenderer lineRenderer, bool destroyOnNextEditorUpdate)
		{
			if (!destroyOnNextEditorUpdate)
				RemoveLineRendererAndGameObjectIfEmpty (lineRenderer);
			else
			{
				Object destroyObject = lineRenderer;
				if (lineRenderer.GetComponents<Component>().Length == 2)
					destroyObject = lineRenderer.gameObject;
				GameManager.DestroyOnNextEditorUpdate (destroyObject);
			}
		}
#endif

		[Serializable]
		public struct Style
		{
			public float width;
			public Color color;
			public Material material;
			public string sortingLayerName;
			[Range(-32768, 32767)]
			public int sortingOrder;
			public byte endCapVertices;
			public bool castShadows;
			public MotionVectorGenerationMode motionVectorGenerationMode;
		}
	}
}