using System;
using System.Text.RegularExpressions;

namespace FightRoom
{ 
	[Serializable]
	public struct _Regex
	{
		public string pattern;
		public RegexOptions options;
		public ByteRange characterRange;
	}
}