﻿using UnityEngine;

namespace FightRoom
{
	[CreateAssetMenu]
	public class AimInLocalDirection : BulletPattern
	{
		public Vector3 shootDirection;

		public override Vector3 GetShootDirection (Transform spawner)
		{
			return spawner.rotation * shootDirection;
		}
	}
}