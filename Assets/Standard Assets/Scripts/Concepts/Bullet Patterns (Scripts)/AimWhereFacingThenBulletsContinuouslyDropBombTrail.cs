using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class AimWhereFacingThenBulletsContinuouslyDropBombTrail : AimWhereFacing
	{
		public float dropInterval;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			EventManager.AddEvent ((object obj) => { DropBomb (output[0]); }, dropInterval);
			return output;
		}

		void DropBomb (object obj)
		{
			
		}
	}
}