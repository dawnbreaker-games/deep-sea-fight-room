﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class TargetNearPlayerThenMoveToTargetThenStopAndDespawnWithDelay : BulletPattern
	{
		public FloatRange offsetRange;
		public LayerMask whatICantTarget;
		public float destroyDelay;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Vector2 targetPosition;
			do
			{
				targetPosition = (Vector2) Player.instance.trs.position + Random.insideUnitCircle.normalized * offsetRange.Get(Random.value);
			} while (Physics2D.OverlapPoint(targetPosition, whatICantTarget) != null);
			targetPosition = (Vector2) spawner.position + Level.instance.GetSmallestVectorToPoint(spawner.position, targetPosition, bulletPrefab.radius);
			Vector2 toTargetPosition = targetPosition - (Vector2) spawner.position;
			Bullet bullet = ObjectPool.instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawner.position, Quaternion.LookRotation(Vector3.forward, toTargetPosition));
			float timeUntilReachTarget = toTargetPosition.magnitude / bullet.moveSpeed;
			bullet.AddEvent ((object obj) => { StopMovingAndDespawnWithDelay ((Bullet) obj); }, timeUntilReachTarget);
			return new Bullet[] { bullet };
		}

		void StopMovingAndDespawnWithDelay (Bullet bullet)
		{
			bullet.velocity = Vector2.zero;
			bullet.rigid.linearVelocity = Vector2.zero;
			ObjectPool.instance.DelayDespawn (bullet.prefabIndex, bullet.gameObject, bullet.trs, destroyDelay);
		}
	}
}