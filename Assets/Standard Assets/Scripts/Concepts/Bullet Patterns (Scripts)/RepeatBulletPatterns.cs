﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class RepeatBulletPatterns : BulletPattern
	{
		// [MakeConfigurable]
		public uint repeatCount;
		public BulletPattern[] bulletPatterns;

		public override void Init (Transform spawner)
		{
			base.Init (spawner);
			for (int i = 0; i < bulletPatterns.Length; i ++)
			{
				BulletPattern bulletPattern = bulletPatterns[i];
				bulletPattern.Init (spawner);
			}
		}

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			List<Bullet> output = new List<Bullet>();
			for (int i = 0; i < repeatCount; i ++)
			{
				for (int i2 = 0; i2 < bulletPatterns.Length; i2 ++)
				{
					BulletPattern bulletPattern = bulletPatterns[i2];
					output.AddRange(bulletPattern.Shoot(spawner, bulletPrefab));
				}
			}
			return output.ToArray();
		}
		
		public override Bullet[] Shoot (Vector3 spawnPos, Vector3 direction, Bullet bulletPrefab)
		{
			List<Bullet> output = new List<Bullet>();
			for (int i = 0; i < repeatCount; i ++)
			{
				for (int i2 = 0; i2 < bulletPatterns.Length; i2 ++)
				{
					BulletPattern bulletPattern = bulletPatterns[i2];
					output.AddRange(bulletPattern.Shoot(spawnPos, direction, bulletPrefab));
				}
			}
			return output.ToArray();
		}
	}
}