using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class ShootBulletPatternAtOtherBulletPatternAiming : BulletPattern
	{
		public BulletPattern bulletPatternToGetAim;
        public BulletPattern shootBulletPattern;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			return shootBulletPattern.Shoot(spawner.position, bulletPatternToGetAim.GetShootDirection(spawner), bulletPrefab);
		}
	}
}