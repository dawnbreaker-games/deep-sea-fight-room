﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class ShootAtPlayerThenBurstAfterDelay : AimAtPlayer
	{
		public float burstDelay;
		public Bullet burstBulletPrefab;
		public BulletPattern bulletPattern;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.AddEvent ((object obj) => { BurstRoutine ((Bullet) obj); }, burstDelay);
			}
			return output;
		}

		void BurstRoutine (Bullet bullet)
		{
			bulletPattern.Shoot (bullet.trs, burstBulletPrefab);
			if (bullet != null)
				ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
		}
	}
}