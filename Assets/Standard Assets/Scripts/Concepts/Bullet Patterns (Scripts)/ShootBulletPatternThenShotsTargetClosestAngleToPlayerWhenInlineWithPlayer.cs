using Extensions;
using UnityEngine;
using System.Collections;

namespace FightRoom
{
	[CreateAssetMenu]
	public class ShootBulletPatternThenShotsTargetClosestAngleToPlayerWhenInlineWithPlayer : BulletPattern
	{
		public BulletPattern bulletPattern;
		public float[] redirectAngles;
		[HideInInspector]
		[SerializeField]
		Vector3[] redirectDirections = new Vector3[0];
		public float lineWidth;

		public override void Init (Transform spawner)
		{
			redirectDirections = new Vector3[redirectAngles.Length];
			for (int i = 0; i < redirectAngles.Length; i ++)
				redirectDirections[i] = VectorExtensions.FromFacingAngle(redirectAngles[i]);
		}

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = bulletPattern.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.AddEvent ((object obj) => { RedirectShotsWhenInlineWithPlayerRoutine ((Bullet) obj); });
			}
			return output;
		}

		public override Bullet[] Shoot (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab)
		{
			Bullet[] output = bulletPattern.Shoot(spawnPosition, direction, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.AddEvent ((object obj) => { RedirectShotsWhenInlineWithPlayerRoutine ((Bullet) obj); });
			}
			return output;
		}

        public virtual void RedirectShotsWhenInlineWithPlayerRoutine (Bullet bullet)
		{
			for (int i = 0; i < redirectDirections.Length; i ++)
			{
				Vector2 redirectDirection = redirectDirections[i];
				if (Physics2DExtensions.LinecastWithWidth(bullet.trs.position, (Vector2) bullet.trs.position + redirectDirection * Vector2.Distance(Player.instance.trs.position, bullet.trs.position), lineWidth, LayerMask.GetMask("Player")))
				{
					Redirect (bullet, redirectDirection);
					return;
				}
			}
			bullet.AddEvent ((object obj) => { RedirectShotsWhenInlineWithPlayerRoutine ((Bullet) obj); });
		}
	}
}