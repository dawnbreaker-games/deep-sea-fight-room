﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class AimAtPlayerWithOffset : AimAtPlayer
	{
		// [MakeConfigurable]
		public float offset;
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(offset);
		}
	}
}