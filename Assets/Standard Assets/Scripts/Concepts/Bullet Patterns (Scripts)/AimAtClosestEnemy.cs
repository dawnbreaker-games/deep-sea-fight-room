using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class AimAtClosestEnemy : AimWhereFacing
	{
		public LayerMask whatIsEnemy;
		public float searchRange;

		public override Vector3 GetShootDirection (Transform spawner)
		{
			Collider2D[] hits = Physics2D.OverlapCircleAll(spawner.position, searchRange, whatIsEnemy);
			Vector3 closestHitPosition = new Vector3();
			float closestHitDistanceSqr = Mathf.Infinity;
			for (int i = 0; i < hits.Length; i ++)
			{
				Vector3 hitPosition = hits[i].bounds.center;
				float hitDistanceSqr = (hitPosition - spawner.position).sqrMagnitude;
				if (hitDistanceSqr < closestHitDistanceSqr)
				{
					closestHitDistanceSqr = hitDistanceSqr;
					closestHitPosition = hitPosition;
				}
			}
			return closestHitPosition - spawner.position;
		}
		
		public override Bullet[] Shoot (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab)
		{
			Collider2D[] hits = Physics2D.OverlapCircleAll(spawnPosition, searchRange, whatIsEnemy);
			Vector3 closestHitPosition = new Vector3();
			float closestHitDistanceSqr = Mathf.Infinity;
			for (int i = 0; i < hits.Length; i ++)
			{
				Vector3 hitPosition = hits[i].bounds.center;
				float hitDistanceSqr = (hitPosition - spawnPosition).sqrMagnitude;
				if (hitDistanceSqr < closestHitDistanceSqr)
				{
					closestHitDistanceSqr = hitDistanceSqr;
					closestHitPosition = hitPosition;
				}
			}
			direction = closestHitPosition - spawnPosition;
			return base.Shoot(spawnPosition, direction, bulletPrefab);
		}
	}
}