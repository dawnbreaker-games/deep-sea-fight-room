﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class AimAtPlayer : BulletPattern
	{
		public override Vector3 GetShootDirection (Transform spawner)
		{
			// return Player.instance.trs.position - spawner.position;
			return Level.instance.GetSmallestVectorToPoint(spawner.position, Player.instance.trs.position, 0);
		}
	}
}