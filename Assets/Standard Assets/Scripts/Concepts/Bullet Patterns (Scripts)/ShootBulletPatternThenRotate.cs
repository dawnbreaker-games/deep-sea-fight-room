using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FightRoom
{
	[CreateAssetMenu]
	public class ShootBulletPatternThenRotate : BulletPattern
	{
		public BulletPattern bulletPattern;
		public Vector3 rotation;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = new Bullet[0];
			if (bulletPattern != null)
				output = bulletPattern.Shoot(spawner, bulletPrefab);
			spawner.Rotate(rotation);
			return output;
		}
	}
}