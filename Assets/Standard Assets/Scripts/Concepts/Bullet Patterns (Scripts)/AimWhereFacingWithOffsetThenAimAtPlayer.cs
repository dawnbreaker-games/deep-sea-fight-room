﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace FightRoom
{
	[CreateAssetMenu]
	public class AimWhereFacingWithOffsetThenAimAtPlayer : AimWhereFacingThenAimAtPlayer
	{
		// [MakeConfigurable]
		public float shootOffset;
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return VectorExtensions.Rotate(GetShootDirection(spawner), shootOffset);
		}
	}
}