using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class AimWhereFacingWithOffset : AimWhereFacing
	{
		public Vector3 offset;

		public override Vector3 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(Quaternion.Euler(offset));
		}
	}
}