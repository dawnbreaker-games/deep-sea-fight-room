using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class ShootWhereFacingThenRedirectToLocalDirection : AimWhereFacing
	{
		public float redirectTime;
		public Vector2 redirectDirection;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				RedirectAfterDelay (bullet, bullet.trs.rotation * redirectDirection, redirectTime);
			}
			return output;
		}
	}
}