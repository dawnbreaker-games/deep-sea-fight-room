﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithOffsetThenBurstAndShootBulletPatternFromBulletWhenPastDistanceToPlayer : AimAtPlayer
	{
		public Vector3 shootOffset;
		public Bullet burstBulletPrefab;
		public BulletPattern bulletPattern;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			spawner.forward = Player.instance.trs.position - spawner.position;
			spawner.Rotate(shootOffset);
			Vector3 spawnPosition = spawner.position;
			Bullet bullet = ObjectPool.instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawnPosition, spawner.rotation);
			bullet.AddEvent ((object obj) => { BurstRoutine ((Bullet) obj, spawnPosition); });
			return new Bullet[1] { bullet };
		}

		void BurstRoutine (Bullet bullet, Vector3 initPosition)
		{
			if ((bullet.trs.position - initPosition).sqrMagnitude >= (Player.instance.trs.position - bullet.trs.position).sqrMagnitude)
			{
				ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
				bulletPattern.Shoot (bullet.trs, burstBulletPrefab);
			}
			else
				bullet.AddEvent ((object obj) => { BurstRoutine ((Bullet) obj, initPosition); });
		}
	}
}