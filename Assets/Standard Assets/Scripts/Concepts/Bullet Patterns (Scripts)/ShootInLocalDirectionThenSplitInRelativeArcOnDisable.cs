using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[CreateAssetMenu]
	public class ShootInLocalDirectionThenSplitInRelativeArcOnDisable : AimInLocalDirection
	{
		public Bullet splitBulletPrefab;
		public Vector3 rotationToSplitArc;
		public Vector3 splitArcRotaAxis;
		public float splitArcDegrees;
		public uint splitNumber;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.onDisable += () => { Split (bullet, splitBulletPrefab); };
			}
			return output;
		}
		
		public override Bullet[] Split (Bullet bullet, Bullet splitBulletPrefab)
		{
			Bullet[] output = new Bullet[splitNumber];
			Quaternion previousRotation = bullet.trs.rotation;
			float rotateAmountToSplitArcCenter = splitArcDegrees / 2 + (splitArcDegrees / splitNumber / 2);
			bullet.trs.Rotate(-splitArcRotaAxis.normalized * rotateAmountToSplitArcCenter);
			bullet.trs.Rotate(rotationToSplitArc);
			for (uint i = 0; i < splitNumber; i ++)
			{
				bullet.trs.Rotate(splitArcRotaAxis.normalized * splitArcDegrees / splitNumber);
				// output[i] = base.Split(bullet, splitDirectionTrs.up, splitBulletPrefab);
				output[i] = ObjectPool.Instance.SpawnComponent<Bullet>(splitBulletPrefab.prefabIndex, bullet.trs.position, bullet.trs.rotation);
			}
			bullet.trs.rotation = previousRotation;
			return output;
		}
	}
}