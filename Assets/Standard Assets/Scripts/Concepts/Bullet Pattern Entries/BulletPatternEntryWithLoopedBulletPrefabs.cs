using System;

namespace FightRoom
{
	[Serializable]
	public class BulletPatternEntryWithLoopedBulletPrefabs : BulletPatternEntry
	{
		public Bullet[] bulletPrefabs = new Bullet[0];
		int bulletPrefabIndex;

		public override Bullet[] Shoot ()
		{
			bulletPrefab = bulletPrefabs[bulletPrefabIndex];
			bulletPrefabIndex ++;
			if (bulletPrefabIndex >= bulletPrefabs.Length)
				bulletPrefabIndex = 0;
			return bulletPattern.Shoot(spawner, bulletPrefab);
		}
	}
}