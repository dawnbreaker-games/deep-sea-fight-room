namespace FightRoom
{
	public static class LocalUserInfo
	{
		public static string username;
		public static float totalTime;
		public static uint totalEnemiesKilled;
		public static uint tasksDone;
		public static bool isPublic;
		public static float duelSkill;
		public static float duelSkillInCurrentSeason;
		public static uint duelKills;
		public static uint duelKillsInCurrentSeason;
		public static uint duelDeaths;
		public static uint duelDeathsInCurrentSeason;
		public static uint duelWins;
		public static uint duelWinsInCurrentSeason;
		public static uint duelLosses;
		public static uint duelLossesInCurrentSeason;
	}
}