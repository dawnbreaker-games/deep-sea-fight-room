using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class Trap : MonoBehaviour
	{
		public Transform trs;
		public AnimationEntry animationEntry;
		public Trap[] triggerTraps = new Trap[0];

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			TriggerTraps ();
		}

		public virtual void OnTriggerStay2D (Collider2D other)
		{
			TriggerTraps ();
		}

		public virtual void TriggerTraps ()
		{
			for (int i = 0; i < triggerTraps.Length; i ++)
			{
				Trap trap = triggerTraps[i];
				trap.Trigger ();
			}
		}

		public virtual void Trigger ()
		{
			if (!string.IsNullOrEmpty(animationEntry.animatorStateName))
				animationEntry.Play ();
		}
	}
}