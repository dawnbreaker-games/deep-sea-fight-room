using UnityEngine;

namespace FightRoom
{
	public class ObjectInWorld : MonoBehaviour
	{
		public Transform trs;
		public ObjectInWorld[] objectsToLoadAndUnloadWithMe = new ObjectInWorld[0];
		public bool dontUnloadAfterLoad;

#if UNITY_EDITOR
		// [HideInInspector]
		public ObjectInWorld duplicate;
		// [HideInInspector]
		public WorldPiece pieceIAmIn;

		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
		}
#endif

		void OnEnable ()
		{
			if (dontUnloadAfterLoad)
				trs.SetParent(null);
		}
	}
}