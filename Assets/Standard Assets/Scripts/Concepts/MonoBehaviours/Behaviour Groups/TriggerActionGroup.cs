using UnityEngine;

namespace FightRoom
{
	public class TriggerActionGroup : BehaviourGroup
	{
		void OnValidate ()
		{
			for (int i = 0; i < members.Length; i ++)
			{
				Behaviour behaviour = members[i];
				TriggerAction triggerAction = behaviour as TriggerAction;
				if (triggerAction == null)
				{
					triggerAction = behaviour.GetComponent<TriggerAction>();
					if (triggerAction != null)
						members[i] = triggerAction;
				}
			}
		}

		public void Reset ()
		{
			for (int i = 0; i < members.Length; i ++)
			{
				TriggerAction triggerAction = (TriggerAction) members[i];
				triggerAction.Restart ();
			}
		}
	}
}