﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class DisableObjectBasedOnInputDevice : MonoBehaviour
	{
		public bool disableIfUsing;
		public InputManager.InputDevice inputDevice;
		
		void OnEnable ()
		{
			gameObject.SetActive(InputManager.usingInputDevicesDict[inputDevice]() != disableIfUsing);
		}
	}
}