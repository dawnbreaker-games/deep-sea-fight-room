using FightRoom;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

[ExecuteInEditMode]
public class InputRebinder : MonoBehaviour
{
	public string actionName;
	public int bindingIndex;
	public DeviceDisplayConfigurator deviceDisplaySettings;
	public Text actionNameText;
	public Text bindingNameText;
	public Image bindingImage;
	public Button rebindButton;
	public Button resetButton;
	public GameObject waitingForInputGo;
	static InputRebinder currentActive;
	InputAction inputAction;
	InputActionRebindingExtensions.RebindingOperation rebindOperation;

	void Awake ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		inputAction = InputManager.instance.inputActionAsset.FindAction(actionName);
		if (string.IsNullOrEmpty(actionNameText.text))
		{
			actionNameText.text = actionName;
			actionNameText.gameObject.SetActive(true);
		}
		UpdateBindingDisplay ();
	}

	void OnDisable ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		if (rebindOperation != null)
			OnDone ();
	}

#if UNITY_EDITOR
	void OnValidate ()
	{
		if (!Application.isPlaying)
		{
			if (actionNameText == null)
				actionNameText = GetComponent<Transform>().GetChild(0).GetComponentInChildren<Text>();
			if (bindingNameText == null)
				bindingNameText = GetComponent<Transform>().GetChild(0).GetComponentsInChildren<Text>()[1];
		}
	}
#endif

	public void BeginRebind ()
	{
		if (currentActive != null)
			currentActive.OnDone ();
		currentActive = this;
		rebindButton.gameObject.SetActive(false);
		resetButton.gameObject.SetActive(false);
		waitingForInputGo.SetActive(true);
		inputAction.Disable();
		rebindOperation = inputAction.PerformInteractiveRebinding(bindingIndex)
			.WithControlsExcluding("<Mouse>/position")
			.WithControlsExcluding("<Mouse>/delta")
			.OnMatchWaitForAnother(0.1f)
			.OnComplete((InputActionRebindingExtensions.RebindingOperation rebingOperation) => { OnDone (); });
		rebindOperation.Start();
	}


	public void OnDone ()
	{
		if (rebindOperation != null)
		{
			rebindOperation.Dispose();
			rebindOperation = null;
		}
		rebindButton.gameObject.SetActive(true);
		resetButton.gameObject.SetActive(true);
		waitingForInputGo.SetActive(false);
		UpdateBindingDisplay ();
		inputAction.Enable();
		SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
	}

	public void ResetBinding ()
	{
		InputActionRebindingExtensions.RemoveAllBindingOverrides(inputAction);
		UpdateBindingDisplay ();
		SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
	}

	void UpdateBindingDisplay ()
	{
		string currentBindingInput = InputControlPath.ToHumanReadableString(inputAction.bindings[bindingIndex].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice);
		Sprite sprite = deviceDisplaySettings.GetDeviceBindingIcon(currentBindingInput);
		if (sprite != null)
		{
			bindingNameText.gameObject.SetActive(false);
			bindingImage.gameObject.SetActive(true);
			bindingImage.sprite = sprite;
		}
		else
		{
			bindingImage.gameObject.SetActive(false);
			bindingNameText.text = currentBindingInput;
			bindingNameText.gameObject.SetActive(true);
		}
	}
}