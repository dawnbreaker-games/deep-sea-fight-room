using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace FightRoom
{
	public class DropPointForDraggable : MonoBehaviour
	{
		public bool allowDropIfFull;
		public uint releaseHoldingIndexIfOverFull;
		public uint maxHoldingCount;
		public RectTransform rectTrs;
		public UnityEvent<Draggable> onBeganHolding;
		public UnityEvent<Draggable> onStopHolding;
		public static DropPointForDraggable[] instances = new DropPointForDraggable[0];
		[HideInInspector]
		public Draggable[] holding = new Draggable[0];

		void OnEnable ()
		{
			instances = instances.Add(this);
		}

		void OnDisable ()
		{
			instances = instances.Remove(this);
		}
	}
}