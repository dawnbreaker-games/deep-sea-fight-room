using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class ShopItemEntry : MonoBehaviour
	{
		[HideInInspector]
		public Item item;
		public Text nameText;
		public Text descriptionText;
		public GameObject changesTrackedStatIndicator;

		public virtual void Init (Item item)
		{
			this.item = item;
			nameText.text = item.name.Replace("(Clone)", "");
			descriptionText.text = item.ToString();
		}
	}
}