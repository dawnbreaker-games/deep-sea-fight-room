using System;

namespace FightRoom
{
	[Serializable]
	public class Team<T>
	{
		public string name;
		public T representative;
		public T[] representatives = new T[0];
		public Team<T> opponent;
		public Team<T>[] opponents = new Team<T>[0];

		public Team (string name = null, params T[] representatives)
		{
			this.name = name;
			this.representatives = representatives;
		}
	}
}