using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class Draggable : _Selectable, IUpdatable
	{
		public RectTransform canvasRectTrs;
		public Vector2 defaultLocalPosition;
		public Transform defaultParent;
		public DropBehaviour dropBehaviour;
		public bool autoDropOnDropPoint;
#if UNITY_EDITOR
		public bool setDefaults;
#endif
		Vector2 startDragPoint;
		Transform parentAtStartDrag;
		bool dragging;

#if UNITY_EDITOR
		public override void OnEnable ()
		{
			base.OnEnable ();
			if (canvasRectTrs == null)
				canvasRectTrs = rectTrs.root.GetComponentInChildren<RectTransform>();
		}

		void OnValidate ()
		{
			if (setDefaults)
			{
				setDefaults = false;
				defaultLocalPosition = rectTrs.localPosition;
				defaultParent = rectTrs.parent;
			}
		}
#endif

		public void StartDrag ()
		{
			dragging = true;
			startDragPoint = rectTrs.position;
			parentAtStartDrag = rectTrs.parent;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void EndDrag ()
		{
			if (!dragging)
				return;
			dragging = false;
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (!DropIfShould())
			{
				DropPointForDraggable dropPoint = GetComponentInParent<DropPointForDraggable>();
				if (dropPoint != null)
				{
					dropPoint.holding = dropPoint.holding.Remove(this);
					if (dropPoint.onStopHolding != null)
						dropPoint.onStopHolding.Invoke(this);
				}
				if (dropBehaviour == DropBehaviour.ToDragStartIfNotOnDropPoint)
				{
					rectTrs.SetParent(parentAtStartDrag);
					rectTrs.localScale = Vector3.one;
					rectTrs.position = startDragPoint;
				}
				else if (dropBehaviour == DropBehaviour.ToDefaultsIfNotOnDropPoint)
				{
					rectTrs.SetParent(defaultParent);
					rectTrs.localScale = Vector3.one;
					rectTrs.localPosition = defaultLocalPosition;
				}
			}
		}

		bool DropIfShould ()
		{
			for (int i = 0; i < DropPointForDraggable.instances.Length; i ++)
			{
				DropPointForDraggable dropPoint = DropPointForDraggable.instances[i];
				Vector2 mousePosition = (Vector2) InputManager.MousePosition;
				if ((dropPoint.allowDropIfFull || dropPoint.holding.Length < dropPoint.maxHoldingCount) && dropPoint.rectTrs.GetWorldRect().Contains(GameCamera.instance.camera.ScreenToWorldPoint(mousePosition)))
				{
					if (dropPoint.holding.Length >= dropPoint.maxHoldingCount)
					{
						Draggable holding = dropPoint.holding[dropPoint.releaseHoldingIndexIfOverFull];
						holding.rectTrs.SetParent(holding.defaultParent);
						holding.rectTrs.localScale = Vector3.one;
						holding.rectTrs.localPosition = holding.defaultLocalPosition;
						dropPoint.holding = dropPoint.holding.Remove(holding);
						if (dropPoint.onStopHolding != null)
							dropPoint.onStopHolding.Invoke(holding);
					}
					DropPointForDraggable previousDropPoint = GetComponentInParent<DropPointForDraggable>();
					if (previousDropPoint != null)
					{
						previousDropPoint.holding = previousDropPoint.holding.Remove(this);
						if (previousDropPoint.onStopHolding != null)
							previousDropPoint.onStopHolding.Invoke(this);
					}
					rectTrs.SetParent(dropPoint.rectTrs);
					rectTrs.localScale = Vector3.one;
					rectTrs.position = dropPoint.rectTrs.position;
					dropPoint.holding = dropPoint.holding.Add(this);
					if (dropPoint.onBeganHolding != null)
						dropPoint.onBeganHolding.Invoke(this);
					dragging = false;
					GameManager.updatables = GameManager.updatables.Remove(this);
					return true;
				}
			}
			return false;
		}

		public void DoUpdate ()
		{
			Vector2 mousePosition = (Vector2) InputManager.MousePosition;
			rectTrs.position = (Vector2) canvasRectTrs.position + canvasRectTrs.sizeDelta * (GameCamera.instance.camera.ScreenToViewportPoint(mousePosition) - Vector3.one / 2) * canvasRectTrs.lossyScale;
			if (autoDropOnDropPoint)
			{
				DropPointForDraggable dropPoint = GetComponentInParent<DropPointForDraggable>();
				if (dropPoint == null || !dropPoint.rectTrs.GetWorldRect().Contains(GameCamera.instance.camera.ScreenToWorldPoint(mousePosition)))
					DropIfShould ();
			}
		}

		public enum DropBehaviour
		{
			InPlace,
			ToDragStartIfNotOnDropPoint,
			ToDefaultsIfNotOnDropPoint
		}
	}
}