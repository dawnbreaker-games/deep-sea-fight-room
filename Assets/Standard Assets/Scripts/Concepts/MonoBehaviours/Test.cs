using UnityEngine;

namespace FightRoom
{
	public class Test : MonoBehaviour
	{
		void Start ()
		{
			print("Hello World!");
			_Test ();
		}

		void _Test ()
		{
			print("Hi!");
		}

		void Update ()
		{
			transform.position = Vector2.right * Mathf.Sin(Time.time) * 100;
			if (InputManager.RestartInput)
				print("R key is pressed");
		}
	}
}