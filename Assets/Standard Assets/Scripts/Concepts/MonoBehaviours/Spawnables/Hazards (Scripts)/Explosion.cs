using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class Explosion : Hazard
	{
		public Animator animator;
		public uint maxHits;
		public LayerMask whatIDamage;
		public CircleCollider2D circleCollider;
		public new static List<Explosion> instances = new List<Explosion>();

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instances.Add(this);
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			instances.Remove(this);
		}
		
		public override void OnTriggerEnter2D (Collider2D collider)
		{
			IDestructable destructable = collider.GetComponentInParent<IDestructable>();
			if (destructable is Player)
				destructable.TakeDamage (damage);
			else
				Player.instance.ApplyDamage (destructable, damage);
		}

		public void DestroyMe ()
		{
			ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
		}

		public void DamageDestructables ()
		{
			Collider2D[] hitColliders = Physics2D.OverlapCircleAll(trs.position, circleCollider.bounds.extents.x, whatIDamage);
			List<Transform> hitTransforms = new List<Transform>();
			for (int i = 0; i < hitColliders.Length; i ++)
			{
				Collider2D hitCollider = hitColliders[i];
				hitTransforms.Add(hitCollider.GetComponent<Transform>());
			}
			for (int i = 0; i < Mathf.Min(hitColliders.Length, maxHits); i ++)
			{
				Transform closestTrs = trs.GetClosestTransform_2D(hitTransforms.ToArray());
				hitTransforms.Remove(closestTrs);
				IDestructable destructable = closestTrs.GetComponentInParent<IDestructable>();
				if (destructable is Player)
					destructable.TakeDamage (damage);
				else
					Player.instance.ApplyDamage (destructable, damage);
			}
		}

		public struct Snapshot
		{
			public int prefabIndex;
			public Explosion explosion;
			public Vector2 position;
			public float normalizedTime;

			public Snapshot (int prefabIndex, Explosion explosion, Vector2 position, float normalizedTime)
			{
				this.prefabIndex = prefabIndex;
				this.explosion = explosion;
				this.position = position;
				this.normalizedTime = normalizedTime;
			}

			public Snapshot (Explosion explosion) : this (explosion.prefabIndex, explosion, explosion.trs.position, Mathf.Infinity)
			{
				normalizedTime = explosion.animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
			}

			public Explosion Apply ()
			{
				if (explosion == null)
					explosion = ObjectPool.instance.SpawnComponent<Explosion>(prefabIndex, position);
				else
					explosion.trs.position = position;
				explosion.animator.Play("Explode", 0, normalizedTime);
				return explosion;
			}
		}
	}
}