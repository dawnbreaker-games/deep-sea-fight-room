﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class Hazard : Spawnable
	{
		public float damage;
		public SpriteRenderer spriteRenderer;
		public float radius;
		public static List<Hazard> instances = new List<Hazard>();

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances.Add(this);
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances.Remove(this);
		}
		
		public virtual void OnCollisionEnter2D (Collision2D coll)
		{
			IDestructable destructable = coll.collider.GetComponentInParent<IDestructable>();
			if (destructable != null)
			{
				if (destructable is Player)
					destructable.TakeDamage (damage);
				else
					Player.instance.ApplyDamage (destructable, damage);
			}
		}
		
		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null)
			{
				if (destructable is Player)
					destructable.TakeDamage (damage);
				else
					Player.instance.ApplyDamage (destructable, damage);
			}	
		}
	}
}