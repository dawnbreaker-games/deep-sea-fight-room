using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class UserInfoIndicator : MonoBehaviour
	{
		public Text usernameText;
		[HideInInspector]
		public string username;
		const string REPLACE_STRING = "_";

		public virtual void Init (string username)
		{
			this.username = username;
			usernameText.text = usernameText.text.Replace(REPLACE_STRING, username);
		}
	}
}