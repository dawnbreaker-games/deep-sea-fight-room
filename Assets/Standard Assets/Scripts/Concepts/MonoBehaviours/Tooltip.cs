using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class Tooltip : UpdateWhileEnabled
	{
		public RectTransform canvasRectTrs;
		public Text text;
		public float showDelay;
		ShowDelayUpdater showDelayUpdater;

#if UNITY_EDITOR
		void OnValidate ()
		{
			text = GetComponentsInChildren<Text>(true)[1];
		}
#endif

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(showDelayUpdater);
		}

		public override void DoUpdate ()
		{
			text.rectTransform.parent.position = canvasRectTrs.position + ((Vector3) (canvasRectTrs.sizeDelta.Multiply(canvasRectTrs.lossyScale)).Multiply(GameCamera.instance.camera.ScreenToViewportPoint((Vector2) InputManager.MousePosition) - Vector3.one / 2));
		}

		public void ShowWithDelay ()
		{
			showDelayUpdater = new ShowDelayUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(showDelayUpdater);
		}

		class ShowDelayUpdater : IUpdatable
		{
			public Tooltip tooltip;
			float delayRemaining;

			public ShowDelayUpdater (Tooltip tooltip)
			{
				this.tooltip = tooltip;
				delayRemaining = tooltip.showDelay;
			}
			
			public void DoUpdate ()
			{
				delayRemaining -= Time.deltaTime;
				if (delayRemaining <= 0)
				{
					tooltip.text.rectTransform.parent.gameObject.SetActive(true);
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}