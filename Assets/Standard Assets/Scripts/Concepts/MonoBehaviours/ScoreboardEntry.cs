using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class ScoreboardEntry : MonoBehaviour
	{
		public Transform trs;
		public Text scoreText;
		public Image teamColorIndicator;

		public void Init (Duel.Team team)
		{
			teamColorIndicator.color = team.color;
		}
	}
}