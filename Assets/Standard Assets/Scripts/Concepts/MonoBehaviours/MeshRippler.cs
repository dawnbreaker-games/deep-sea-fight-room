using UnityEngine;

namespace FightRoom
{
	public class MeshRippler : UpdateWhileEnabled
	{
		public Transform trs;
		public Rigidbody2D rigid;
		public RippleMesh rippleMesh;
		public bool useVelocity;
		public float radius;
		public float magnitude;

		void Awake ()
		{
			enabled = SettingsMenu.AllowWaterRipples;
			rippleMesh = RippleMesh.Instance;
		}

		public override void DoUpdate ()
		{
			Vector3 velocity = Vector3.zero;
			if (rigid != null)
				velocity = rigid.linearVelocity;
			rippleMesh.Ripple (trs.position, radius, velocity, useVelocity, magnitude);
		}
	}
}