using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	public class FollowWaypoints : UpdateWhileEnabled
	{
#if UNITY_EDITOR
		public bool autoSetWaypoints = true;
		public bool autoSetLineRenderersAndWaypointPathData = true;
		public SetLineRenderersMethod setLineRenderersMethod;
		public Transform waypointsParent;
#endif
		public Transform trs;
		public FollowType followType;
		public Waypoint[] waypoints = new Waypoint[0];
		public float moveSpeed;
		public float rotateSpeed;
		public int currentWaypointIndex;
		public bool isBacktracking;
		public LineRendererExtensions.Style lineRendererStyle;
		Waypoint currentWaypoint;
		
		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			for (int i = 0; i < waypoints.Length; i ++)
			{
				Waypoint waypoint = waypoints[i];
				waypoint.trs.SetParent(null);
			}
			currentWaypoint = waypoints[currentWaypointIndex];
			base.OnEnable ();
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (autoSetWaypoints)
			{
				autoSetWaypoints = false;
				if (waypointsParent != null)
				{
					waypoints = new Waypoint[waypointsParent.childCount];
					for (int i = 0; i < waypointsParent.childCount; i ++)
					{
						Transform child = waypointsParent.GetChild(i);
						Waypoint waypoint = waypoints[i];
						waypoints[i] = new Waypoint(child, waypoint.pathStartPoint, waypoint.pathEndPoint);
					}
				}
				else
					waypointsParent = trs;
			}
			if (autoSetLineRenderersAndWaypointPathData)
			{
				autoSetLineRenderersAndWaypointPathData = false;
				SetLineRenderersAndWaypointPathData ();
			}
		}

		public LineRenderer[] SetLineRenderersAndWaypointPathData ()
		{
			List<LineRenderer> output = new List<LineRenderer>();
			LineRenderer[] lineRenderers = GetComponentsInChildren<LineRenderer>();
			for (int i = 0; i < lineRenderers.Length; i ++)
			{
				LineRenderer lineRenderer = lineRenderers[i];
				LineRendererExtensions.RemoveLineRendererAndGameObjectIfEmpty (lineRenderer, true);
			}
			if (setLineRenderersMethod == SetLineRenderersMethod.ShapeOutline)
			{
				int previousCurrentWaypointIndex = currentWaypointIndex;
				bool previousIsBacktracking = isBacktracking;
				Waypoint nextWaypoint = waypoints[currentWaypointIndex];
				isBacktracking = !isBacktracking;
				OnReachedWaypoint ();
				int previousWaypointIndex = currentWaypointIndex;
				Waypoint previousWaypoint = waypoints[previousWaypointIndex];
				currentWaypointIndex = previousCurrentWaypointIndex;
				isBacktracking = previousIsBacktracking;
				int passedPreviousCurrentWaypointCount = 0;
				List<LineRenderer> joiningLineRenderers = new List<LineRenderer>();
				while (true)
				{
					Waypoint waypoint = nextWaypoint;
					if (currentWaypointIndex == previousCurrentWaypointIndex)
						passedPreviousCurrentWaypointCount ++;
					OnReachedWaypoint ();
					nextWaypoint = waypoints[currentWaypointIndex];
					if (followType == FollowType.Loop)
					{
						if (passedPreviousCurrentWaypointCount == 2)
							break;
					}
					else if (followType == FollowType.PingPong)
					{
						if ((passedPreviousCurrentWaypointCount == 2 && (previousCurrentWaypointIndex == 0 || previousCurrentWaypointIndex == waypoints.Length - 1)) || passedPreviousCurrentWaypointCount == 3)
							break;
					}
					else// if (followType == FollowType.Once)
					{
						if (previousWaypointIndex == currentWaypointIndex)
							break;
					}
					if (waypoint.trs.eulerAngles == previousWaypoint.trs.eulerAngles)
					{
						lineRenderers = new LineRenderer[4];
						for (int i = 0; i < 4; i ++)
							lineRenderers[i] = LineRendererExtensions.AddLineRendererToGameObjectOrMakeNew(gameObject, lineRendererStyle);
						SetLineRenderersAtWaypoint (lineRenderers, waypoint, previousWaypoint);
						if (output.Count > 3)
						{
							for (int i = 0; i < 4; i ++)
							{
								LineRenderer lineRenderer = LineRendererExtensions.AddLineRendererToGameObjectOrMakeNew(gameObject, lineRendererStyle);
								lineRenderer.SetPositions(new Vector3[] { output[output.Count - 4 + i].GetPosition(0), lineRenderers[i].GetPosition(0) });
								joiningLineRenderers.Add(lineRenderer);
							}
						}
						output.AddRange(lineRenderers);
					}
					else
					{
					}
					previousWaypoint = waypoint;
					previousWaypointIndex = currentWaypointIndex;
				}
				currentWaypointIndex = previousCurrentWaypointIndex;
				isBacktracking = previousIsBacktracking;
				output.AddRange(joiningLineRenderers);
			}
			else// if (setLineRenderersMethod == SetLineRenderersMethod.Path)
			{
				if (followType == FollowType.Loop)
				{
					Waypoint previousWaypoint = waypoints[waypoints.Length - 1];
					for (int i = 0; i < waypoints.Length; i ++)
					{
						Waypoint waypoint = waypoints[i];
						Vector2 startPoint = previousWaypoint.trs.position;
						Vector2 endPoint = waypoint.trs.position;
						LineRenderer lineRenderer = LineRendererExtensions.AddLineRendererToGameObjectOrMakeNew(gameObject, lineRendererStyle);
						Level level = GetComponentInParent<Level>();
						if (level != null && level.type.HasFlag(Level.Type.Teleport))
						{
							LineSegment2D lineSegment = new LineSegment2D(startPoint, endPoint);
							Rect levelRect = new Rect((Vector2) level.trs.position - Vector2.one * level.innerSize / 2, Vector2.one * level.innerSize);
							LineSegment2D[] edgeLineSegments = levelRect.GetSides();
							for (int i2 = 0; i2 < edgeLineSegments.Length; i2 ++)
							{
								LineSegment2D edgeLineSegment = edgeLineSegments[i2];
								Vector2 startPoint2;
								if (lineSegment.GetIntersectionWith(edgeLineSegment, out startPoint2))
								{
									endPoint = startPoint2;
									startPoint2 = level.GetPositionToTeleportTo(endPoint, lineRendererStyle.width);
									Vector2 toNextWaypoint = (Vector2) waypoints[(i + 1) % waypoints.Length].trs.position - startPoint2;
									RaycastHit2D hit = Physics2D.CircleCast(startPoint2, lineRendererStyle.width / 2, toNextWaypoint, LayerMask.GetMask("Portal Wall"));
									if (hit.collider != null)
									{
										Waypoint nextWaypoint = waypoints[i + 1];
										LineRenderer lineRenderer2 = LineRendererExtensions.AddLineRendererToGameObjectOrMakeNew(gameObject, lineRendererStyle);
										lineRenderer2.SetPositions(new Vector3[] { startPoint2, nextWaypoint.trs.position });
									}
									else
									{
										Waypoint nextWaypoint = waypoints[(i + 1) % waypoints.Length];
										LineRenderer lineRenderer2 = LineRendererExtensions.AddLineRendererToGameObjectOrMakeNew(gameObject, lineRendererStyle);
										lineRenderer2.SetPositions(new Vector3[] { startPoint2, nextWaypoint.trs.position });
									}
									break;
								}
							}
						}
						lineRenderer.SetPositions(new Vector3[] { startPoint, endPoint });
						waypoint.pathStartPoint = startPoint;
						waypoint.pathEndPoint = endPoint;
						waypoints[i] = waypoint;
						output.Add(lineRenderer);
						previousWaypoint = waypoint;
					}
				}
				else
				{
					Waypoint previousWaypoint = waypoints[0];
					Vector2 startPoint = previousWaypoint.trs.position;
					Vector2 endPoint = Vector2.zero;
					bool setNextPoints = true;
					for (int i = 1; i < waypoints.Length; i ++)
					{
						Waypoint waypoint = waypoints[i];
						if (setNextPoints)
						{
							startPoint = previousWaypoint.trs.position;
							endPoint = waypoint.trs.position;
						}
						setNextPoints = true;
						LineRenderer lineRenderer = LineRendererExtensions.AddLineRendererToGameObjectOrMakeNew(gameObject, lineRendererStyle);
						Level level = GetComponentInParent<Level>();
						if (level != null && level.type.HasFlag(Level.Type.Teleport))
						{
							LineSegment2D lineSegment = new LineSegment2D(startPoint, endPoint);
							Rect levelRect = new Rect((Vector2) level.trs.position - Vector2.one * level.innerSize / 2, Vector2.one * level.innerSize);
							LineSegment2D[] edgeLineSegments = levelRect.GetSides();
							for (int i2 = 0; i2 < edgeLineSegments.Length; i2 ++)
							{
								LineSegment2D edgeLineSegment = edgeLineSegments[i2];
								if (lineSegment.GetIntersectionWith(edgeLineSegment, out endPoint))
								{
									setNextPoints = false;
									break;
								}
							}
						}
						if (i == waypoints.Length - 1)
							endPoint = waypoint.trs.position;
						else if (!setNextPoints)
							endPoint += (startPoint - endPoint).normalized * lineRendererStyle.width / 2;
						lineRenderer.SetPositions(new Vector3[] { startPoint, endPoint });
						waypoint.pathStartPoint = startPoint;
						waypoint.pathEndPoint = endPoint;
						waypoints[i] = waypoint;
						if (!setNextPoints)
							startPoint = level.GetPositionToTeleportTo(endPoint, lineRendererStyle.width / 2);	
						output.Add(lineRenderer);
						previousWaypoint = waypoint;
					}
				}
			}
			Waypoint firstWaypoint = waypoints[0];
			firstWaypoint.pathStartPoint = firstWaypoint.trs.position;
			firstWaypoint.pathEndPoint = firstWaypoint.pathStartPoint;
			waypoints[0] = firstWaypoint;
			return output.ToArray();
		}
		
		Rect GetBoundsRect ()
		{
			Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
			if (colliders.Length == 0)
				return RectExtensions.INFINITE;
			Rect[] childBoundsRectsArray = new Rect[colliders.Length];
			for (int i = 0; i < colliders.Length; i ++)
			{
				Collider2D collider = colliders[i];
				childBoundsRectsArray[i] = collider.bounds.ToRect();
			}
			return childBoundsRectsArray.Combine();
		}

		void SetLineRenderersAtWaypoint (LineRenderer[] lineRenderers, Waypoint waypoint, Waypoint previousWaypoint)
		{
			Vector2 boundsRectSize = GetBoundsRect().size;
			Rect boundsRect = new Rect((Vector2) waypoint.trs.position - boundsRectSize / 2, boundsRectSize);
			LineSegment2D[] sides = boundsRect.GetSides();
			for (int i = 0; i < 4; i ++)
			{
				LineSegment2D side = sides[i];
				lineRenderers[i].SetPositions(new Vector3[] { side.start, side.end });
			}
		}
#endif

		public override void DoUpdate ()
		{
			if (GameManager.paused || _SceneManager.isLoading)
				return;
			if (moveSpeed != 0)
			{
				Vector2 newPosition;
				if (!isBacktracking)
					newPosition = Vector3.Lerp(trs.position, (Vector2) currentWaypoint.pathEndPoint, moveSpeed * Time.deltaTime * (1f / Vector2.Distance(trs.position, currentWaypoint.pathEndPoint)));
				else
					newPosition = Vector3.Lerp(trs.position, (Vector2) currentWaypoint.pathStartPoint, moveSpeed * Time.deltaTime * (1f / Vector2.Distance(trs.position, currentWaypoint.pathStartPoint)));
				if (!float.IsNaN(newPosition.x))
					trs.position = newPosition;
			}
			if (rotateSpeed != 0)
				trs.rotation = Quaternion.Slerp(trs.rotation, currentWaypoint.trs.rotation, rotateSpeed * Time.deltaTime * (1f / Quaternion.Angle(trs.rotation, currentWaypoint.trs.rotation)));
			bool reachedWaypoint = moveSpeed == 0 || ((!isBacktracking && (Vector2) trs.position == (Vector2) currentWaypoint.pathEndPoint) || (isBacktracking && (Vector2) trs.position == (Vector2) currentWaypoint.pathStartPoint));
			reachedWaypoint &= trs.eulerAngles == currentWaypoint.trs.eulerAngles || rotateSpeed == 0;
			if (reachedWaypoint)
				OnReachedWaypoint ();
		}
		
		void OnReachedWaypoint ()
		{
			if (isBacktracking)
				currentWaypointIndex --;
			else
				currentWaypointIndex ++;
			switch (followType)
			{
				case FollowType.Once:
					if (currentWaypointIndex == waypoints.Length)
						currentWaypointIndex = waypoints.Length - 1;
					else if (currentWaypointIndex == -1)
						currentWaypointIndex = 0;
					break;
				case FollowType.Loop:
					if (currentWaypointIndex == waypoints.Length)
						currentWaypointIndex = 0;
					else if (currentWaypointIndex == -1)
						currentWaypointIndex = waypoints.Length - 1;
					break;
				case FollowType.PingPong:
					if (currentWaypointIndex == waypoints.Length)
					{
						currentWaypointIndex -= 2;
						isBacktracking = !isBacktracking;
					}
					else if (currentWaypointIndex == -1)
					{
						currentWaypointIndex += 2;
						isBacktracking = !isBacktracking;
					}
					break;
			}
			if (isBacktracking)
			{
				currentWaypoint = waypoints[currentWaypointIndex + 1];
				trs.position = currentWaypoint.pathEndPoint;
			}
			else
			{
				currentWaypoint = waypoints[currentWaypointIndex];
				trs.position = currentWaypoint.pathStartPoint;
			}
		}

		[Serializable]
		public struct Waypoint
		{
			public Transform trs;
			// [HideInInspector]
			public Vector2 pathStartPoint;
			// [HideInInspector]
			public Vector2 pathEndPoint;

			public Waypoint (Transform trs, Vector2 pathStartPoint = default(Vector2), Vector2 pathEndPoint = default(Vector2))
			{
				this.trs = trs;
				this.pathStartPoint = pathStartPoint;
				this.pathEndPoint = pathEndPoint;
			}
		}

		public enum FollowType
		{
			Once,
			Loop,
			PingPong
		}

#if UNITY_EDITOR
		public enum SetLineRenderersMethod
		{
			ShapeOutline,
			Path
		}
#endif
	}
}