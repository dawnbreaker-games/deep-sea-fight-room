using FightRoom;
using Extensions;
using UnityEngine;

public class TentacleAnimation : UpdateWhileEnabled
{
	public Transform trs;
	public float missileMapRadius;
	public float missileTurnRate;
	public float missileSpeed;
	public HandleMapEdgeMode handleMapEdgeMode;
	Vector2 missileDestination;
	Vector2 missileVelocity;
	float missileDistanceToDestinationSqr;
	bool missileHasReducedDistanceToDestination;
	Vector2 initMissilePosition;
	Vector2 missilePosition;
	float turnAmountMultiplier;
	
	void Awake ()
	{
		initMissilePosition = trs.position;
		missileVelocity = Random.insideUnitCircle.normalized * missileSpeed;
		missileDestination = initMissilePosition + Random.insideUnitCircle * missileMapRadius;
		missilePosition = initMissilePosition;
		turnAmountMultiplier = (Random.value < .5f).PositiveOrNegative();
	}

#if UNITY_EDITOR
	void OnValidate ()
	{
		if (trs == null)
			trs = GetComponent<Transform>();
	}
#endif

	public override void DoUpdate ()
	{
		if (GameManager.framesSinceLevelLoaded <= GameManager.LAG_FRAMES_AFTER_SCENE_LOAD)
			return;
		float previousMissileDistanceToDestinationSqr = (missilePosition - missileDestination).sqrMagnitude;
		float missileIdealTurnAmount = Vector2.Angle(missileVelocity, missileDestination - missilePosition);
		float missileTurnAmount = Mathf.Clamp(missileIdealTurnAmount, -missileTurnRate * Time.unscaledDeltaTime, missileTurnRate * Time.unscaledDeltaTime);
		missileVelocity = VectorExtensions.Rotate(missileVelocity, missileTurnAmount * turnAmountMultiplier);
		missileVelocity = missileVelocity.normalized * missileSpeed * Time.unscaledDeltaTime;
		missilePosition += missileVelocity;
		missilePosition = initMissilePosition + Vector2.ClampMagnitude(missilePosition - initMissilePosition, missileMapRadius);
		if ((missilePosition - initMissilePosition).sqrMagnitude >= missileMapRadius * missileMapRadius)
		{
			if (handleMapEdgeMode == HandleMapEdgeMode.BounceMissile)
			{
				missileVelocity = Vector2.Reflect(missileVelocity, missilePosition - initMissilePosition);
				missileVelocity = missileVelocity.normalized * missileSpeed * Time.unscaledDeltaTime;
			}
		}
		float missileDistanceToDestinationSqr = (missilePosition - missileDestination).sqrMagnitude;
		if (missileHasReducedDistanceToDestination && missileDistanceToDestinationSqr > previousMissileDistanceToDestinationSqr)
		{
			missileDestination = initMissilePosition + Random.insideUnitCircle * missileMapRadius;
			missileHasReducedDistanceToDestination = false;
			turnAmountMultiplier = (Random.value < .5f).PositiveOrNegative();
		}
		else if (missileDistanceToDestinationSqr < previousMissileDistanceToDestinationSqr)
			missileHasReducedDistanceToDestination = true;
		trs.position = missilePosition;
	}

	public enum HandleMapEdgeMode
	{
		ClampMissilePosition,
		BounceMissile
	}
}
