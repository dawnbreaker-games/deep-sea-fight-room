using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(LineRenderer))]
	public class _LineRenderer : UpdateWhileEnabled
	{
		public LineRenderer lineRenderer;
		public Transform[] points = new Transform[0];

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (lineRenderer == null)
					lineRenderer = GetComponent<LineRenderer>();
				DoUpdate ();
				return;
			}
#endif
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			List<Vector3> positions = new List<Vector3>();
			for (int i = 0; i < points.Length; i ++)
			{
				Transform point = points[i];
				if (point != null)
					positions.Add(point.position);
			}
			lineRenderer.positionCount = positions.Count;
			lineRenderer.SetPositions(positions.ToArray());
		}
	}
}