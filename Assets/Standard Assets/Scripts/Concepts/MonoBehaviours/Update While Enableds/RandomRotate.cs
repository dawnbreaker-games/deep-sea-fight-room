using FightRoom;
using Extensions;
using UnityEngine;

public class RandomRotate : UpdateWhileEnabled
{
	public Transform trs;
	public float missileMapRadius;
	public float missileTurnRate;
	public float missileSpeed;
	public float maxAngleToInitFacing;
	Vector2 missilePosition;
	Vector2 missileDestination;
	Vector2 missileVelocity;
	float missileDistToDestSqr;
	bool missileHasReducedDistToDest;
	Vector3 initFacing;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
		}
#endif
	
	public override void OnEnable ()
	{
		initFacing = trs.forward;
		missileVelocity = Random.insideUnitCircle.normalized * missileSpeed;
		missileDestination = Random.insideUnitCircle * missileMapRadius;
		base.OnEnable ();
	}

	public override void DoUpdate ()
	{
		float prevMissileDistToDestSqr = (missilePosition - missileDestination).sqrMagnitude;
		float missileIdealTurnAmount = Vector2.Angle(missileVelocity, missileDestination - missilePosition);
		float missileTurnAmount = Mathf.Clamp(missileIdealTurnAmount, -missileTurnRate * Time.deltaTime, missileTurnRate * Time.deltaTime);
		missileVelocity = missileVelocity.Rotate(missileTurnAmount);
		missilePosition += missileVelocity * Time.deltaTime;
		float missileDistToDestSqr = (missilePosition - missileDestination).sqrMagnitude;
		if (missileHasReducedDistToDest && missileDistToDestSqr > prevMissileDistToDestSqr)
		{
			missileDestination = Random.insideUnitCircle * missileMapRadius;
			missileHasReducedDistToDest = false;
		}
		else if (missileDistToDestSqr < prevMissileDistToDestSqr)
			missileHasReducedDistToDest = true;
		trs.RotateAround(trs.position, trs.up, missileVelocity.x * Time.deltaTime);
		trs.RotateAround(trs.position, trs.right, missileVelocity.y * Time.deltaTime);
		float angleToInitFacing = Vector3.Angle(trs.forward, initFacing);
		float angleOvershoot = angleToInitFacing - maxAngleToInitFacing;
		if (angleOvershoot > 0)
			trs.forward = Vector3.RotateTowards(trs.forward, initFacing, angleOvershoot * Mathf.Deg2Rad, 0);
	}
}
