using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FightRoom
{
	public class Tutorial2 : SingletonUpdateWhileEnabled<Tutorial2>
	{
		public GameObject[] activateOnFinish = new GameObject[0];
		public Button previousButton;
		[Multiline(25)]
		public string text;
		public uint showTextsCount;
		public _Text2 textPrefab;
		[HideInInspector]
		public _Text2[] texts = new _Text2[0];
		public Text continueText;
		public Animator continueButtonAnimator;
		public int finishAtTextIndex;
		public Tutorial2 previousTutorial;
		public UnityEvent onPressPreviousButton;
#if UNITY_EDITOR
		public Transform textsParent;
		public bool setTexts;
#endif
		protected int currentTextIndex;
		_Text2 currentText;
		bool previousShootInput;
		bool wasLevelEnabled;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (textsParent == null)
				textsParent = transform;
			if (setTexts)
			{
				setTexts = false;
				SetTexts ();
				EditorUtility.SetDirty(this);
			}
		}

		void SetTexts ()
		{
			// bool wasActive = gameObject.activeSelf;
			// gameObject.SetActive(true);
			this.texts = textsParent.GetComponentsInChildren<_Text2>(true);
			// Dictionary<int, string> textsNamesDict = new Dictionary<int, string>();
			// Dictionary<int, _Text2.Event[]> textsEventsDict = new Dictionary<int, _Text2.Event[]>();
			// for (int i = 0; i < this.texts.Length; i ++)
			// {
			// 	_Text2 text = this.texts[i];
			// 	text.Init ();
			// 	textsNamesDict.Add(i, text.name);
			// 	textsEventsDict.Add(i, new List<_Text2.Event>(text.events).ToArray());
			// 	GameManager.DestroyOnNextEditorUpdate (text.gameObject);
			// }
			// string[] lines = this.text.Split("\n", StringSplitOptions.RemoveEmptyEntries);
			// int textIndex = 0;
			// List<_Text2> texts = new List<_Text2>();
			// _Text2 previousText = null;
			// for (int i = 0; i < lines.Length; i ++)
			// {
			// 	string line = lines[i];
			// 	if (line.Length > 0)
			// 	{
			// 		_Text2 text = (_Text2) PrefabUtility.InstantiatePrefab(textPrefab, textsParent);
			// 		text.text.rectTransform.Copy (textsParent.GetComponentInChildren<RectTransform>());
			// 		text.text.text = line;
			// 		if (textsNamesDict.Count > i)
			// 		{
			// 			_Text2.Event[] events = textsEventsDict[textIndex];
			// 			for (int i2 = 0; i2 < events.Length; i2 ++)
			// 			{
			// 				_Text2.Event _event = events[i2];
			// 				_Text2.CustomEvent customEvent = _event as _Text2.CustomEvent;
			// 				if (customEvent != null)
			// 					text.customEvents = text.customEvents.Add(customEvent);
			// 				_Text2.SetTextBasedOnUsingInputDeviceEvent setTextBasedOnUsingInputDeviceEvent = _event as _Text2.SetTextBasedOnUsingInputDeviceEvent;
			// 				if (setTextBasedOnUsingInputDeviceEvent != null)
			// 					text.setTextBasedOnUsingInputDeviceEvents = text.setTextBasedOnUsingInputDeviceEvents.Add(setTextBasedOnUsingInputDeviceEvent);
			// 			}
			// 			text.events = events;
			// 			text.name = textsNamesDict[textIndex];
			// 		}
			// 		textIndex ++;
			// 		texts.Add(text);
			// 		previousText = text;
			// 	}
			// }
			// for (int i = 0; i < texts.Count; i ++)
			// {
			// 	_Text2 text = texts[i];
			// 	text.gameObject.SetActive(false);
			// }
			// this.texts = texts.ToArray();
			// gameObject.SetActive(wasActive);
		}
#endif

		public override void OnEnable ()
		{
			base.OnEnable ();
			TutorialsMenu.currentTutorialName = name;
			instance = this;
			if (previousButton != null)
				previousButton.interactable = previousTutorial != null;
			if (Player.Instance != null)
				Player.instance.Enabled = false;
			currentTextIndex = 0;
			currentText = texts[0];
			currentText.gameObject.SetActive(true);
			if (continueText != null)
				continueText.gameObject.SetActive(true);
			if (continueButtonAnimator != null)
				continueButtonAnimator.Play("Flash");
			wasLevelEnabled = Level.instance.enabled;
			Level.instance.enabled = false;
			GameManager.Paused = true;
		}

		public void NextText ()
		{
			currentTextIndex ++;
			if (currentTextIndex >= texts.Length)
			{
				if (currentTextIndex == finishAtTextIndex)
					Finish ();
				return;
			}
			if (currentTextIndex == finishAtTextIndex)
			{
				Finish ();
				return;
			}
			currentText = texts[currentTextIndex];
			if (currentTextIndex >= showTextsCount)
			{
				_Text2 _text = texts[currentTextIndex - (int) showTextsCount];
				_text.gameObject.SetActive(false);
			}
			for (int i = Mathf.Clamp(currentTextIndex - (int) showTextsCount + 1, 0, texts.Length - 1); i <= currentTextIndex; i ++)
			{
				_Text2 _text = texts[i];
				_text.gameObject.SetActive(true);
			}
			if (previousButton != null)
				previousButton.interactable = true;
		}

		public static void _NextText ()
		{
			instance.NextText ();
		}

		public void PreviousText ()
		{
			previousButton.interactable = true;
			onPressPreviousButton.Invoke();
			if (currentTextIndex > 0)
			{
				int previousTextIndex = currentTextIndex - 1;
				gameObject.SetActive(false);
				gameObject.SetActive(true);
				for (int i = 0; i < previousTextIndex; i ++)
					NextText ();
			}
			else
			{
				if (previousTutorial != null)
				{
					if (!gameObject.activeSelf)
					{
						previousTutorial.gameObject.SetActive(true);
						for (int i = 3; i < previousTutorial.texts.Length; i ++)
							previousTutorial.NextText ();
					}
					gameObject.SetActive(false);
				}
				else
					previousButton.interactable = false;
			}
		}

		public void _PreviousText ()
		{
			instance.PreviousText ();
		}

		public virtual void Finish ()
		{
			gameObject.SetActive(false);
			for (int i = 0; i < activateOnFinish.Length; i ++)
			{
				GameObject go = activateOnFinish[i];
				go.SetActive(true);
			}
		}

		public void SetParent (Transform parent)
		{
			textsParent.parent.SetParent(parent);
		}

		public override void DoUpdate ()
		{
			bool shootInput = InputManager.ShootInput;
			bool isOverButton = false;
			for (int i = 0; i < _Button.instances.Length; i ++)
			{
				_Button button = _Button.instances[i];
				if (button.canvasRectTrs != null && button.rectTrs.GetRectInCanvasNormalized(button.canvasRectTrs).Contains(CameraScript.instance.camera.ScreenToViewportPoint((Vector2) InputManager.MousePosition)))
				{
					isOverButton = true;
					break;
				}
			}
            if (!isOverButton && shootInput && !previousShootInput && !Player.instance.enabled)
				NextText ();
			previousShootInput = shootInput;
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			for (int i = 0; i < texts.Length; i ++)
			{
				_Text2 text = texts[i];
				text.gameObject.SetActive(false);
			}
			if (continueText != null)
				continueText.gameObject.SetActive(false);
			if (continueButtonAnimator != null)
				continueButtonAnimator.Play("None");
			if (Player.instance != null)
			{
				Player.instance.enabled = true;
				if (Level.instance != null)
					Level.instance.enabled = wasLevelEnabled;
			}
			GameManager.Paused = false;
		}
	}
}