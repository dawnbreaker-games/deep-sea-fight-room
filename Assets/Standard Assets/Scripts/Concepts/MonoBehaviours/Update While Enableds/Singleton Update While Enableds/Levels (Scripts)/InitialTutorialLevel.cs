using UnityEngine;

namespace FightRoom
{
	public class InitialTutorialLevel : Level
	{
		public void SetPlayerName (string name)
		{
			SaveAndLoadManager.saveData.playerCustomName = name;
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
		}
	}
}