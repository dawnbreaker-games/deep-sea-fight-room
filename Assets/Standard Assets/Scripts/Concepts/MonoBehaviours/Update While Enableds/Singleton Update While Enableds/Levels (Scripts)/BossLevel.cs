using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace FightRoom
{
	public class BossLevel : Level
	{
		[HideInInspector]
		public float totalMaxHp;
		[HideInInspector]
		public float totalDamage;

		public override void Begin ()
		{
			OnBegin ();
			for (int i = 0; i < enemySpawnEntries.Length; i ++)
			{
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[i];
				Enemy boss = enemySpawnEntry.SpawnEnemy();
				totalMaxHp += boss.maxHp;
			}
			currentTimeText.text = "Damage: 0";
			enemiesKilledText.gameObject.SetActive(false);
			GameManager.instance.bossHealthBarTrs.parent.gameObject.SetActive(true);
		}

		public override void End ()
		{
			if (totalDamage > BestTimeReached)
				BestTimeReached = totalDamage;
			Achievement.instances = FindObjectsByType<Achievement>(FindObjectsSortMode.None);
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (!achievement.complete && achievement.ShouldBeComplete())
					achievement.Complete ();
			}
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			_SceneManager.instance.RestartScene ();
		}
	}
}