using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

namespace FightRoom
{
	public class Tutorial : SingletonUpdateWhileEnabled<Tutorial>
	{
		public PlayableDirector playableDirector;
		public GameObject[] activateOnFinish = new GameObject[0];
		public Button previousButton;

		public override void OnEnable ()
		{
			base.OnEnable ();
			instance = this;
			Tutorial previousTutorial = null;
			Tutorial[] instances = FindObjectsOfType<Tutorial>(true);
			for (int i = 0; i < instances.Length; i ++)
			{
				Tutorial tutorial = instances[i];
				if (tutorial.activateOnFinish.Contains(instance.gameObject))
				{
					previousTutorial = tutorial;
					break;
				}
			}
			previousButton.interactable = previousTutorial != null;
			if (playableDirector != null)
				playableDirector.time = 0;
		}

		public virtual void Finish ()
		{
			gameObject.SetActive(false);
			for (int i = 0; i < activateOnFinish.Length; i ++)
			{
				GameObject go = activateOnFinish[i];
				go.SetActive(true);
			}
		}

		public static void FinishCurrent ()
		{
			_Text[] texts = FindObjectsOfType<_Text>(true);
			for (int i = 0; i < texts.Length; i ++)
			{
				_Text text = texts[i];
				text.UpdateText ();
			}
			if (instance.playableDirector != null)
				instance.playableDirector.time = instance.playableDirector.duration;
			instance.Finish ();
		}

		public static void GoToPrevious ()
		{
			Tutorial previousTutorial = null;
			Tutorial[] instances = FindObjectsOfType<Tutorial>(true);
			for (int i = 0; i < instances.Length; i ++)
			{
				Tutorial tutorial = instances[i];
				if (tutorial.activateOnFinish.Contains(instance.gameObject))
				{
					previousTutorial = tutorial;
					break;
				}
			}
			if (previousTutorial == null)
				instance.previousButton.interactable = false;
			_Text[] texts = FindObjectsOfType<_Text>(true);
			for (int i = 0; i < texts.Length; i ++)
			{
				_Text text = texts[i];
				text.UpdateText ();
			}
			instance.gameObject.SetActive(false);
			previousTutorial.gameObject.SetActive(true);
		}
	}
}