﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	//[ExecuteInEditMode]
	[RequireComponent(typeof(Camera))]
	[DisallowMultipleComponent]
	public class CameraScript : SingletonUpdateWhileEnabled<CameraScript>
	{
		public Transform trs;
		public new Camera camera;
		public Vector2 viewSize;
		[HideInInspector]
		public Rect viewRect;
		public Rect fitInNormalizedScreenViewRect;
		protected Rect normalizedScreenViewRect;
		protected float screenAspect;
		public new static CameraScript Instance
		{
			get
			{
				if (instance == null)
					instance = Camera.main.GetComponent<CameraScript>();
				return instance;
			}
		}
		
		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (camera == null)
					camera = GetComponent<Camera>();
				return;
			}
#endif
			viewRect.size = viewSize;
			HandlePosition ();
			HandleViewSize ();
		}

		public override void DoUpdate ()
		{
			HandlePosition ();
			HandleViewSize ();
		}
		
		public virtual void HandlePosition ()
		{
			viewRect.center = trs.position;
		}
		
		public virtual void HandleViewSize ()
		{
			screenAspect = (float) Screen.width / Screen.height;
			camera.aspect = viewSize.x / viewSize.y;
			camera.orthographicSize = Mathf.Max(viewSize.x / 2 / camera.aspect, viewSize.y / 2);
			normalizedScreenViewRect = new Rect();
			normalizedScreenViewRect.size = fitInNormalizedScreenViewRect.size * new Vector2(camera.aspect / screenAspect, Mathf.Min(1, screenAspect / camera.aspect));
			normalizedScreenViewRect.center = fitInNormalizedScreenViewRect.center;
			camera.rect = normalizedScreenViewRect;
			viewRect.size = viewSize;
		}
	}
}