using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace FightRoom
{
	public class Level : SingletonUpdateWhileEnabled<Level>
	{
		public string displayName;
		public bool hasShop;
		public float enemiesSpawnDurationPerWave;
		public Transform trs;
		public float innerSize;
		public float size;
		public bool unlocked;
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public Wave[] waves = new Wave[0];
		public GameObject moveToBeginTextGo;
		public Text currentTimeText;
		public Text bestTimeReachedText;
		public Text enemiesKilledText;
		public Text mostEnemiesKilledText;
		public Transform playerSpawnPoint;
		public GameObject lockedIndicatorGo;
		public GameObject untriedIndicatorGo;
		public Type type;
		public GameObject fireTrailEmitterGo;
		public float windSpeed;
		public float rewindDelay;
		public float rewindDuration;
		public float branchInterval;
		public float playbackTimeOffset;
		public float dividePlaybackColor;
		public float divideObjectsAlphaInNextBranch;
		public Timeline timeline;
		public float addToDifficultyPerWave;
		public float divideDifficulty;
		public float addToDifficulty;
		[HideInInspector]
		public float rewindDurationRemaining;
		public LayerMask whatEnemiesCantSpawnIn;
		public Text waveTimerText;
		public float multiplyEnemyHp;
		public float addToMultiplyEnemyHp;
		public FloatRange timeBetweenWavesRange;
		public Collider2D spawnZoneCollider;
		public Tutorial2 tutorialBeforeLevel;
		public float enemyMoneyMultiplier;
		[HideInInspector]
		public float spawnAtTime;
		public byte addToWaveDuration;
		public float enableEnemyDelay;
		public float BestTimeReached
		{
			get
			{
				float output = 0;
				SaveAndLoadManager.saveData.bestLevelTimesDict.TryGetValue(name, out output);
				return output;
			}
			set
			{
				SaveAndLoadManager.saveData.bestLevelTimesDict[name] = value;
			}
		}
		public uint MostEnemiesKilled
		{
			get
			{
				uint output = 0;
				SaveAndLoadManager.saveData.mostEnemiesKilled.TryGetValue(name, out output);
				return output;
			}
			set
			{
				SaveAndLoadManager.saveData.mostEnemiesKilled[name] = value;
			}
		}
		public float MostDamageHealed
		{
			get
			{
				float output = 0;
				SaveAndLoadManager.saveData.mostDamageHealed.TryGetValue(name, out output);
				return output;
			}
			set
			{
				SaveAndLoadManager.saveData.mostDamageHealed[name] = value;
			}
		}
		public static Level[] instances = new Level[0];
		public static float currentTime;
		public static uint enemiesKilled;
		public static List<GameObject> gosSpawnedThisWave = new List<GameObject>();
		public ClampedByte waveDuration;
		static Bullet[] bulletsInBranch = new Bullet[0];
		static Explosion[] explosionsInBranch = new Explosion[0];
		static Bullet[] bulletsInNextBranch = new Bullet[0];
		static Explosion[] explosionsInNextBranch = new Explosion[0];
		static Dictionary<Bullet, Bullet> bulletsPlaybacksDict = new Dictionary<Bullet, Bullet>();
		static Dictionary<Explosion, Explosion> explosionsPlaybacksDict = new Dictionary<Explosion, Explosion>();
		static Dictionary<Entity, Entity> entitiesPlaybacksDict = new Dictionary<Entity, Entity>();
		static List<Bullet> waitingToRemoveBulletsPlaybacks = new List<Bullet>();
		static List<Explosion> waitingToRemoveExplosionsPlaybacks = new List<Explosion>();
		static List<Entity> waitingToRemoveEntitiesPlaybacks = new List<Entity>();
		static uint waveCount;
		static uint wave;
		static float waveTimer;
		static SortedList<float, EnemySpawnEntry> spawnEnemyEntriesAtTimes = new SortedList<float, EnemySpawnEntry>();
		float rewindDelayRemaining;
		float timeUntilBranch;
		float difficulty;
		Zone2D spawnZone;
		SpawnEnemiesUpdater spawnEnemiesUpdater = new SpawnEnemiesUpdater();

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (type.HasFlag(Type.Teleport))
			{
				Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
				for (int i = 0; i < colliders.Length; i ++)
				{
					Collider2D collider = colliders[i];
					if (collider.name != "Spawn Zone" && collider is not CompositeCollider2D)
						collider.usedByComposite = true;
				}
				CompositeCollider2D compositeCollider = gameObject.GetComponent<CompositeCollider2D>();
				if (compositeCollider == null)
					compositeCollider = gameObject.AddComponent<CompositeCollider2D>();
				compositeCollider.isTrigger = true;
				Rigidbody2D rigid = GetComponent<Rigidbody2D>();
				rigid.bodyType = RigidbodyType2D.Static;
			}
			else
			{
				Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
				for (int i = 0; i < colliders.Length; i ++)
				{
					Collider2D collider = colliders[i];
					if (collider.name != "Spawn Zone" && collider is not CompositeCollider2D)
						collider.usedByComposite = false;
				}
				GameManager.DestroyOnNextEditorUpdate (GetComponent<CompositeCollider2D>());
				GameManager.DestroyOnNextEditorUpdate (GetComponent<Rigidbody2D>());
			}
		}
#endif

		public override void Awake ()
		{
			base.Awake ();
			Riposte.ownedCount = 0;
			PiggyBank.ownedCount = 0;
			CombatTraining.ownedCount = 0;
			Tartigrade.ownedCount = 0;
			if (untriedIndicatorGo != null)
				untriedIndicatorGo.SetActive(unlocked && BestTimeReached == 0);
		}

		public virtual void Begin ()
		{
			if (waves.Length > 0)
				enemySpawnEntries = waves[waveCount % waves.Length].enemySpawnEntries;
			waveCount = 0;
			waveTimer = waveDuration.value;
			ShopMenu.openedCountThisWave = 0;
			OnBegin ();
		}

		public void OnBegin ()
		{
			if (spawnZoneCollider != null)
			{
				spawnZone = new Zone2D(spawnZoneCollider);
				Trap[] traps = GetComponentsInChildren<Trap>();
				for (int i = 0; i < traps.Length; i ++)
				{
					Trap trap = traps[i];
					trap.enabled = true;
				}
				FollowWaypoints[] followWaypoints = GetComponentsInChildren<FollowWaypoints>();
				for (int i = 0; i < followWaypoints.Length; i ++)
				{
					FollowWaypoints _followWaypoints = followWaypoints[i];
					_followWaypoints.enabled = true;
				}
				RotateTransform[] rotateTransforms = GetComponentsInChildren<RotateTransform>();
				for (int i = 0; i < rotateTransforms.Length; i ++)
				{
					RotateTransform rotateTransform = rotateTransforms[i];
					rotateTransform.enabled = true;
				}
			}
			timeUntilBranch = branchInterval;
			bulletsInBranch = new Bullet[0];
			explosionsInBranch = new Explosion[0];
			bulletsInNextBranch = new Bullet[0];
			explosionsInNextBranch = new Explosion[0];
			bulletsPlaybacksDict.Clear();
			explosionsPlaybacksDict.Clear();
			entitiesPlaybacksDict.Clear();
			waitingToRemoveBulletsPlaybacks.Clear();
			waitingToRemoveExplosionsPlaybacks.Clear();
			waitingToRemoveEntitiesPlaybacks.Clear();
			moveToBeginTextGo.SetActive(false);
			if (bestTimeReachedText != null)
			{
				bestTimeReachedText.gameObject.SetActive(false);
				mostEnemiesKilledText.gameObject.SetActive(false);
				enemiesKilledText.gameObject.SetActive(true);
				untriedIndicatorGo.SetActive(false);
			}
			GameManager.instance.pauseButtonGo.SetActive(true);
			if (!SaveAndLoadManager.saveData.triedPlayers.Contains(Player.Instance.name))
				SaveAndLoadManager.saveData.triedPlayers = SaveAndLoadManager.saveData.triedPlayers.Add(Player.instance.name);
			Player.instance.enabled = true;
			Player.instance.polarityTimer = Random.Range(-Player.instance.negativePolarityDuration, Player.instance.unbalanced.Apply(Player.instance.negativePolarityDuration));
			Player.instance.animator.enabled = true;
			Player.instance.untriedIndicatorGo.SetActive(false);
            Player.instance.hpBarImage.rectTransform.parent.gameObject.SetActive(true);
            Player.instance.moneyText.gameObject.SetActive(true);
			if (waveTimerText != null)
				waveTimerText.gameObject.SetActive(true);
			if (type.HasFlag(Type.Fire))
				fireTrailEmitterGo.SetActive(true);
			GameManager.paused = false;
			currentTime = 0;
			enemiesKilled = 0;
			gosSpawnedThisWave.Clear();
			if (Duel.instance == null)
			{
				difficulty = addToDifficulty;
				SpawnEnemies ();
				GameManager.updatables = GameManager.updatables.Add(spawnEnemiesUpdater);
			}
			enabled = true;
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			if (Boss.Instance == null && enemySpawnEntries.Length > 0)
			{
				waveTimer -= Time.deltaTime;
				waveTimerText.text = "" + (uint) waveTimer;
				if (waveTimer <= 0)
				{
					EndWave ();
					return;
				}
			}
			if (type.HasFlag(Type.TimeRewind))
			{
				if (rewindDelayRemaining > 0)
				{
					rewindDelayRemaining -= Time.deltaTime;
					currentTime += Time.deltaTime;
					timeline.InsertPointAtTime (currentTime);
					if (rewindDelayRemaining <= 0)
						rewindDurationRemaining = rewindDuration - rewindDelayRemaining;
				}
				else
				{
					rewindDurationRemaining -= Time.deltaTime;
					currentTime -= Time.deltaTime;
					for (int i = timeline.points.Count - 1; i >= 0; i --)
					{
						Timeline.Point timelinePoint = timeline.points[i];
						if (timelinePoint.time > currentTime)
							timeline.points.RemoveAt(i);
						else
							break;
					}
					Timeline.Point lastPoint = timeline.points[timeline.points.Count - 1];
					lastPoint.Apply ();
					timeline.timeOfLastPoint = lastPoint.time;
					if (rewindDurationRemaining <= 0)
						rewindDelayRemaining = rewindDelay - rewindDurationRemaining;
				}
			}
			else
				currentTime += Time.deltaTime;
			if (type.HasFlag(Type.Wind))
			{
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					bullet.extraVelocity = ((Vector2) (bullet.trs.position - trs.position)).Rotate270().normalized * windSpeed;
					bullet.rigid.linearVelocity = bullet.velocity + bullet.extraVelocity;
					bullet.trs.up = bullet.rigid.linearVelocity;
				}
			}
			if (type.HasFlag(Type.Branch))
			{
				timeUntilBranch -= Time.deltaTime;
				List<Bullet> bullets = new List<Bullet>(Bullet.instances);
				bullets.RemoveEach(bulletsInBranch);
				List<Explosion> explosions = new List<Explosion>(Explosion.instances);
				explosions.RemoveEach(explosionsInBranch);
				timeline.InsertPointAtTime (currentTime, bullets.ToArray(), explosions.ToArray(), null);
				if (timeUntilBranch <= 0)
				{
					for (int i = 0; i < bulletsInBranch.Length; i ++)
					{
						Bullet bullet = bulletsInBranch[i];
						if (bullet != null)
						{
							BombBullet bombBullet = bullet as BombBullet;
							if (bombBullet != null)
								bombBullet.explodeOnDisable = false;
							ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
						}
					}
					for (int i = 0; i < bulletsInNextBranch.Length; i ++)
					{
						Bullet bullet = bulletsInNextBranch[i];
						if (bullet != null)
							ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
					}
					for (int i = 0; i < explosionsInBranch.Length; i ++)
					{
						Explosion explosion = explosionsInBranch[i];
						if (explosion != null)
							ObjectPool.instance.Despawn (explosion.prefabIndex, explosion.gameObject, explosion.trs);
					}
					for (int i = 0; i < explosionsInNextBranch.Length; i ++)
					{
						Explosion explosion = explosionsInNextBranch[i];
						if (explosion != null)
							ObjectPool.instance.Despawn (explosion.prefabIndex, explosion.gameObject, explosion.trs);
					}
					ShowNextBranchIndicator ();
					MakeBranch ();
					int removeCount = timeline.GetPointIndexAtTime(currentTime + timeUntilBranch) + 1;
					removeCount = Mathf.Clamp(removeCount, 0, timeline.points.Count - 1);
					timeline.points.RemoveRange(0, removeCount);
					timeline.timeOfFirstPoint = timeline.points[0].time;
					timeUntilBranch += branchInterval;
				}
			}
			if (type.HasFlag(Type.Playback))
			{
				List<Bullet> bullets = new List<Bullet>(Bullet.instances);
				bullets = bullets.RemoveEach(bulletsPlaybacksDict.Values);
				List<Explosion> explosions = new List<Explosion>(Explosion.instances);
				explosions = explosions.RemoveEach(explosionsPlaybacksDict.Values);
				List<Entity> entities = new List<Entity>(Entity.GetInstances());
				entities = entities.RemoveEach(entitiesPlaybacksDict.Values);
				timeline.InsertPointAtTime (currentTime, bullets.ToArray(), explosions.ToArray(), entities.ToArray());
				if (currentTime >= playbackTimeOffset)
				{
					Timeline.Point timelinePoint = timeline.GetPointAtTime(currentTime - playbackTimeOffset);
					for (int i = 0; i < timelinePoint.bulletSnapshots.Length; i ++)
					{
						Bullet.Snapshot bulletSnapshot = timelinePoint.bulletSnapshots[i];
						Bullet bullet = bulletSnapshot.bullet;
						Bullet clonedBullet;
						if (bullet != null && bullet.gameObject.activeInHierarchy)
						{
							if (!bulletsPlaybacksDict.TryGetValue(bullet, out clonedBullet))
							{
								bulletSnapshot.bullet = null;
								bulletsPlaybacksDict.Add(bullet, bulletSnapshot.Apply());
							}
							else
							{
								bulletSnapshot.bullet = clonedBullet;
								bulletSnapshot.Apply (false);
							}
						}
						else if (bulletsPlaybacksDict.TryGetValue(bullet, out clonedBullet))
						{
							if (clonedBullet == null || !clonedBullet.gameObject.activeInHierarchy)
							{
								bulletsPlaybacksDict.Remove(clonedBullet);
								waitingToRemoveBulletsPlaybacks.Remove(clonedBullet);
							}
							else if (!waitingToRemoveBulletsPlaybacks.Contains(clonedBullet))
							{
								ObjectPool.instance.DelayDespawn (clonedBullet.prefabIndex, clonedBullet.gameObject, clonedBullet.trs, playbackTimeOffset);
								waitingToRemoveBulletsPlaybacks.Add(clonedBullet);
								bulletSnapshot.bullet = clonedBullet;
								bulletSnapshot.Apply (false);
							}
						}
					}
					for (int i = 0; i < timelinePoint.explosionSnapshots.Length; i ++)
					{
						Explosion.Snapshot explosionSnapshot = timelinePoint.explosionSnapshots[i];
						Explosion explosion = explosionSnapshot.explosion;
						Explosion clonedExplosion;
						if (explosion != null && explosion.gameObject.activeInHierarchy)
						{
							if (!explosionsPlaybacksDict.TryGetValue(explosion, out clonedExplosion))
							{
								explosionSnapshot.explosion = null;
								explosionsPlaybacksDict.Add(explosion, explosionSnapshot.Apply());
							}
							else
							{
								explosionSnapshot.explosion = clonedExplosion;
								explosionSnapshot.Apply ();
							}
						}
						else if (explosionsPlaybacksDict.TryGetValue(explosion, out clonedExplosion))
						{
							if (clonedExplosion == null || !clonedExplosion.gameObject.activeInHierarchy)
							{
								explosionsPlaybacksDict.Remove(clonedExplosion);
								waitingToRemoveExplosionsPlaybacks.Remove(clonedExplosion);
							}
							else if (!waitingToRemoveExplosionsPlaybacks.Contains(clonedExplosion))
							{
								ObjectPool.instance.DelayDespawn (clonedExplosion.prefabIndex, clonedExplosion.gameObject, clonedExplosion.trs, playbackTimeOffset);
								waitingToRemoveExplosionsPlaybacks.Add(clonedExplosion);
								explosionSnapshot.explosion = clonedExplosion;
								explosionSnapshot.Apply ();
							}
						}
					}
					for (int i = 0; i < timelinePoint.entitySnapshots.Length; i ++)
					{
						Entity.Snapshot entitySnapshot = timelinePoint.entitySnapshots[i];
						Entity entity = entitySnapshot.entity;
						Entity clonedEntity;
						if (entity != null && entity.gameObject.activeInHierarchy)
						{
							if (!entitiesPlaybacksDict.TryGetValue(entity, out clonedEntity))
							{
								entitySnapshot.entity = null;
								clonedEntity = entitySnapshot.Apply();
								clonedEntity.enabled = false;
								clonedEntity.maxHp = -1;
								clonedEntity.rigid.constraints = RigidbodyConstraints2D.FreezeAll;
								GameObject colliderGo = clonedEntity.collider.gameObject;
								colliderGo.layer = LayerMask.NameToLayer(LayerMask.LayerToName(colliderGo.layer).Replace(" Clone", "") + " Clone");
								Material material = new Material(clonedEntity.renderer.sharedMaterial);
								Color tint = material.GetColor("_tint");
								material.SetColor("_tint", tint / dividePlaybackColor);
								clonedEntity.renderer.material = material;
								entitiesPlaybacksDict.Add(entity, clonedEntity);
							}
							else
							{
								entitySnapshot.entity = clonedEntity;
								entitySnapshot.Apply ();
							}
						}
						else if (entitiesPlaybacksDict.TryGetValue(entity, out clonedEntity))
						{
							if (clonedEntity == null || !clonedEntity.gameObject.activeInHierarchy)
							{
								entitiesPlaybacksDict.Remove(clonedEntity);
								waitingToRemoveEntitiesPlaybacks.Remove(clonedEntity);
							}
							else if (!waitingToRemoveEntitiesPlaybacks.Contains(clonedEntity))
							{
								ObjectPool.instance.DelayDespawn (clonedEntity.prefabIndex, clonedEntity.gameObject, clonedEntity.trs, playbackTimeOffset);
								waitingToRemoveEntitiesPlaybacks.Add(clonedEntity);
								entitySnapshot.entity = clonedEntity;
								entitySnapshot.Apply ();
							}
						}
					}
				}
			}
			if (Boss.instance == null && currentTimeText != null)
				currentTimeText.text = string.Format("{0:0.#}", currentTime);
		}

		public void EndWave ()
		{
			enabled = false;
			for (int i = 0; i < Enemy.instances.Count; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				ObjectPool.instance.Despawn (enemy.prefabIndex, enemy.gameObject, enemy.trs);
				i --;
			}
			for (int i = 0; i < Bullet.instances.Count; i ++)
			{
				Bullet bullet = Bullet.instances[i];
				ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
				i --;
			}
			for (int i = 0; i < Explosion.instances.Count; i ++)
			{
				Explosion explosion = Explosion.instances[i];
				ObjectPool.instance.Despawn (explosion.prefabIndex, explosion.gameObject, explosion.trs);
				i --;
			}
			for (int i = 0; i < Money.instances.Count; i ++)
			{
				Money money = Money.instances[i];
				Player.instance.AddMoney (money.amount);
				Destroy(money.gameObject);
				i --;
			}
			if (Player.instance.rageUpdater != null)
				Player.instance.rageUpdater.End ();
			NextWave ();
		}

		public void NextWave ()
		{
			if (waves.Length > 0)
				enemySpawnEntries = waves[waveCount % waves.Length].enemySpawnEntries;
			for (int i = 0; i < ShopMenu.ITEM_COUNT; i ++)
			{
				BuyItemEntry itemEntry = ShopMenu.buyItemEntries[i];
				if (itemEntry != null && !itemEntry.Locked)
					Destroy(itemEntry.gameObject);
			}
			if (enemyMoneyMultiplier > 0)
				Player.instance.AddMoney (PiggyBank.ownedCount * PiggyBank.SAVE_AMOUNT);
			Player.instance.damage.AddToValue (CombatTraining.ownedCount * CombatTraining.DAMAGE_AMOUNT);
			float deltaTime = Time.fixedDeltaTime;
			float timeBetween = timeBetweenWavesRange.Get(Random.value);
			for (float time = Time.time; time < timeBetween; time += deltaTime)
			{
				if (Player.instance.polarized.value != 0)
					Player.instance.HandlePolarity (deltaTime);
				Player.instance.TakeDamage (Player.instance.hpRegen.GetValue());
				if (enemyMoneyMultiplier > 0)
					Player.instance.AddMoney (Player.instance.harvesting.GetValue() * deltaTime);
			}
			ShopMenu.Instance.totalCost += ShopMenu.instance.addToTotalCost;
			ShopMenu.instance.multiplyCosts += ShopMenu.instance.addToMultiplyCosts;
			ShopMenu.openedCountThisWave = 0;
			Tartigrade.hitsBlocked = 0;
            waveTimer += waveDuration.value;
			waveDuration.SetValue ((byte) (waveDuration.value + addToWaveDuration));
            multiplyEnemyHp += addToMultiplyEnemyHp;
			if (Duel.instance != null)
				ShopMenu.Instance.Open ();
			else
			{
				SpawnEnemies ();
				GameManager.updatables = GameManager.updatables.Add(spawnEnemiesUpdater);
			}
			enabled = true;
		}

		void SpawnEnemies ()
		{
			while (spawnEnemyEntriesAtTimes.Count == 0)
			{
				difficulty += addToDifficultyPerWave;
				float difficultyRemaining = difficulty;
				spawnEnemyEntriesAtTimes.Clear();
				List<EnemySpawnEntry> remainingEnemySpawnEntries = new List<EnemySpawnEntry>(enemySpawnEntries);
				while (remainingEnemySpawnEntries.Count > 0)
				{
					int randomIndex = Random.Range(0, remainingEnemySpawnEntries.Count);
					EnemySpawnEntry enemySpawnEntry = remainingEnemySpawnEntries[randomIndex];
					float difficulty = enemySpawnEntry.enemyPrefab.difficulty / divideDifficulty / GameManager.instance.divideDifficulty;
					if (difficultyRemaining - difficulty < 0)
						remainingEnemySpawnEntries.RemoveAt(randomIndex);
					else
					{
						spawnEnemyEntriesAtTimes.Add(Random.Range(0, enemiesSpawnDurationPerWave), enemySpawnEntry);
						difficultyRemaining -= difficulty;
					}
				}
			}
		}

		void ShowNextBranchIndicator ()
		{
			Timeline.Point nextBranchPoint = timeline.GetPointAtTime(currentTime + timeUntilBranch);
			bulletsInNextBranch = new Bullet[nextBranchPoint.bulletSnapshots.Length];
			for (int i = 0; i < bulletsInNextBranch.Length; i ++)
			{
				Bullet.Snapshot bulletSnapshot = nextBranchPoint.bulletSnapshots[i];
				bulletSnapshot.bullet = null;
				Bullet bullet = bulletSnapshot.Apply();
				BombBullet bombBullet = bullet as BombBullet;
				if (bombBullet != null)
					bombBullet.explodeOnDisable = false;
				bullet.enabled = false;
				bullet.rigid.simulated = false;
				bullet.spriteRenderer.color = bullet.spriteRenderer.color.DivideAlpha(divideObjectsAlphaInNextBranch);
				bulletsInNextBranch[i] = bullet;
			}
			explosionsInNextBranch = new Explosion[nextBranchPoint.explosionSnapshots.Length];
			for (int i = 0; i < explosionsInNextBranch.Length; i ++)
			{
				Explosion.Snapshot explosionSnapshot = nextBranchPoint.explosionSnapshots[i];
				explosionSnapshot.explosion = null;
				Explosion explosion = explosionSnapshot.Apply();
				explosion.enabled = false;
				explosion.circleCollider.enabled = false;
				explosion.spriteRenderer.color = explosion.spriteRenderer.color.DivideAlpha(divideObjectsAlphaInNextBranch);
				explosionsInNextBranch[i] = explosion;
			}
		}

		void MakeBranch ()
		{
			Timeline.Point branchPoint = timeline.GetPointAtTime(currentTime - branchInterval + timeUntilBranch);
			bulletsInBranch = new Bullet[branchPoint.bulletSnapshots.Length];
			for (int i = 0; i < bulletsInBranch.Length; i ++)
			{
				Bullet.Snapshot bulletSnapshot = branchPoint.bulletSnapshots[i];
				bulletSnapshot.bullet = null;
				bulletsInBranch[i] = bulletSnapshot.Apply();
			}
			explosionsInBranch = new Explosion[branchPoint.explosionSnapshots.Length];
			for (int i = 0; i < explosionsInBranch.Length; i ++)
			{
				Explosion.Snapshot explosionSnapshot = branchPoint.explosionSnapshots[i];
				explosionSnapshot.explosion = null;
				explosionsInBranch[i] = explosionSnapshot.Apply();
			}
		}

		public virtual void End ()
		{
			if (currentTime > BestTimeReached)
			{
				if (rewindDurationRemaining > 0)
					BestTimeReached = MathfExtensions.RoundToInterval(currentTime, rewindDelay, MathfExtensions.RoundingMethod.RoundUpIfNotInteger);
				else
					BestTimeReached = currentTime;
				Achievement.instances = FindObjectsByType<Achievement>(FindObjectsSortMode.None);
				for (int i = 0; i < Achievement.instances.Length; i ++)
				{
					Achievement achievement = Achievement.instances[i];
					if (!achievement.complete && achievement.ShouldBeComplete())
						achievement.Complete ();
				}
			}
			if (Player.damageHealed > MostDamageHealed)
				MostDamageHealed = Player.damageHealed;
			if (NetworkManager.client == null)
			{
				if (!string.IsNullOrEmpty(LocalUserInfo.username))
					NetworkManager.Connect (OnConnectSuccess, null);
			}
			else
				NetworkManager.client.BigDB.Load("PlayerObjects", LocalUserInfo.username, OnLoadDBObjectSuccess);
			currentTime = 0;
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			_SceneManager.instance.RestartScene ();
		}

		static void OnConnectSuccess (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.BigDB.Load("PlayerObjects", LocalUserInfo.username, OnLoadDBObjectSuccess);
		}

		static void OnLoadDBObjectSuccess (DatabaseObject dbObj)
		{
			DatabaseArray dbArray = dbObj.GetArray("times");
			dbArray.Set(Level.instances.IndexOf(Level.instance), Level.instance.BestTimeReached);
			float totalTime = 0;
			for (int i = 0; i < dbArray.Count; i ++)
			{
				if (dbArray.Contains(i))
					totalTime += dbArray.GetFloat(i, 0);
			}
			dbObj.Set("totalTime", totalTime);
			dbArray = dbObj.GetArray("enemiesKilled");
			dbArray.Set(Level.instances.IndexOf(Level.instance), Level.instance.MostEnemiesKilled);
			uint totalEnemiesKilled = 0;
			for (int i = 0; i < dbArray.Count; i ++)
			{
				if (dbArray.Contains(i))
					totalEnemiesKilled += dbArray.GetUInt(i, 0);
			}
			dbObj.Set("totalEnemiesKilled", enemiesKilled);
			dbObj.Set("tasksDone", Achievement.completeCount);
			dbObj.Save();
		}

		public Vector2 GetPositionToTeleportTo (Vector2 fromPosition, float radius)
		{
			if (!type.HasFlag(Type.Teleport))
				return fromPosition;
			else if (fromPosition.x >= trs.position.x + innerSize / 2 - radius)
				return fromPosition + Vector2.left * (innerSize - radius * 2);
			else if (fromPosition.x <= trs.position.x - innerSize / 2 + radius)
				return fromPosition + Vector2.right * (innerSize - radius * 2);
			else if (fromPosition.y >= trs.position.y + innerSize / 2 - radius)
				return fromPosition + Vector2.down * (innerSize - radius * 2);
			else if (fromPosition.y <= trs.position.y - innerSize / 2 + radius)
				return fromPosition + Vector2.up * (innerSize - radius * 2);
			else
				return fromPosition;
		}

		public Vector2 GetSmallestVectorToPoint (Vector2 fromPosition, Vector2 toPosition, float radius)
		{
			if (!type.HasFlag(Type.Teleport))
				return toPosition - fromPosition;
			else
			{
				float x = trs.position.x + innerSize / 2 - radius;
				Vector2 vectorThroughRightWall = GetVectorToPoint(fromPosition, toPosition, x, true, radius);
				x = trs.position.x - innerSize / 2 + radius;
				Vector2 vectorThroughLeftWall = GetVectorToPoint(fromPosition, toPosition, x, true, radius);
				float y = trs.position.y + innerSize / 2 - radius;
				Vector2 vectorThroughUpWall = GetVectorToPoint(fromPosition, toPosition, y, false, radius);
				y = trs.position.y - innerSize / 2 + radius;
				Vector2 vectorThroughDownWall = GetVectorToPoint(fromPosition, toPosition, y, false, radius);
				float vectorThroughRightWallMagnitude = vectorThroughRightWall.magnitude;
				float vectorThroughLeftWallMagnitude = vectorThroughLeftWall.magnitude;
				float vectorThroughUpWallMagnitude = vectorThroughUpWall.magnitude;
				float vectorThroughDownWallMagnitude = vectorThroughDownWall.magnitude;
				Vector2 toToPosition = toPosition - fromPosition;
				float minDistance = Mathf.Min(toToPosition.magnitude, vectorThroughRightWallMagnitude, vectorThroughLeftWallMagnitude, vectorThroughUpWallMagnitude, vectorThroughDownWallMagnitude);
				if (minDistance == vectorThroughRightWallMagnitude)
					return vectorThroughRightWall;
				if (minDistance == vectorThroughLeftWallMagnitude)
					return vectorThroughLeftWall;
				if (minDistance == vectorThroughUpWallMagnitude)
					return vectorThroughUpWall;
				if (minDistance == vectorThroughDownWallMagnitude)
					return vectorThroughDownWall;
				else
					return toToPosition;
			}
		}

		Vector2 GetVectorToPoint (Vector2 fromPosition, Vector2 toPosition, float hitPointComponent, bool isHitPointComponentX, float radius)
		{
			float fromPositionComponent;
			float toPositionComponent;
			float fromPositionOtherComponent;
			float toPositionOtherComponent;
			float oppositeSideOfHitPointComponent;
			if (isHitPointComponentX)
			{
				fromPositionComponent = fromPosition.x;
				toPositionComponent = toPosition.x;
				fromPositionOtherComponent = fromPosition.y;
				toPositionOtherComponent = toPosition.y;
				if (hitPointComponent == trs.position.x - innerSize / 2 + radius)
					oppositeSideOfHitPointComponent = trs.position.x + innerSize / 2 - radius;
				else
					oppositeSideOfHitPointComponent = trs.position.x - innerSize / 2 + radius;
			}
			else
			{
				fromPositionComponent = fromPosition.y;
				toPositionComponent = toPosition.y;
				fromPositionOtherComponent = fromPosition.x;
				toPositionOtherComponent = toPosition.x;
				if (hitPointComponent == trs.position.y - innerSize / 2 + radius)
					oppositeSideOfHitPointComponent = trs.position.y + innerSize / 2 - radius;
				else
					oppositeSideOfHitPointComponent = trs.position.y - innerSize / 2 + radius;
			}
			float toToPositionComponent = Mathf.Abs(hitPointComponent - fromPositionComponent) + Mathf.Abs(toPositionComponent - oppositeSideOfHitPointComponent);
			toToPositionComponent *= Mathf.Sign(hitPointComponent - fromPositionComponent);
			float toToPositionOtherComponent = toPositionOtherComponent - fromPositionOtherComponent;
			float hypotenuseLength = MathfExtensions.GetHypotenuse(toToPositionComponent, toToPositionOtherComponent);
			float angleToToPosition;
			if (isHitPointComponentX)
				angleToToPosition = Mathf.Atan2(toToPositionOtherComponent, toToPositionComponent) * Mathf.Rad2Deg;
			else
				angleToToPosition = Mathf.Atan2(toToPositionComponent, toToPositionOtherComponent) * Mathf.Rad2Deg;
			return VectorExtensions.FromFacingAngle(angleToToPosition) * hypotenuseLength;
		}

		void EnableEnemy (Enemy enemy)
		{
			enemy.collider.enabled = true;
			Material material = new Material(enemy.renderer.sharedMaterial);
			material.SetColor("_tint", material.GetColor("_tint").SetAlpha(1));
			enemy.renderer.sharedMaterial = material;
			enemy.animator.enabled = true;
			enemy.enabled = true;
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			Transform trs = other.GetComponent<Transform>().parent;
			float radius;
			Entity entity = trs.GetComponent<Entity>();
			if (entity != null)
				radius = entity.radius;
			else
			{
				Bullet bullet = trs.GetComponent<Bullet>();
				radius = bullet.radius;
			}
			trs.position = GetPositionToTeleportTo(trs.position, radius);
		}

		void OnTriggerStay2D (Collider2D other)
		{
			OnTriggerEnter2D (other);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(spawnEnemiesUpdater);
		}

		public class SpawnEnemiesUpdater : IUpdatable
		{
			public void DoUpdate ()
			{
				if (instance.waveDuration.value - waveTimer >= spawnEnemyEntriesAtTimes.Keys[0])
				{
					spawnEnemyEntriesAtTimes.Values[0].SpawnEnemy ();
					spawnEnemyEntriesAtTimes.RemoveAt(0);
					if (spawnEnemyEntriesAtTimes.Count == 0)
						GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}

		[Serializable]
		public struct Wave
		{
			public EnemySpawnEntry[] enemySpawnEntries;

			public Wave (EnemySpawnEntry[] enemySpawnEntries)
			{
				this.enemySpawnEntries = enemySpawnEntries;
			}
		}

		[Serializable]
		public struct EnemySpawnEntry
		{
			public Enemy enemyPrefab;

			public Enemy SpawnEnemy ()
			{
				Vector3 spawnPosition = instance.spawnZone.GetRandomPoint();
				Enemy enemy = ObjectPool.instance.SpawnComponent<Enemy>(enemyPrefab, spawnPosition);
				enemy.maxHp *= instance.multiplyEnemyHp;
				enemy.hp = enemy.maxHp;
				EventManager.AddEvent ((object obj) => { instance.EnableEnemy (enemy); }, instance.enableEnemyDelay);
				return enemy;
			}
		}

		[Flags]
		public enum Type
		{
			Fire = 2,
			Wind = 4,
			Teleport = 8,
			TimeRewind = 16,
			Branch = 32,
			Playback = 64
		}
	}
}