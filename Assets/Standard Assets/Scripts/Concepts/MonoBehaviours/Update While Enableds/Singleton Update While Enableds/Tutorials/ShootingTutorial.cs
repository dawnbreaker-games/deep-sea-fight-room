using Extensions;

namespace FightRoom
{
	public class ShootingTutorial : Tutorial
	{
		public Enemy[] enemies = new Enemy[0];

		public override void OnEnable ()
		{
			base.OnEnable ();
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				enemy.onDeath = OnEnemyDeath;
			}
		}

		void OnEnemyDeath ()
		{
			enemies = enemies.RemoveAt(0);
			if (enemies.Length == 0)
				Finish ();
			else
				enemies[0].gameObject.SetActive(true);
		}
	}
}