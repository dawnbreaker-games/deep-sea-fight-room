using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class MovementTutorial : Tutorial
	{
		public Transform[] moveToPoints = new Transform[0];
		public GameObject wKeyIndicator;
		public GameObject aKeyIndicator;
		public GameObject sKeyIndicator;
		public GameObject dKeyIndicator;
		[HideInInspector]
		public Transform currentPoint;
		int currentPointIndex;
		MoveToPointsUpdater moveToPointsUpdater;

		public void BeginMoveToPoints ()
		{
			instance = this;
			for (int i = 1; i < moveToPoints.Length; i ++)
			{
				Transform point = moveToPoints[i];
				point.gameObject.SetActive(false);
			}
			currentPointIndex = 0;
			currentPoint = moveToPoints[0];
			currentPoint.gameObject.SetActive(true);
			moveToPointsUpdater = new MoveToPointsUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(moveToPointsUpdater);
		}

		public void OnMovedToPoint ()
		{
			currentPointIndex ++;
			currentPoint.gameObject.SetActive(false);
			if (currentPointIndex >= moveToPoints.Length)
				Finish ();
			else
			{
				currentPoint = moveToPoints[currentPointIndex];
				currentPoint.gameObject.SetActive(true);
			}
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(moveToPointsUpdater);
		}

		public class MoveToPointsUpdater : IUpdatable
		{
			MovementTutorial movementTutorial;

			public MoveToPointsUpdater (MovementTutorial movementTutorial)
			{
				this.movementTutorial = movementTutorial;
			}

			public void DoUpdate ()
			{
				if (movementTutorial == null)
					return;
				Vector2 toPoint = movementTutorial.currentPoint.position - Player.instance.trs.position;
				float toPointFacingAngle = toPoint.GetFacingAngle();
				toPointFacingAngle = MathfExtensions.RoundToInterval(toPointFacingAngle, 45);
				Vector2 correctMoveDirection = VectorExtensions.FromFacingAngle(toPointFacingAngle);
				movementTutorial.dKeyIndicator.SetActive(correctMoveDirection.x > 0.1f);
				movementTutorial.aKeyIndicator.SetActive(correctMoveDirection.x < -0.1f);
				movementTutorial.wKeyIndicator.SetActive(correctMoveDirection.y > 0.1f);
				movementTutorial.sKeyIndicator.SetActive(correctMoveDirection.y < -0.1f);
			}
		}
	}
}