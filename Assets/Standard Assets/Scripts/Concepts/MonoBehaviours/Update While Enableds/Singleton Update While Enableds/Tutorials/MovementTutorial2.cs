using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class MovementTutorial2 : Tutorial
	{
		public Transform[] moveToPoints = new Transform[0];
		public GameObject wKeyIndicator;
		public GameObject aKeyIndicator;
		public GameObject sKeyIndicator;
		public GameObject dKeyIndicator;
		MoveToPointsUpdater moveToPointsUpdater;
		int moveToPointsRemaining;

		public void BeginMoveToPoints ()
		{
			for (int i = 0; i < moveToPoints.Length; i ++)
			{
				Transform point = moveToPoints[i];
				point.gameObject.SetActive(true);
			}
			moveToPointsUpdater = new MoveToPointsUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(moveToPointsUpdater);
			moveToPointsRemaining = moveToPoints.Length;
		}

		public void OnMovedToPoint ()
		{
			Transform point = Player.instance.trs.GetClosestTransform_2D(moveToPoints);
			moveToPoints = moveToPoints.Remove(point);
			moveToPointsRemaining --;
			if (moveToPointsRemaining == 0)
				Finish ();
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(moveToPointsUpdater);
		}

		public class MoveToPointsUpdater : IUpdatable
		{
			MovementTutorial2 movementTutorial2;

			public MoveToPointsUpdater (MovementTutorial2 movementTutorial2)
			{
				this.movementTutorial2 = movementTutorial2;
			}

			public void DoUpdate ()
			{
				if (movementTutorial2.moveToPoints.Length == 0)
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					return;
				}
				Transform point = Player.instance.trs.GetClosestTransform_2D(movementTutorial2.moveToPoints);
				Vector2 toPoint = point.position - Player.instance.trs.position;
				float toPointFacingAngle = toPoint.GetFacingAngle();
				toPointFacingAngle = MathfExtensions.RoundToInterval(toPointFacingAngle, 45);
				Vector2 correctMoveDirection = VectorExtensions.FromFacingAngle(toPointFacingAngle);
				movementTutorial2.dKeyIndicator.SetActive(correctMoveDirection.x > 0.1f);
				movementTutorial2.aKeyIndicator.SetActive(correctMoveDirection.x < -0.1f);
				movementTutorial2.wKeyIndicator.SetActive(correctMoveDirection.y > 0.1f);
				movementTutorial2.sKeyIndicator.SetActive(correctMoveDirection.y < -0.1f);
			}
		}
	}
}