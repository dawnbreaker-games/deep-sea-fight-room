using UnityEngine;

namespace FightRoom
{
	public class AbilityTutorial : Tutorial
	{
		bool bulletWasHit;

		public void Retry ()
		{
			bulletWasHit = false;
		}

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			for (int i = 0; i < Bullet.instances.Count; i ++)
			{
				Bullet bullet = Bullet.instances[i];
				if (bullet.gameObject.layer == LayerMask.NameToLayer("Enemy Bullet"))
				{
					if (bullet.onDisable == null)
						bullet.onDisable = OnDisableBullet;
					else if (!bulletWasHit && Player.instance.trs.position.y > bullet.trs.position.y)
					{
						Finish ();
						return;
					}
				}
			}
		}

		void OnDisableBullet ()
		{
			if (Player.instance.invulnerable)
				return;
			bulletWasHit = true;
			gameObject.SetActive(false);
			gameObject.SetActive(true);
		}
	}
}