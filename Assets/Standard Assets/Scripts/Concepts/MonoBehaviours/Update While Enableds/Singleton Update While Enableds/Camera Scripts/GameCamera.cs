namespace FightRoom
{
	public class GameCamera : CameraScript
	{
		public static GameCamera instance;
		public static GameCamera Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<GameCamera>();
				return instance;
			}
			set
			{
				instance = value;
			}
		}
	}
}