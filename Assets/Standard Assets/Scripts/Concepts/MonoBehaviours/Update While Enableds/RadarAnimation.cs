using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class RadarAnimation : UpdateWhileEnabled
	{
		public Transform trs;
		public SpriteMask spriteMask;
		public float duration;
		public float maxSize;
		public float delay;
		public float maxAlphaCutoff;
		float delayRemaining;
		float initAlphaCutoff;

		public override void OnEnable ()
		{
			initAlphaCutoff = spriteMask.alphaCutoff;
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			delayRemaining -= Time.deltaTime;
			if (delayRemaining <= 0)
			{
				trs.localScale += Vector3.one * (maxSize / duration) * Time.deltaTime;
				spriteMask.alphaCutoff = Mathf.Lerp(initAlphaCutoff, maxAlphaCutoff, trs.localScale.x / maxSize);
			}
			if (trs.localScale.x >= maxSize)
			{
				trs.localScale = Vector3.zero;
				delayRemaining = delay;
			}
		}
	}
}