using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FightRoom
{
	[ExecuteInEditMode]
	public class RippleMesh : SingletonUpdateWhileEnabled<RippleMesh>
	{
		public float springiness;
		public float dampen;
		public MeshObject meshObject;
		public SerializableDictionary<Vector3Int, Point> pointsDict = new SerializableDictionary<Vector3Int, Point>();
		public bool serializeData;
		public ClampPoint[] clampPoints = new ClampPoint[0];
		public float pointSeparation;
		public float checkInterval;
		public float pointMoveLimit;
		bool previousSerializeData;
		Mesh mesh;

		public override void OnEnable ()
		{
			mesh = meshObject.meshFilter.sharedMesh;
			mesh.MarkDynamic();
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				base.OnEnable ();
				if (serializeData)
				{
					OnEnabled ();
					return;
				}
#if UNITY_EDITOR
			}
#endif
			List<Vector3> _vertices = new List<Vector3>();
			mesh.GetVertices(_vertices);
			Vector3[] vertices = _vertices.ToArray();
			pointsDict.Clear();
			meshObject.trs.TransformPoints(vertices);
			for (int i = 0; i < vertices.Length; i ++)
			{
				Vector3 vertex = vertices[i];
				Point point = new Point(_vertices[i]);
				Vector3Int position = (vertex * pointSeparation).ToVec3Int();
				for (int i2 = 0; i2 < clampPoints.Length; i2 ++)
				{
					ClampPoint clampPoint = clampPoints[i2];
					if (clampPoint.defaultPosition == point.defaultPosition)
					{
						point = clampPoint;
						break;
					}
				}
				if (!pointsDict.ContainsKey(position))
					pointsDict.Add(position, point);
			}
#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
#endif
			OnEnabled ();
		}

		void OnEnabled ()
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
#endif
				mesh = meshObject.meshFilter.mesh;
		}

#if UNITY_EDITOR
		public virtual void OnValidate ()
		{
			if (Application.isPlaying)
				return;
			if (serializeData && !previousSerializeData)
				OnEnable ();
			else if (!serializeData && previousSerializeData)
				pointsDict.Clear();
			previousSerializeData = serializeData;
			EditorUtility.SetDirty(this);
		}
#endif

		public override void DoUpdate ()
		{
			if (GameManager.paused || GameManager.framesSinceLevelLoaded <= GameManager.LAG_FRAMES_AFTER_SCENE_LOAD)
				return;
			Vector3[] vertices = new Vector3[pointsDict.Count];
			int[] triangles = mesh.triangles;
			for (int i = 0; i < pointsDict.Count; i ++)
			{
				Point point = pointsDict.values[i];
				Vector3 velocity = point.velocity;
				Vector3 velocityDirection = velocity.normalized;
				Vector3 toDefaultPosition = point.defaultPosition - point.position;
				velocity += toDefaultPosition * springiness;
				velocity /= 1f + dampen * Time.unscaledDeltaTime;
				ClampPoint clampPoint = point as ClampPoint;
				if (clampPoint != null)
					velocity /= 1f + clampPoint.clampAmount;
				point.velocity = velocity;
				point.position = point.defaultPosition + Vector3.ClampMagnitude(-toDefaultPosition + velocity * Time.unscaledDeltaTime, pointMoveLimit);
				vertices[i] = point.position;
			}
			List<Vector2> uvs = new List<Vector2>();
			mesh.GetUVs(0, uvs);
			mesh.Clear();
			mesh.vertices = vertices;
			mesh.uv = uvs.ToArray();
			mesh.triangles = triangles;
			mesh.RecalculateNormals();
		}

		public void Ripple (Vector3 center, float radius, Vector3 velocity, bool useVelocity = true, float magnitude = 0)
		{
			List<Vector3Int> rippledPositions = new List<Vector3Int>();
			for (float x = center.x - radius; x <= center.x + radius; x += checkInterval)
			{
				for (float y = center.y - radius; y <= center.y + radius; y += checkInterval)
				{
					for (float z = center.z - radius; z <= center.z + radius; z += checkInterval)
					{
						Vector3 position = new Vector3(x, y, z);
						Point point = null;
						Vector3Int newPosition = (position * pointSeparation).ToVec3Int();
						if (!rippledPositions.Contains(newPosition) && pointsDict.TryGetValue(newPosition, out point))
						{
							rippledPositions.Add(newPosition);
							if (useVelocity)
								point.velocity = velocity;
							else
								point.velocity = (position - center).normalized * magnitude;
						}
					}
				}
			}
		}

		[Serializable]
		public class Point
		{
			public Vector3 position;
			public Vector3 velocity;
			public Vector3 defaultPosition;

			public Point (Vector3 defaultPosition)
			{
				position = defaultPosition;
				this.defaultPosition = defaultPosition;
			}
		}

		[Serializable]
		public class ClampPoint : Point
		{
			public float clampAmount;

			public ClampPoint (Vector2 defaultPosition, float clampAmount) : base (defaultPosition)
			{
				this.clampAmount = clampAmount;
			}
		}
	}
}