using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace FightRoom
{
	public class AttractOrRepulse : UpdateWhileEnabled
	{
		public Transform trs;
		public FloatRange attractAmountRange;
		public AnimationCurve multiplyAttractAmountCurve;
		public FloatRange multiplyTimeRange;
		public AttractEntry[] attractEntries = new AttractEntry[0];
#if UNITY_EDITOR
		public bool addNewAttractEntries;
		public bool updateAttractEntries;

		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (updateAttractEntries)
			{
				updateAttractEntries = false;
				UpdateAttractEntries ();
			}
			else if (addNewAttractEntries)
			{
				addNewAttractEntries = false;
				List<AttractOrRepulse> instances = new List<AttractOrRepulse>(FindObjectsByType<AttractOrRepulse>(FindObjectsSortMode.None));
				for (int i = 0; i < this.attractEntries.Length; i ++)
				{
					AttractEntry attractEntry = this.attractEntries[i];
					instances.Remove(attractEntry.attractTo.GetComponent<AttractOrRepulse>());
				}
				instances.Remove(this);
				List<AttractEntry> attractEntries = new List<AttractEntry>(this.attractEntries);
				for (int i = 0; i < instances.Count; i ++)
				{
					AttractOrRepulse instance = instances[i];
					attractEntries.Add(new AttractEntry(instance.trs, attractAmountRange.Get(Random.value), multiplyTimeRange.Get(Random.value), Random.value * multiplyAttractAmountCurve.keys[multiplyAttractAmountCurve.keys.Length - 1].time));
				}
				this.attractEntries = attractEntries.ToArray();
			}
		}
#endif

		public override void OnEnable ()
		{
			UpdateAttractEntries ();
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			for (int i = 0; i < attractEntries.Length; i ++)
			{
				AttractEntry attractEntry = attractEntries[i];
				trs.position += (Vector3) ((Vector2) (attractEntry.attractTo.position - trs.position)).normalized * attractEntry.amount * multiplyAttractAmountCurve.Evaluate(GameManager.UnscaledTimeSinceLevelLoad * attractEntry.multiplyTime + attractEntry.offsetTime) * Time.unscaledDeltaTime;
			}
		}

		void UpdateAttractEntries ()
		{
			List<AttractOrRepulse> instances = new List<AttractOrRepulse>(FindObjectsByType<AttractOrRepulse>(FindObjectsSortMode.None));
			instances.Remove(this);
			attractEntries = new AttractEntry[instances.Count];
			for (int i = 0; i < instances.Count; i ++)
			{
				AttractOrRepulse instance = instances[i];
				attractEntries[i] = new AttractEntry(instance.trs, attractAmountRange.Get(Random.value), multiplyTimeRange.Get(Random.value), Random.value * multiplyAttractAmountCurve.keys[multiplyAttractAmountCurve.keys.Length - 1].time);
			}
		}

		[Serializable]
		public class AttractEntry
		{
			public Transform attractTo;
			public float amount;
			public float multiplyTime;
			public float offsetTime;

			public AttractEntry (Transform attractTo, float amount, float multiplyTime, float offsetTime)
			{
				this.attractTo = attractTo;
				this.amount = amount;
				this.multiplyTime = multiplyTime;
				this.offsetTime = offsetTime;
			}
		}
	}
}