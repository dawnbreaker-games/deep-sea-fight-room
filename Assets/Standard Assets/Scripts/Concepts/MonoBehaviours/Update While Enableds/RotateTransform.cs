using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	public class RotateTransform : UpdateWhileEnabled
	{
		public Transform trs;
		public float rate;

		public override void DoUpdate ()
		{
			if (GameManager.paused || _SceneManager.isLoading)
				return;
			trs.eulerAngles += Vector3.forward * rate * Time.deltaTime;
		}
	}
}