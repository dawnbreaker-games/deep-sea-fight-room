using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace FightRoom
{
	public class Enemy : Entity
	{
		public FloatRange targetRangeFromPlayer;
		public Transform bulletSpawnersParent;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public BulletPatternEntryWithLoopedBulletPrefabs[] bulletPatternEntriesWithLoopedBulletPrefabs = new BulletPatternEntryWithLoopedBulletPrefabs[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public float dodgeBulletDistance;
		public float safeDistance;
		public float difficulty;
		public float attackRadius;
		public LayerMask whatBlocksAttacks;
		public float moneyPerDifficulty;
		public float dropRadius;
		public Action onDeath;
		public static List<Enemy> instances = new List<Enemy>();
		PauseShootingUpdater pauseShootingUpdater;
		Vector2 velocity;
		Vector2 toPlayer;
		float toPlayerDistanceSqr;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instances.Add(this);
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			instances.Remove(this);
		}

		public override void Death ()
		{
			base.Death ();
			if (onDeath != null)
				onDeath ();
			GameManager.updatables = GameManager.updatables.Remove(pauseShootingUpdater);
			if (_SceneManager.CurrentScene.name != "Game")
				return;
			uint pickupCount = ReliableMagnet.ownedCount * ReliableMagnet.AMOUNT_TO_PICKUP;
			uint moneyAmount = (uint) Player.instance.enemyMoney.Apply(difficulty * moneyPerDifficulty);
			for (uint i = 0; i < moneyAmount; i ++)
			{
				Money moneyPrefab = GameManager.instance.moneyPrefab;
				if (i * moneyPrefab.amount < pickupCount || Random.value < UnreliableMagnet.PICKUP_CHANCE * UnreliableMagnet.ownedCount)
					Player.instance.AddMoney (moneyPrefab.amount);
				else
					Instantiate(GameManager.instance.moneyPrefab, (Vector2) trs.position + Random.insideUnitCircle * dropRadius, Quaternion.Euler(Vector3.forward * Random.value * 360));
			}
			if (moneyAmount > 0)
				GameManager.instance.StartTutorialIfShould (GameManager.instance.tutorialOnEnemyDroppedMoney);
			Level.enemiesKilled ++;
			Level.instance.enemiesKilledText.text = "Enemies destroyed: " + Level.enemiesKilled;
			if (Level.enemiesKilled > Level.instance.MostEnemiesKilled)
				Level.instance.MostEnemiesKilled = Level.enemiesKilled;
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (bulletSpawnersParent != null)
				Destroy(bulletSpawnersParent.gameObject);
		}

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				attackRadius = 0;
				for (int i = 0; i < bulletPatternEntries.Length; i ++)
				{
					BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
					attackRadius = Mathf.Max(bulletPatternEntry.bulletPrefab.radius, attackRadius);
				}
				base.Awake ();
				return;
			}
#endif
			base.Awake ();
			hp *= Level.instance.multiplyEnemyHp;
			bulletSpawnersParent.SetParent(null);
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
			for (int i = 0; i < bulletPatternEntriesWithLoopedBulletPrefabs.Length; i ++)
			{
                BulletPatternEntryWithLoopedBulletPrefabs bulletPatternEntryWithLoopedBulletPrefabs = bulletPatternEntriesWithLoopedBulletPrefabs[i];
				bulletPatternEntriesDict.Add(bulletPatternEntryWithLoopedBulletPrefabs.name, bulletPatternEntryWithLoopedBulletPrefabs);
			}
		}

		public void ShootBulletPatternEntryIfShould (string name)
		{
			if (this == null)
				return;
			BulletPatternEntry bulletPatternEntry = bulletPatternEntriesDict[name];
			Bullet bulletPrefab = bulletPatternEntry.bulletPrefab;
			BombBullet bombBulletPrefab = bulletPrefab as BombBullet;
			if (bombBulletPrefab != null)
			{
				Transform bulletSpawner = bulletPatternEntry.spawner;
				RaycastHit2D hit = Physics2D.CircleCast(bulletSpawner.position, bulletPrefab.radius, bulletSpawner.up, bulletPrefab.range, ((LayerMask) Physics2D.GetLayerCollisionMask(bulletPrefab.gameObject.layer)).RemoveFromMask("Player"));
				if (hit.collider != null && hit.distance <= bombBulletPrefab.explosionPrefab.radius + radius)
				{
					animator.speed = 0;
					pauseShootingUpdater = new PauseShootingUpdater(this, bulletPatternEntry);
					GameManager.updatables = GameManager.updatables.Add(pauseShootingUpdater);
					return;
				}
			}
			ShootBulletPatternEntry (bulletPatternEntry);
		}

		public void ShootBulletPatternEntry (string name)
		{
			ShootBulletPatternEntry (bulletPatternEntriesDict[name]);
		}

		void ShootBulletPatternEntry (BulletPatternEntry bulletPatternEntry)
		{
			if (this == null)
				return;
			Bullet[] bullets = bulletPatternEntry.Shoot();
			for (int i = 0; i < bullets.Length; i ++)
			{
				Bullet bullet = bullets[i];
				bullet.shooter = this;
			}
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleAttacking ();
			base.DoUpdate ();
		}

		public override void HandleRotating ()
		{
			trs.up = Level.instance.GetSmallestVectorToPoint(trs.position, Player.instance.trs.position, radius);
		}

		public override void HandleMoving ()
		{
			toPlayer = Level.instance.GetSmallestVectorToPoint(trs.position, Player.instance.trs.position, radius + Player.instance.radius);
			toPlayerDistanceSqr = toPlayer.sqrMagnitude;
			velocity = Vector2.zero;
			if (dodgeBulletDistance > 0)
			{
				float closestBulletDistanceSqr = dodgeBulletDistance * dodgeBulletDistance;
				Bullet closestBullet = null;
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					if (bullet.gameObject.layer == LayerMask.NameToLayer("Player Bullet"))
					{
						float distanceSqr = Level.instance.GetSmallestVectorToPoint(trs.position, bullet.trs.position, radius + bullet.radius).sqrMagnitude;
						if (distanceSqr < closestBulletDistanceSqr)
						{
							closestBulletDistanceSqr = distanceSqr;
							closestBullet = bullet;
						}
					}
				}
				if (closestBullet != null)
				{
					velocity = Level.instance.GetSmallestVectorToPoint(trs.position, closestBullet.trs.position, radius + closestBullet.radius).Rotate90().normalized * moveSpeed;
					Vector2 toClosestBullet = Level.instance.GetSmallestVectorToPoint(trs.position, closestBullet.trs.position, radius + closestBullet.radius);
					if ((((Vector2) trs.position + velocity * Time.fixedDeltaTime) - toClosestBullet).sqrMagnitude < (((Vector2) trs.position - velocity * Time.fixedDeltaTime) - toClosestBullet).sqrMagnitude)
						velocity *= -1;
					if (toPlayerDistanceSqr < targetRangeFromPlayer.min * targetRangeFromPlayer.min)
						velocity -= toPlayer.normalized * moveSpeed;
				}
				else
					HandleChaseOrEscapePlayer (ref velocity);
			}
			else
				HandleChaseOrEscapePlayer (ref velocity);
			bool willCrashIntoHazard = true;
			for (int i = 0; i < 4; i ++)
			{
				LayerMask whatIDodge;
				if (dodgeBulletDistance == 0)
					whatIDodge = LayerMask.GetMask("Hazard");
				else
					whatIDodge = LayerMask.GetMask("Player Bullet", "Hazard");
				if (Physics2D.CircleCast(trs.position, radius, velocity, safeDistance, whatIDodge).collider == null)
				{
					willCrashIntoHazard = false;
					break;
				}
				velocity = velocity.Rotate90();
			}
			if (willCrashIntoHazard)
				velocity = Vector2.zero;
			if (Level.instance.type.HasFlag(Level.Type.Wind))
				velocity += ((Vector2) (trs.position - Level.instance.trs.position)).Rotate270().normalized * Level.instance.windSpeed;
			rigid.linearVelocity = velocity;
		}

		void HandleChaseOrEscapePlayer (ref Vector2 velocity)
		{
			if (toPlayerDistanceSqr > targetRangeFromPlayer.max * targetRangeFromPlayer.max)
				velocity = toPlayer.normalized * moveSpeed;
			else if (toPlayerDistanceSqr < targetRangeFromPlayer.min * targetRangeFromPlayer.min)
				velocity = -toPlayer.normalized * moveSpeed;
		}

		public virtual void HandleAttacking ()
		{
			bulletSpawnersParent.position = trs.position;
		}

		class PauseShootingUpdater : IUpdatable
		{
			Enemy enemy;
			BulletPatternEntry bulletPatternEntry;

			public PauseShootingUpdater (Enemy enemy, BulletPatternEntry bulletPatternEntry)
			{
				this.enemy = enemy;
				this.bulletPatternEntry = bulletPatternEntry;
			}

			public void DoUpdate ()
			{
				if (enemy == null)
					return;
				BombBullet bombBulletPrefab = (BombBullet) bulletPatternEntry.bulletPrefab;
				Transform bulletSpawner = bulletPatternEntry.spawner;
				RaycastHit2D hit = Physics2D.CircleCast(bulletSpawner.position, bombBulletPrefab.radius, bulletSpawner.up, bombBulletPrefab.range, ((LayerMask) Physics2D.GetLayerCollisionMask(bombBulletPrefab.gameObject.layer)).RemoveFromMask("Player"));
				if (hit.collider != null && hit.distance <= bombBulletPrefab.explosionPrefab.radius + enemy.radius)
					return;
				enemy.animator.speed = enemy.animatorSpeed;
				enemy.ShootBulletPatternEntry (bulletPatternEntry);
				GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}
	}
}