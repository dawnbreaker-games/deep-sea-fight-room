using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class Boss : Enemy
	{
		public AnimationCurve animatorSpeedPerDamage;
		public static Boss instance;
		public static Boss Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Boss>();
				return instance;
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			animator.speed = animatorSpeedPerDamage.Evaluate(0) * GameManager.instance.divideDifficulty;
		}

		public override void TakeDamage (float amount, Entity attacker)
		{
			float previousHp = hp;
			base.TakeDamage (amount);
			BossLevel bossLevel = (BossLevel) Level.instance;
			bossLevel.totalDamage += previousHp - hp;
			bossLevel.currentTimeText.text = "Damage: " + string.Format("{0:0.#}", bossLevel.totalDamage);
			if (bossLevel.totalDamage >= bossLevel.totalMaxHp)
				bossLevel.End ();
			else
			{
				GameManager.instance.bossHealthBarTrs.localScale = GameManager.instance.bossHealthBarTrs.localScale.SetX(hp / maxHp);
				animator.speed = animatorSpeedPerDamage.Evaluate(bossLevel.totalDamage) * GameManager.instance.divideDifficulty;
			}
		}
	}
}