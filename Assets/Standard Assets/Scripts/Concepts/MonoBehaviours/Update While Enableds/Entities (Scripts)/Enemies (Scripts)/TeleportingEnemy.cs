using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace FightRoom
{
	public class TeleportingEnemy : Enemy
	{
		public FloatRange teleportDistanceRange;
		public FloatRange teleportDelayRange;
		float teleportDelay;
		const uint TELEPORT_DISTANCE_SAMPLES = 24;

		public override void OnEnable ()
		{
			base.OnEnable ();
			teleportDelay = teleportDelayRange.Get(Random.value);
		}

		public virtual void HandleMoving ()
		{
			base.HandleMoving ();
			HandleTeleporting ();
		}

		void HandleTeleporting ()
		{
			teleportDelay -= Time.deltaTime;
			if (teleportDelay < 0)
			{
				teleportDelay = teleportDelayRange.Get(Random.value);
				Vector2 destination;
				float teleportDistanceNormalized = 1;
				float teleportDistance = teleportDistanceRange.Get(1);
				for (uint i = 0; i < TELEPORT_DISTANCE_SAMPLES; i ++)
				{
					destination = (Vector2) trs.position + rigid.linearVelocity.normalized * teleportDistance;
					if (Physics2D.OverlapCircle(destination, radius, Physics2D.GetLayerCollisionMask(gameObject.layer)) == null)
					{
						destination = (Vector2) trs.position + rigid.linearVelocity.normalized * teleportDistance;
						Teleport (destination);
						return;
					}
					teleportDistanceNormalized -= 1f / (TELEPORT_DISTANCE_SAMPLES - 1);
					teleportDistance = teleportDistanceRange.Get(teleportDistanceNormalized);
				}
			}
		}

		void Teleport (Vector2 destination)
		{
			trs.position = destination;
			Physics2D.SyncTransforms();
		}
	}
}