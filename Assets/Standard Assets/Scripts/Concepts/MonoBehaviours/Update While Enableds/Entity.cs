using Extensions;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FightRoom
{
	[ExecuteInEditMode]
	public class Entity : UpdateWhileEnabled, IDestructable, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		[HideInInspector]
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public float maxHp;
		public float MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public Rigidbody2D rigid;
		public Renderer renderer;
		public Collider2D collider;
		public float moveSpeed;
		public float radius;
		public Transform bloodTrsPrefab;
		public Animator animator;
		public float animatorSpeed;
		// public AnimationEntry[] animationEntries = new AnimationEntry[0];
		// public Dictionary<string, AnimationEntry> animationEntriesDict = new Dictionary<string, AnimationEntry>();
		// public AudioClip[] deathAudioClips = new AudioClip[0];
		protected bool dead;

		public virtual void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (collider == null)
					collider = GetComponentInChildren<Collider2D>();
				radius = collider.bounds.extents.magnitude;
				EditorUtility.SetDirty(this);
				return;
			}
#endif
			hp = maxHp;
			animator.speed = animatorSpeed;
			// for (int i = 0; i < animationEntries.Length; i ++)
			// {
			// 	AnimationEntry animationEntry = animationEntries[i];
			// 	animationEntriesDict.Add(animationEntry.animatorStateName, animationEntry);
			// }
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleRotating ();
			HandleMoving ();
		}

		public virtual void HandleRotating ()
		{
		}
		
		public virtual void HandleMoving ()
		{
		}

		public virtual void TakeDamage (float amount, Entity attacker = null)
		{
			if (dead || maxHp < 0 || amount == 0)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public virtual void Death ()
		{
			Destroy(gameObject);
			// PlayAnimationEntry ("Death");
			// AudioManager.instance.MakeSoundEffect (deathAudioClips[Random.Range(0, deathAudioClips.Length)], trs.position);
		}

		// public void PlayAnimationEntry (string name)
		// {
		// 	animationEntriesDict[name].Play ();
		// }

		public static Entity[] GetInstances ()
		{
			Entity[] output = new Entity[Enemy.instances.Count + 1];
			output[0] = Player.instance;
			for (int i = 0; i < Enemy.instances.Count; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				output[i + 1] = enemy;
			}
			return output;
		}

		public struct Snapshot
		{
			public int prefabIndex;
			public Entity entity;
			public Vector2 position;
			public float rotation;
			public AnimationEntry animationEntry;
			public float normalizedAnimationEntryTime;

			public Snapshot (int prefabIndex, Entity entity, Vector2 position, float rotation, AnimationEntry animationEntry, float normalizedAnimationEntryTime)
			{
				this.prefabIndex = prefabIndex;
				this.entity = entity;
				this.position = position;
				this.rotation = rotation;
				this.animationEntry = animationEntry;
				this.normalizedAnimationEntryTime = normalizedAnimationEntryTime;
			}

			public Snapshot (Entity entity) : this (entity.prefabIndex, entity, entity.trs.position, entity.trs.eulerAngles.z, new AnimationEntry(), Mathf.Infinity)
			{
				normalizedAnimationEntryTime = entity.animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
			}

			public Entity Apply ()
			{
				if (entity == null)
					entity = ObjectPool.instance.SpawnComponent<Entity>(prefabIndex, position, Quaternion.Euler(Vector3.forward * rotation));
				else
				{
					entity.trs.position = position;
					entity.trs.eulerAngles = Vector3.forward * rotation;
				}
				return entity;
			}
		}
	}
}