using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class DuelMenu : SingletonMonoBehaviour<DuelMenu>
	{
		public Toggle rankedToggle;
		public TMP_InputField duelNameInputField;
		public static bool isPrivate;

		public override void Awake ()
		{
			base.Awake ();
			rankedToggle.isOn = SaveAndLoadManager.saveData.rankedDuels;
			duelNameInputField.text = SaveAndLoadManager.saveData.privateDuelName;
		}

		public void SetRanked (bool ranked)
		{
			SaveAndLoadManager.saveData.rankedDuels = ranked;
		}

		string GetInputFieldText (TMP_InputField inputField)
		{
			string output = inputField.text;
			if (string.IsNullOrEmpty(output))
				output = ((TMP_Text) inputField.placeholder).text;
			return output;
		}
	}
}