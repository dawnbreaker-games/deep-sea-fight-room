﻿using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace FightRoom
{
	public class ObjectPool : SingletonMonoBehaviour<ObjectPool>
	{
		public bool preloadOnAwake = true;
		public Transform trs;
		public SpawnEntry[] spawnEntries = new SpawnEntry[0];
		public DelayedDespawn[] delayedDespawns = new DelayedDespawn[0];
		public RangedDespawn[] rangedDespawns = new RangedDespawn[0];
		public List<SpawnedEntry> spawnedEntries = new List<SpawnedEntry>();
		
#if UNITY_EDITOR
		void OnValidate ()
		{
			if (Application.isPlaying)
				return;
			for (int i = 0 ; i < spawnEntries.Length; i ++)
			{
				SpawnEntry spawnEntry = spawnEntries[i];
				if (spawnEntry.trs != null)
					spawnEntry.go = spawnEntry.trs.gameObject;
			}
		}
#endif

		public override void Awake ()
		{
			base.Awake ();
			enabled = false;
			gameObject.SetActive(false);
			if (!preloadOnAwake)
				return;
			for (int i = 0 ; i < spawnEntries.Length; i ++)
			{
				SpawnEntry spawnEntry = spawnEntries[i];
				for (int i2 = 0; i2 < spawnEntry.preload; i2 ++)
					Preload (i);
			}
		}
		
		public void DoUpdate ()
		{
			for (int i = 0; i < delayedDespawns.Length; i ++)
			{
				DelayedDespawn delayedDespawn = delayedDespawns[i];
				delayedDespawn.timeRemaining -= Time.deltaTime;
				if (delayedDespawn.timeRemaining <= 0)
				{
					Despawn (delayedDespawn.prefabIndex, delayedDespawn.go, delayedDespawn.trs);
					if (i >= delayedDespawns.Length)
						continue;
					delayedDespawns = delayedDespawns.RemoveAt(i);
					i --;
					if (delayedDespawns.Length == 0 && rangedDespawns.Length == 0)
						enabled = false;
				}
			}
			for (int i = 0; i < rangedDespawns.Length; i ++)
			{
				RangedDespawn rangedDespawn = rangedDespawns[i];
				rangedDespawn.rangeRemaining -= (rangedDespawn.trs.position - rangedDespawn.previousPosition).magnitude;
				rangedDespawn.previousPosition = rangedDespawn.trs.position;
				if (rangedDespawn.rangeRemaining <= 0)
				{
					Despawn (rangedDespawn.prefabIndex, rangedDespawn.go, rangedDespawn.trs);
					if (i >= rangedDespawns.Length)
						continue;
					rangedDespawns = rangedDespawns.RemoveAt(i);
					i --;
					if (rangedDespawns.Length == 0 && delayedDespawns.Length == 0)
						enabled = false;
				}
			}
		}
		
		public virtual DelayedDespawn DelayDespawn (int prefabIndex, GameObject clone, Transform trs, float delay)
		{
			DelayedDespawn delayedDespawn = new DelayedDespawn(delay, prefabIndex, clone, trs);
			delayedDespawns = delayedDespawns.Add(delayedDespawn);
			enabled = true;
			return delayedDespawn;
		}
		
		public virtual DelayedDespawn DelayDespawn (GameObject clone, Transform trs, float delay)
		{
			DelayedDespawn delayedDespawn = new DelayedDespawn();
			delayedDespawn.timeRemaining = delay;
			delayedDespawn.go = clone;
			delayedDespawn.trs = trs;
			delayedDespawns = delayedDespawns.Add(delayedDespawn);
			enabled = true;
			return delayedDespawn;
		}

		public virtual void CancelDelayedDespawn (DelayedDespawn delayedDespawn)
		{
			int indexOfDelayedDespawn = delayedDespawns.IndexOf(delayedDespawn);
			if (indexOfDelayedDespawn != -1)
			{
				delayedDespawns = delayedDespawns.RemoveAt(indexOfDelayedDespawn);
				if (delayedDespawns.Length == 0 && rangedDespawns.Length == 0)
					enabled = false;
			}
		}
		
		public virtual RangedDespawn RangeDespawn (int prefabIndex, GameObject clone, Transform trs, float range)
		{
			RangedDespawn rangedDespawn = new RangedDespawn(range, prefabIndex, clone, trs);
			rangedDespawns = rangedDespawns.Add(rangedDespawn);
			enabled = true;
			return rangedDespawn;
		}

		public virtual void CancelRangedDespawn (RangedDespawn rangedDespawn)
		{
			int indexOfRangedDespawn = rangedDespawns.IndexOf(rangedDespawn);
			if (indexOfRangedDespawn != -1)
			{
				rangedDespawns = rangedDespawns.RemoveAt(indexOfRangedDespawn);
				if (rangedDespawns.Length == 0 && delayedDespawns.Length == 0 && this != null)
					enabled = false;
			}
		}
		
		public virtual T SpawnComponent<T> (int prefabIndex, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion(), Transform parent = null) where T : Object
		{
			return Instantiate(spawnEntries[prefabIndex].go.GetComponent<T>(), position, rotation, parent);
			// SpawnEntry spawnEntry = spawnEntries[prefabIndex];
			// if (spawnEntry.cache.Count == 0)
			// 	Preload (prefabIndex);
			// KeyValuePair<GameObject, Transform> cacheEntry = spawnEntry.cache[0];
			// spawnEntry.cache.RemoveAt(0);
			// cacheEntry.Value.position = position;
			// cacheEntry.Value.rotation = rotation;
			// cacheEntry.Value.localScale = spawnEntry.trs.localScale;
			// cacheEntry.Value.SetParent(parent, true);
			// cacheEntry.Key.SetActive(true);
			// spawnEntries[prefabIndex] = spawnEntry;
			// return cacheEntry.Key.GetComponent<T>();
		}

		public virtual T SpawnComponent<T> (T component, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion(), Transform parent = null) where T : Object
		{
			return (T) Instantiate(component, position, rotation, parent);
		}
		
		public virtual KeyValuePair<GameObject, Transform> Despawn (SpawnedEntry spawnedEntry)
		{
			return Despawn(spawnedEntry.prefabIndex, spawnedEntry.go, spawnedEntry.trs);
		}
		
		public virtual KeyValuePair<GameObject, Transform> Despawn (int prefabIndex, GameObject go, Transform trs = null)
		{
			if (go == null)
				return new KeyValuePair<GameObject, Transform>();
			trs.SetParent(this.trs);
			Destroy(go);
			return new KeyValuePair<GameObject, Transform>();
			// trs.SetParent(this.trs, true);
			// KeyValuePair<GameObject, Transform> output = new KeyValuePair<GameObject, Transform>(go, trs);
			// spawnEntries[prefabIndex].cache.Add(output);
			// return output;
		}
		
		public virtual KeyValuePair<GameObject, Transform> Preload (int prefabIndex)
		{
			SpawnEntry spawnEntry = spawnEntries[prefabIndex];
			GameObject clone = Instantiate(spawnEntry.go, trs);
			KeyValuePair<GameObject, Transform> output = new KeyValuePair<GameObject, Transform>(clone, clone.GetComponent<Transform>());
			spawnEntry.cache.Add(output);
			spawnEntries[prefabIndex] = spawnEntry;
			return output;
		}

		[Serializable]
		public class ObjectPoolEntry
		{
			[HideInInspector]
			public int prefabIndex;
			public GameObject go;
			public Transform trs;
			
			public ObjectPoolEntry ()
			{
			}
			
			public ObjectPoolEntry (int prefabIndex, GameObject go, Transform trs)
			{
				this.prefabIndex = prefabIndex;
				this.go = go;
				this.trs = trs;
			}
		}
		
		[Serializable]
		public class SpawnEntry : ObjectPoolEntry
		{
			public int preload;
			public List<KeyValuePair<GameObject, Transform>> cache = new List<KeyValuePair<GameObject, Transform>>();
			
			public SpawnEntry ()
			{
			}
			
			public SpawnEntry (int preload, List<KeyValuePair<GameObject, Transform>> cache, int prefabIndex, GameObject go, Transform trs) : base (prefabIndex, go, trs)
			{
				this.preload = preload;
				this.cache = cache;
			}
		}
		
		public class SpawnedEntry : ObjectPoolEntry
		{
			public SpawnedEntry ()
			{
			}
			
			public SpawnedEntry (int prefabIndex, GameObject go, Transform trs) : base (prefabIndex, go, trs)
			{
			}
		}
		
		public class DelayedDespawn : SpawnedEntry
		{
			public float timeRemaining;
			
			public DelayedDespawn ()
			{
			}
			
			public DelayedDespawn (float timeRemaining, int prefabIndex, GameObject go, Transform trs) : base (prefabIndex, go, trs)
			{
				this.timeRemaining = timeRemaining;
			}
		}
		
		public class RangedDespawn : SpawnedEntry
		{
			public Vector3 previousPosition;
			public float rangeRemaining;
			
			public RangedDespawn ()
			{
			}
			
			public RangedDespawn (float rangeRemaining, int prefabIndex, GameObject go, Transform trs) : base (prefabIndex, go, trs)
			{
				this.previousPosition = trs.localPosition;
				this.rangeRemaining = rangeRemaining;
			}
		}
	}
}