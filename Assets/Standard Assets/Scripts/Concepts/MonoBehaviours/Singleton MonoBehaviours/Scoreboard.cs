using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class Scoreboard : SingletonMonoBehaviour<Scoreboard>
	{
		public ScoreboardEntry scoreboardEntryPrefab;
		public Transform scoreboardEntriesParent;
		static Dictionary<Duel.Team, ScoreboardEntry> scoreboardEntriesDict = new Dictionary<Duel.Team, ScoreboardEntry>();

		public void AddEntry (Duel.Team team)
		{
			ScoreboardEntry scoreboardEntry = Instantiate(scoreboardEntryPrefab, scoreboardEntriesParent);
			scoreboardEntry.Init (team);
			scoreboardEntriesDict[team] = scoreboardEntry;
		}

		public void UpdateEntry (Duel.Team team)
		{
			ScoreboardEntry scoreboardEntry = scoreboardEntriesDict[team];
			scoreboardEntry.scoreText.text = "" + team.score;
			foreach (KeyValuePair<Duel.Team, ScoreboardEntry> keyValuePair in scoreboardEntriesDict)
			{
				if (keyValuePair.Value.trs == null)
					return;
				if (team.score >= keyValuePair.Key.score)
					scoreboardEntry.trs.SetSiblingIndex(keyValuePair.Value.trs.GetSiblingIndex() - 1);
				else
					scoreboardEntry.trs.SetSiblingIndex(keyValuePair.Value.trs.GetSiblingIndex() + 1);
			}
		}

		public void RemoveEntry (Duel.Team team)
		{
			ScoreboardEntry scoreboardEntry;
			if (scoreboardEntriesDict.TryGetValue(team, out scoreboardEntry))
			{
				Destroy(scoreboardEntry.gameObject);
				scoreboardEntriesDict.Remove(team);
			}
		}
	}
}