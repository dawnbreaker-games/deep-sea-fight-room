using System.IO;
using UnityEngine;
using System.Diagnostics;

namespace FightRoom
{
	public class Minigame: SingletonMonoBehaviour<Minigame>
	{
		public string exePath;
		public string processName;

		public void Begin ()
		{
#if UNITY_STANDALONE_LINUX
			string exePath = this.exePath.Replace("exe", "x86_64");
#endif
			ProcessStartInfo processStartInfo = new ProcessStartInfo(Application.dataPath + Path.DirectorySeparatorChar + exePath);
			Process process = new Process();
			process.StartInfo = processStartInfo;
			process.Start();
		}

		public bool IsPlaying ()
		{
			return Process.GetProcessesByName(processName).Length > 0;
		}
	}
}