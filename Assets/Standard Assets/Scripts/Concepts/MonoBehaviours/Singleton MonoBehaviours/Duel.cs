using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace FightRoom
{
	public class Duel : SingletonMonoBehaviour<Duel>, IUpdatable
	{
		public Text waitingForOtherPlayerText;
		public byte countdownDuration;
		public Text countdownTimerText;
		public byte moneyOnKill;
		public bool virtualEnemy;
		public bool competitiveShop;
		public static Dictionary<bool, OnlinePlayer> playersDict = new Dictionary<bool, OnlinePlayer>();
		public static Dictionary<bool, Team> playerTeamsDict = new Dictionary<bool, Team>();
		public static OnlinePlayer localPlayer;
		public static bool ranked;
		public static bool isPublic;
		public static bool isLocalPlayerReady;
		public static bool isEnemyPlayerReady;
		static bool localPlayerId;
		static byte maxScore;
		static bool hasEnded;
		const float MAX_SKILL_IN_CURRENT_SEASON_DIFFERENCE = .7f;
		const string PRIVATE_GAME_INDICATOR = "Private: ";
		OnlinePlayer[] players = new OnlinePlayer[0];
		float countdownTimer;
		VirtualEnemyUpdater virtualEnemyUpdater;

		public override void Awake ()
		{
			base.Awake ();
			Application.runInBackground = true;
			players = FindObjectsByType<OnlinePlayer>(FindObjectsSortMode.InstanceID);
			playersDict.Clear();
			playerTeamsDict.Clear();
			hasEnded = false;
			NetworkManager.Disconnect ();
			NetworkManager.Connect (OnConnectSuccess, OnConnectFail);
		}

		public void DoUpdate ()
		{
			countdownTimer -= Time.deltaTime;
			countdownTimerText.text = "Or wait " + (int) countdownTimer + " to begin";
			if (!isLocalPlayerReady && InputManager.MoveInput != Vector2.zero)
			{
				isLocalPlayerReady = true;
				Level.instance.moveToBeginTextGo.GetComponentInChildren<Text>().text = "The other player has to move to begin";
				NetworkManager.connection.Send("Player Ready");
				if (virtualEnemy)
					isEnemyPlayerReady = true;
			}
			if (countdownTimer <= 0 || (isLocalPlayerReady && isEnemyPlayerReady))
			{
				localPlayer.enabled = true;
				countdownTimerText.gameObject.SetActive(false);
				Level.Instance.Begin ();
				if (virtualEnemy && virtualEnemyUpdater == null)
				{
					virtualEnemyUpdater = new VirtualEnemyUpdater();
					GameManager.updatables = GameManager.updatables.Add(virtualEnemyUpdater);
				}
				GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}

		void MakeDuel ()
		{
			maxScore = 20;
			if (isPublic)
				NetworkManager.client.Multiplayer.CreateJoinRoom(LocalUserInfo.username, name, true, GetMakeRoomData(), null, OnMakeOrJoinDuelSucess, NetworkManager.DisplayError);
			else
				NetworkManager.client.Multiplayer.CreateJoinRoom(PRIVATE_GAME_INDICATOR + SaveAndLoadManager.saveData.privateDuelName, name, false, GetMakeRoomData(), null, OnMakeOrJoinDuelSucess, NetworkManager.DisplayError);
		}
		
		void ListDuels ()
		{
			isPublic = !DuelMenu.isPrivate;
			ranked = isPublic && SaveAndLoadManager.saveData.rankedDuels;
			NetworkManager.client.Multiplayer.ListRooms(Instance.name, new Dictionary<string, string>() { { "ranked", ranked.ToString() } }, 0, 0, instance.OnListDuelsSuccess, NetworkManager.DisplayError);
		}

		void OnListDuelsSuccess (RoomInfo[] roomInfos)
		{
			for (int i = 0; i < roomInfos.Length; i ++)
			{
				RoomInfo roomInfo = roomInfos[i];
				if (roomInfo.OnlineUsers < 2 && (isPublic || roomInfo.Id == PRIVATE_GAME_INDICATOR + SaveAndLoadManager.saveData.privateDuelName) && (!ranked || Mathf.Abs(LocalUserInfo.duelSkillInCurrentSeason - float.Parse(roomInfo.RoomData["duelSkillInCurrentSeason"])) <= MAX_SKILL_IN_CURRENT_SEASON_DIFFERENCE))
				{
					JoinDuel (roomInfo, OnMakeOrJoinDuelSucess, NetworkManager.DisplayError);
					return;
				}
			}
			MakeDuel ();
		}

		void OnConnectSuccess (Client client)
		{
			NetworkManager.client = client;
			client.Multiplayer.UseSecureConnections = true;
			ListDuels ();
		}

		void OnConnectFail (PlayerIOError error)
		{
			GameManager.instance.DisplayNotification ("Error: " + error.ToString());
			NetworkManager.Connect (OnConnectSuccess, OnConnectFail);
		}

		Dictionary<string, string> GetMakeRoomData ()
		{
			Dictionary<string, string> output = new Dictionary<string, string>();
			output.Add("ranked" , ranked.ToString());
			output.Add("maxScore", "" + maxScore);
			if (ranked)
				output.Add("duelSkillInCurrentSeason", "" + LocalUserInfo.duelSkillInCurrentSeason);
			return output;
		}

		Dictionary<string, string> GetJoinRoomData ()
		{
			return null;
		}

		void JoinDuel (RoomInfo roomInfo, Callback<Connection> onSuccess, Callback<PlayerIOError> onFail)
		{
			ranked = bool.Parse(roomInfo.RoomData["ranked"]);
			maxScore = byte.Parse(roomInfo.RoomData["maxScore"]);
			NetworkManager.client.Multiplayer.JoinRoom(roomInfo.Id, GetJoinRoomData(), onSuccess, onFail);
		}

		void OnMakeOrJoinDuelSucess (Connection connection)
		{
			NetworkManager.connection = connection;
			connection.OnMessage += OnMessage;
			connection.OnDisconnect += OnDisconnect;
			// VoiceChat.instance.Init ();
		}

		void OnMessage (object sender, Message message)
		{
			switch (message.Type)
			{
				case "Spawn Player":
					OnSpawnPlayerMessage (message);
					break;
				case "Remove Player":
					OnRemovePlayerMessage (message);
					break;
				case "Move Player":
					OnMovePlayerMessage (message);
					break;
				case "Rotate Player":
					OnRotatePlayerMessage (message);
					break;
				case "Shoot":
					OnShootMessage (message);
					break;
				case "Kill Player":
					OnKillPlayerMessage (message);
					break;
				case "Use Ability":
					OnUseAbilityMessage (message);
					break;
				case "Begin Use Ability":
					OnBeginUseAbilityMessage (message);
					break;
				case "Stop Use Ability":
					OnStopUseAbilityMessage (message);
					break;
				case "Player Ready":
					OnPlayerReadyMessage (message);
					break;
				case "Player Bought Item":
					OnBoughtItemMessage (message);
					break;
				case "Player Rerolled":
					OnRerolled (message);
					break;
			}
		}

		void OnDisconnect (object sender, string reason)
		{
			if (!hasEnded)
				End ();
		}

		OnlinePlayer OnSpawnPlayerMessage (Message message)
		{
			bool playerId = playersDict.Count == 0;
			OnlinePlayer player = players[playerId.GetHashCode()];
			Color teamColor = new Color(message.GetFloat(0), message.GetFloat(1), message.GetFloat(2));
			player.owner = new Team(teamColor, 0, "");
			ApplyTeam (player);
			if (virtualEnemy)
			{
				OnlinePlayer enemyPlayer = players[(!playerId).GetHashCode()];
				enemyPlayer.owner = new Team(ColorExtensions.GetRandom(), 0, "");
				ApplyTeam (enemyPlayer);
			}
			if (localPlayer == null)
			{
				localPlayer = player;
				localPlayerId = playerId;
				if (!playerId)
				{
					Player firstPlayer = players[0];
					Player secondPlayer = players[1];
					Text hpText = firstPlayer.hpText;
					firstPlayer.hpText = secondPlayer.hpText;
					secondPlayer.hpText = hpText;
					Image hpBarImage = firstPlayer.hpBarImage;
					firstPlayer.hpBarImage = secondPlayer.hpBarImage;
					secondPlayer.hpBarImage = hpBarImage;
				}
			}
			else
			{
				player.rigid.bodyType = RigidbodyType2D.Static;
				player.useableItems[0].cooldownIndicatorImage = null;
			}
			player.initPosition = player.trs.position;
 			Scoreboard.instance.AddEntry (player.owner);
			if (playersDict.Count >= 2 || virtualEnemy)
			{
				waitingForOtherPlayerText.gameObject.SetActive(false);
				countdownTimer = countdownDuration;
				playersDict[!localPlayerId].hpBarImage.rectTransform.parent.gameObject.SetActive(true);
				Level.instance.moveToBeginTextGo.SetActive(true);
				GameManager.updatables = GameManager.updatables.Add(this);
			}
			return player;
		}

		void ApplyTeam (OnlinePlayer player)
		{
			Material material = new Material(player.renderer.sharedMaterial);
			material.SetColor("_tint", player.owner.color);
			player.renderer.sharedMaterial = material;
			playersDict[player.id] = player;
			playerTeamsDict[player.id] = player.owner;
		}

		void OnRemovePlayerMessage (Message message)
		{
			bool playerId = !localPlayerId;
			OnlinePlayer player = playersDict[!playerId];
			if (player != null)
			{
				player.owner.representatives = player.owner.representatives.Remove(player);
				Destroy(player.gameObject);
			}
			Scoreboard.instance.RemoveEntry (playerTeamsDict[playerId]);
			playersDict.Remove(playerId);
			playerTeamsDict.Remove(playerId);
		}

		void OnMovePlayerMessage (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			if (player != null)
			{
				Vector2 position = new Vector2(message.GetFloat(0), message.GetFloat(1));
				player.trs.position = position;
			}
		}

		void OnRotatePlayerMessage (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			if (player != null)
			{
				float rotation = message.GetFloat(0);
				player.trs.eulerAngles = Vector3.forward * rotation;
			}
		}

		void OnShootMessage (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			if (player != null)
			{
				foreach (KeyValuePair<string, BulletPatternEntry> keyValuePair in player.bulletPatternEntriesSortedList)
					keyValuePair.Value.Shoot ();
			}
		}

		void OnKillPlayerMessage (Message message)
		{
			OnlinePlayer killer = playersDict[localPlayerId];
			OnlinePlayer victim = playersDict[!localPlayerId];
			OnPlayerDied (victim, killer);
		}

		void OnUseAbilityMessage (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			player.useableItems[0].Use ();
		}

		void OnBeginUseAbilityMessage (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			player.useableItems[0].OnBeganUsing ();
		}

		void OnStopUseAbilityMessage (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			player.useableItems[0].OnStopUsing ();
		}

		void OnPlayerReadyMessage (Message message)
		{
			isEnemyPlayerReady = true;
		}

		void OnBoughtItemMessage (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			uint itemIndex = message.GetUInt(0);
			Item item = ShopMenu.instance.allItems[itemIndex];
			BuyItemEntry shopItemEntry = Instantiate(ShopMenu.instance.buyItemEntryPrefab);
			shopItemEntry.Init (item);
			shopItemEntry.BuyItem (player);
		}

		void OnRerolled (Message message)
		{
			OnlinePlayer player = playersDict[!localPlayerId];
			ShopMenu.instance.Reroll (player);
		}

		public void OnContinueButtonPressed ()
		{
			ShopMenu.instance.gameObject.SetActive(false);
			isLocalPlayerReady = true;
			NetworkManager.connection.Send("Player Ready");
			if (isEnemyPlayerReady)
				ShopMenu.instance.Close ();
		}

		public void OnPlayerDied (OnlinePlayer victim, OnlinePlayer killer)
		{
			if (killer != victim)
			{
				killer.owner.score ++;
				killer.AddMoney (killer.enemyMoney.Apply(moneyOnKill));
				Scoreboard.instance.UpdateEntry (killer.owner);
				if (ranked)
				{
					if (localPlayer == killer)
						OnEnemyPlayerDeadInRanked ();
					else
						OnLocalPlayerDeadInRanked ();
				}
				if (!hasEnded && killer.owner.score >= maxScore)
				{
					if (ranked)
					{
						if (localPlayer == killer)
							OnLocalPlayerWonRanked ();
						else
							OnLocalPlayerLostRanked ();
					}
					End ();
					return;
				}
			}
			else
			{
				victim.owner.score --;
				Scoreboard.instance.UpdateEntry (victim.owner);
				if (localPlayer == victim && ranked)
					OnLocalPlayerDeadInRanked ();
			}
			victim.trs.position = victim.initPosition;
			killer.trs.position = killer.initPosition;
			Level.Instance.EndWave ();
		}

		void OnLocalPlayerDeadInRanked ()
		{
			LocalUserInfo.duelDeaths ++;
			LocalUserInfo.duelDeathsInCurrentSeason ++;
		}

		void OnEnemyPlayerDeadInRanked ()
		{
			LocalUserInfo.duelKills ++;
			LocalUserInfo.duelKillsInCurrentSeason ++;
		}

		void OnLocalPlayerWonRanked ()
		{
			LocalUserInfo.duelWins ++;
			LocalUserInfo.duelWinsInCurrentSeason ++;
			OnlinePlayer victim = playersDict[!localPlayerId];
			float skillChange = 1f - ((float) victim.owner.score / maxScore);
			LocalUserInfo.duelSkill += skillChange;
			LocalUserInfo.duelSkillInCurrentSeason += skillChange;
		}

		void OnLocalPlayerLostRanked ()
		{
			LocalUserInfo.duelLosses ++;
			LocalUserInfo.duelLossesInCurrentSeason ++;
			float skillChange = 1f - ((float) localPlayer.owner.score / maxScore);
			LocalUserInfo.duelSkill -= skillChange;
			LocalUserInfo.duelSkillInCurrentSeason -= skillChange;
		}

		void End ()
		{
			hasEnded = true;
			_SceneManager.instance.LoadScene (0);
		}

		void OnDestroy ()
		{
			playersDict.Clear();
			playerTeamsDict.Clear();
			GameManager.updatables = GameManager.updatables.Remove(ShopMenu.instance);
			GameManager.updatables = GameManager.updatables.Remove(virtualEnemyUpdater);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		[Serializable]
		public class Team : Team<OnlinePlayer>
		{
			public Color color;
			public int score;

			public Team ()
			{
			}

			public Team (Color color, int score = 0, string name = "", params OnlinePlayer[] representatives) : base (name, representatives)
			{
				this.color = color;
				this.score = score;
			}
		}

		public class VirtualEnemyUpdater : IUpdatable
		{
			OnlinePlayer onlinePlayer;

			public VirtualEnemyUpdater ()
			{
				onlinePlayer = playersDict[!localPlayerId];
				onlinePlayer.weapons[0].OnGain (onlinePlayer);
				onlinePlayer.initPosition = onlinePlayer.trs.position;
				Scoreboard.instance.AddEntry (onlinePlayer.owner);
			}
			
			public void DoUpdate ()
			{
				if (ShopMenu.Instance.gameObject.activeSelf)
				{
					if (instance.competitiveShop)
					{
						for (int i = 0; i < ShopMenu.buyItemEntries.Length; i ++)
						{
							BuyItemEntry itemEntry = ShopMenu.buyItemEntries[i];
							if (itemEntry != null && itemEntry.buyButton.interactable)
								itemEntry.BuyItem (onlinePlayer);
						}
					}
					isEnemyPlayerReady = true;
				}
				else
				{
					onlinePlayer.Move ((localPlayer.trs.position - onlinePlayer.trs.position).normalized * onlinePlayer.moveSpeed);
					onlinePlayer.trs.up = localPlayer.trs.position - onlinePlayer.trs.position;
					if (onlinePlayer.weapons[0].animationEntry.animator != null)
						onlinePlayer.weapons[0].animationEntry.Play ();
					onlinePlayer.useableItems[0].TryToUse ();
				}
			}
		}
	}
}