using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class AchievementPanel : SingletonMonoBehaviour<AchievementPanel>
	{
		public Transform achievementIndicatorsParent1;
		public Transform achievementIndicatorsParent2;
		public AchievementIndicator achievementIndicatorPrefab;

		public void DoUpdate ()
		{
			AchievementIndicator[] achievementIndicators = FindObjectsByType<AchievementIndicator>(FindObjectsSortMode.None);
			for (int i = 0; i < achievementIndicators.Length; i ++)
			{
				AchievementIndicator achievementIndicator = achievementIndicators[i];
				Destroy(achievementIndicator.gameObject);
			}
			Achievement[] achievements = MapMenu.targetLevel.GetComponentsInChildren<Achievement>();
			for (int i = 0; i < achievements.Length; i ++)
			{
				Achievement achievement = achievements[i];
				Transform achievementIndicatorParent = achievementIndicatorsParent1;
				if (i >= (float) achievements.Length / 2)
					achievementIndicatorParent = achievementIndicatorsParent2;
				AchievementIndicator achievementIndicator = Instantiate(achievementIndicatorPrefab, achievementIndicatorParent);
				achievementIndicator.text.rectTransform.localScale = Vector3.one;
				string text;
				if (achievement.unlockLevelOnComplete.trs.position.x > MapMenu.targetLevel.trs.position.x)
					text = ">";
				else if (achievement.unlockLevelOnComplete.trs.position.x < MapMenu.targetLevel.trs.position.x)
					text = "<";
				else if (achievement.unlockLevelOnComplete.trs.position.y > MapMenu.targetLevel.trs.position.y)
					text = "^";
				else
					text = "v";
				if (achievement.complete)
					text += " (Done)";
				text += " " + achievement.displayName;
				if (!achievement.complete)
					text += " (" + achievement.GetProgress() + "/" + achievement.GetMaxProgress() + ")";
				achievementIndicator.text.text = text;
			}
		}
	}
}