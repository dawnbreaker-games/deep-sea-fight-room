using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class Menu : SingletonMonoBehaviour<Menu>
	{
		public static Menu previousMenu;

		public void OpenPreviousMenu ()
		{
			previousMenu.Open ();
		}

		public virtual void Open ()
		{
			if (DuelMenu.instance == null || ShopMenu.instance == this)
			{
				GameManager.paused = true;
				_Animator.instances = FindObjectsByType<_Animator>(FindObjectsSortMode.None);
				for (int i = 0; i < _Animator.instances.Length; i ++)
				{
					_Animator animator = _Animator.instances[i];
					animator.animator.enabled = false;
				}
			}
			gameObject.SetActive(true);
			previousMenu = this;
		}

		public virtual void Close ()
		{
			if ((Tutorial.instance == null || !Tutorial.instance.gameObject.activeSelf) && DuelMenu.instance == null || ShopMenu.instance == this)
			{
				GameManager.paused = false;
				_Animator.instances = FindObjectsByType<_Animator>(FindObjectsSortMode.None);
				for (int i = 0; i < _Animator.instances.Length; i ++)
				{
					_Animator animator = _Animator.instances[i];
					animator.animator.enabled = true;
				}
			}
			if (this != null)
				gameObject.SetActive(false);
		}
	}
}