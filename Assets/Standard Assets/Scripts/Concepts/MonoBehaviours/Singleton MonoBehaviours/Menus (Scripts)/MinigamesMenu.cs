using Extensions;
using UnityEngine.UI;

namespace FightRoom
{
	public class MinigamesMenu : Menu, IUpdatable
	{
		public Button[] buttons = new Button[0];
		public Minigame[] minigames = new Minigame[0];
		public new static MinigamesMenu instance;
		public new static MinigamesMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<MinigamesMenu>(true);
				return instance;
			}
		}

		public override void Open ()
		{
			base.Open ();
			if (SaveAndLoadManager.saveData.unlockedMinigames.Length > 0)
				GameManager.updatables = GameManager.updatables.Add(this);
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			for (int i = 0; i < buttons.Length; i ++)
			{
				Button button = buttons[i];
				Minigame minigame = minigames[i];
				button.interactable = SaveAndLoadManager.saveData.unlockedMinigames.Contains(minigame.name) && !minigame.IsPlaying();
			}
		}
	}
}