namespace FightRoom
{
	public class PauseMenu : Menu
	{
		public new static PauseMenu instance;
		public new static PauseMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<PauseMenu>(true);
				return instance;
			}
		}
	}
}