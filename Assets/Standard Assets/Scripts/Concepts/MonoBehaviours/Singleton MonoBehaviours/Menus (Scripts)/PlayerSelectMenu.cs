using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace FightRoom
{
	public class PlayerSelectMenu : Menu, IUpdatable
	{
		public Text currentPlayerNameText;
		public Transform playersParent;
		public Transform currentPlayerIndicatorTrs;
		public Text itemsDescriptionText;
		public new static PlayerSelectMenu instance;
		public new static PlayerSelectMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<PlayerSelectMenu>(true);
				return instance;
			}
		}
		public static Player[] players = new Player[0];
		public static uint currentPlayerIndex;
		static int previousSwitchPlayerInput;
		public static bool justClosed;
		bool previousSelectInput = true;

		public void Init ()
		{
			players = playersParent.GetComponentsInChildren<Player>();
			SetCurrentPlayer (players[currentPlayerIndex]);
		}

		public override void Open ()
		{
			base.Open ();
			SetCurrentPlayer (players[currentPlayerIndex]);
			GameManager.updatables = GameManager.updatables.Add(this);
			justClosed = false;
		}

		public override void Close ()
		{
			gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
			currentPlayerNameText.gameObject.SetActive(false);
			justClosed = true;
		}

		public void DoUpdate ()
		{
			bool selectInput = InputManager.SelectInput;
			Vector2 movementInput = InputManager.MoveInput;
			if (movementInput.x != 0 && previousSwitchPlayerInput == 0)
			{
				if (movementInput.x < 0)
				{
					if (currentPlayerIndex > 0)
						currentPlayerIndex --;
					else
						currentPlayerIndex = (uint) players.Length - 1;
				}
				else
				{
					if (currentPlayerIndex < players.Length - 1)
						currentPlayerIndex ++;
					else
						currentPlayerIndex = 0;
				}
				SetCurrentPlayer (players[currentPlayerIndex]);
			}
			if ((Player.instance.unlocked || BuildManager.instance.unlockEverything) && !EventSystem.current.IsPointerOverGameObject() && selectInput && !previousSelectInput)
				Close ();
			previousSwitchPlayerInput = MathfExtensions.Sign(movementInput.x);
			previousSelectInput = selectInput;
		}

		public void SetCurrentPlayer (Player player)
		{
			Player.instance = player;
			currentPlayerIndicatorTrs.position = player.trs.position;
			string text = player.displayName;
			if (player.unlockOnCompleteAchievement != null)
			{
				text += "\n";
				if (player.unlockOnCompleteAchievement.complete)
					text += "(Done) ";
				text += player.unlockOnCompleteAchievement.displayName;
			}
			currentPlayerNameText.text = text;
			text = "Max health: " + player.maxHp + "\n";
			if (player.weapons.Length == 1)
				text += "\nWeapon:\n";
			else if (player.weapons.Length > 1)
				text += "\nWeapons:\n";
			for (int i = 0; i < player.weapons.Length; i ++)
			{
				Weapon weapon = player.weapons[i];
				text += weapon.ToString() + "\n";
			}
			if (player.useableItems.Length == 1)
				text += "\nAbility:\n";
			else if (player.useableItems.Length > 1)
				text += "\nAbilities:\n";
			for (int i = 0; i < player.useableItems.Length; i ++)
			{
				UseableItem useableItem = player.useableItems[i];
				text += useableItem.ToString() + "\n";
			}
			text += "Stats:\n";
			for (int i = 0; i < StatsMenu.Instance.statIndicators.Length; i ++)
			{
				StatIndicator statIndicator = StatsMenu.instance.statIndicators[i];
				print(statIndicator.text == null);
				text += statIndicator.text.text + "\n";
			}
			itemsDescriptionText.text = text;
		}
	}
}