using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace FightRoom
{
	public class StatsMenu : Menu
	{
		public StatIndicator[] statIndicators = new StatIndicator[0];
		public Transform trackedStatsIndicatorsParent;
		public new static StatsMenu instance;
		public new static StatsMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<StatsMenu>(true);
				return instance;
			}
			set
			{
				instance = value;
			}
		}
		public static Dictionary<Stat, TrackedStat> trackedStatsDict = new Dictionary<Stat, TrackedStat>();

#if UNITY_EDTIOR
		void OnValidate ()
		{
			if (Application.isPlaying)
				return;
			if (statIndicators.Length == 0)
				statIndicators = GetComponentsInChildren<StatIndicator>(true);
			for (int i = 0; i < statIndicators.Length; i ++)
			{
				StatIndicator statIndicator = statIndicators[i];
				if (statIndicator.childStatIndicator != null)
					statIndicator.childStatIndicator.tooltipText.text = statIndicator.tooltipText.text;
			}
		}
#endif

        public override void Awake ()
		{
			base.Awake ();
			trackedStatsDict.Clear();
		}

		public override void Open ()
		{
			base.Open ();
			for (int i = 0; i < Player.instance.stats.Length; i ++)
			{
				Stat stat = Player.instance.stats[i];
				string tooltipText = stat.GetNewTooltipText();
				stat.indicator.tooltipText.text = tooltipText;
				stat.indicator.childStatIndicator.tooltipText.text = tooltipText;
			}
		}

		public override void Close ()
		{
			base.Close ();
			for (int i = 0; i < statIndicators.Length; i ++)
			{
				StatIndicator statIndicator = statIndicators[i];
				statIndicator.tooltipText.rectTransform.parent.gameObject.SetActive(false);
			}
		}

		public static void OnMousedOverStatTooltip (Tooltip tooltip)
		{
			if (!SaveAndLoadManager.saveData.mousedOverStats.Contains(tooltip.name))
				SaveAndLoadManager.saveData.mousedOverStats = SaveAndLoadManager.saveData.mousedOverStats.Add(tooltip.name);
		}

		public void TrackStat (Draggable draggable)
		{
			draggable.GetComponents<EventTrigger>()[1].enabled = true;
			StatIndicator[] trackedStatsIndicators = trackedStatsIndicatorsParent.GetComponentsInChildren<StatIndicator>();
			StatIndicator trackedStatIndicator = trackedStatsIndicators[draggable.rectTrs.parent.GetSiblingIndex()];
			Stat stat = Player.instance.stats[statIndicators.IndexOf(draggable.defaultParent.GetComponent<StatIndicator>())];
			trackedStatsDict[stat] = new TrackedStat(stat, trackedStatIndicator);
			trackedStatIndicator.text.text = draggable.GetComponent<Text>().text;
			trackedStatIndicator.tooltipText.text = draggable.GetComponent<Tooltip>().text.text;
		}

		public void UntrackStat (Draggable draggable)
		{
			draggable.GetComponents<EventTrigger>()[1].enabled = false;
			draggable.GetComponent<Tooltip>().text.rectTransform.parent.gameObject.SetActive(false);
			Stat stat = Player.instance.stats[statIndicators.IndexOf(draggable.defaultParent.GetComponent<StatIndicator>())];
			StatIndicator statIndicator = trackedStatsDict[stat].indicator;
			statIndicator.text.text = "";
			trackedStatsDict.Remove(stat);
		}

		public class TrackedStat
		{
			public Stat stat;
			public StatIndicator indicator;

			public TrackedStat (Stat stat, StatIndicator indicator)
			{
				this.stat = stat;
				this.indicator = indicator;
			}
		}
	}
}