using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace FightRoom
{
	public class MapMenu : Menu, IUpdatable
	{
		public AchievementIndicator achievementIndicatorPrefab;
		public Text levelNameText;
		public Text pressSelectToBeginText;
		public Button openPlayerSelectMenuButon;
		public float cameraLerpRateForRooms;
		public float cameraLerpRateForZones;
		public CameraScript mapCameraScript;
		public Camera mouseCamera;
		public Transform currentLevelIndicatorTrs;
		public Text zoneNameText;
		public Text zoneDescriptionText;
		public Zone[] zones = new Zone[0];
		public GameObject activateWhileNotOpen;
		public new static MapMenu instance;
		public new static MapMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<MapMenu>(true);
				return instance;
			}
		}
		public static Level targetLevel;
		public static bool ShouldUpdate
		{
			get
			{
				return shouldUpdate;
			}
			set
			{
				shouldUpdate = value;
			}
		}
		static Zone currentZone;
		static bool shouldUpdate = true;
		static CameraUpdater cameraUpdaterForRooms;
		static CameraUpdater cameraUpdaterForZones;
		static Vector2 moveInput;
		static Vector2? previousMoveInput;
		bool previousOpenPlayerSelectMenuInput;
		bool selectInput;
		bool previousSelectInput = true;
		Rect targetZoneBoundsRect;

		public override void Open ()
		{
			base.Open ();
			string text = "Press ";
			for (int i = 0; i < GameManager.instance.selectInputRebinders.Length; i ++)
			{
				InputRebinder inputRebinder = GameManager.instance.selectInputRebinders[i];
				if (i > 0)
					text += " or ";
				text += inputRebinder.bindingNameText.text;
			}
			text += " to begin";
			pressSelectToBeginText.text = text;
			previousMoveInput = null;
			Achievement.instances = FindObjectsByType<Achievement>(FindObjectsSortMode.None);
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (!achievement.complete && achievement.ShouldBeComplete())
					achievement.Complete ();
			}
			List<Level> levels = new List<Level>(FindObjectsByType<Level>(FindObjectsSortMode.None));
			for (int i = 0; i < levels.Count; i ++)
			{
				Level level = levels[i];
				if (!level.gameObject.activeSelf)
				{
					levels.RemoveAt(i);
					i --;
				}
				else if (level.BestTimeReached > 0)
					level.untriedIndicatorGo.SetActive(false);
			}
			Level.instances = levels.ToArray();
			SetTargetLevel (Level.Instance);
			if (targetLevel == null)
				SetTargetLevel (GameManager.Instance.firstLevel);
			else
				SetTargetLevel (targetLevel);
			zoneNameText.gameObject.SetActive(true);
			PlayerSelectMenu.instance.currentPlayerNameText.gameObject.SetActive(true);
			CameraScript.Instance.trs.position = targetLevel.trs.position.SetZ(CameraScript.instance.trs.position.z);
			GameManager.updatables = GameManager.updatables.Add(this);
			activateWhileNotOpen.SetActive(false);
			GameCamera.Instance.camera.enabled = false;
			if (!PlayerSelectMenu.players[1].unlocked)
				openPlayerSelectMenuButon.interactable = false;
		}

		public override void Close ()
		{
			base.Close ();
			zoneNameText.gameObject.SetActive(false);
			GameCamera.instance.trs.position = Level.instance.trs.position.SetZ(GameCamera.instance.trs.position.z);
			GameCamera.instance.camera.enabled = true;
			targetLevel.bestTimeReachedText.gameObject.SetActive(false);
			targetLevel.mostEnemiesKilledText.gameObject.SetActive(false);
			activateWhileNotOpen.SetActive(true);
			PauseMenu.Instance.Close ();
			shouldUpdate = true;
        }

		public void DoUpdate ()
		{
			if (PlayerSelectMenu.Instance == null)
			{
				GameManager.updatables = GameManager.updatables.Remove(this);
				return;
			}
			if (!ShouldUpdate || PlayerSelectMenu.instance.gameObject.activeSelf)
				return;
			bool selectInput = InputManager.SelectInput;
			bool openPlayerSelectMenuInput = InputManager.AbilityInput;
			moveInput = InputManager.MoveInput;
			HandleSetTargetLevel ();
			if (openPlayerSelectMenuInput && !previousOpenPlayerSelectMenuInput)
			{
				if (!Level.instance.enabled)
					PlayerSelectMenu.instance.Open ();
				else
					GameManager.instance.DisplayNotification ("You can't open the character select menu during gameplay!");
			}
			if (PlayerSelectMenu.justClosed)
				PlayerSelectMenu.justClosed = false;
			else if ((targetLevel.unlocked || BuildManager.instance.unlockEverything) && !EventSystem.current.IsPointerOverGameObject() && selectInput && !previousSelectInput)
			{
				if (targetLevel != Level.instance)
				{
					Level previousLevel = Level.instance;
					Level.instance = targetLevel;
					SceneManager.sceneLoaded += OnSceneLoaded;
					previousLevel.End ();
				}
				else if (!Level.instance.enabled)
					OnSceneLoaded ();
				else
					Close ();
			}
			previousMoveInput = moveInput;
			previousOpenPlayerSelectMenuInput = openPlayerSelectMenuInput;
			previousSelectInput = selectInput;
			
		}

	 	public void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
			instance.Close ();
			if (currentZone.hasTutorialScene && !SaveAndLoadManager.saveData.triedZones.Contains(zoneNameText.text))
			{
				SaveAndLoadManager.saveData.triedZones = SaveAndLoadManager.saveData.triedZones.Add(zoneNameText.text);
				SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
				_SceneManager.instance.LoadScene (zoneNameText.text + " Tutorial");
				return;
			}
			else if (Level.instance.tutorialBeforeLevel != null)
				GameManager.instance.StartTutorialIfShould (Level.instance.tutorialBeforeLevel);
			Player.Instance.untriedIndicatorGo.SetActive(false);
			Player.instance.trs.position = Level.instance.playerSpawnPoint.position;
			PlayerSelectMenu.instance.currentPlayerNameText.gameObject.SetActive(false);
			Player.instance.trs.SetParent(null);
			Level.instance.moveToBeginTextGo.SetActive(true);
			GameManager.paused = true;
			RippleMesh.instance.meshObject.trs.position = Level.instance.trs.position.SetZ(RippleMesh.instance.meshObject.trs.position.z) + new Vector3(1, 1) * .1f;
			RippleMesh.instance.enabled = true;
			GameCamera.instance.camera.enabled = true;
			GameManager.updatables = GameManager.updatables.Add(new BeginLevelUpdater());
		}

		void HandleSetTargetLevel ()
		{
			Level newTargetLevel = null;
			if (moveInput != Vector2.zero && previousMoveInput == Vector2.zero)
			{
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level level = Level.instances[i];
					Vector2 toLevel = level.trs.position - targetLevel.trs.position;
					float manhattenDistance = toLevel.GetManhattenMagnitude();
					if (manhattenDistance == level.size && Vector2.Angle(toLevel, moveInput) < 45)
					{
						newTargetLevel = level;
						break;
					}
				}
				if (newTargetLevel == null)
				{
					for (int i = 0; i < Level.instances.Length; i ++)
					{
						Level level = Level.instances[i];
						Vector2 toLevel = level.trs.position - targetLevel.trs.position;
						float manhattenDistance = toLevel.GetManhattenMagnitude();
						if (manhattenDistance == level.size * 2 && Vector2.Angle(toLevel, moveInput) < 45)
						{
							newTargetLevel = level;
							break;
						}
					}
					if (newTargetLevel == null)
						newTargetLevel = GetLevelAtWrappedMapMoveInput(moveInput);
				}
			}
			if (newTargetLevel != null && newTargetLevel != targetLevel)
				SetTargetLevel (newTargetLevel);
		}

		public void SetTargetLevel (Level level)
		{
			targetLevel = level;
			if (!targetZoneBoundsRect.Contains(level.trs.position))
			{
                for (int i = 0; i < zones.Length; i ++)
				{
					Zone zone = zones[i];
					Rect zoneBoundsRect = zone.boundsRect;
					if (zoneBoundsRect.Contains(level.trs.position))
					{
                        targetZoneBoundsRect = zoneBoundsRect;
						zoneNameText.text = zone.name;
						zoneDescriptionText.text = zone.description;
						currentZone = zone;
						break;
					}
				}
			}
			if (cameraUpdaterForZones != null)
				GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForZones);
			cameraUpdaterForZones = new CameraUpdater(mapCameraScript.trs, level.trs.position, cameraLerpRateForZones);
			GameManager.updatables = GameManager.updatables.Add(cameraUpdaterForZones);
			if (Level.instance == null || !Level.instance.enabled)
				Level.instance = level;
			if (level is BossLevel)
			{
				level.bestTimeReachedText.text = "Best damage: " + string.Format("{0:0.#}", level.BestTimeReached);
				level.bestTimeReachedText.gameObject.SetActive(true);
				level.mostEnemiesKilledText.gameObject.SetActive(false);
			}
			else
			{
				level.bestTimeReachedText.text = "Best time: " + string.Format("{0:0.#}", level.BestTimeReached);
				level.bestTimeReachedText.gameObject.SetActive(true);
				level.mostEnemiesKilledText.text = "Most enemies destroyed: " + level.MostEnemiesKilled;
				level.mostEnemiesKilledText.gameObject.SetActive(true);
			}
            currentLevelIndicatorTrs.position = level.trs.position;
			levelNameText.text = level.displayName;
			if (CameraScript.Instance != null)
			{
				if (cameraUpdaterForRooms != null)
					GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForRooms);
				cameraUpdaterForRooms = new CameraUpdater(CameraScript.instance.trs, level.trs.position, cameraLerpRateForRooms);
			}
			GameManager.updatables = GameManager.updatables.Add(cameraUpdaterForRooms);
			AchievementPanel.Instance.DoUpdate ();
			Achievement[] achievements = targetLevel.GetComponentsInChildren<Achievement>();
			for (int i = 0; i < achievements.Length; i ++)
			{
				Achievement achievement = achievements[i];
				Vector2 spawnPosition = new Vector2();
				Vector3[] positions = new Vector3[2];
				achievement.incompleteIndicatorLineRenderer.GetPositions(positions);
				spawnPosition = (positions[0] + positions[1]) / 2;
				AchievementIndicator achievementIndicator = Instantiate(achievementIndicatorPrefab, spawnPosition, Quaternion.identity);
				string text;
				if (!achievement.complete)
					text = achievement.GetProgress() + "/" + achievement.GetMaxProgress();
				else
					text = "Done";
				achievementIndicator.text.text = text;
			}
			if (LeaderboardMenu.Instance.gameObject.activeSelf && LeaderboardMenu.entryValueType == LeaderboardEntry.ValueType.Time)
				LeaderboardMenu.instance.ReloadLeaderboard ();
        }

        static Level GetLevelAtWrappedMapMoveInput (Vector2 input)
		{
			Level output = null;
			float closestManhattenDistance = 0;
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				Vector2 toLevel = level.trs.position - targetLevel.trs.position;
				float manhattenDistance = toLevel.GetManhattenMagnitude();
				if ((toLevel.x == 0 || toLevel.y == 0) && manhattenDistance > closestManhattenDistance && Vector2.Angle(toLevel, input) > 135)
				{
					output = level;
					closestManhattenDistance = manhattenDistance;
				}
			}
			return output;
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForRooms);
			GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForZones);
			if (GameCamera.Instance != null)
			{
				GameCamera.instance.enabled = true;
				GameCamera.instance.camera.enabled = true;
			}
			openPlayerSelectMenuButon.interactable = false;
		}

		void OnDestroy ()
		{
			GameManager.paused = false;
		}

		class CameraUpdater : IUpdatable
		{
			Transform cameraTrs;
			Vector2 destination;
			float lerpRate;

			public CameraUpdater (Transform cameraTrs, Vector2 destination, float lerpRate)
			{
				this.cameraTrs = cameraTrs;
				this.destination = destination;
				this.lerpRate = lerpRate;
			}

			public void DoUpdate ()
			{
				if (cameraTrs == null)
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					return;
				}
				float levelDistance = ((Vector2) cameraTrs.position - destination).magnitude;
				float moveAmount = levelDistance * lerpRate * Time.deltaTime;
				if (levelDistance > moveAmount)
					cameraTrs.position = Vector2.Lerp(cameraTrs.position.SetZ(0), destination, lerpRate * Time.deltaTime).SetZ(cameraTrs.position.z);
				else
				{
					cameraTrs.position = destination.SetZ(cameraTrs.position.z);
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}

		class BeginLevelUpdater : IUpdatable
		{
			public void DoUpdate ()
			{
				if ((Tutorial.instance == null || !Tutorial.instance.gameObject.activeSelf) && InputManager.MoveInput != Vector2.zero)
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					Level.instance.Begin ();
					instance.OnDisable ();
				}
			}
		}

		[Serializable]
		public struct Zone
		{
			public string name;
			public string description;
			public Rect boundsRect;
			public bool hasTutorialScene;
		}
	}
}