using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace FightRoom
{
	public class ShopMenu : Menu, IUpdatable
	{
		public BuyItemEntry buyItemEntryPrefab;
		public SellItemEntry sellItemEntryPrefab;
		public uint totalCost;
		public Item[] allItems = new Item[0];
		public float maxCostVariationPerItem;
		public Transform[] shopItemEntriesParents = new Transform[0];
		public Transform sellItemEntriesParent;
		public float multiplySellValue;
		public Button rerollButtton;
		public Text rerollCostText;
		public float rerollCostPerTotalCost;
		public uint addToTotalCost;
		public float multiplyCosts;
		public float addToMultiplyCosts;
		public byte openDuration;
		public Text openTimerText;
		public Text moneyText;
		public new static ShopMenu instance;
		public new static ShopMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<ShopMenu>(true);
				return instance;
			}
			set
			{
				instance = value;
			}
		}
		public static uint rerollCost;
		public static uint openedCountThisWave;
		public static BuyItemEntry[] buyItemEntries = new BuyItemEntry[ITEM_COUNT];
		public const byte ITEM_COUNT = 4;
		sbyte freeRerollsLeft;
		float openTimer;
		const uint MAX_OPEN_COUNT_PER_WAVE = 1;

		public override void Open ()
		{
			if (openedCountThisWave >= MAX_OPEN_COUNT_PER_WAVE)
				return;
			moneyText.text = "" + Player.instance.money;
			GameManager.updatables = GameManager.updatables.Remove(Duel.instance);
			if (openedCountThisWave == 0)
				Make ();
			UpdateButtons ();
			base.Open ();
			openedCountThisWave ++;
			if (openDuration > 0)
			{
				openTimer = openDuration;
				GameManager.updatables = GameManager.updatables.Add(this);
			}
			else
				GameManager.instance.StartTutorialIfShould (GameManager.instance.tutorialOnOpenedShop);
		}

		public override void Close ()
		{
			base.Close ();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			openTimer -= Time.deltaTime;
			openTimerText.text = "Game continues in " + (byte) openTimer + " seconds";
			if (openTimer <= 0)
				Close ();
		}

		public void Make ()
		{
			if (Player.instance.freeRerolls.GetValue() > 0)
			{
				freeRerollsLeft = (sbyte) Player.instance.freeRerolls.GetValue();
				rerollCost = 0;
			}
			else
				rerollCost = (uint) ((float) totalCost * rerollCostPerTotalCost * multiplyCosts);
			rerollCostText.text = "Reroll (" + Player.instance.haggling.Apply(rerollCost) + ")";
			List<Item> remainingItems = new List<Item>(allItems);
			while (true)
			{
				int indexOfNull = buyItemEntries.IndexOf<BuyItemEntry>(null);
				if (indexOfNull == -1)
					break;
				int itemIndex = Random.Range(0, remainingItems.Count);
				Item item = remainingItems[itemIndex];
				uint targetCost = (uint) ((float) totalCost / ITEM_COUNT);
				uint costVariation = (uint) (targetCost * maxCostVariationPerItem);
				UIntRange costRange = new UIntRange((uint) Mathf.Clamp(targetCost - costVariation, 0, targetCost), (uint) Mathf.Clamp(targetCost + costVariation, targetCost, uint.MaxValue));
				if (costRange.Contains(item.value, true))
				{
					bool hasItemLimit = false;
					if (item.maxCount > 0)
					{
						byte currentCount = 0;
						for (int i = 0; i < remainingItems.Count; i ++)
						{
							Item item2 = remainingItems[i];
							if (item.name == item2.name)
							{
								currentCount ++;
								if (currentCount >= item.maxCount)
								{
									hasItemLimit = true;
									break;
								}
							}
						}
					}
					if (!hasItemLimit)
					{
						BuyItemEntry itemEntry = Instantiate(buyItemEntryPrefab, shopItemEntriesParents[indexOfNull]);
						itemEntry.Init (item);
						buyItemEntries[indexOfNull] = itemEntry;
					}
				}
				remainingItems.RemoveAt(itemIndex);
				if (remainingItems.Count == 0)
					break;
			}
			for (int i = 0; i < buyItemEntries.Length; i ++)
				HandleChangesTrackedStatIndicator (buyItemEntries[i]);
			SellItemEntry[] sellItemEntries = sellItemEntriesParent.GetComponentsInChildren<SellItemEntry>();
			for (int i = 0; i < sellItemEntries.Length; i ++)
				HandleChangesTrackedStatIndicator (sellItemEntries[i]);
		}

		static void HandleChangesTrackedStatIndicator (ShopItemEntry shopItemEntry)
		{
			for (int i = 0; i < shopItemEntry.item.combinedWith.Length; i ++)
			{
				Item item = shopItemEntry.item.combinedWith[i];
				AddToStatItem addToStatItem = item as AddToStatItem;
				if (addToStatItem != null)
				{
					foreach (KeyValuePair<Stat, StatsMenu.TrackedStat> keyValuePair in StatsMenu.trackedStatsDict)
					{
						if (keyValuePair.Key.name == addToStatItem.statName)
						{
							shopItemEntry.changesTrackedStatIndicator.SetActive(true);
							return;
						}
					}
				}
			}
			shopItemEntry.changesTrackedStatIndicator.SetActive(false);
		}

		public void Reroll ()
		{
			Reroll (Player.instance);
		}

		public void Reroll (Player player)
		{
			player.AddMoney (player.haggling.Apply(-rerollCost));
			sbyte freeRerolls = freeRerollsLeft;
			freeRerolls --;
			for (int i = 0; i < buyItemEntries.Length; i ++)
			{
				BuyItemEntry itemEntry = buyItemEntries[i];
				if (itemEntry != null && !itemEntry.Locked)
					Destroy(itemEntry.gameObject);
			}
			Make ();
			freeRerollsLeft = freeRerolls;
			if (freeRerolls <= 0)
			{
				rerollCost = (uint) ((float) totalCost * rerollCostPerTotalCost * multiplyCosts);
				rerollCostText.text = "Reroll (" + player.haggling.Apply(rerollCost) + ")";
			}
		}

		public void UpdateButtons ()
		{
			rerollButtton.interactable = Player.instance.money >= Player.instance.haggling.Apply(rerollCost);
			for (int i = 0; i < buyItemEntries.Length; i ++)
			{
				BuyItemEntry itemEntry = buyItemEntries[i];
				if (itemEntry != null)
					itemEntry.buyButton.interactable = Player.instance.money >= Player.instance.haggling.Apply((float) itemEntry.item.value * multiplyCosts);
			}
		}
	}
}