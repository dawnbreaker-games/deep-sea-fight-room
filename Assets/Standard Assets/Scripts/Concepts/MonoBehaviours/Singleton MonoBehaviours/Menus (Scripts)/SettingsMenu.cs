using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class SettingsMenu : Menu
	{
		public Button[] difficultyButtons = new Button[0];
		public Sprite selectedDifficultySprite;
		public Sprite notSelectedDifficultySprite;
		public Toggle lightToggle;
		public Toggle aimAssistToggle;
		public Toggle showAimDirection;
		public Toggle waterRipplesToggle;
		public _Slider volumeSlider;
		public new static SettingsMenu instance;
		public new static SettingsMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<SettingsMenu>(true);
				return instance;
			}
		}
		public static bool AllowLights
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Allow lights", true);
			}
			set
			{
				SetLightsActive (value);
				PlayerPrefsExtensions.SetBool ("Allow lights", value);
			}
		}
		public static bool AimAssist
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Aim assist", true);
			}
			set
			{
				if (!value)
					PlayerPrefsExtensions.SetBool ("Disabled aim assist", true);
				PlayerPrefsExtensions.SetBool ("Aim assist", value);
			}
		}
		public static bool ShowAimDirection
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Show aim direction", true);
			}
			set
			{
				_ShowAimDirection (value);
				PlayerPrefsExtensions.SetBool ("Show aim direction", value);
			}
		}
		public static bool AllowWaterRipples
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Allow water ripples", true);
			}
			set
			{
				SetWaterRipplesActive (value);
				PlayerPrefsExtensions.SetBool ("Allow water ripples", value);
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			lightToggle.isOn = AllowLights;
			aimAssistToggle.isOn = AimAssist;
			showAimDirection.isOn = ShowAimDirection;
			waterRipplesToggle.isOn = AllowWaterRipples;
			volumeSlider.slider.value = AudioManager.Volume;
		}

		public override void Open ()
		{
			base.Open ();
			int difficultyIndex = GameManager.instance.allDifficultySettings.IndexOf(SaveAndLoadManager.saveData.difficultySettings);
			if (difficultyIndex > -1)
				((Image) difficultyButtons[difficultyIndex].targetGraphic).sprite = selectedDifficultySprite;
		}

		public override void Close ()
		{
			gameObject.SetActive(false);
			if (!MapMenu.instance.gameObject.activeSelf)
				GameCamera.instance.camera.enabled = true;
			else
				MapMenu.ShouldUpdate = true;
		}

		public static void SetLightsActive (bool allow)
		{
			Light[] lights = FindObjectsByType<Light>(FindObjectsInactive.Include, FindObjectsSortMode.None);
			for (int i = 0; i < lights.Length; i ++)
			{
				Light light = lights[i];
				light.gameObject.SetActive(allow);
			}
		}

		public static void _ShowAimDirection (bool show)
		{
			Player[] players = FindObjectsByType<Player>(FindObjectsInactive.Include, FindObjectsSortMode.None);
			for (int i = 0; i < players.Length; i ++)
			{
				Player player = players[i];
				player.aimIndicatorTrs.gameObject.SetActive(show);
			}
		}

		public static void UpdateAimIndicator ()
		{
			_ShowAimDirection (ShowAimDirection);
		}

		public static void SetWaterRipplesActive (bool allow)
		{
			if (RippleMesh.instance != null)
			{
				RippleMesh.instance.enabled = allow;
				MeshRippler[] meshRipplers = FindObjectsByType<MeshRippler>(FindObjectsInactive.Include, FindObjectsSortMode.None);
				for (int i = 0; i < meshRipplers.Length; i ++)
				{
					MeshRippler meshRippler = meshRipplers[i];
					meshRippler.enabled = allow;
				}
			}
		}
	}
}