using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class LoginAndRegisterMenu : SingletonMonoBehaviour<LoginAndRegisterMenu>
	{
		public TMP_InputField[] usernameInputFields = new TMP_InputField[0];
		public TMP_InputField[] passwordInputFields = new TMP_InputField[0];
		public Toggle[] rememberLoginInfoToggles = new Toggle[0];
		public Button[] submitButtons = new Button[0];
		public GameObject[] activateGosOnLogin = new GameObject[0];
		public GameObject[] deactivateGosOnLogin = new GameObject[0];
		public Selectable[] activateSelectablesOnLogin = new Selectable[0];
		public Selectable[] deactivateSelectablesOnLogin = new Selectable[0];
		public static string RememberedUsername
		{
			get
			{
				return PlayerPrefs.GetString("Username");
			}
			set
			{
				PlayerPrefs.SetString("Username", value);
			}
		}
		public static string RememberedPassword
		{
			get
			{
				return PlayerPrefs.GetString("Password");
			}
			set
			{
				PlayerPrefs.SetString("Password", value);
			}
		}
		public static bool RememberLoginInfo
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Remember login info", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Remember login info", value);
			}
		}
		public static string username = "";
		static string password = "";
		static DatabaseObject dbObj;

		public override void Awake ()
		{
			base.Awake ();
			if (RememberLoginInfo)
			{
				for (int i = 0; i < usernameInputFields.Length; i ++)
				{
					TMP_InputField usernameInputField = usernameInputFields[i];
					usernameInputField.text = RememberedUsername;
				}
				for (int i = 0; i < passwordInputFields.Length; i ++)
				{
					TMP_InputField passwordInputField = passwordInputFields[i];
					passwordInputField.text = RememberedPassword;
				}
			}
			for (int i = 0; i < rememberLoginInfoToggles.Length; i ++)
			{
				Toggle rememberLoginInfoToggle = rememberLoginInfoToggles[i];
				rememberLoginInfoToggle.isOn = RememberLoginInfo;
			}
		}

		public void SetUsername (string username)
		{
			if (RememberLoginInfo)
				RememberedUsername = username;
			LoginAndRegisterMenu.username = username;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetPassword (string password)
		{
			if (RememberLoginInfo)
				RememberedPassword = password;
			LoginAndRegisterMenu.password = password;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetRememberLoginInfo (bool remember)
		{
			RememberLoginInfo = remember;
			if (remember)
			{
				RememberedUsername = username;
				RememberedPassword = password;
			}
		}

		public void Register ()
		{
			NetworkManager.Connect (OnConnectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnConnectSuccess_Register (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.BigDB.LoadOrCreate("Usernames", "usernames", OnLoadOrCreateUsernamesDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnLoadOrCreateUsernamesDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			DatabaseArray usernamesDBArray = dbObj.GetOrMakeValue("usernames", new DatabaseArray());
			if (usernamesDBArray.IndexOf(username) == -1)
				usernamesDBArray.Add(username);
			else
			{
				GameManager.instance.DisplayNotification ("An account with that username already exists!");
				return;
			}
			LoginAndRegisterMenu.dbObj = dbObj;
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnSaveDBObjectSuccess_Register ()
		{
			NetworkManager.client.BigDB.LoadOrCreate("PlayerObjects", username, OnLoadOrCreatePlayerDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnSaveDBObjectFail_Register (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnLoadOrCreatePlayerDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			dbObj.GetOrMakeValue ("password", password);
			dbObj.GetOrMakeValue ("isPublic", true);
			dbObj.GetOrMakeValue ("times", new DatabaseArray());
			dbObj.GetOrMakeValue ("totalTime", (float) 0);
			dbObj.GetOrMakeValue ("totalEnemiesKilled", (uint) 0);
			dbObj.GetOrMakeValue ("tasksDone", (uint) 0);
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
			GameManager.instance.DisplayNotification ("Account created and you are now logged in! While logged in you can see other people's scores and progress in the leaderboard and also they can see yours. You can hide your account from the public in the settings.");
			// GameManager.instance.DisplayNotification ("Account created and you are now logged in! While logged in do you want your scores and progress to be shown on the leaderboard so that other players can see?");
			// GameManager.instance.DisplayChoices (new string[] { "Yes", "No" }, new Action[] { () => { LeaderboardMenu.instance.MakeLocalAccountUserInfoPublic (true); }, () => { LeaderboardMenu.instance.MakeLocalAccountUserInfoPublic (false); }});
			OnLogin ();
		}

		void OnRegisterOrLoginFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
		}

		public void Login ()
		{
			NetworkManager.Connect (OnConnectSuccess_Login, OnRegisterOrLoginFail);
		}

		public void OnConnectSuccess_Login (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.BigDB.Load("PlayerObjects", username, OnLoadPlayerDBObjectSuccess_Login, OnRegisterOrLoginFail);
		}

		void OnLoadPlayerDBObjectSuccess_Login (DatabaseObject dbObj)
		{
			if (dbObj.GetString("password") != password)
			{
				GameManager.instance.DisplayNotification ("Incorrect login info.");
				return;
			}
			LocalUserInfo.username = username;
			LocalUserInfo.totalTime = dbObj.GetFloat("totalTime");
			LocalUserInfo.totalEnemiesKilled = dbObj.GetUInt("totalEnemiesKilled");
			LocalUserInfo.tasksDone = dbObj.GetUInt("tasksDone");
			LocalUserInfo.isPublic = dbObj.GetBool("isPublic");
			GameManager.instance.DisplayNotification ("You are now logged in! While logged in you can see other people's scores and progress in the leaderboard and also they can see yours. You can hide your account from the public in the settings.");
			OnLogin ();
		}

		void OnLogin ()
		{
			for (int i = 0; i < activateGosOnLogin.Length; i ++)
			{
				GameObject go = activateGosOnLogin[i];
				go.SetActive(true);
			}
			for (int i = 0; i < deactivateGosOnLogin.Length; i ++)
			{
				GameObject go = deactivateGosOnLogin[i];
				go.SetActive(false);
			}
			for (int i = 0; i < activateSelectablesOnLogin.Length; i ++)
			{
				Selectable selectable = activateSelectablesOnLogin[i];
				selectable.interactable = true;
			}
			for (int i = 0; i < deactivateSelectablesOnLogin.Length; i ++)
			{
				Selectable selectable = deactivateSelectablesOnLogin[i];
				selectable.interactable = false;
			}
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
		}

		public void Logout ()
		{
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
			}
			LocalUserInfo.username = null;
			NetworkManager.Disconnect ();
		}
	}
}