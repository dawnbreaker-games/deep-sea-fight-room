using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class TutorialsMenu : SingletonMonoBehaviour<TutorialsMenu>
	{
		public Button[] zoneTutorialButtons = new Button[0];
		public SerializableDictionary<string, Button> nonZoneTutorialButtonsDict = new SerializableDictionary<string, Button>();
		public static bool wasActiveOnExitScene;
		public static string currentTutorialName;

		public override void Awake ()
		{
			base.Awake ();
			SaveAndLoadManager.Instance.LoadMostRecent ();
			nonZoneTutorialButtonsDict.Init ();
			foreach (KeyValuePair<string, Button> keyValuePair in nonZoneTutorialButtonsDict)
				keyValuePair.Value.interactable = SaveAndLoadManager.saveData.triedTutorials.Contains(keyValuePair.Key);
			for (int i = 0; i < zoneTutorialButtons.Length; i ++)
			{
				Button zoneTutorialButton = zoneTutorialButtons[i];
				zoneTutorialButton.interactable = SaveAndLoadManager.saveData.triedZones.Contains("Zone " + (i + 1));
			}
		}
	}
}