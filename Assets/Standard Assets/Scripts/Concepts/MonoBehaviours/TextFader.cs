using FightRoom;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

public class TextFader : MonoBehaviour
{
	public Text text;
	public float alphaWhenCollidersInside;
	uint collidersInside;

#if UNITY_EDITOR
	void OnValidate ()
	{
		if (text == null)
			text = GetComponent<Text>();
	}
#endif

	void OnTriggerEnter2D (Collider2D other)
	{
		collidersInside ++;
		if (collidersInside == 1)
			text.color = text.color.SetAlpha (alphaWhenCollidersInside);
	}

	void OnTriggerExit2D (Collider2D other)
	{
		collidersInside --;
		if (collidersInside == 0)
			text.color = text.color.SetAlpha (1);
	}
}