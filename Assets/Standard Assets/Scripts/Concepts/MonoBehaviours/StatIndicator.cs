using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class StatIndicator : MonoBehaviour
	{
		public Text text;
		public Text tooltipText;
		public StatIndicator childStatIndicator;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (Application.isPlaying)
				return;
			if (text == null)
				text = GetComponent<Text>();
			if (tooltipText == null)
				tooltipText = transform.GetChild(0).GetComponentInChildren<Text>(true);
			if (childStatIndicator == null)
			{
				StatIndicator[] statIndicators = GetComponentsInChildren<StatIndicator>();
				if (statIndicators.Length > 1)
					childStatIndicator = statIndicators[1];
			}
		}
#endif
    }
}