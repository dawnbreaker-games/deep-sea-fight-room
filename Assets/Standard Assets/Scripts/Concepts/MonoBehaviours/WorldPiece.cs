using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class WorldPiece : MonoBehaviour
	{
		public Transform trs;
		[HideInInspector]
		public Vector2Int location;
		[HideInInspector]
		public RectInt rect;
		[HideInInspector]
		public WorldPiece[] piecesToLoadAndUnloadWithMe = new WorldPiece[0];

		void OnDrawGizmosSelected ()
		{
			Gizmos.color = Color.green;
			Gizmos.DrawWireCube(rect.center, rect.size.ToVec3());
		}
	}
}