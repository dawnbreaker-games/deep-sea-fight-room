﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class DisableObjectBasedOnPlatform : MonoBehaviour
	{
		public RuntimePlatform[] platforms = new RuntimePlatform[0];
		public bool disableIfUsing;

		void Awake ()
		{
			for (int i = 0; i < platforms.Length; i ++)
			{
				RuntimePlatform platform = platforms[i];
				if (platform == Application.platform == disableIfUsing)
				{
					gameObject.SetActive(false);
					return;
				}
			}
		}
	}
}