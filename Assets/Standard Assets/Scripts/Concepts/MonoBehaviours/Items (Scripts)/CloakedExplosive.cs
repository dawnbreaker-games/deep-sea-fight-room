using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class CloakedExplosive : Item, IUpdatable
	{
		public float makeBombInterval;
		public SpawnObject spawnObject;
		float makeBombTimer;
		Player owner;

		public override void OnGain (Player player)
		{
			owner = player;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			makeBombTimer -= Time.deltaTime;
			if (makeBombTimer <= 0)
			{
				makeBombTimer += makeBombInterval;
				MakeBomb ();
			}
		}

		void MakeBomb ()
		{
			spawnObject.OnGain (owner);
		}

		public override void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}