namespace FightRoom
{
	public class ReliableMagnet : Item
	{
		public static uint ownedCount;
		public const uint AMOUNT_TO_PICKUP = 1;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			ownedCount ++;
		}

		public override void OnDisable ()
		{
			ownedCount --;
		}
	}
}