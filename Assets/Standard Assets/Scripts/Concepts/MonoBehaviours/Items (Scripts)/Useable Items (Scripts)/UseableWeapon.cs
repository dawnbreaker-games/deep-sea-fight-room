namespace FightRoom
{
	public class UseableWeapon : UseableItem
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;
		public bool autoShoot;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			if (!autoShoot)
				Player.instance.bulletPatternEntriesSortedList.Add(bulletPatternEntry.name, bulletPatternEntry);
		}

		public override void Use ()
		{
            base.Use ();
			if (autoShoot)
	            bulletPatternEntry.Shoot ();
			animationEntry.Play ();
		}
	}
}