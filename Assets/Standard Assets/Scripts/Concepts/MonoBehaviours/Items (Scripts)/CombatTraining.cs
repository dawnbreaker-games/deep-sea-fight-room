using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class CombatTraining : Item
	{
		public static byte ownedCount;
		public const byte DAMAGE_AMOUNT = 4;
		public const string REPLACE_WITH_DAMAGE_AMOUNT = "_";

		public override void OnGain (Player player)
		{
			ownedCount ++;
		}

		public override string ToString ()
		{
			return base.ToString().Replace(REPLACE_WITH_DAMAGE_AMOUNT, "" + DAMAGE_AMOUNT);
		}
	}
}