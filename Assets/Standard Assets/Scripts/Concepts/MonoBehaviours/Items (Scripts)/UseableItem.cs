using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;

namespace FightRoom
{
	public class UseableItem : Item
	{
		public InputAction useAction;
		public float cooldown;
		[HideInInspector]
		public float cooldownRemaining;
		public float maxUseTime;
		public Image cooldownIndicatorImage;
		float lastUseTime;
		UseUpdater useUpdater;
		CooldownUpdater cooldownUpdater;

		public override void OnGain (Player player)
		{
			useAction.Enable();
			if (cooldown > 0)
				cooldownIndicatorImage.rectTransform.parent.gameObject.SetActive(true);
		}

		public void TryToUse (InputAction.CallbackContext context = default(InputAction.CallbackContext))
		{
			if (!GameManager.paused && useUpdater == null && GameManager.UnscaledTimeSinceLevelLoad - lastUseTime >= cooldown)
				Use ();
		}

		public virtual void Use ()
		{
			lastUseTime = GameManager.UnscaledTimeSinceLevelLoad;
			if (cooldownIndicatorImage != null)
				cooldownIndicatorImage.fillAmount = 0;
			if (maxUseTime > 0)
			{
				if (useUpdater == null)
					OnBeganUsing ();
			}
			else
			{
				cooldownUpdater = new CooldownUpdater(this);
				GameManager.updatables = GameManager.updatables.Add(cooldownUpdater);
				if (Duel.instance != null && Player.instance.useableItems.Contains(this))
					NetworkManager.connection.Send("Use Ability");
			}
		}

		public virtual void OnBeganUsing ()
		{
			useUpdater = new UseUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(useUpdater);	
			if (Duel.instance != null && Player.instance.useableItems.Contains(this))
				NetworkManager.connection.Send("Begin Use Ability");
		}

		public virtual void OnStopUsing ()
		{
			float timeOvershoot = GameManager.UnscaledTimeSinceLevelLoad - lastUseTime - maxUseTime;
			if (timeOvershoot > 0)
				lastUseTime = GameManager.UnscaledTimeSinceLevelLoad - timeOvershoot;
			GameManager.updatables = GameManager.updatables.Remove(useUpdater);
			useUpdater = null;
			cooldownUpdater = new CooldownUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(cooldownUpdater);
			if (Duel.instance != null && Player.instance.useableItems.Contains(this))
				NetworkManager.connection.Send("Stop Use Ability");
			else
				PlayerPrefs.SetInt("Used ability", PlayerPrefs.GetInt("Used ability", 0) + 1);
		}

		public override void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(useUpdater);
			GameManager.updatables = GameManager.updatables.Remove(cooldownUpdater);
			useAction.Disable();
		}

		public override string ToString ()
		{
			string output = base.ToString() + " ";
			if (maxUseTime > 0)
			{
				output += "Max use time: " + maxUseTime;
				if (maxUseTime < Mathf.Infinity)
					output += "s. ";
				else
					output += " ";
			}
			output += "Cooldown: " + cooldown + "s.\n";
			return output;
		}

		class UseUpdater : IUpdatable
		{
			UseableItem useableItem;
			float usedTime;

			public UseUpdater (UseableItem useableItem)
			{
				this.useableItem = useableItem;
			}

			public void DoUpdate ()
			{
				if (!useableItem.useAction.enabled || useableItem.useAction.ReadValue<float>() == 1)
				{
					useableItem.Use ();
					usedTime += Time.unscaledDeltaTime;
					if (usedTime >= useableItem.maxUseTime)
						useableItem.OnStopUsing ();
				}
				else
					useableItem.OnStopUsing ();
			}
		}

		class CooldownUpdater : IUpdatable
		{
			UseableItem useableItem;

			public CooldownUpdater (UseableItem useableItem)
			{
				this.useableItem = useableItem;
			}

			public void DoUpdate ()
			{
				if (useableItem == null || useableItem.cooldownIndicatorImage == null)
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					return;
				}
				useableItem.cooldownIndicatorImage.fillAmount = (GameManager.UnscaledTimeSinceLevelLoad - useableItem.lastUseTime) / useableItem.cooldown;
				if (GameManager.UnscaledTimeSinceLevelLoad - useableItem.lastUseTime >= useableItem.cooldown)
				{
					useableItem.cooldownIndicatorImage.fillAmount = 1;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}