using UnityEngine;

namespace FightRoom
{
	public class UnreliableMagnet : Item
	{
		public static uint ownedCount;
		public const float PICKUP_CHANCE = 0.15f;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			ownedCount ++;
		}

		public override void OnDisable ()
		{
			ownedCount --;
		}
	}
}