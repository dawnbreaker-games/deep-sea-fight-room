using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class DamagePlayerOverTimeItem : Item, IUpdatable
	{
		public float amount;
		public const string DAMAGE_INDICATOR_IN_DESCRIPTION = "~";

		public override string ToString ()
		{
			description = description.Replace(DAMAGE_INDICATOR_IN_DESCRIPTION, "" + amount);
			return base.ToString();
		}

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			Player.instance.TakeDamage (amount * Time.deltaTime);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}