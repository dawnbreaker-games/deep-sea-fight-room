namespace FightRoom
{
	public class AddToMaterialistic : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.materialistic.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.materialistic.AddToValue (-amount);
		}
	}
}