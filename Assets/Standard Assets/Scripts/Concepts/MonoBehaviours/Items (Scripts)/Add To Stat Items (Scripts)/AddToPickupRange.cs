namespace FightRoom
{
	public class AddToPickupRange : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.pickupRange.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.pickupRange.AddToValue (-amount);
		}
	}
}