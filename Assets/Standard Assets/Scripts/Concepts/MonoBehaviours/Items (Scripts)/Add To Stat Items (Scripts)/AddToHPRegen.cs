namespace FightRoom
{
	public class AddToHPRegen : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.hpRegen.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.hpRegen.AddToValue (-amount);
		}
	}
}