namespace FightRoom
{
	public class AddToArmor : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.armor.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.armor.AddToValue (-amount);
		}
	}
}