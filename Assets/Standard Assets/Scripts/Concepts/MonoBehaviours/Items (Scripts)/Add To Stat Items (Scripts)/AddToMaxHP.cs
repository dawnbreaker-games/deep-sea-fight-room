using UnityEngine;

namespace FightRoom
{
	public class AddToMaxHP : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance._maxHp.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance._maxHp.AddToValue (-amount);
		}
	}
}