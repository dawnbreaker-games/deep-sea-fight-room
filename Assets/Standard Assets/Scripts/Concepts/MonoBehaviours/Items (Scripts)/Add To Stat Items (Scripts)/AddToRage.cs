namespace FightRoom
{
	public class AddToRage : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.rage.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.rage.AddToValue (-amount);
		}
	}
}