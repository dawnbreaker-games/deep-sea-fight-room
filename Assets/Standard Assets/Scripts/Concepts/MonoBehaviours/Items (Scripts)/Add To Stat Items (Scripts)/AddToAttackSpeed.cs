namespace FightRoom
{
	public class AddToAttackSpeed : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.attackSpeed.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.attackSpeed.AddToValue (-amount);
		}
	}
}