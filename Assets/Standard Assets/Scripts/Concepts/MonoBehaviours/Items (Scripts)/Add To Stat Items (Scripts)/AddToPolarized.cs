namespace FightRoom
{
	public class AddToPolarized : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.polarized.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.polarized.AddToValue (-amount);
		}
	}
}