using UnityEngine;

namespace FightRoom
{
	public class AddToHaggling : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.haggling.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.haggling.AddToValue (-amount);
		}
	}
}