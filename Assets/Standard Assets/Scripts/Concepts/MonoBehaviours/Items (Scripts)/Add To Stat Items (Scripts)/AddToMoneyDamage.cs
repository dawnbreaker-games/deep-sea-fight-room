using UnityEngine;

namespace FightRoom
{
	public class AddToMoneyDamage : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.moneyDamage.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.moneyDamage.AddToValue (-amount);
		}
	}
}