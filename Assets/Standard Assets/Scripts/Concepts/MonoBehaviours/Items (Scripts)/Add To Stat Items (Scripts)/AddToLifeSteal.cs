namespace FightRoom
{
	public class AddToLifeSteal : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.lifeSteal.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.lifeSteal.AddToValue (-amount);
		}
	}
}