namespace FightRoom
{
	public class AddToPiercing : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.piercing.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.piercing.AddToValue (-amount);
		}
	}
}