namespace FightRoom
{
	public class AddToEnemyMoney : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.enemyMoney.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.enemyMoney.AddToValue (-amount);
		}
	}
}