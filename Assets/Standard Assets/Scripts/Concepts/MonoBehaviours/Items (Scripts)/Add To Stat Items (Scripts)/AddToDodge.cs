namespace FightRoom
{
	public class AddToDodge : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.dodge.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.dodge.AddToValue (-amount);
		}
	}
}