namespace FightRoom
{
	public class AddToMastery : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.mastery.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.mastery.AddToValue (-amount);
		}
	}
}