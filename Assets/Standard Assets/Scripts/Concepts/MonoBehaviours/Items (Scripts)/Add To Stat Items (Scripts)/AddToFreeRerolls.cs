namespace FightRoom
{
	public class AddToFreeRerolls : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.freeRerolls.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.freeRerolls.AddToValue (-amount);
		}
	}
}