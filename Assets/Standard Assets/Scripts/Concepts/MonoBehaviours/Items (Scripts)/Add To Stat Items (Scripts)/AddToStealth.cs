namespace FightRoom
{
	public class AddToStealth : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.stealth.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.stealth.AddToValue (-amount);
		}
	}
}