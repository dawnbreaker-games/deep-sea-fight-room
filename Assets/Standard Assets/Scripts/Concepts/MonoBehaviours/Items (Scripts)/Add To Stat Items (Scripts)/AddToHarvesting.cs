namespace FightRoom
{
	public class AddToHarvesting : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.harvesting.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.harvesting.AddToValue (-amount);
		}
	}
}