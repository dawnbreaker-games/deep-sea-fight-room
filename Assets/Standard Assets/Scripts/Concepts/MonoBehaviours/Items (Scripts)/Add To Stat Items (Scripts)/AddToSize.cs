namespace FightRoom
{
	public class AddToSize : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.size.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.size.AddToValue (-amount);
		}
	}
}