namespace FightRoom
{
	public class AddToUnbalanced : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.unbalanced.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.unbalanced.AddToValue (-amount);
		}
	}
}