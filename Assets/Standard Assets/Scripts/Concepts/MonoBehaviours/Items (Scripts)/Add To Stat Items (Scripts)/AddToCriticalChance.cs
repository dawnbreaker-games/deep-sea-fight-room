namespace FightRoom
{
	public class AddToCriticalChance : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.criticalChance.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.criticalChance.AddToValue (-amount);
		}
	}
}