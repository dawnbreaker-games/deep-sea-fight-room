using UnityEngine;

namespace FightRoom
{
	public class AddToDamage : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance.damage.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance.damage.AddToValue (-amount);
		}
	}
}