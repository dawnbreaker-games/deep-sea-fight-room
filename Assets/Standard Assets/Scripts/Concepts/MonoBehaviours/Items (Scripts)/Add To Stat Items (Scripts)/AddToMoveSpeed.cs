namespace FightRoom
{
	public class AddToMoveSpeed : AddToStatItem
	{
		public override void OnGain (Player player)
		{
			Player.instance._moveSpeed.AddToValue (amount);
		}

		public override void OnDisable ()
		{
			Player.instance._moveSpeed.AddToValue (-amount);
		}
	}
}