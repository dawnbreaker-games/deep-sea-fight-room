namespace FightRoom
{
	public class PiggyBank : Item
	{
		public static uint ownedCount;
		public const byte SAVE_AMOUNT = 20;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			ownedCount ++;
		}

		public override void OnDisable ()
		{
			ownedCount --;
		}
	}
}