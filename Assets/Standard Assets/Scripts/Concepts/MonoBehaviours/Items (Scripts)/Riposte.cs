namespace FightRoom
{
	public class Riposte : Item
	{
		public static byte ownedCount;
		public const float DAMAGE = 3;
		public const string DAMAGE_INDICATOR_IN_DESCRIPTION = "~";

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			ownedCount ++;
		}

		public override void OnDisable ()
		{
			ownedCount --;
		}

		public override string ToString ()
		{
			return base.ToString().Replace(DAMAGE_INDICATOR_IN_DESCRIPTION, "" + DAMAGE);
		}
	}
}