using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class RadiationWeapon : Weapon, IUpdatable
	{
		public float damageOverTime;
		public float distanceScale;

		public override void OnGain (Player player)
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			for (int i = 0; i < Enemy.instances.Count; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				Vector2 toEnemy = enemy.trs.position - Player.instance.trs.position;
				float damage = distanceScale / toEnemy.magnitude * damageOverTime * animationEntry.animator.GetFloat("attackSpeed") * Time.deltaTime;
				Player.instance.ApplyDamage (enemy, damage);
			}
		}

		public override void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}