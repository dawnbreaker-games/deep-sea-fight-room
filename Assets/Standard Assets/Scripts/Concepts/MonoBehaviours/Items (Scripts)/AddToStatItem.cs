namespace FightRoom
{
	public class AddToStatItem : Item
	{
		public bool useDescriptionBeforeStatName;
		public string statName;
		public int amount;

		public override string ToString ()
		{
			string output = "";
			if (useDescriptionBeforeStatName)
				output = description + "\n";
			if (amount > 0)
				output = "+";
			output += "" + amount + " " + statName;
			if (!useDescriptionBeforeStatName)
				output += "\n" + description;
			return output;
		}
	}
}