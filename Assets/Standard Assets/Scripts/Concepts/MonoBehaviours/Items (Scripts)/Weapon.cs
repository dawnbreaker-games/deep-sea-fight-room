using UnityEngine;

namespace FightRoom
{
	public class Weapon : Item
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;

		public override void OnGain (Player player)
		{
			bulletPatternEntry.spawner = player.aimIndicatorTrs;
			animationEntry.animator = player.animator;
			player.bulletPatternEntriesSortedList[bulletPatternEntry.name] = bulletPatternEntry;
			if (animationEntry.animator != null)
				animationEntry.Play ();
		}

		public override void OnDisable ()
		{
			Player.instance.bulletPatternEntriesSortedList.Remove(bulletPatternEntry.name);
			if (animationEntry.animator != null)
				animationEntry.animator.Play("None", animationEntry.layer);
		}

		public override string ToString ()
		{
			string output = base.ToString();
			Bullet bulletPrefab = bulletPatternEntry.bulletPrefab;
			if (bulletPrefab != null)
			{
				output += " Projectile move speed: " + bulletPrefab.moveSpeed + ". Projectile damage: " + bulletPrefab.damage + ".";
				if (bulletPrefab.autoDespawnMode == Bullet.AutoDespawnMode.RangedAutoDespawn)
					output +=  " Projectile range: " + bulletPrefab.range + ".";
				else if (bulletPrefab.autoDespawnMode == Bullet.AutoDespawnMode.DelayedAutoDespawn)
					output += " Projectile range: " + bulletPrefab.duration * bulletPrefab.moveSpeed + ".";
			}
			output += " Cooldown: " + animationEntry.duration + "s.\n";
			return output;
		}
	}
}