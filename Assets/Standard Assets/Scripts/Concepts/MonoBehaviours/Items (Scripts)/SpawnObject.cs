using UnityEngine;

namespace FightRoom
{
	public class SpawnObject : Item
	{
		public GameObject prefab;

		public override void OnGain (Player player)
		{
			GameObject go = Instantiate(prefab, Player.instance.trs.position, Quaternion.identity);
			Level.gosSpawnedThisWave.Add(go);
		}
	}
}