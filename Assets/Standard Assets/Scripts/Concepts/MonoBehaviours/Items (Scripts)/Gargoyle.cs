using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class Gargoyle : Item, IUpdatable
	{
		bool previousIsPlayerMoving;
		Player owner;

		public override void OnGain (Player player)
		{
			previousIsPlayerMoving = InputManager.MoveInput != Vector2.zero;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			bool isPlayerMoving = InputManager.MoveInput != Vector2.zero;
			if (!isPlayerMoving && previousIsPlayerMoving)
			{
				for (int i = 0; i < combinedWith.Length; i ++)
				{
					Item item = combinedWith[i];
					item.OnGain (owner);
				}
			}
			else if (isPlayerMoving && !previousIsPlayerMoving)
			{
				for (int i = 0; i < combinedWith.Length; i ++)
				{
					Item item = combinedWith[i];
					item.OnDisable ();
				}
			}
			previousIsPlayerMoving = isPlayerMoving;
		}

		public override void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (!previousIsPlayerMoving)
			{
				for (int i = 0; i < combinedWith.Length; i ++)
				{
					Item item = combinedWith[i];
					item.OnGain (owner);
				}
			}
		}
	}
}