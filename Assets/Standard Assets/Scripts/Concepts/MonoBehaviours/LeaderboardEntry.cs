using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class LeaderboardEntry : MonoBehaviour
	{
		public Text rankText;
		public Text valueText;
		public Text usernameText;
		public GameObject[] valueTypeIndicators = new GameObject[0];

		public enum ValueType
		{
			Time,
			TotalTime,
			TotalEnemiesKilled,
			Tasks
		}
	}
}