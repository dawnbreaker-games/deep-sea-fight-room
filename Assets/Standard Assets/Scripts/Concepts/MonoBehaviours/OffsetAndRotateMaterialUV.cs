using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class OffsetAndRotateMaterialUV : MonoBehaviour
{
	public Renderer renderer;
	public Vector2 offset;
	public float rotate;

	void OnEnable ()
	{
#if UNITY_EDITOR
		if (renderer == null)
			renderer = GetComponent<Renderer>();
#endif
		Material matarial = new Material(renderer.sharedMaterial);
		matarial.SetVector("_offsetUV", offset);
		matarial.SetFloat("_rotateUV", rotate);
		renderer.sharedMaterial = matarial;
	}
}
