using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace FightRoom
{
	public class KillingAchievement : Achievement
	{
		public uint enemiesKilled;
		public uint levelsThatNeedToReachEnemiesKilled;

		public override uint GetProgress ()
		{
			if (levelsThatNeedToReachEnemiesKilled == 0)
			{
				float totalEnemiesKilled = 0;
				for (int i = 0; i < levels.Length; i ++)
				{
					Level level = levels[i];
					totalEnemiesKilled += level.MostEnemiesKilled;
				}
				return (uint) totalEnemiesKilled;
			}
			else
			{
				uint levelsThatReachEnemiesKilled = 0;
				for (int i = 0; i < levels.Length; i ++)
				{
					Level level = levels[i];
					if (level.MostEnemiesKilled >= enemiesKilled)
						levelsThatReachEnemiesKilled ++;
				}
				return levelsThatReachEnemiesKilled;
			}
		}

		public override uint GetMaxProgress ()
		{
			if (levelsThatNeedToReachEnemiesKilled > 0)
				return levelsThatNeedToReachEnemiesKilled;
			else
				return enemiesKilled;
		}
	}
}