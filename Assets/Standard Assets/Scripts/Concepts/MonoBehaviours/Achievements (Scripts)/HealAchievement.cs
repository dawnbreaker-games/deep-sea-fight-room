namespace FightRoom
{
	public class HealAchievement : Achievement
	{
		public uint amount;
		public uint levelsThatNeedToReachAmount;

		public override uint GetProgress ()
		{
			if (levelsThatNeedToReachAmount == 0)
			{
				float totalAmount = 0;
				for (int i = 0; i < levels.Length; i ++)
				{
					Level level = levels[i];
					totalAmount += level.MostDamageHealed;
				}
				return (uint) totalAmount;
			}
			else
			{
				uint levelsThatReachAmount = 0;
				for (int i = 0; i < levels.Length; i ++)
				{
					Level level = levels[i];
					if (level.MostDamageHealed >= amount)
						levelsThatReachAmount ++;
				}
				return levelsThatReachAmount;
			}
		}

		public override uint GetMaxProgress ()
		{
			if (levelsThatNeedToReachAmount > 0)
				return levelsThatNeedToReachAmount;
			else
				return amount;
		}
	}
}