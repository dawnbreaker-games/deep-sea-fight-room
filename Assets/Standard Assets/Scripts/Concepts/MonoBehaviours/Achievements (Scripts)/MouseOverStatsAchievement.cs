using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace FightRoom
{
	public class MouseOverStatsAchievement : Achievement
	{
		public byte statCount;

		public override uint GetProgress ()
		{
			return (uint) SaveAndLoadManager.saveData.mousedOverStats.Length;
		}

		public override uint GetMaxProgress ()
		{
			return statCount;
		}
	}
}