using System;
using Extensions;
using UnityEngine;

namespace FightRoom
{
	public class PlayerPrefsAchievement : Achievement
	{
		public PlayerPrefsEntry[] playerPrefsEntries = new PlayerPrefsEntry[0];
		public byte playerPrefsToReachRequiredValues;

		public override uint GetProgress ()
		{
			uint output = 0;
			for (int i = 0; i < playerPrefsEntries.Length; i ++)
			{
				PlayerPrefsEntry playerPrefsEntry = playerPrefsEntries[i];
				if (playerPrefsEntry.type == PlayerPrefsEntry.Type.Bool && PlayerPrefsExtensions.GetBool(playerPrefsEntry.key, playerPrefsEntry.defaultBoolValue) != playerPrefsEntry.defaultBoolValue)
					output ++;
				else if (playerPrefsEntry.type == PlayerPrefsEntry.Type.Int && PlayerPrefs.GetInt(playerPrefsEntry.key, playerPrefsEntry.defaultIntValue) == playerPrefsEntry.requiredIntValue)
					output ++;
			}
			return output;
		}

		public override uint GetMaxProgress ()
		{
			return playerPrefsToReachRequiredValues;
		}


		[Serializable]
		public struct PlayerPrefsEntry
		{
			public string key;
			public Type type;
			public bool defaultBoolValue;
			public int defaultIntValue;
			public int requiredIntValue;

			public enum Type
			{
				Bool,
				Int
			}
		}
	}
}