using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class BuyItemEntry : ShopItemEntry
	{
		public Button buyButton;
		public Text buyButtonText;
		public bool Locked
		{
			get
			{
				return locked;
			}
			set
			{
				locked = value;
			}
		}
		bool locked;

		public override void Init (Item item)
		{
			base.Init (item);
			buyButtonText.text = "Buy (" + Player.instance.haggling.Apply((float) item.value * ShopMenu.instance.multiplyCosts) + ")";
			buyButton.interactable = Player.instance.money >= Player.instance.haggling.Apply((float) item.value * ShopMenu.instance.multiplyCosts);
		}

		public void BuyItem ()
		{
			BuyItem (Player.instance);
		}

		public void BuyItem (Player player)
		{
			Item item = Instantiate(this.item, player.itemsParent);
			player.AddMoney (player.haggling.Apply((float) -item.value * ShopMenu.instance.multiplyCosts));
			player.items = player.items.Add(item);
			item.OnGain (player);
			Destroy(gameObject);
			if (player == Player.instance)
			{
				if (NetworkManager.connection != null)
					NetworkManager.connection.Send("Player Bought Item", (uint) ShopMenu.instance.allItems.IndexOf(this.item));
				ShopMenu.instance.multiplyCosts += ShopMenu.instance.addToMultiplyCosts;
				// ShopMenu.rerollCost = 0;
				// ShopMenu.instance.rerollCostText.text = "Reroll (0)";
				ShopMenu.instance.UpdateButtons ();
				SellItemEntry sellItemEntry = Instantiate(ShopMenu.instance.sellItemEntryPrefab, ShopMenu.instance.sellItemEntriesParent);
				sellItemEntry.Init (item);
				PlayerPrefsExtensions.SetBool ("Bought item", true);
			}
		}
	}
}