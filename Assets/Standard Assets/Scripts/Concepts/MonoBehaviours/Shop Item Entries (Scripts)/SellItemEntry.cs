using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	public class SellItemEntry : ShopItemEntry
	{
		public Text valueText;

		public void Init (Item item)
		{
			base.Init (item);
			valueText.text = "Sell (" + (uint) Player.instance.haggling.Apply(ShopMenu.instance.multiplySellValue * item.value * ShopMenu.instance.multiplyCosts) + ")";
		}

		public void SellItem ()
		{
			Player.instance.AddMoney (Player.instance.haggling.Apply(ShopMenu.instance.multiplySellValue * item.value * ShopMenu.instance.multiplyCosts));
			Player.instance.moneyText.text = "" + Player.instance.money;
			Player.instance.items = Player.instance.items.Remove(item);
			ShopMenu.instance.UpdateButtons ();
			Destroy(gameObject);
			Destroy(item.gameObject);
		}
	}
}