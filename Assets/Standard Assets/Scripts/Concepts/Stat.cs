using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace FightRoom
{
	[Serializable]
	public class Stat
	{
		public string name;
		public int value;
		public IntRange valueRange;
		public bool isPercent;
		public StatIndicator indicator;
		const string REPLACE_WITH_STAT_VALUE = "_";
		string initTooltipText;

		public void Init ()
		{
			initTooltipText = indicator.tooltipText.text;
		}

		public virtual void AddToValue (int value)
		{
			this.value += value;
			indicator.text.text = name + ": " + this.value;
			indicator.childStatIndicator.text.text = name + ": " + this.value;
			StatsMenu.TrackedStat trackedStat;
			if (StatsMenu.trackedStatsDict.TryGetValue(this, out trackedStat))
			{
				trackedStat.indicator.text.text = name + ": " + this.value;
				trackedStat.indicator.tooltipText.text = GetNewTooltipText();
			}
		}

		public virtual float GetValue ()
		{
			float output = valueRange.Clamp((int) Player.instance.polarized.Apply(value));
			if (isPercent)
				output /= 100;
			return output;
		}

		public virtual float Apply (float value)
		{
			throw new NotImplementedException();
		}

		public virtual string GetNewTooltipText ()
		{
			return initTooltipText.Replace(REPLACE_WITH_STAT_VALUE, "" + value);
		}
	}
}