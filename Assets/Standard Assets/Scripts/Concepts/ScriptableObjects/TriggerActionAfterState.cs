using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class TriggerActionAfterState : StateMachineBehaviour
	{
		public string goName;

		public override void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			GameObject go = GameObject.Find(goName);
			if (go != null)
				go.GetComponent<TriggerAction>().Trigger ();
		}
	}
}