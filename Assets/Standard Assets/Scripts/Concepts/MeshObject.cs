using System;
using Extensions;
using UnityEngine;

[Serializable]
public class MeshObject
{
	public Transform trs;
	public MeshFilter meshFilter;
	public MeshRenderer meshRenderer;

	public MeshObject (Transform trs, MeshFilter meshFilter, MeshRenderer meshRenderer)
	{
		this.trs = trs;
		this.meshFilter = meshFilter;
		this.meshRenderer = meshRenderer;
	}

	public Shape3D ToShape3D (bool useSharedMesh)
	{
		Mesh mesh;
		if (useSharedMesh)
			mesh = meshFilter.sharedMesh;
		else
			mesh = meshFilter.mesh;
		Shape3D output = Shape3D.FromMesh(mesh);
		output = output.Transform(trs);
		return output;
	}

	public Mesh FromShape3D (Shape3D shape, bool useSharedMesh)
	{
		Mesh mesh = shape.InverseTransform(trs).ToMesh();
		if (useSharedMesh)
			meshFilter.sharedMesh = mesh;
		else
			meshFilter.mesh = mesh;
		return mesh;
	}

	public int[] GetVertexIndicesOfFace (Shape3D.Face face, bool useSharedMesh)
	{
		Mesh mesh;
		if (useSharedMesh)
			mesh = meshFilter.sharedMesh;
		else
			mesh = meshFilter.mesh;
		for (int i = 0; i < mesh.triangles.Length; i += 3)
		{
			int vertexIndex0 = mesh.triangles[i];
			Vector3[] vertices = mesh.vertices;
			if (face.corners.Contains(vertices[vertexIndex0]))
			{
				int vertexIndex1 = mesh.triangles[i + 1];
				if (face.corners.Contains(vertices[vertexIndex1]))
				{
					int vertexIndex2 = mesh.triangles[i + 2];
					if (face.corners.Contains(vertices[vertexIndex2]))
						return new int[] { vertexIndex0, vertexIndex1, vertexIndex2 };
				}
			}
		}
		return new int[0];
	}
	
	public Color GetColorAtCorner (int cornerIndex, bool useSharedMesh)
	{
		Color output;
		Mesh mesh;
		if (useSharedMesh)
			mesh = meshFilter.sharedMesh;
		else
			mesh = meshFilter.mesh;
		if (cornerIndex < 0 || cornerIndex >= mesh.uv.Length)
			return new Color();
		Material material = meshRenderer.material;
		Texture _texture = material.mainTexture;
		if (_texture != null)
		{
			Vector2 uv = mesh.uv[cornerIndex];
			Vector2 uv2 = mesh.uv2[cornerIndex];
			Vector2 uv3 = mesh.uv3[cornerIndex];
			Vector2 uv4 = mesh.uv4[cornerIndex];
			Vector2 uv5 = mesh.uv5[cornerIndex];
			Vector2 uv6 = mesh.uv6[cornerIndex];
			Vector2 uv7 = mesh.uv7[cornerIndex];
			Vector2 uv8 = mesh.uv8[cornerIndex];
			Texture2D texture = (Texture2D) _texture;
			Color[] colors = new Color[8];
			colors[0] = texture.GetPixelBilinear(uv.x, uv.y);
			colors[1] = texture.GetPixelBilinear(uv2.x, uv2.y);
			colors[2] = texture.GetPixelBilinear(uv3.x, uv3.y);
			colors[3] = texture.GetPixelBilinear(uv4.x, uv4.y);
			colors[4] = texture.GetPixelBilinear(uv5.x, uv5.y);
			colors[4] = texture.GetPixelBilinear(uv6.x, uv6.y);
			colors[6] = texture.GetPixelBilinear(uv7.x, uv7.y);
			colors[7] = texture.GetPixelBilinear(uv8.x, uv8.y);
			Color average = ColorExtensions.GetAverage(colors);
			output = average;
			if (material.HasProperty("_brightness"))
				output *= material.GetFloat("_brightness");
			if (material.HasProperty("_tint"))
			{
				Color tint = material.GetColor("_tint");
				output *= tint;
				output = output.SetAlpha(tint.a * average.a);
			}
		}
		else
		{
			output = Color.white;
			if (material.HasProperty("_brightness"))
				output *= material.GetFloat("_brightness");
			if (material.HasProperty("_tint"))
			{
				Color tint = material.GetColor("_tint");
				output *= tint;
				output = output.SetAlpha(tint.a);
			}
		}
		return output;
	}
}