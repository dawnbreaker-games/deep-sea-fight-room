using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Size : Stat
	{
		public override void AddToValue (int value)
		{
			float previousMultiplier = Apply(1);
			base.AddToValue (value);
			Player.instance.trs.localScale /= previousMultiplier;
			Player.instance.trs.localScale = Vector3.one * Apply(Player.instance.trs.localScale.x);
		}

		public override float Apply (float value)
		{
			return value * (1f + GetValue());
		}
	}
}