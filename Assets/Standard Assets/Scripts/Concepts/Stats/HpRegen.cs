using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class HpRegen : Stat
	{
		public override float GetValue ()
		{
			return -base.GetValue() * Time.unscaledDeltaTime;
		}
	}
}