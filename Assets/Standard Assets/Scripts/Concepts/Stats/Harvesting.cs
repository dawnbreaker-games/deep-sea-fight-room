using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Harvesting : Stat
	{
		const string REPLACE_WITH_MIN_TIME_BETWEEN_WAVES = "|";
		const string REPLACE_WITH_MAX_TIME_BETWEEN_WAVES = "~";

		public override string GetNewTooltipText ()
		{
			return base.GetNewTooltipText().Replace(REPLACE_WITH_MIN_TIME_BETWEEN_WAVES, "" + Level.instance.timeBetweenWavesRange.min).Replace(REPLACE_WITH_MAX_TIME_BETWEEN_WAVES, "" + Level.instance.timeBetweenWavesRange.max);
		}
	}
}