using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Stealth : Stat
	{
		const string REPLACE_WITH_MIN_TIME_BETWEEN_WAVES = "|";
		const string REPLACE_WITH_MAX_TIME_BETWEEN_WAVES = "~";

		public override string GetNewTooltipText ()
		{
			return base.GetNewTooltipText().Replace(REPLACE_WITH_MIN_TIME_BETWEEN_WAVES, "" + Level.instance.timeBetweenWavesRange.min).Replace(REPLACE_WITH_MAX_TIME_BETWEEN_WAVES, "" + Level.instance.timeBetweenWavesRange.max);
		}

		public override float GetValue ()
		{
			return 1f + base.GetValue();
		}

		public override void AddToValue (int value)
		{
			float _value = GetValue();
			Level.instance.timeBetweenWavesRange.min /= _value;
			Level.instance.timeBetweenWavesRange.max /= _value;
			base.AddToValue (value);
			_value = GetValue();
			Level.instance.timeBetweenWavesRange.min *= _value;
			Level.instance.timeBetweenWavesRange.max *= _value;
		}
	}
}