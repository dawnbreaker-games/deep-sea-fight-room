using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Polarized : Stat
	{
		const string REPLACE_WITH_NEGATIVE_POLARITY_DURATION = "|";
		
		public override void AddToValue (int value)
		{
			base.AddToValue (value);
			Player.instance._maxHp.AddToValue (0);
            Player.instance._moveSpeed.AddToValue (0);
            Player.instance.damage.AddToValue (0);
            Player.instance.piercing.AddToValue (0);
            Player.instance.attackSpeed.AddToValue (0);
            Player.instance.pickupRange.AddToValue (0);
            Player.instance.mastery.AddToValue (0);
            Player.instance.stealth.AddToValue (0);
            Player.instance.size.AddToValue (0);
		}

		public override float GetValue ()
		{
			return (float) valueRange.Clamp(value) / 100;
		}

		public override float Apply (float value)
		{
			return value * (1f + GetValue());
		}

		public override string GetNewTooltipText ()
		{
			return base.GetNewTooltipText().Replace(REPLACE_WITH_NEGATIVE_POLARITY_DURATION, "" + Player.instance.negativePolarityDuration);
		}
	}
}