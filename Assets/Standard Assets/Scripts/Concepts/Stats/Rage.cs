using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Rage : Stat
	{
		const string REPLACE_WITH_RAGE_DURATION = "|";

		public override string GetNewTooltipText ()
		{
			return base.GetNewTooltipText().Replace(REPLACE_WITH_RAGE_DURATION, "" + Player.instance.rageDuration);
		}
	}
}