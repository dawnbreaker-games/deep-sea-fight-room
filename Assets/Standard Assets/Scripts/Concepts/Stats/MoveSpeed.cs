using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class MoveSpeed : Stat
	{
		public override void AddToValue (int value)
		{
			float previousMultiplier = Apply(1);
			base.AddToValue (value);
			Player.instance.moveSpeed /= previousMultiplier;
			Player.instance.moveSpeed = Apply(Player.instance.moveSpeed);
		}

		public override float Apply (float value)
		{
			return value * (1f + GetValue());
		}
	}
}