using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class PickupRange : Stat
	{
		public override void AddToValue (int value)
		{
			base.AddToValue (value);
			Player.instance.pickupRangeTrs.localScale = Vector3.one * GetValue();
		}

		public override float GetValue ()
		{
			return 1f + base.GetValue();
		}
	}
}