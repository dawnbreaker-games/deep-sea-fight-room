using System;
using Extensions;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class MaxHp : Stat
	{
		public override void AddToValue (int value)
		{
			base.AddToValue (value);
			float _maxHp = GetValue();
			Player.instance.maxHp = _maxHp;
			Player.instance.hp = Mathf.Clamp(Player.instance.hp, 0, _maxHp);
			Player.instance.hpText.text = "" + Player.instance.hp + " / " + _maxHp;
            //Player.instance.hpBarTrs.localScale = Player.instance.hpBarTrs.localScale.SetX(Player.instance.hp / _maxHp);
            Player.instance.hpBarImage.fillAmount = Player.instance.hp / _maxHp;
        }
    }
}