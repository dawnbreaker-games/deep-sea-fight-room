using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class LifeSteal : Stat
	{
		public override float Apply (float value)
		{
			return -value * GetValue();
		}
	}
}