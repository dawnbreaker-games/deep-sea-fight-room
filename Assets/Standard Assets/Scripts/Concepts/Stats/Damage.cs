using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Damage : Stat
	{
		public override void AddToValue (int value)
		{
			float previousMultiplier = Apply(1);
			base.AddToValue (value);
			for (int i = 0; i < Player.instance.bulletPatternEntriesSortedList.Values.Count; i ++)
			{
				BulletPatternEntry bulletPatternEntry = Player.instance.bulletPatternEntriesSortedList.Values[i];
				Bullet bullet = bulletPatternEntry.bulletPrefab;
				bullet.damage /= previousMultiplier;
				bullet.damage = Apply(bullet.damage);
			}
		}

		public override float Apply (float value)
		{
			return value * (1f + GetValue());
		}
	}
}