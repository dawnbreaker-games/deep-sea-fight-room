using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class MoneyDamage : Stat
	{
		public override float Apply (float value)
		{
			return value * GetValue();
		}
	}
}