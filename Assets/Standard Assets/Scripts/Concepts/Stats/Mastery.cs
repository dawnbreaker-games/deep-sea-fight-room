using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Mastery : Stat
	{
		public override void AddToValue (int value)
		{
			base.AddToValue (value);
			if (value > 0)
			{
				if (this.value >= Player.instance.nextMasteryTier.masteryRequired)
				{
					Player.instance.nextMasteryTier.Apply ();
					Player.instance.nextMasteryTierIndex ++;
					if (Player.instance.nextMasteryTierIndex < Player.instance.masteryTiers.Length)
						Player.instance.nextMasteryTier = Player.instance.masteryTiers[Player.instance.nextMasteryTierIndex];
				}
			}
			else if (value < 0)
			{
				for (int i = Player.instance.nextMasteryTierIndex - 1; i >= 0; i --)
				{
					Player.MasteryTier masteryTier = Player.instance.masteryTiers[i];
					masteryTier.Apply ();
					if (this.value >= masteryTier.masteryRequired)
						return;
					Player.instance.nextMasteryTierIndex = i;
					Player.instance.nextMasteryTier = masteryTier;
				}
			}
		}
	}
}