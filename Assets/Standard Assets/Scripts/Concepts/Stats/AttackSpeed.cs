using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class AttackSpeed : Stat
	{
		public override void AddToValue (int value)
		{
			base.AddToValue (value);
			Player.instance.animator.SetFloat("attackSpeed", GetValue());
		}

		public override float GetValue ()
		{
			return 1f + base.GetValue();
		}
	}
}