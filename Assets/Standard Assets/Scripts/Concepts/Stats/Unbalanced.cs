using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Unbalanced : Stat
	{
		const string REPLACE_WITH_NEGATIVE_POLARITY_DURATION = "|";

		public override string GetNewTooltipText ()
		{
			return base.GetNewTooltipText().Replace(REPLACE_WITH_NEGATIVE_POLARITY_DURATION, "" + Player.instance.negativePolarityDuration);
		}

		public override float GetValue ()
		{
			return (float) valueRange.Clamp(value) / 100;
		}

		public override float Apply (float value)
		{
			return value * (1f + GetValue());
		}
	}
}