using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Haggling : Stat
	{
		public override float Apply (float value)
		{
			return value / (1f + GetValue());
		}
	}
}