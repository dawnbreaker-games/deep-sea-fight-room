using System;
using UnityEngine;

namespace FightRoom
{
	[Serializable]
	public class Piercing : Stat
	{
		public override void AddToValue (int value)
		{
			int previousPiercing = (int) GetValue();
			base.AddToValue (value);
			for (int i = 0; i < Player.instance.bulletPatternEntriesSortedList.Values.Count; i ++)
			{
				BulletPatternEntry bulletPatternEntry = Player.instance.bulletPatternEntriesSortedList.Values[i];
				bulletPatternEntry.bulletPrefab.hitsTillDespawn -= (byte) previousPiercing;
				bulletPatternEntry.bulletPrefab.hitsTillDespawn += (byte) GetValue();
			}
		}
	}
}