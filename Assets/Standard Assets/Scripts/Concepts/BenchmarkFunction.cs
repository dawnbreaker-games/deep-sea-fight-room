using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Diagnostics;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	public class BenchmarkFunction<TInput, TResult> : IUpdatable
	{
		public TInput input;
		public string filePath;
		Func<TInput, TResult> function;
		long stopwatchTicks;

		public BenchmarkFunction (Func<TInput, TResult> function, TInput input = default(TInput), string filePath = null)
		{
			SetFunction (function);
			this.input = input;
			this.filePath = filePath;
		}
		
		public void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			stopwatchTicks = Stopwatch.GetTimestamp();
			function(input);
			stopwatchTicks = Stopwatch.GetTimestamp() - stopwatchTicks;
		}

		public void SetFunction (Func<TInput, TResult> function)
		{
			this.function = function;
			StackTrace stackTrace = new StackTrace();
			this.function += (TInput input) =>
			{
				string text = stackTrace.GetFrame(0).GetMethod() + " took " + stopwatchTicks + " seconds";
				MonoBehaviour.print(text);
				if (filePath != null)
					File.AppendAllLines(filePath, new string[] { text });
				throw new Exception("text");
			};
		}
	}
}