using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[Serializable]
public class Shape3D
{
	public Vector3[] corners = new Vector3[0];
	public LineSegment3D[] edges = new LineSegment3D[0];
	public Face[] faces = new Face[0];

	public Shape3D ()
	{
	}

	public Shape3D (Face[] faces)
	{
		this.faces = faces;
		SetEdgesFromFaces ();
		SetCornersFromEdges ();
	}

	public Shape3D (Shape3D shape)
	{
		corners = new Vector3[shape.corners.Length];
		shape.corners.CopyTo(corners, 0);
		edges = new LineSegment3D[shape.edges.Length];
		shape.edges.CopyTo(edges, 0);
		faces = new Face[shape.faces.Length];
		shape.faces.CopyTo(faces, 0);
	}

#if UNITY_EDITOR
	public void DrawGizmos (Color color)
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment3D edge = edges[i];
			edge.DrawGizmos (color);
		}
	}
#endif

	public void SetCornersFromEdges ()
	{
		List<Vector3> corners = new List<Vector3>();
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment3D edge = edges[i];
			if (!corners.Contains(edge.end))
				corners.Add(edge.end);
		}
		this.corners = corners.ToArray();
	}

	public void SetCornersFromFaces ()
	{
		throw new NotImplementedException();
	}

	public void SetEdgesFromFaces ()
	{
		List<LineSegment3D> edges = new List<LineSegment3D>();
		for (int i = 0; i < faces.Length; i ++)
		{
			Face face = faces[i];
			for (int i2 = 0; i2 < face.edges.Length; i2 ++)
			{
				LineSegment3D edge = face.edges[i2];
				if (!edges.Contains(edge) && !edges.Contains(edge.Flip()))
					edges.Add(edge);
			}
		}
		this.edges = edges.ToArray();
	}

	public Shape3D Transform (Transform trs)
	{
		Face[] faces = new Face[this.faces.Length];
		for (int i = 0; i < faces.Length; i ++)
			faces[i] = this.faces[i].Transform(trs);
		return new Shape3D(faces);
	}

	public Shape3D InverseTransform (Transform trs)
	{
		Face[] faces = new Face[this.faces.Length];
		for (int i = 0; i < faces.Length; i ++)
			faces[i] = this.faces[i].InverseTransform(trs);
		return new Shape3D(faces);
	}

	public Shape3D Move (Vector3 movement)
	{
		Face[] faces = new Face[this.faces.Length];
		for (int i = 0; i < faces.Length; i ++)
			faces[i] = this.faces[i].Move(movement);
		return new Shape3D(faces);
	}

	public Shape3D Rotate (Vector3 pivotPoint, Quaternion rotation)
	{
		Face[] faces = new Face[this.faces.Length];
		for (int i = 0; i < faces.Length; i ++)
			faces[i] = this.faces[i].Rotate(pivotPoint, rotation);
		return new Shape3D(faces);
	}

	public Shape3D AddSize (float add)
	{
		Face[] faces = new Face[this.faces.Length];
		for (int i = 0; i < faces.Length; i ++)
			faces[i] = this.faces[i].AddSize(add);
		return new Shape3D(faces);
	}

	public Shape3D MultiplySize (float multiply)
	{
		Face[] faces = new Face[this.faces.Length];
		for (int i = 0; i < faces.Length; i ++)
			faces[i] = this.faces[i].MultiplySize(multiply);
		return new Shape3D(faces);
	}

	public float GetPerimeter ()
	{
		float output = 0;
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment3D edge = edges[i];
			output += edge.GetLength();
		}
		return output;
	}

	public Vector3 GetPointOnPerimeter (float distance)
	{
		while (true)
		{
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment3D edge = edges[i];
				float edgeLength = edge.GetLength();
				distance -= edgeLength;
				if (distance <= 0)
					return edge.GetPointWithDirectedDistance(edgeLength + distance);
			}
		}
	}

	public bool Contains (Vector3 point, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		LineSegment3D checkLineSegment = new LineSegment3D(point, point + (Random.onUnitSphere.normalized * checkDistance));
		int collisionCount = 0;
		for (int i = 0; i < faces.Length; i ++)
		{
			Face face = faces[i];
			if (face.DoIIntersectWithLineSegment(checkLineSegment, equalPointsIntersect, checkDistance))
				collisionCount ++;
		}
		return collisionCount % 2 == 1;
	}

	public bool DoIIntersectWithLineSegment (LineSegment3D lineSegment, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		if (Contains(lineSegment.start, equalPointsIntersect, checkDistance) || Contains(lineSegment.end, equalPointsIntersect, checkDistance))
			return true;
		for (int i = 0; i < faces.Length; i ++)
		{
			Face face = faces[i];
			if (face.DoIIntersectWithLineSegment(lineSegment, equalPointsIntersect, checkDistance))
				return true;
		}
		return false;
	}

	public virtual Vector3? GetIntersectionPointWithLineSegment (LineSegment3D lineSegment, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		for (int i = 0; i < faces.Length; i ++)
		{
			Face face = faces[i];
			Vector3? hitPoint = face.GetIntersectionPointWithLineSegment(lineSegment, equalPointsIntersect, checkDistance);
			if (hitPoint != null)
				return hitPoint;
		}
		return null;
	}

	public virtual (Face, Vector3) GetFaceAndIntersectionPointWithLineSegment (LineSegment3D lineSegment, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		for (int i = 0; i < faces.Length; i ++)
		{
			Face face = faces[i];
			Vector3? hitPoint = face.GetIntersectionPointWithLineSegment(lineSegment, equalPointsIntersect, checkDistance);
			if (hitPoint != null)
				return (face, (Vector3) hitPoint);
		}
		return (null, default(Vector3));
	}

	public Vector3 GetRandomPointOnEdges ()
	{
		return GetPointOnPerimeter(Random.Range(0f, GetPerimeter()));
	}

	public Vector3 GetRandomPoint (bool checkIfContained = true, bool containsFaces = true, float checkDistance = 99999)
	{
		while (true)
		{
			Face face1 = faces[Random.Range(0, faces.Length)];
			Face face2 = faces[Random.Range(0, faces.Length)];
			Vector3 point1 = face1.GetRandomPoint(checkIfContained, containsFaces, checkDistance);
			Vector3 point2 = face2.GetRandomPoint(checkIfContained, containsFaces, checkDistance);
			Vector3 output = (point1 + point2) / 2;
			if (!checkIfContained || Contains(output, containsFaces, checkDistance))
				return output;
		}
	}

	public Vector3 GetClosestPoint (Vector3 point, float checkDistance = 99999)
	{
		(Vector3 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance);
		return closestPointAndDistanceSqr.point;
	}

	public float GetDistanceSqr (Vector3 point, float checkDistance = 99999)
	{
		(Vector3 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance);
		return closestPointAndDistanceSqr.distanceSqr;
	}

	public (Vector3, float) GetClosestPointAndDistanceSqr (Vector3 point, float checkDistance = 99999)
	{
		Vector3 closestPoint = new Vector3();
		float closestDistanceSqr = Mathf.Infinity;
		for (int i = 0; i < faces.Length; i ++)
		{
			Face face = faces[i];
			(Vector3 point, float distanceSqr) closestPointAndDistanceSqr = face.GetClosestPointAndDistanceSqr(point, checkDistance);
			if (closestPointAndDistanceSqr.distanceSqr < closestDistanceSqr)
			{
				closestDistanceSqr = closestPointAndDistanceSqr.distanceSqr;
				closestPoint = closestPointAndDistanceSqr.point;
			}
		}
		return (closestPoint, closestDistanceSqr);
	}

	public Shape3D MergeForPorEnlcosed (Shape3D shape, int assumeSharedFaces = int.MaxValue)
	{
		List<Face> outputFaces = new List<Face>(faces);
		outputFaces.AddRange(shape.faces);
		if (assumeSharedFaces > 0)
		{
			for (int i = 0; i < faces.Length; i ++)
			{
				Face face = faces[i];
				for (int i2 = 0; i2 < shape.faces.Length; i2 ++)
				{
					Face face2 = shape.faces[i2];
					if (face.HasSamePoints(face2))
					{
						outputFaces.Remove(face);
						assumeSharedFaces --;
						if (assumeSharedFaces <= 0)
							return new Shape3D(outputFaces.ToArray());
					}
				}
			}
		}
		return new Shape3D(outputFaces.ToArray());
	}

	public Shape3D BooleanOnto (Shape3D shapeToBoolean)
	{
		throw new NotImplementedException();
	}

	public Shape3D SetCorner (int cornerIndex, Vector3 newPosition)
	{
		Face[] faces = new Face[this.faces.Length];
		Vector3 corner = corners[cornerIndex];
		for (int i = 0; i < faces.Length; i ++)
		{
			Face face = this.faces[i];
			int cornerIndexInFace = face.corners.IndexOf(corner);
			if (cornerIndexInFace != -1)
			{
				face.corners[cornerIndexInFace] = newPosition;
				face = new Face(face.corners);
			}
			faces[i] = face;
		}
		return new Shape3D(faces);
	}

	public virtual Mesh ToMesh ()
	{
		Mesh output = new Mesh();
		output.vertices = corners;
		List<Face> oldFaces = new List<Face>(this.faces);
		List<Face> faces = new List<Face>(this.faces);
		List<int> triangles = new List<int>(new int[faces.Count * 3]);
		List<int> facesCornerCounts = new List<int>(new int[faces.Count]);
		for (int i = 0; i < corners.Length; i ++)
		{
			Vector3 corner = corners[i];
			for (int i2 = 0; i2 < faces.Count; i2 ++)
			{
				Face face = faces[i2];
				int cornerIndex = face.corners.IndexOf(corner);
				if (cornerIndex > -1)
				{
					if (facesCornerCounts[i2] < 3)
						facesCornerCounts[i2] ++;
					else
					{
						faces.RemoveAt(i2);
						facesCornerCounts.RemoveAt(i2);
						i2 --;
					}
					triangles[oldFaces.IndexOf(face) * 3 + cornerIndex] = i;
				}
			}
		}
		output.SetTriangles(triangles, 0);
		return output;
	}

	public static Shape3D FromMesh (Mesh mesh)
	{
		Shape3D output;
		List<Vector3> vertices = new List<Vector3>();
		mesh.GetVertices(vertices);
		Face[] faces = new Face[mesh.triangles.Length / 3];
		for (int i = 0; i < mesh.triangles.Length; i += 3)
		{
			int vertexIndex1 = mesh.triangles[i];
			int vertexIndex2 = mesh.triangles[i + 1];
			int vertexIndex3 = mesh.triangles[i + 2];
			Vector3 vertex1 = vertices[vertexIndex1];
			Vector3 vertex2 = vertices[vertexIndex2];
			Vector3 vertex3 = vertices[vertexIndex3];
			faces[i / 3] = new Face(vertex1, vertex2, vertex3);
		}
		return new Shape3D(faces);
	}

	public static Shape3D FromBounds (Bounds bounds)
	{
		Shape3D output = new Shape3D();
		output.faces = new Face[6];
		Vector3[] corners = new Vector3[4];
		corners[0] = bounds.min;
		corners[1] = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z);
		corners[2] = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);
		corners[3] = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z);
		output.faces[0] = new Face(corners); // bottom
		corners[0] = new Vector3(bounds.min.x, bounds.max.y, bounds.min.z);
		corners[1] = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);
		corners[3] = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z);
		corners[2] = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);
		output.faces[1] = new Face(corners); // top
		corners[0] = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z);
		corners[1] = new Vector3(bounds.min.x, bounds.max.y, bounds.min.z);
		corners[3] = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z);
		corners[2] = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z);
		output.faces[1] = new Face(corners); // left
		corners[0] = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z);
		corners[1] = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);
		corners[3] = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);
		corners[2] = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);
		output.faces[1] = new Face(corners); // right
		corners[0] = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z);
		corners[1] = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z);
		corners[3] = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);
		corners[2] = new Vector3(bounds.min.x, bounds.max.y, bounds.min.z);
		output.faces[1] = new Face(corners); // back
		corners[0] = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z);
		corners[1] = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);
		corners[3] = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);
		corners[2] = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z);
		output.faces[1] = new Face(corners); // front
		return output;
	}

	[Serializable]
	public class Face
	{
		public Vector3 normal;
		public Shape2D shapeOnPlane;
		public Vector3[] corners = new Vector3[0];
		public LineSegment3D[] edges = new LineSegment3D[0];

		public Face (params Vector3[] corners)
		{
			this.corners = corners;
			SetEdgesFromCorners ();
			SetShapeOnPlaneFromCorners ();
		}

		public Face (params LineSegment3D[] edges)
		{
			this.edges = edges;
			SetCornersFromEdges ();
			SetShapeOnPlaneFromCorners ();
		}

#if UNITY_EDITOR
		public void DrawGizmos (Color color)
		{
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment3D edge = edges[i];
				edge.DrawGizmos (color);
			}
		}
#endif

		public void SetCornersFromShapeOnPlane ()
		{
			corners = new Vector3[shapeOnPlane.corners.Length];
			for (int i = 0; i < corners.Length; i ++)
			{
				Vector3 corner = shapeOnPlane.corners[i];
				corners[i] = TransformPoint(corner);
			}
		}

		public void SetEdgesFromShapeOnPlane ()
		{
			edges = new LineSegment3D[shapeOnPlane.edges.Length];
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment2D _edge = shapeOnPlane.edges[i];
				edges[i] = new LineSegment3D(TransformPoint(_edge.start), TransformPoint(_edge.end));
			}
		}

		public void SetShapeOnPlaneFromCorners ()
		{
			Plane plane = new Plane(this.corners[0], this.corners[1], this.corners[2]);
			normal = plane.normal;
			Vector2[] corners = new Vector2[this.corners.Length];
			for (int i = 0; i < this.corners.Length; i ++)
			{
				Vector3 corner = this.corners[i];
				corner = InverseTransformPoint(corner);
				corners[i] = corner;
			}
			shapeOnPlane = Shape2D.PolygonFromCorners(corners);
		}

		public void SetShapeOnPlaneFromEdges ()
		{
			throw new NotImplementedException();
		}

		public void SetCornersFromEdges ()
		{
			corners = new Vector3[edges.Length];
			for (int i = 0; i < edges.Length; i ++)
				corners[i] = edges[i].end;
		}

		public void SetEdgesFromCorners ()
		{
			edges = new LineSegment3D[corners.Length];
			Vector3 previousCorner = corners[corners.Length - 1];
			for (int i = 0; i < corners.Length; i ++)
			{
				Vector3 corner = corners[i];
				edges[i] = new LineSegment3D(previousCorner, corner);
				previousCorner = corner;
			}
		}

		public Face Move (Vector3 movement)
		{
			Vector3[] corners = new Vector3[this.corners.Length];
			for (int i = 0; i < corners.Length; i ++)
				corners[i] = this.corners[i] + movement;
			return new Face(corners);
		}

		public Face Rotate (Vector3 pivotPoint, Quaternion rotation)
		{
			Vector3[] corners = new Vector3[this.corners.Length];
			for (int i = 0; i < corners.Length; i ++)
				corners[i] = this.corners[i].Rotate(pivotPoint, rotation);
			return new Face(corners);
		}

		public Face AddSize (float add)
		{
			Vector3 averagePoint = GetAveragePoint();
			Vector3[] corners = new Vector3[this.corners.Length];
			for (int i = 0; i < corners.Length; i ++)
			{
				Vector3 corner = this.corners[i];
				corners[i] = corner + ((corner - averagePoint).normalized * add);
			}
			return new Face(corners);
		}

		public Face MultiplySize (float multiply)
		{
			Vector3 averagePoint = GetAveragePoint();
			Vector3[] corners = new Vector3[this.corners.Length];
			for (int i = 0; i < corners.Length; i ++)
			{
				Vector3 corner = this.corners[i];
				corners[i] = corner + ((corner - averagePoint) * (multiply - 1));
			}
			return new Face(corners);
		}

		public bool Contains (Vector3 point, bool equalPointsIntersect = true, float checkDistance = 99999)
		{
			point = InverseTransformPoint(point);
			return shapeOnPlane.ContainsForPolygon(point, equalPointsIntersect, checkDistance);
		}

		public bool DoIIntersectWithLineSegment (LineSegment3D lineSegment, bool equalPointsIntersect = true, float checkDistance = 99999)
		{
			float hitDistance;
			Ray ray = new Ray(lineSegment.start, lineSegment.GetDirection());
			Plane plane = new Plane(normal, corners[0]);
			if (!plane.Raycast(ray, out hitDistance) || hitDistance > lineSegment.GetLength())
				return false;
			Vector3 hitPoint = ray.GetPoint(hitDistance);
			hitPoint = InverseTransformPoint(hitPoint);
			return shapeOnPlane.ContainsForPolygon(hitPoint, equalPointsIntersect, checkDistance);
		}

		public virtual Vector3? GetIntersectionPointWithLineSegment (LineSegment3D lineSegment, bool equalPointsIntersect = true, float checkDistance = 99999)
		{
			float hitDistance;
			Ray ray = new Ray(lineSegment.start, lineSegment.GetDirection());
			Plane plane = new Plane(normal, corners[0]);
			if (!plane.Raycast(ray, out hitDistance) || hitDistance > lineSegment.GetLength())
				return null;
			Vector3 hitPoint = ray.GetPoint(hitDistance);
			Vector2 localHitPoint = InverseTransformPoint(hitPoint);
			if (shapeOnPlane.ContainsForPolygon(localHitPoint, equalPointsIntersect, checkDistance))
				return hitPoint;
			else
				return null;
		}

		public bool IntersectsFace (Face face, bool checkIfContained = true, bool equalPointsIntersect = true, float checkDistance = 99999)
		{
			return Contains(face.GetRandomPoint(checkIfContained, equalPointsIntersect, checkDistance), equalPointsIntersect, checkDistance) || face.Contains(GetRandomPoint(checkIfContained, equalPointsIntersect, checkDistance), equalPointsIntersect, checkDistance);
		}

		public Vector3 GetRandomPoint (bool checkIfContained = true, bool containsEdges = true, float checkDistance = 99999)
		{
			return TransformPoint(shapeOnPlane.GetRandomPoint(checkIfContained, containsEdges, checkDistance));
		}

		public Vector3 GetAveragePoint ()
		{
			return VectorExtensions.GetAverage(corners);
		}

		public Face Transform (Transform trs)
		{
			Vector3[] corners = new Vector3[this.corners.Length];
			for (int i = 0; i < corners.Length; i ++)
				corners[i] = trs.TransformPoint(this.corners[i]);
			return new Face(corners);
		}

		public Face InverseTransform (Transform trs)
		{
			Vector3[] corners = new Vector3[this.corners.Length];
			for (int i = 0; i < corners.Length; i ++)
				corners[i] = trs.InverseTransformPoint(this.corners[i]);
			return new Face(corners);
		}

		public Vector3 TransformPoint (Vector3 localPoint)
		{
			Vector3 averagePoint = GetAveragePoint();
			localPoint = localPoint.Rotate(Vector3.zero, Quaternion.LookRotation(normal));
			localPoint += averagePoint;
			return localPoint;
		}

		public Vector3 InverseTransformPoint (Vector3 worldPoint)
		{
			Vector3 averagePoint = GetAveragePoint();
			worldPoint -= averagePoint;
			worldPoint = worldPoint.Rotate(Vector3.zero, Quaternion.LookRotation(-normal));
			return worldPoint;
		}

		public float GetPerimeter ()
		{
			float output = 0;
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment3D edge = edges[i];
				output += edge.GetLength();
			}
			return output;
		}

		public Vector3 GetPointOnPerimeter (float distance)
		{
			while (true)
			{
				for (int i = 0; i < edges.Length; i ++)
				{
					LineSegment3D edge = edges[i];
					float edgeLength = edge.GetLength();
					distance -= edgeLength;
					if (distance <= 0)
						return edge.GetPointWithDirectedDistance(edgeLength + distance);
				}
			}
		}

		public Vector3 GetClosestPoint (Vector3 point, float checkDistance = 99999)
		{
			(Vector3 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance);
			return closestPointAndDistanceSqr.point;
		}

		public float GetDistanceSqr (Vector3 point, float checkDistance = 99999)
		{
			(Vector3 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance);
			return closestPointAndDistanceSqr.distanceSqr;
		}

		public (Vector3, float) GetClosestPointAndDistanceSqr (Vector3 point, float checkDistance = 99999)
		{
			Vector2 localPoint = InverseTransformPoint(point);
			(Vector2 point, float distanceSqr) closestPointAndDistanceSqr = shapeOnPlane.GetClosestPointAndDistanceSqr(localPoint, checkDistance);
			return (TransformPoint(closestPointAndDistanceSqr.point), closestPointAndDistanceSqr.distanceSqr);
		}

		public bool HasSamePoints (Face face)
		{
			if (corners.Length != face.corners.Length)
				return false;
			for (int i = 0; i < corners.Length; i ++)
			{
				Vector2 corner = corners[i];
				if (!face.corners.Contains(corner))
					return false;
			}
			return true;
		}
	}
}