﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class BulletPattern : ScriptableObject//, IConfigurable
	{
		public bool canSpawn = true;
		public bool setSpawnerAimingOnShoot;

		public virtual void Init (Transform spawner)
		{
		}

		public virtual Vector3 GetShootDirection (Transform spawner)
		{
			return spawner.up;
		}
		
		public virtual Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			if (spawner == null)
				return new Bullet[0];
			Vector3 direction = GetShootDirection(spawner);
			if (setSpawnerAimingOnShoot)
				spawner.up = direction;
			if (!canSpawn)
				return new Bullet[0];
			Bullet bullet = ObjectPool.Instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawner.position, Quaternion.LookRotation(Vector3.forward, direction));
			return new Bullet[] { bullet };
		}
		
		public virtual Bullet[] Shoot (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab)
		{
			if (!canSpawn)
				return new Bullet[0];
			Bullet bullet = ObjectPool.Instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawnPosition, Quaternion.LookRotation(Vector3.forward, direction));
			return new Bullet[] { bullet };
		}
		
		public virtual EventManager.Event RedirectAfterDelay (Bullet bullet, float delay)
		{
			return bullet.AddEvent((object obj) => { Redirect ((Bullet) obj); }, delay);
		}
		
		public virtual EventManager.Event RedirectAfterDelay (Bullet bullet, Vector3 direction, float delay)
		{
			return bullet.AddEvent((object obj) => { Redirect ((Bullet) obj, direction); }, delay);
		}

		public virtual Bullet Redirect (Bullet bullet)
		{
			bullet.velocity = GetRedirectDirection(bullet) * bullet.moveSpeed;
			bullet.rigid.linearVelocity = bullet.velocity + bullet.extraVelocity;
			if (bullet.rigid.linearVelocity != Vector2.zero)
				bullet.trs.up = bullet.rigid.linearVelocity;
			return bullet;
		}
		
		public virtual Bullet Redirect (Bullet bullet, Vector3 direction)
		{
			bullet.velocity = direction * bullet.moveSpeed;
			bullet.rigid.linearVelocity = bullet.velocity + bullet.extraVelocity;
			if (bullet.rigid.linearVelocity != Vector2.zero)
				bullet.trs.up = bullet.rigid.linearVelocity;
			return bullet;
		}
		
		public virtual Vector3 GetRedirectDirection (Bullet bullet)
		{
			return bullet.trs.up;
		}
		
		public virtual EventManager.Event SplitAfterDelay (Bullet bullet, Bullet splitBulletPrefab, float delay)
		{
			return bullet.AddEvent((object obj) => { Split ((Bullet) obj, splitBulletPrefab); }, delay);
		}
		
		public virtual EventManager.Event SplitAfterDelay (Bullet bullet, Vector3 direction, Bullet splitBulletPrefab, float delay)
		{
			return bullet.AddEvent((object obj) => { Split ((Bullet) obj, direction, splitBulletPrefab); }, delay);
		}

		public virtual Bullet[] Split (Bullet bullet, Bullet splitBulletPrefab)
		{
			return Shoot(bullet.trs.position, GetSplitDirection(bullet), splitBulletPrefab);
		}

		public virtual Bullet[] Split (Bullet bullet, Vector3 direction, Bullet splitBulletPrefab)
		{
			return Shoot(bullet.trs.position, direction, splitBulletPrefab);
		}
		
		public virtual Vector3 GetSplitDirection (Bullet bullet)
		{
			return bullet.trs.up;
		}
	}
}