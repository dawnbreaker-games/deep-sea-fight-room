﻿using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.TextCore;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	public class _Text : MonoBehaviour, IUpdatable
	{
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				if (text != value)
				{
					text = value;
					UpdateText ();
				}
			}
		}
		public Collider2D collider;
		public ColliderEffect colliderEffect;
		public float colliderCheckDistance = 1;
		public bool editFinalSize;
		public float multiplyFinalSize = 1;
		public float addToFinalSize;
		public RectTransform rectTrs;
		public Transform scalerTrs;
		public Transform textCharactersParent;
		public Transform backgroundTrs;
		public SpriteRenderer backgroundSpriteRenderer;
		public TextCharacter textCharacterPrefab;
		public TMP_FontAsset fontAsset;
		public TextAlignmentOptions alignmentOptions;
		[HideInInspector]
		public float minGlyphWidth;
		[HideInInspector]
		public float maxGlyphWidth;
		[HideInInspector]
		public float minGlyphHeight;
		[HideInInspector]
		public float maxGlyphHeight;
		public Material material;
		public SpriteMaskInteraction maskInteraction;
		public bool overrideSorting;
		public string sortingLayerName;
		public int sortingOrder;
		public DisplayDelayMode displayDelayMode;
		public float displayDelayPerCharacter;
		public float addToTotalDisplayDelay;
		public bool forceOneLine;
		[Multiline]
		public string text;
		[HideInInspector]
		public TextCharacter[] textCharacters = new TextCharacter[0];
		[HideInInspector]
		public int currentCharacterIndex;
		[HideInInspector]
		public uint lineCount;
		[HideInInspector]
		public float lineHeight;
		public LayoutElement layoutElement;
		[HideInInspector]
		public Event[] events = new Event[0];
		public CustomEvent[] customEvents = new CustomEvent[0];
		public WaitEvent[] waitEvents = new WaitEvent[0];
		public SetTextBasedOnUsingInputDeviceEvent[] setTextBasedOnUsingInputDeviceEvents = new SetTextBasedOnUsingInputDeviceEvent[0];
		[HideInInspector]
		public Vector2 min;
		[HideInInspector]
		public Vector2 max;
#if UNITY_EDITOR
		public bool updateText;
		public Color debugBoxColor;
#endif
		float displayDelayRemaining;
		int currentSentenceIndex;
		List<TextCharacter[]> sentences = new List<TextCharacter[]>();
		TextCharacter[] currentSentence = new TextCharacter[0];

#if UNITY_EDITOR
		void OnValidate ()
		{
			List<Event> events = new List<Event>(customEvents);
			events.AddRange(waitEvents);
			events.AddRange(setTextBasedOnUsingInputDeviceEvents);
			this.events = events.ToArray();
			if (updateText)
			{
				updateText = false;
				minGlyphWidth = Mathf.Infinity;
				maxGlyphWidth = -Mathf.Infinity;
				minGlyphHeight = Mathf.Infinity;
				maxGlyphHeight = -Mathf.Infinity;
				for (int i = 0; i < fontAsset.glyphTable.Count; i ++)
				{
					Glyph glyph = fontAsset.glyphTable[i];
					GlyphMetrics glyphMetrics = glyph.metrics;
					minGlyphWidth = Mathf.Min(glyphMetrics.horizontalBearingX, minGlyphWidth);
					maxGlyphWidth = Mathf.Max(glyphMetrics.width * glyph.scale + glyphMetrics.horizontalBearingX + glyphMetrics.horizontalAdvance, maxGlyphWidth);
					minGlyphHeight = Mathf.Min(glyphMetrics.horizontalBearingY, minGlyphHeight);
					maxGlyphHeight = Mathf.Max(glyphMetrics.height * glyph.scale + glyphMetrics.horizontalBearingY, maxGlyphHeight);
				}
				UpdateText ();
			}
		}

		void OnDrawGizmos ()
		{
			Vector3[] corners = new Vector3[4];
			rectTrs.GetLocalCorners(corners);
			Vector3 boundsMin = rectTrs.TransformPoint(corners[0]);
			Vector3 boundsMax = rectTrs.TransformPoint(corners[2]);
			Bounds bounds = new Bounds((boundsMax + boundsMin) / 2, Quaternion.Inverse(rectTrs.rotation) * (boundsMax - boundsMin));
			DebugExtensions.DrawBounds (bounds, debugBoxColor, 0, rectTrs.rotation);
		}
#endif

		public IEnumerator Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				yield break;
#endif
			yield return new WaitUntil(() => { return GameManager.framesSinceLevelLoaded > GameManager.LAG_FRAMES_AFTER_SCENE_LOAD; });
			if (gameObject.activeInHierarchy)
				UpdateText ();
		}

		public void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void UpdateText ()
		{
			string text = this.text;
			ClearAndStopUpdate ();
			lineCount = 0;
			if (string.IsNullOrEmpty(text))
			{
				gameObject.SetActive(false);
				return;
			}
			lineCount = 1;
			Vector2 drawingPosition = Vector2.zero;
			Vector3[] corners = new Vector3[4];
			Canvas.ForceUpdateCanvases();
			rectTrs.GetLocalCorners(corners);
			Vector3 boundsMin = corners[0];
			Vector3 boundsMax = corners[2];
			Bounds bounds = new Bounds((boundsMax + boundsMin) / 2, boundsMax - boundsMin);
			if (bounds.size.x == 0 || bounds.size.y == 0)
			{
				gameObject.SetActive(false);
				return;
			}
			scalerTrs.localScale = Vector3.one;
			textCharactersParent.localScale = Vector3.one;
			textCharactersParent.localPosition = Vector3.zero;
			List<Event> events = new List<Event>(customEvents);
			events.AddRange(waitEvents);
			events.AddRange(setTextBasedOnUsingInputDeviceEvents);
			this.events = events.ToArray();
			string[] lines = text.Split("\n");
			int maxLineLength = 0;
			for (int i = 0; i < lines.Length; i ++)
			{
				string line = lines[i];
				maxLineLength = Mathf.Max(line.Length, maxLineLength);
			}
			float maxGlyphSizeX = maxGlyphWidth - minGlyphWidth;
			float targetMaxLineWidth = maxGlyphSizeX * text.Length;
			lineHeight = maxGlyphHeight - minGlyphHeight;
			int _lineCount = lines.Length;
			_lineCount += (int) ((float) maxLineLength * maxGlyphWidth / targetMaxLineWidth);
			float _textWidth = targetMaxLineWidth;
			float _textHeight = _lineCount * lineHeight;
			bool sizesAreBothThickerInSameAxis = (_textWidth >= _textHeight) == (bounds.size.x >= bounds.size.y);
			while (targetMaxLineWidth > maxGlyphSizeX)
			{
				int lineCount = lines.Length;
				lineCount += (int) ((float) maxLineLength * maxGlyphWidth / targetMaxLineWidth);
				float textWidth = targetMaxLineWidth;
				float textHeight = lineCount * lineHeight;
				bool newSizesAreBothThickerInSameAxis = (textWidth >= textHeight) == (bounds.size.x >= bounds.size.y);
				if (sizesAreBothThickerInSameAxis != newSizesAreBothThickerInSameAxis)
					break;
				targetMaxLineWidth -= maxGlyphSizeX;
			}
			targetMaxLineWidth += maxGlyphSizeX;
			min = VectorExtensions.INFINITE2;
			max = Vector2.zero;
			List<TextCharacter> _currentSentence = new List<TextCharacter>();
			List<TextCharacter> _textCharacters = new List<TextCharacter>();
			for (int i = 0; i < text.Length; i ++)
			{
				char c = text[i];
				string _text = text.Substring(i);
				Event _event = GetEventAtStart(_text);
				if (_event != null)
				{
					if (displayDelayMode == DisplayDelayMode.NoDelay)
					{
						if (!_event.displayIndicator)
							text = text.Remove(i, _event.indicator.Length);
						_event.Do (this);
					}
					if (!_event.displayIndicator)
						continue;
				}
				if (c == '\n' && !forceOneLine)
				{
					drawingPosition.x = 0;
					drawingPosition.y -= lineHeight;
					lineCount ++;
					min = Vector2.Min(drawingPosition, min);
				}
				else
				{
					TMP_Character character;
					if (fontAsset.characterLookupTable.TryGetValue(c, out character))
					{
						Glyph glyph = character.glyph;
						GlyphRect glyphRect = glyph.glyphRect;
						GlyphMetrics glyphMetrics = glyph.metrics;
						Vector2 glyphOffset = new Vector2(glyphMetrics.horizontalBearingX, glyphMetrics.horizontalBearingY);
						Vector2 textCharacterSize = new Vector2(glyphMetrics.width, glyphMetrics.height) * glyph.scale;
						Vector2 textCharacterMin = drawingPosition + glyphOffset;
						Vector2 textCharacterMax = textCharacterMin + textCharacterSize;
						if (drawingPosition.x != 0 && textCharacterMax.x > targetMaxLineWidth && c == ' ' && !forceOneLine)
						{
							drawingPosition.x = 0;
							drawingPosition.y -= lineHeight;
							lineCount ++;
							textCharacterMin = drawingPosition + glyphOffset;
							textCharacterMax = textCharacterMin + textCharacterSize;
							min = Vector2.Min(textCharacterMin, min);
							max = Vector2.Max(textCharacterMax, max);
						}
						else
						{
							textCharacterMin = drawingPosition + glyphOffset;
							textCharacterMax = textCharacterMin + textCharacterSize;
							Rect textCharacterRect = Rect.MinMaxRect(textCharacterMin.x, textCharacterMin.y, textCharacterMax.x, textCharacterMax.y);
							min = Vector2.Min(textCharacterMin, min);
							max = Vector2.Max(textCharacterMax, max);
							if (collider == null || colliderEffect == ColliderEffect.None)
								MakeCharacter (glyphRect, textCharacterRect.center, textCharacterSize, ref _currentSentence, ref _textCharacters);
							drawingPosition.x += textCharacterSize.x + glyphMetrics.horizontalAdvance;
						}
					}
					if ( c == '.' || c == '?' || c == '!')
					{
						sentences.Add(_currentSentence.ToArray());
						_currentSentence.Clear();
					}
				}
			}
			if (min.x == Mathf.Infinity)
			{
				gameObject.SetActive(false);
				return;
			}
			Vector2 size = max - min;
			gameObject.SetActive(true);
			Vector2 newLocalScale = bounds.size.Divide(size);
			float minLocalScaleComponent = Mathf.Min(newLocalScale.x, newLocalScale.y);
			if (minLocalScaleComponent == Mathf.Infinity)
			{
				gameObject.SetActive(false);
				return;
			}
			scalerTrs.localScale = new Vector3(minLocalScaleComponent, minLocalScaleComponent, 1);
			string alignmentOptionsString = alignmentOptions.ToString();
			bool alignmentOptionsContainsLeft = alignmentOptionsString.Contains("Left");
			bool alignmentOptionsContainsRight = alignmentOptionsString.Contains("Right");
			bool alignmentOptionsContainsDown = alignmentOptionsString.Contains("Bottom");
			bool alignmentOptionsContainsUp = alignmentOptionsString.Contains("Top");
			if (alignmentOptionsContainsLeft)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(bounds.min.x / minLocalScaleComponent);
			else if (alignmentOptionsContainsRight)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(bounds.max.x / minLocalScaleComponent - size.x);
			else
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(bounds.center.x / minLocalScaleComponent - size.x / 2);
			if (alignmentOptionsContainsDown)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(bounds.min.y / minLocalScaleComponent);
			else if (alignmentOptionsContainsUp)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(bounds.max.y / minLocalScaleComponent - size.y);
			else
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(bounds.center.y / minLocalScaleComponent - size.y / 2);
			if (overrideSorting)
			{
				backgroundSpriteRenderer.sortingLayerName = sortingLayerName;
				backgroundSpriteRenderer.sortingOrder = sortingOrder - 1;
			}
			backgroundTrs.gameObject.layer = gameObject.layer;
			backgroundTrs.localPosition = (Vector2) textCharactersParent.localPosition + size / 2;
			textCharactersParent.localPosition -= (Vector3) min;
			backgroundTrs.localScale = size.SetZ(backgroundTrs.localScale.z);
			if (collider != null && colliderEffect != ColliderEffect.None)
			{
				min = VectorExtensions.INFINITE2;
				max = Vector2.zero;
				lineCount = 1;
				drawingPosition = Vector2.zero;
				for (int i = 0; i < text.Length; i ++)
				{
					char c = text[i]; 
					if (c == '\n' && !forceOneLine)
					{
						drawingPosition.x = 0;
						drawingPosition.y -= lineHeight;
						lineCount ++;
					}
					else
					{
						TMP_Character character;
						if (fontAsset.characterLookupTable.TryGetValue(c, out character))
						{
							Glyph glyph = character.glyph;
							GlyphRect glyphRect = glyph.glyphRect;
							GlyphMetrics glyphMetrics = glyph.metrics;
							Vector2 glyphOffset = new Vector2(glyphMetrics.horizontalBearingX, glyphMetrics.horizontalBearingY);
							Vector2 textCharacterSize = new Vector2(glyphMetrics.width, glyphMetrics.height) * glyph.scale;
							Vector2 textCharacterMin = drawingPosition + glyphOffset;
							Vector2 textCharacterMax = textCharacterMin + textCharacterSize;
							min = Vector2.Min(textCharacterMin, min);
							max = Vector2.Max(textCharacterMax, max);
							if (drawingPosition.x != 0 && textCharacterMax.x > targetMaxLineWidth && c == ' ' && !forceOneLine)
							{
								drawingPosition.x = 0;
								drawingPosition.y -= lineHeight;
								lineCount ++;
							}
							else
							{
								textCharacterMin = drawingPosition + glyphOffset;
								textCharacterMax = textCharacterMin + textCharacterSize;
								min = Vector2.Min(textCharacterMin, min);
								max = Vector2.Max(textCharacterMax, max);
								Vector2 textCharacterMinWorld = textCharactersParent.TransformPoint(textCharacterMin);
								Vector2 textCharacterMaxWorld = textCharactersParent.TransformPoint(textCharacterMax);
								Vector2 previousDrawingPosition = drawingPosition;
								uint previousLineCount = lineCount;
								while (collider.OverlapPoint(textCharacterMinWorld) != (colliderEffect == ColliderEffect.StayInside) || collider.OverlapPoint(textCharacterMaxWorld) != (colliderEffect == ColliderEffect.StayInside) || collider.OverlapPoint(new Vector2(textCharacterMinWorld.x, textCharacterMaxWorld.y)) != (colliderEffect == ColliderEffect.StayInside) || collider.OverlapPoint(new Vector2(textCharacterMaxWorld.x, textCharacterMinWorld.y)) != (colliderEffect == ColliderEffect.StayInside))
								{
									drawingPosition.x += colliderCheckDistance;
									textCharacterMin = drawingPosition + glyphOffset;
									textCharacterMax = textCharacterMin + textCharacterSize;
									min = Vector2.Min(textCharacterMin, min);
									max = Vector2.Max(textCharacterMax, max);
									if (textCharacterMax.x > targetMaxLineWidth && c == ' ' && !forceOneLine)
									{
										drawingPosition.x = 0;
										drawingPosition.y -= lineHeight;
										lineCount ++;
									}
									textCharacterMin = drawingPosition + glyphOffset;
									textCharacterMax = textCharacterMin + textCharacterSize;
									min = Vector2.Min(textCharacterMin, min);
									max = Vector2.Max(textCharacterMax, max);
									textCharacterMinWorld = textCharactersParent.TransformPoint(textCharacterMin);
									textCharacterMaxWorld = textCharactersParent.TransformPoint(textCharacterMax);
									if (textCharacterMinWorld.y < rectTrs.TransformPoint(bounds.min).y || textCharacterMaxWorld.x > rectTrs.TransformPoint(bounds.max).x)
									{
										drawingPosition = previousDrawingPosition;
										textCharacterMin = drawingPosition + glyphOffset;
										textCharacterMax = textCharacterMin + textCharacterSize;
										lineCount = previousLineCount;
										break;
									}
								}
								Rect textCharacterRect = Rect.MinMaxRect(textCharacterMin.x, textCharacterMin.y, textCharacterMax.x, textCharacterMax.y);
								MakeCharacter (glyphRect, textCharacterRect.center, textCharacterSize, ref _currentSentence, ref _textCharacters);
								drawingPosition.x += textCharacterSize.x + glyphMetrics.horizontalAdvance;
							}
						}
						if ( c == '.' || c == '?' || c == '!')
						{
							sentences.Add(_currentSentence.ToArray());
							_currentSentence.Clear();
						}
					}
				}
			}
			if (editFinalSize)
			{
				rectTrs.localScale *= multiplyFinalSize;
				rectTrs.localScale += Vector3.one * addToFinalSize;
			}
			textCharacters = _textCharacters.ToArray();
			backgroundSpriteRenderer.maskInteraction = maskInteraction;
			if (displayDelayMode != DisplayDelayMode.NoDelay)
			{
				if (displayDelayMode == DisplayDelayMode.DelayEachCharacter)
				{
					Event _event = GetEventAtStart(text);
					if (_event != null)
						_event.Do (this);
					if (textCharacters.Length > 1)
					{
						currentCharacterIndex = 1;
						displayDelayRemaining = displayDelayPerCharacter + addToTotalDisplayDelay / textCharacters.Length;
						GameManager.updatables = GameManager.updatables.Add(this);
					}
				}
				else if (sentences.Count > 1)
				{
					currentSentence = sentences[0];
					currentSentenceIndex = 1;
					currentCharacterIndex = currentSentence.Length;
					text = this.text.Substring(0, currentSentenceIndex);
					while (text.Length > 0)
					{
						Event _event = GetEventAtStart(text);
						if (_event != null)
							_event.Do (this);
						text = text.Remove(0, 1);
					}
					displayDelayRemaining = displayDelayPerCharacter * currentSentence.Length + addToTotalDisplayDelay / sentences.Count;
					currentSentence = sentences[currentSentenceIndex];
					GameManager.updatables = GameManager.updatables.Add(this);
				}
			}
		}

		public void ClearAndStopUpdate ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			for (int i = 0; i < textCharactersParent.childCount; i ++)
			{
				Transform child = textCharactersParent.GetChild(i);
#if UNITY_EDITOR
				if (!Application.isPlaying)
				{
					GameManager.DestroyOnNextEditorUpdate (child.gameObject);
					continue;
				}
#endif
				TextCharacter textCharacter = child.GetComponent<TextCharacter>();
				ObjectPool.Instance.Despawn (textCharacter.prefabIndex, textCharacter.gameObject, textCharacter.trs);
				i --;
			}
			if (editFinalSize)
				rectTrs.localScale = Vector3.one;
		}

		public void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			displayDelayRemaining -= Time.deltaTime;
			if (displayDelayRemaining <= 0)
			{
				if (displayDelayMode == DisplayDelayMode.DelayEachCharacter)
				{
					DisplayNextCharacter ();
					displayDelayRemaining += displayDelayPerCharacter + addToTotalDisplayDelay / textCharacters.Length;
				}
				else
				{
					for (int i = 0; i < currentSentence.Length; i ++)
						DisplayNextCharacter ();
					currentSentenceIndex ++;
					if (currentSentenceIndex < sentences.Count)
					{
						displayDelayRemaining += displayDelayPerCharacter * currentSentence.Length + addToTotalDisplayDelay / sentences.Count;
						currentSentence = sentences[currentSentenceIndex];
					}
				}
			}
		}

		TextCharacter MakeCharacter (GlyphRect glyphRect, Vector2 position, Vector2 size, ref List<TextCharacter> currentSentence, ref List<TextCharacter> textCharacters)
		{
			TextCharacter textCharacter = ObjectPool.Instance.SpawnComponent<TextCharacter>(textCharacterPrefab, rotation:rectTrs.rotation, parent:textCharactersParent);
			textCharacter.gameObject.layer = gameObject.layer;
			Material material = new Material(this.material);
			material.SetVector("_glyphPositionInAtlas", new Vector2(glyphRect.x, glyphRect.y));
			material.SetVector("_glyphSizeInAtlas", new Vector2(glyphRect.width, glyphRect.height));
			textCharacter.spriteRenderer.sharedMaterial = material;
			textCharacter.spriteRenderer.maskInteraction = maskInteraction;
			if (overrideSorting)
			{
				textCharacter.spriteRenderer.sortingLayerName = sortingLayerName;
				textCharacter.spriteRenderer.sortingOrder = sortingOrder;
			}
			textCharacter.trs.localScale = size.SetZ(textCharacter.trs.localScale.z);
			textCharacter.trs.localPosition = position;
			if (displayDelayMode == DisplayDelayMode.NoDelay || (textCharacters.Count == 0 && displayDelayMode == DisplayDelayMode.DelayEachCharacter) || (sentences.Count == 0 && displayDelayMode == DisplayDelayMode.DelayEachSentence))
			{
			}
			else
				textCharacter.gameObject.SetActive(false);
			currentSentence.Add(textCharacter);
			textCharacters.Add(textCharacter);
			return textCharacter;
		}

		Event GetEventAtStart (string text)
		{
			for (int i = 0; i < events.Length; i ++)
			{
				Event _event = events[i];
				if (text.StartsWith(_event.indicator))
					return _event;
			}
			return null;
		}

		public void DisplayNextCharacter ()
		{
			if (currentCharacterIndex >= textCharacters.Length)
			{
				GameManager.updatables = GameManager.updatables.Remove(this);
				// throw new Exception("currentCharacterIndex is " + currentCharacterIndex + " and textCharacters.Length is " + textCharacters.Length + " when running DisplayNextCharacter");
				return;
			}
			string text = this.text.Substring(currentCharacterIndex);
			Event _event = GetEventAtStart(text);
			if (_event != null)
			{
				if (!_event.displayIndicator)
					currentCharacterIndex += _event.indicator.Length - 1;
				_event.Do (this);
			}
			TextCharacter currentChracter = textCharacters[currentCharacterIndex];
			if (currentChracter != null)
				currentChracter.gameObject.SetActive(true);
			currentCharacterIndex ++;
			if (currentCharacterIndex >= textCharacters.Length)
				GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void SetAlpha (float alpha)
		{
			Material material = new Material(backgroundSpriteRenderer.sharedMaterial);
			material.SetColor("_tint", material.GetColor("_tint").SetAlpha(alpha));
			backgroundSpriteRenderer.sharedMaterial = material;
			for (int i = 0; i < textCharacters.Length; i ++)
			{
				TextCharacter textCharacter = textCharacters[i];
				material = new Material(textCharacter.spriteRenderer.sharedMaterial);
				material.SetColor("_tint", material.GetColor("_tint").SetAlpha(alpha));
				textCharacter.spriteRenderer.sharedMaterial = material;
			}
		}

		public float GetDisplayDelay ()
		{
			float output = 0;
			if (displayDelayMode == DisplayDelayMode.DelayEachCharacter)
			{
				if (textCharacters.Length > 1)
					output = displayDelayPerCharacter + addToTotalDisplayDelay / textCharacters.Length;
			}
			else if (sentences.Count > 1)
				output = displayDelayPerCharacter * currentSentence.Length + addToTotalDisplayDelay / sentences.Count;
			return output;
		}

		[Serializable]
		public class Event
		{
			public string indicator;
			public bool displayIndicator;

			public virtual void Do (_Text text)
			{
			}
		}

		[Serializable]
		public class CustomEvent : Event
		{
			public UnityEvent unityEvent;

			public override void Do (_Text text)
			{
				unityEvent.Invoke();
			}
		}

		[Serializable]
		public class WaitEvent : Event
		{
			public float delay;

			public override void Do (_Text text)
			{
				text.displayDelayRemaining += delay;
			}
		}

		[Serializable]
		public class SetTextBasedOnUsingInputDeviceEvent : Event
		{
			[Multiline]
			public string text;
			public bool mustBeUsingInputDevice;
			public InputManager.InputDevice inputDevice;

			public override void Do (_Text text)
			{
				if (InputManager.instance.inputDevice == inputDevice == mustBeUsingInputDevice)
					text.Text = this.text;
			}
		}

		public enum DisplayDelayMode
		{
			NoDelay,
			DelayEachCharacter,
			DelayEachSentence
		}

		public enum ColliderEffect
		{
			None,
			StayInside,
			StayOutside
		}
	}
}