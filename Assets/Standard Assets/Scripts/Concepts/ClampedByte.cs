﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class ClampedByte
{
	public byte value;
	public ByteRange valueRange;
	
	public byte GetValue ()
	{
		return (byte) Mathf.Clamp(value, valueRange.min, valueRange.max);
	}
	
	public void SetValue (byte value)
	{
		this.value = value;
		this.value = GetValue();
	}
}