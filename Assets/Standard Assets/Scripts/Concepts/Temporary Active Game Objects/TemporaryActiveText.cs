﻿using System;
using UnityEngine.UI;
using System.Collections;

namespace FightRoom
{
	[Serializable]
	public class TemporaryActiveText : TemporaryActiveGameObject
	{
		public Text text;
		public float durationPerCharacter;
		
		public override IEnumerator DoRoutine ()
		{
			duration = text.text.Length * durationPerCharacter;
			yield return base.DoRoutine ();
		}
	}
}