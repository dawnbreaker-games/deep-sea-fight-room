using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class UIntRange : Range<uint>
{
	public UIntRange ()
	{
	}

	public UIntRange (uint min, uint max) : base (min, max)
	{
	}

	public bool DoesIntersect (UIntRange intRange, bool containsMinAndMax = true)
	{
		if (containsMinAndMax)
			return (min >= intRange.min && min <= intRange.max) || (intRange.min >= min && intRange.min <= max) || (max <= intRange.max && max >= intRange.min) || (intRange.max <= max && intRange.max >= min);
		else
			return (min > intRange.min && min < intRange.max) || (intRange.min > min && intRange.min < max) || (max < intRange.max && max > intRange.min) || (intRange.max < max && intRange.max > min);
	}

	public bool Contains (uint f, bool containsMinAndMax = true)
	{
		return Contains(f, containsMinAndMax, containsMinAndMax);
	}

	public bool Contains (uint f, bool containsMin = true, bool containsMax = true)
	{
		bool greaterThanMin = min < f;
		if (containsMin)
			greaterThanMin |= min == f;
		bool lessThanMax = f < max;
		if (containsMax)
			lessThanMax |= f == max;
		return greaterThanMin && lessThanMax;
	}

	public bool GetIntersectionRange (UIntRange intRange, out UIntRange intersectionRange, bool containsMinAndMax = true)
	{
		intersectionRange = null;
		if (DoesIntersect(intRange, containsMinAndMax))
			intersectionRange = new UIntRange((uint) Mathf.Max((int) min, (int) intRange.min), (uint) Mathf.Min((int) max, (int) intRange.max));
		return intersectionRange != null;
	}

	public uint Clamp (uint value)
	{
		return (uint) Mathf.Clamp(value, min, max);
	}

	public override uint Get (float normalizedValue)
	{
		// return (uint) Mathf.RoundToInt((max - min) * normalizedValue + min);
		return (uint) Mathf.RoundToInt(Mathf.Lerp(min - .5f, max + .5f, normalizedValue));
	}

	public override float InverseGet (uint value)
	{
		return Mathf.InverseLerp(min, max, value);
	}
}