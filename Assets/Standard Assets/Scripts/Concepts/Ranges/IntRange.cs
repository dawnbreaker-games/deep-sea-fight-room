using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class IntRange : Range<int>
{
	public IntRange ()
	{
	}

	public IntRange (int min, int max) : base (min, max)
	{
	}

	public bool DoesIntersect (IntRange intRange, bool containsMinAndMax = true)
	{
		if (containsMinAndMax)
			return (min >= intRange.min && min <= intRange.max) || (intRange.min >= min && intRange.min <= max) || (max <= intRange.max && max >= intRange.min) || (intRange.max <= max && intRange.max >= min);
		else
			return (min > intRange.min && min < intRange.max) || (intRange.min > min && intRange.min < max) || (max < intRange.max && max > intRange.min) || (intRange.max < max && intRange.max > min);
	}

	public bool Contains (int f, bool containsMinAndMax = true)
	{
		return Contains(f, containsMinAndMax, containsMinAndMax);
	}

	public bool Contains (int f, bool containsMin = true, bool containsMax = true)
	{
		bool greaterThanMin = min < f;
		if (containsMin)
			greaterThanMin |= min == f;
		bool lessThanMax = f < max;
		if (containsMax)
			lessThanMax |= f == max;
		return greaterThanMin && lessThanMax;
	}

	public bool GetIntersectionRange (IntRange intRange, out IntRange intersectionRange, bool containsMinAndMax = true)
	{
		intersectionRange = null;
		if (DoesIntersect(intRange, containsMinAndMax))
			intersectionRange = new IntRange(Mathf.Max(min, intRange.min), Mathf.Min(max, intRange.max));
		return intersectionRange != null;
	}

	public int Clamp (int value)
	{
		return Mathf.Clamp(value, min, max);
	}

	public override int Get (float normalizedValue)
	{
		// return Mathf.RoundToInt((max - min) * normalizedValue + min);
		return Mathf.RoundToInt(Mathf.Lerp(min - .5f, max + .5f, normalizedValue));
	}

	public override float InverseGet (int value)
	{
		return Mathf.InverseLerp(min, max, value);
	}
}