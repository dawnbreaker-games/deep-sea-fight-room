﻿using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FightRoom
{
	[ExecuteInEditMode]
	public class _Text2 : MonoBehaviour
	{
		public Text text;
		[HideInInspector]
		public Event[] events = new Event[0];
		public CustomEvent[] customEvents = new CustomEvent[0];
		public SetTextBasedOnUsingInputDeviceEvent[] setTextBasedOnUsingInputDeviceEvents = new SetTextBasedOnUsingInputDeviceEvent[0];
		[HideInInspector]
		public string _text;
#if UNITY_EDITOR
		public bool initialize;
#endif
		const string REPLACE_WITH_PLAYER_CUSTOM_NAME = "\\";

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (!initialize)
				return;
			initialize = false;
			Init ();
		}
#endif

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			UpdateText ();
		}

		public void Init ()
		{
			if (text == null)
				text = GetComponent<Text>();
			_text = text.text;
			List<Event> events = new List<Event>(customEvents);
			events.AddRange(setTextBasedOnUsingInputDeviceEvents);
			this.events = events.ToArray();
#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
#endif
		}

		public void UpdateText ()
		{
			text.text = _text.Replace(REPLACE_WITH_PLAYER_CUSTOM_NAME, SaveAndLoadManager.saveData.playerCustomName);
			for (int i = 0; i < _text.Length; i ++)
			{
				string substring = _text.Substring(i);
				for (int i2 = 0; i2 < customEvents.Length; i2 ++)
				{
					CustomEvent _event = customEvents[i2];
					if (substring.StartsWith(_event.indicator))
					{
						text.text = text.text.Replace(_event.indicator, "");
						_event.Do (this);
						break;
					}
				}
				for (int i2 = 0; i2 < setTextBasedOnUsingInputDeviceEvents.Length; i2 ++)
				{
					SetTextBasedOnUsingInputDeviceEvent _event = setTextBasedOnUsingInputDeviceEvents[i2];
					if (substring.StartsWith(_event.indicator))
					{
						if (InputManager.usingInputDevicesDict[_event.inputDevice]() == _event.mustBeUsingInputDevice)
							text.text = text.text.Replace(_event.indicator, _event.text);
						else
							text.text = text.text.Replace(_event.indicator, "");
						_event.Do (this);
						break;
					}
				}
			}
		}

		[Serializable]
		public class Event
		{
			public string indicator;

			public virtual void Do (_Text2 text)
			{
			}
		}

		[Serializable]
		public class CustomEvent : Event
		{
			public UnityEvent unityEvent;

			public override void Do (_Text2 text)
			{
				unityEvent.Invoke();
			}
		}

		[Serializable]
		public class SetTextBasedOnUsingInputDeviceEvent : Event
		{
			[Multiline]
			public string text;
			public bool mustBeUsingInputDevice;
			public InputManager.InputDevice inputDevice;
		}
	}
}