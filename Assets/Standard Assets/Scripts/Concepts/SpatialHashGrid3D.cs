using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public struct SpatialHashGrid3D<T>
{
	public Bounds bounds;
	public float cellSize;
	public Vector3 cellOffset;
	public List<Agent>[,,] agents;

	public SpatialHashGrid3D (float cellSize, Vector3Int cellCount, Vector3 minCellPosition)
	{
		Vector3 boundsSize = (Vector3) cellCount * cellSize;
		bounds = new Bounds(minCellPosition + boundsSize / 2, boundsSize);
		this.cellSize = cellSize;
		cellOffset = minCellPosition + Vector3.one * cellSize / 2;
		agents = new List<Agent>[cellCount.x, cellCount.y, cellCount.z];
		Init ();
	}

	public SpatialHashGrid3D (Bounds bounds, float cellSize)
	{
		this.bounds = bounds;
		this.cellSize = cellSize;
		Vector3Int cellCount = new Vector3Int((int) Mathf.Ceil(bounds.size.x / cellSize), (int) Mathf.Ceil(bounds.size.y / cellSize), (int) Mathf.Ceil(bounds.size.z / cellSize));
		Vector3 minCellPosition = bounds.min - (cellCount - bounds.size / cellSize) / 2;
		cellOffset = minCellPosition + Vector3.one * cellSize / 2;
		agents = new List<Agent>[cellCount.x, cellCount.y, cellCount.z];
		Init ();
	}

	public void Init ()
	{
		for (int x = 0; x < agents.GetLength(0); x ++)
		{
			for (int y = 0; y < agents.GetLength(1); y ++)
			{
				for (int z = 0; z < agents.GetLength(2); z ++)
					agents[x, y, z] = new List<Agent>();
			}
		}
	}

	public Vector3Int Evaluate (Vector3 position)
	{
		return ((position - cellOffset) / cellSize).ToVec3Int();
	}

	public Agent[] GetClosebyAgents (Vector3 position)
	{
		return GetClosebyAgents(Evaluate(position));
	}

	public Agent[] GetClosebyAgents (Vector3Int cellPosition)
	{
		List<Agent> output = new List<Agent>(agents[cellPosition.x, cellPosition.y, cellPosition.z]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y - 1, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y - 1, cellPosition.z]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y - 1, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x, cellPosition.y - 1, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x, cellPosition.y - 1, cellPosition.z]);
		output.AddRange(agents[cellPosition.x, cellPosition.y - 1, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y - 1, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y - 1, cellPosition.z]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y - 1, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y, cellPosition.z]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y + 1, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y + 1, cellPosition.z]);
		output.AddRange(agents[cellPosition.x + 1, cellPosition.y + 1, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x, cellPosition.y + 1, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x, cellPosition.y + 1, cellPosition.z]);
		output.AddRange(agents[cellPosition.x, cellPosition.y + 1, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y + 1, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y + 1, cellPosition.z]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y + 1, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y, cellPosition.z]);
		output.AddRange(agents[cellPosition.x - 1, cellPosition.y, cellPosition.z + 1]);
		output.AddRange(agents[cellPosition.x, cellPosition.y, cellPosition.z - 1]);
		output.AddRange(agents[cellPosition.x, cellPosition.y, cellPosition.z + 1]);
		return output.ToArray();
	}

	[Serializable]
	public struct Agent
	{
		public Vector3 position;
		public Vector3Int cellPosition;
		public T value;
		public SpatialHashGrid3D<T> spatialHashGrid;

		public Agent (Vector3 position, T value, SpatialHashGrid3D<T> spatialHashGrid)
		{
			this.position = position;
			cellPosition = spatialHashGrid.Evaluate(position);
			this.value = value;
			this.spatialHashGrid = spatialHashGrid;
			spatialHashGrid.agents[cellPosition.x, cellPosition.y, cellPosition.z].Add(this);
		}

		public void Remove ()
		{
			spatialHashGrid.agents[cellPosition.x, cellPosition.y, cellPosition.z].Remove(this);
		}

		public void Update (Vector3 position)
		{
			Remove ();
			this.position = position;
			cellPosition = spatialHashGrid.Evaluate(position);
			spatialHashGrid.agents[cellPosition.x, cellPosition.y, cellPosition.z].Add(this);
		}
	}
}