﻿using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace FightRoom
{
	public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
	{
		public SaveData defaultSaveData;
		public static SaveData saveData;
		public static string MostRecentSaveFileName
		{
			get
			{
				return PlayerPrefs.GetString("Most recent save file name", null);
			}
			set
			{
				PlayerPrefs.SetString("Most recent save file name", value);
			}
		}
		public static string filePath;

		void Start ()
		{
			Init ();
		}
		
		static void OnAboutToSave ()
		{
			Init ();
			Achievement.instances = FindObjectsByType<Achievement>(FindObjectsSortMode.InstanceID);
			List<string> completeAchievementsNames = new List<string>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (achievement.complete)
					completeAchievementsNames.Add(achievement.name);
			}
			saveData.completeAchievementsNames = completeAchievementsNames.ToArray();
			if (Level.instance != null)
				saveData.levelName = Level.instance.name;
			if (Player.instance != null)
				saveData.playerName = Player.instance.name;
			saveData.trackedStats = new string[0];
			foreach (KeyValuePair<Stat, StatsMenu.TrackedStat> keyValuePair in StatsMenu.trackedStatsDict)
				saveData.trackedStats = saveData.trackedStats.Add(keyValuePair.Key.name);
			saveData.inputRebindings = InputManager.instance.inputActionAsset.SaveBindingOverridesAsJson();
		}

		public static void Init ()
		{
			filePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Auto-Save";
			print(filePath);
			if (saveData.Equals(default(SaveData)))
				saveData = Instance.defaultSaveData;
			if (saveData.completeAchievementsNames == null)
				saveData.completeAchievementsNames = new string[0];
			if (saveData.bestLevelTimesDict == null)
				saveData.bestLevelTimesDict = new Dictionary<string, float>();
			if (saveData.mostEnemiesKilled == null)
				saveData.mostEnemiesKilled = new Dictionary<string, uint>();
			if (saveData.mostDamageHealed == null)
				saveData.mostDamageHealed = new Dictionary<string, float>();
			if (saveData.triedPlayers == null)
				saveData.triedPlayers = new string[0];
			if (saveData.triedZones == null)
				saveData.triedZones = new string[0];
			if (saveData.triedTutorials == null)
				saveData.triedTutorials = new string[0];
			if (saveData.mousedOverStats == null)
				saveData.mousedOverStats = new string[0];
			if (saveData.unlockedMinigames == null)
				saveData.unlockedMinigames = new string[0];
            if (saveData.trackedStats == null)
                saveData.trackedStats  = new string[0];
        }
		
		public static void Save (string fileName)
		{
			OnAboutToSave ();
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, saveData);
			fileStream.Close();
			MostRecentSaveFileName = fileName;
		}
		
		public void Load (string fileName)
		{
			Init ();
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
			OnLoad (fileName);
		}

		void OnLoad (string fileName)
		{
			MostRecentSaveFileName = fileName;
			OnLoaded ();
		}

		void OnLoaded ()
		{
			Achievement.instances = FindObjectsByType<Achievement>(FindObjectsSortMode.InstanceID);
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				achievement.Init ();
			}
			Level.instances = FindObjectsByType<Level>(FindObjectsSortMode.None);
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				if (level.name == saveData.levelName)
				{
					Level.instance = level;
					break;
				}
			}
			if (!string.IsNullOrEmpty(saveData.inputRebindings))
				InputManager.instance.inputActionAsset.LoadBindingOverridesFromJson(saveData.inputRebindings);
			GameManager.instance.divideDifficulty = saveData.difficultySettings.difficulty;
			if (PlayerSelectMenu.Instance == null || string.IsNullOrEmpty(saveData.playerName))
				return;
			Player player = PlayerSelectMenu.Instance.playersParent.Find(saveData.playerName).GetComponent<Player>();
			PlayerSelectMenu.players = PlayerSelectMenu.instance.playersParent.GetComponentsInChildren<Player>();
			PlayerSelectMenu.currentPlayerIndex = (uint) PlayerSelectMenu.players.IndexOf(player);
			PlayerSelectMenu.instance.SetCurrentPlayer (player);
			if (StatsMenu.Instance == null)
				return;
			for (int i = 0; i < saveData.trackedStats.Length; i ++)
			{
				string trackedStatName = saveData.trackedStats[i];
				for (int i2 = 0; i2 < StatsMenu.instance.statIndicators.Length; i2 ++)
				{
					StatIndicator statIndicator = StatsMenu.instance.statIndicators[i2];
					if (player.stats[i2].name == trackedStatName)
					{
						Draggable draggable = statIndicator.GetComponentInChildren<Draggable>();
						draggable.rectTrs.position = StatsMenu.instance.trackedStatsIndicatorsParent.GetChild(i).position;
						StatsMenu.instance.TrackStat (draggable);
						break;
					}
				}
			}
		}
		
		public void LoadMostRecent ()
		{
			if (!string.IsNullOrEmpty(MostRecentSaveFileName))
				Load (MostRecentSaveFileName);
		}
		
		[Serializable]
		public struct SaveData
		{
			public string[] completeAchievementsNames;
			public string levelName;
			public string playerName;
			public Dictionary<string, float> bestLevelTimesDict;
			public Dictionary<string, uint> mostEnemiesKilled;
			public Dictionary<string, float> mostDamageHealed;
			public string[] triedPlayers;
			public string inputRebindings;
			public string[] triedZones;
			public GameManager.DifficultySettings difficultySettings;
			public bool rankedDuels;
			public bool privateDuels;
			public string privateDuelName;
			public string[] triedTutorials;
			public string[] mousedOverStats;
			public string[] unlockedMinigames;
			public string playerCustomName;
			public string[] trackedStats;
		}
	}
}