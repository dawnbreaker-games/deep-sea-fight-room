﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace FightRoom
{
	public class _SceneManager : SingletonMonoBehaviour<_SceneManager>, ISaveableAndLoadable
	{
		public static bool isLoading;
		public static Scene CurrentScene
		{
			get
			{
				return SceneManager.GetActiveScene();
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			isLoading = false;
		}
		
		public void LoadScene (string sceneName)
		{
			if (TutorialsMenu.Instance != null)
				TutorialsMenu.wasActiveOnExitScene = TutorialsMenu.instance.gameObject.activeInHierarchy;
			isLoading = true;
			SceneManager.LoadScene(sceneName);
		}
		
		public void LoadScene (int sceneId)
		{
			if (TutorialsMenu.Instance != null)
				TutorialsMenu.wasActiveOnExitScene = TutorialsMenu.instance.gameObject.activeInHierarchy;
			isLoading = true;
			SceneManager.LoadScene(sceneId);
		}
		
		public void LoadSceneAdditive (string sceneName)
		{
			isLoading = true;
			SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
		}
		
		public void LoadSceneAdditive (int sceneId)
		{
			isLoading = true;
			SceneManager.LoadScene(sceneId, LoadSceneMode.Additive);
		}
		
		public AsyncOperation LoadSceneAsyncAdditive (string sceneName)
		{
			isLoading = true;
			return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
		}
		
		public AsyncOperation LoadSceneAsyncAdditive (int sceneId)
		{
			isLoading = true;
			return SceneManager.LoadSceneAsync(sceneId, LoadSceneMode.Additive);
		}
		
		public void RestartScene ()
		{
			LoadScene (CurrentScene.name);
		}
		
		public void NextScene ()
		{
			LoadScene (CurrentScene.buildIndex + 1);
		}
	}
}