using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

namespace FightRoom
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		public static Connection connection;
		public static Client client;

		public static void Connect (Callback<Client> onSuccess, Callback<PlayerIOError> onFail)
		{
			PlayerIO.UseSecureApiRequests = true;
			PlayerIO.Authenticate("fight-room-9selufu7bun0mnymblh4w",
				"public",
				new Dictionary<string, string> {
					{ "userId", LoginAndRegisterMenu.username },
				},
				null,
				onSuccess,
				onFail
			);
		}

		public static void Disconnect ()
		{
			if (connection != null)
				connection.Disconnect();
			if (client != null)
				client.Logout();
		}

		public static void DisplayError (PlayerIOError error)
		{
			GameManager.instance.DisplayNotification (error.ToString());
		}

		public static void DisplayErrorAndRetry (PlayerIOError error)
		{
			DisplayError (error);
			new StackTrace().GetFrame(1).GetMethod().Invoke(null, null);
		}

		public static void PrintError (PlayerIOError error)
		{
			print(error.ToString());
		}
	}
}