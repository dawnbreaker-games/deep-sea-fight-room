﻿using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FightRoom
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, ISaveableAndLoadable
	{
		public TemporaryActiveText temporaryActiveTextNotification;
		public SerializableDictionary<MenuType, Menu> menusDict = new SerializableDictionary<MenuType, Menu>();
		public Menu firstMenu;
		public GameObject mainMenuGo;
		public GameObject clearDataPopupGo;
		public TemporaryActiveGameObject noDataToClearPopup;
		public Level firstLevel;
		public Transform acidMeterTrs;
		public Transform bossHealthBarTrs;
		public float divideDifficulty;
		public Button buttonPrefab;
		public Button openLeaderboardButton;
		public InputRebinder[] selectInputRebinders = new InputRebinder[0];
		public GameObject difficultyMenuGo;
		public DifficultySettings[] allDifficultySettings = new DifficultySettings[0];
		public Money moneyPrefab;
		public GameObject pauseButtonGo;
		public RectTransform cursorRectTrs;
		public Image cursorImage;
		public CursorEntry defaultMenusCursorEntry;
		public CursorEntry defaultGameplayCursorEntry;
		public CursorEntry mousedOverEnemyCusrorEntry;
		public CursorEntry shootingCursorEntry;
		public CursorEntry shootingAndMousedOverEnemyCursorEntry;
		public Tutorial2 tutorialOnEnemyDroppedMoney;
		public Tutorial2 tutorialOnGotMoney;
		public Tutorial2 tutorialOnOpenedShop;
		public static Menu currentMenu;
		public static bool paused;
		public static bool Paused
		{
			get
			{
				return paused;
			}
			set
			{
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					bullet.rigid.simulated = !value;
				}
				paused = value;
			}
		}
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoaded;
		public static bool isQuitting;
		public static float pausedTime;
		public static float unscaledPausedTime;
		public static float TimeSinceLevelLoad
		{
			get
			{
				return Time.timeSinceLevelLoad - pausedTime;
			}
		}
		public static float UnscaledTimeSinceLevelLoad
		{
			get
			{
				return Time.unscaledTime - unscaledPausedTime;
			}
		}
		public const byte LAG_FRAMES_AFTER_SCENE_LOAD = 2;
		static MenuType currentMenuType;
		static bool pauseInput;
		static bool previousPauseInput;
		static bool shopInput;
		static bool previousShopInput;
		static bool statsInput;
		static bool previousStatsInput;
		static bool restartInput;
		static bool previousRestartInput;
		static List<string> namesOfAlteredSpriteRenderers = new List<string>();
		static List<string> namesOfAlteredLineRenderers = new List<string>();
		const int INIT_DIFFICULTY_SETTINGS_INDEX = 1;

		public override void Awake ()
		{
			base.Awake ();
			defaultMenusCursorEntry.Apply ();
			if (BuildManager.Instance.clearDataOnFirstStartupOfThisVerison && BuildManager.VersionIndex < BuildManager.Instance.versionIndex)
			{
				bool isFirstStartup = BuildManager.IsFirstStartup;
				PlayerPrefs.DeleteAll();
				BuildManager.VersionIndex = BuildManager.instance.versionIndex;
				BuildManager.IsFirstStartup = isFirstStartup;
			}
			pausedTime = 0;
#if UNITY_EDITOR
			Achievement.completeCount = 0;
			paused = false;
#endif
			SaveAndLoadManager.Init ();
			if (string.IsNullOrEmpty(SaveAndLoadManager.MostRecentSaveFileName))
			{
				SaveAndLoadManager.saveData.difficultySettings = allDifficultySettings[INIT_DIFFICULTY_SETTINGS_INDEX];
				Level.instance = firstLevel;
#if UNITY_EDITOR
				PlayerSelectMenu.currentPlayerIndex = 0;
#endif
				if (PlayerSelectMenu.Instance != null)
					PlayerSelectMenu.instance.Init ();
				if (difficultyMenuGo != null)
					difficultyMenuGo.SetActive(true);
			}
			else
				SaveAndLoadManager.Instance.LoadMostRecent ();
			foreach (KeyValuePair<IDestructable, PoisonBullet.PoisonUpdater> keyValuePair in PoisonBullet.poisonUpdatersDict)
				updatables = updatables.Remove(keyValuePair.Value);
			PoisonBullet.poisonUpdatersDict.Clear();
			if (firstLevel != null)
			{
				firstLevel.lockedIndicatorGo.SetActive(false);
 				string levelName = SaveAndLoadManager.saveData.levelName;
				if (!string.IsNullOrEmpty(levelName))
				{
					Level.instances = FindObjectsByType<Level>(FindObjectsSortMode.None);
					for (int i = 0; i < Level.instances.Length; i ++)
					{
						Level level = Level.instances[i];
						if (level.name == levelName)
						{
							MapMenu.Instance.SetTargetLevel (level);
							break;
						}
					}
				}
				else
					MapMenu.Instance.SetTargetLevel (firstLevel);
				if (openLeaderboardButton != null)
					openLeaderboardButton.gameObject.SetActive(!string.IsNullOrEmpty(LocalUserInfo.username));
			}
			menusDict.Init ();
			currentMenu = MapMenu.Instance;
			if (currentMenu != null)
				currentMenu.Open ();
			else
				Menu.previousMenu = firstMenu;
			framesSinceLevelLoaded = 0;
			Canvas.ForceUpdateCanvases();
		}

		void Update ()
		{
			InputSystem.Update ();
			HandleCursor ();
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			if (!paused)
			{
				Physics2D.Simulate(Time.deltaTime);
				Physics2D.SyncTransforms();
				if (ObjectPool.Instance.enabled)
					ObjectPool.instance.DoUpdate ();
			}
			else
			{
				defaultMenusCursorEntry.Apply ();
				pausedTime += Time.deltaTime;
				unscaledPausedTime += Time.unscaledDeltaTime;
			}
			pauseInput = InputManager.PauseInput;
			shopInput = InputManager.ShopInput;
			statsInput = InputManager.StatsInput;
			if ((Level.instance == null || Level.instance.enabled) && (MapMenu.instance == null || !MapMenu.instance.gameObject.activeSelf))
				HandleMenus ();
			framesSinceLevelLoaded ++;
			previousPauseInput = pauseInput;
			previousShopInput = shopInput;
			previousStatsInput = statsInput;
		}

		void HandleMenus ()
		{
			if (pauseInput && !previousPauseInput && (Menu.previousMenu == null || Menu.previousMenu == PauseMenu.Instance || !Menu.previousMenu.gameObject.activeSelf) && !SettingsMenu.Instance.gameObject.activeSelf)
			{
				if (PauseMenu.Instance.gameObject.activeSelf)
					PauseMenu.instance.Close ();
				else if (!paused && !PauseMenu.instance.gameObject.activeSelf)
					PauseMenu.instance.Open ();
			}
			if (shopInput && !previousShopInput && ShopMenu.Instance != null && Duel.instance == null && Level.instance != null && Level.instance.hasShop)
			{
				if (ShopMenu.instance.gameObject.activeSelf)
					ShopMenu.instance.Close ();
				else if (!paused)
					ShopMenu.instance.Open ();
			}
			if (statsInput && !previousStatsInput && StatsMenu.Instance != null)
			{
				if (StatsMenu.instance.gameObject.activeSelf)
					StatsMenu.instance.Close ();
				else if (!paused && !StatsMenu.Instance.gameObject.activeSelf)
					StatsMenu.instance.Open ();
			}
			if (pauseButtonGo != null)
				pauseButtonGo.SetActive(!paused);
		}

		void HandleCursor ()
		{
			Cursor.visible = false;
			cursorRectTrs.parent.position = (Vector3) InputManager.MousePosition;
		}

		public void StartGame ()
		{
			if (BuildManager.IsFirstStartup)
			{
				paused = false;
				BuildManager.IsFirstStartup = false;
				_SceneManager.Instance.LoadScene ("Initial Tutorial");
			}
			else
				_SceneManager.Instance.LoadScene ("Game");
		}

		public void StartTutorialIfShould (Tutorial2 tutorial)
		{
			if (!SaveAndLoadManager.saveData.triedTutorials.Contains(tutorial.name))
			{
				SaveAndLoadManager.saveData.triedTutorials = SaveAndLoadManager.saveData.triedTutorials.Add(tutorial.name);
				SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
				tutorial.gameObject.SetActive(true);
			}
		}

		public void RetryTutorial (Tutorial2 tutorial)
		{
			RetryTutorial (tutorial.name);
		}

		public void RetryTutorial (string tutorialName)
		{
			SaveAndLoadManager.saveData.triedTutorials = SaveAndLoadManager.saveData.triedTutorials.Remove(tutorialName);
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			TutorialsMenu.currentTutorialName = tutorialName;
			SceneManager.sceneLoaded += OnSceneLoaded4;
		}

		public void EndTutorial ()
		{
			if (TutorialsMenu.wasActiveOnExitScene)
			{
				SceneManager.sceneLoaded += OnSceneLoaded;
				_SceneManager.Instance.LoadScene ("Menu");
			}
			else
			{
				SceneManager.sceneLoaded += OnSceneLoaded2;
				_SceneManager.Instance.LoadScene ("Game");
			}
		}

		public void OpenMainMenu ()
		{
			SceneManager.sceneLoaded += OnSceneLoaded3;
			_SceneManager.Instance.LoadScene ("Menu");
		}

		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
			if (Instance != this)
			{
				instance.OnSceneLoaded ();
				return;
			}
			firstMenu.gameObject.SetActive(false);
			if (TutorialsMenu.wasActiveOnExitScene)
				TutorialsMenu.Instance.gameObject.SetActive(true);
			else
				mainMenuGo.SetActive(true);
		}

		void OnSceneLoaded2 (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSceneLoaded2;
			if (Instance != this)
			{
				instance.OnSceneLoaded2 ();
				return;
			}
			MapMenu.Instance.OnSceneLoaded ();
		}

		void OnSceneLoaded3 (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSceneLoaded3;
			if (Instance != this)
			{
				instance.OnSceneLoaded3 ();
				return;
			}
			firstMenu.gameObject.SetActive(false);
			mainMenuGo.SetActive(true);
		}

		void OnSceneLoaded4 (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSceneLoaded4;
			if (Instance != this)
			{
				instance.OnSceneLoaded4 ();
				return;
			}
			Level.instances = FindObjectsByType<Level>(FindObjectsSortMode.None);
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				if (level.tutorialBeforeLevel != null && level.name == TutorialsMenu.currentTutorialName)
				{
					MapMenu.Instance.SetTargetLevel (level);
					MapMenu.instance.OnSceneLoaded ();
					return;
				}
			}
		}

		public void SetDifficultySettings (int difficultySettingsIndex)
		{
			if (Instance != this)
			{
				instance.SetDifficultySettings (difficultySettingsIndex);
				return;
			}
			int previousDifficultyIndex = allDifficultySettings.IndexOf(SaveAndLoadManager.saveData.difficultySettings);
			if (previousDifficultyIndex != -1)
				((Image) SettingsMenu.Instance.difficultyButtons[previousDifficultyIndex].targetGraphic).sprite = SettingsMenu.instance.notSelectedDifficultySprite;
			DifficultySettings difficultySettings = allDifficultySettings[difficultySettingsIndex];
			((Image) SettingsMenu.Instance.difficultyButtons[difficultySettingsIndex].targetGraphic).sprite = SettingsMenu.instance.selectedDifficultySprite;
			divideDifficulty = difficultySettings.difficulty;
			SaveAndLoadManager.saveData.difficultySettings = difficultySettings;
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
		}

		public void SetCurrentMenu (MenuType menuType)
		{
			currentMenuType = menuType;
			currentMenu.Close ();
			currentMenu = menusDict[currentMenuType];
			currentMenu.Open ();
		}

		public static void HandleOverlappingLineRenderers ()
		{
			LineRenderer[] lineRenderers = FindObjectsByType<LineRenderer>(FindObjectsSortMode.None);
			for (int i = 0; i < lineRenderers.Length; i ++)
			{
				LineRenderer lineRenderer = lineRenderers[i];
				if (!lineRenderer.enabled)
					continue;
				LineSegment2D lineSegment = new LineSegment2D(lineRenderer.GetPosition(0), lineRenderer.GetPosition(1));
				if (!lineRenderer.useWorldSpace)
				{
					Transform lineRendererTrs = lineRenderer.GetComponent<Transform>();
					lineSegment = new LineSegment2D(lineRendererTrs.TransformPoint(lineSegment.start), lineRendererTrs.TransformPoint(lineSegment.end));
				}
				bool sharesPositionsWithOtherLineRenderer = false;
				int i2 = 0;
				if (i < lineRenderers.Length - 1)
					i2 = i + 1;
				for (i2 = i2; i2 < lineRenderers.Length; i2 ++)
				{
					if (i == i2)
						continue;
					LineRenderer lineRenderer2 = lineRenderers[i2];
					if (!lineRenderer2.enabled)
						continue;
					LineSegment2D lineSegment2 = new LineSegment2D(lineRenderer2.GetPosition(0), lineRenderer2.GetPosition(1));
					if (!lineRenderer2.useWorldSpace)
					{
						Transform lineRenderer2Trs = lineRenderer2.GetComponent<Transform>();
						lineSegment2 = new LineSegment2D(lineRenderer2Trs.TransformPoint(lineSegment2.start), lineRenderer2Trs.TransformPoint(lineSegment2.end));
					}
					if (lineSegment.Encapsulates(lineSegment2) || lineSegment2.Encapsulates(lineSegment))
					{
						Vector2 offset = lineSegment.GetPerpendicular().GetDirection();
						lineSegment = lineSegment.Move(offset * lineRenderer.startWidth / 2);
						sharesPositionsWithOtherLineRenderer = true;
						if (!namesOfAlteredLineRenderers.Contains(lineRenderer.name))
						{
							bool previousUsingWorldSpace = lineRenderer.useWorldSpace;
							lineRenderer.SetUseWorldSpace (true);
							lineRenderer.SetPositions(new Vector3[] { lineSegment.start, lineSegment.end });
							lineRenderer.SetUseWorldSpace (previousUsingWorldSpace);
							namesOfAlteredLineRenderers.Add(lineRenderer.name);
						}
						if (!namesOfAlteredLineRenderers.Contains(lineRenderer2.name))
						{
							lineSegment2 = lineSegment2.Move(-offset * lineRenderer2.startWidth / 2);
							bool previousUsingWorldSpace = lineRenderer2.useWorldSpace;
							lineRenderer2.SetUseWorldSpace (true);
							lineRenderer2.SetPositions(new Vector3[] { lineSegment2.start, lineSegment2.end });
							lineRenderer2.SetUseWorldSpace (previousUsingWorldSpace);
							namesOfAlteredLineRenderers.Add(lineRenderer2.name);
						}
					}
					break;
				}
				if (!sharesPositionsWithOtherLineRenderer && namesOfAlteredLineRenderers.Contains(lineRenderer.name))
				{
					Vector2 offset = lineSegment.GetPerpendicular().GetDirection();
					lineSegment = lineSegment.Move(-offset * lineRenderer.startWidth / 2);
					bool previousUsingWorldSpace = lineRenderer.useWorldSpace;
					lineRenderer.SetUseWorldSpace (true);
					lineRenderer.SetPositions(new Vector3[] { lineSegment.start, lineSegment.end });
					lineRenderer.SetUseWorldSpace (previousUsingWorldSpace);
					namesOfAlteredLineRenderers.Remove(lineRenderer.name);
				}
			}
		}
		
		public static void HandleOverlappingSpriteRenderers ()
		{
			SpriteRenderer[] spriteRenderers = FindObjectsByType<SpriteRenderer>(FindObjectsSortMode.None);
			for (int i = 0; i < spriteRenderers.Length; i ++)
			{
				SpriteRenderer spriteRenderer = spriteRenderers[i];
				Transform spriteRendererTrs = spriteRenderer.GetComponent<Transform>();
				bool sharesPositionWithOtherSpriteRenderer = false;
				int i2 = 0;
				if (i < spriteRenderers.Length - 1)
					i2 = i + 1;
				for (i2 = i2; i2 < spriteRenderers.Length; i2 ++)
				{
					if (i == i2)
						continue;
					SpriteRenderer spriteRenderer2 = spriteRenderers[i2];
					Transform spriteRenderer2Trs = spriteRenderer2.GetComponent<Transform>();
					if (spriteRendererTrs.position == spriteRenderer2Trs.position && (spriteRenderer.color != spriteRenderer2.color || spriteRenderer.sharedMaterial != spriteRenderer2.sharedMaterial))
					{
						sharesPositionWithOtherSpriteRenderer = true;
						if (!namesOfAlteredSpriteRenderers.Contains(spriteRenderer.name))
						{
							spriteRenderer.material.SetColor("_tint", spriteRenderer.material.GetColor("_tint").DivideAlpha(2));
							namesOfAlteredSpriteRenderers.Add(spriteRenderer.name);
						}
						if (!namesOfAlteredSpriteRenderers.Contains(spriteRenderer2.name))
						{
							spriteRenderer2.material.SetColor("_tint", spriteRenderer2.material.GetColor("_tint").DivideAlpha(2));
							namesOfAlteredSpriteRenderers.Add(spriteRenderer2.name);
						}
					}
					break;
				}
				if (!sharesPositionWithOtherSpriteRenderer && namesOfAlteredSpriteRenderers.Contains(spriteRenderer.name))
				{
					spriteRenderer.material.SetColor("_tint", spriteRenderer.material.GetColor("_tint").MultiplyAlpha(2));
					namesOfAlteredSpriteRenderers.Remove(spriteRenderer.name);
				}
			}
		}

#if UNITY_WEGBL
		[DllImport("__Internal")]
    	private static extern void Exit ();
#endif

		public void Quit ()
		{
#if UNITY_EDITOR
			EditorApplication.isPlaying = false;
#else
#if UNITY_WEGBL
			Exit ();
#endif
			Application.Quit();
#endif
		}

		void OnApplicationQuit ()
		{
			isQuitting = true;
			// PlayerPrefs.DeleteAll();
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			updatables = new IUpdatable[0];
		}

		public void ToggleGameObject (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public void ToggleGameObjectNextFrame (GameObject go)
		{
			EventManager.AddEvent ((object obj) => { go.SetActive(!go.activeSelf); } );
		}

		public void DeactivateGameObjectNextFrame (GameObject go)
		{
			EventManager.AddEvent ((object obj) => { go.SetActive(false); } );
		}

		public void DestroyChildren (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				Destroy(trs.GetChild(i).gameObject);
		}

		public void DestroyChildrenImmediate (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				DestroyImmediate(trs.GetChild(i).gameObject);
		}

		public void OnClearDataButtonPressed ()
		{
			if (instance != this)
			{
				instance.OnClearDataButtonPressed ();
				return;
			}
			if (string.IsNullOrEmpty(SaveAndLoadManager.MostRecentSaveFileName))
				noDataToClearPopup.Do ();
			else
				clearDataPopupGo.SetActive(true);
		}

		public void ClearData ()
		{
			PlayerPrefs.DeleteAll();
			BuildManager.VersionIndex = BuildManager.instance.versionIndex;
			BuildManager.IsFirstStartup = true;
			SaveAndLoadManager.saveData = SaveAndLoadManager.instance.defaultSaveData;
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			_SceneManager.Instance.LoadScene ("Menu");
		}

		public void DisplayNotification (string text)
		{
			temporaryActiveTextNotification.text.text = text;
			temporaryActiveTextNotification.Do ();
		}

		public void DisplayChoices (string[] choices, Action[] results, Button buttonPrefab = null)
		{
			if (buttonPrefab == null)
				buttonPrefab = instance.buttonPrefab;
			for (int i = 0; i < choices.Length; i ++)
			{
				string choice = choices[i];
				Button button = Instantiate(buttonPrefab);
				button.GetComponentInChildren<_Text>().Text = choices[i];
				Action result = results[i];
				if (result != null)
					button.onClick.AddListener(() => { result(); } );
			}
		}

		public void PressButton (Button buutton)
		{
			buutton.onClick.Invoke();
		}

		public static void Log (object obj)
		{
			print(obj);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
#endif

		public static void DoActionToObjectAndSpawnedInstances<T> (Action<T> action, T obj) where T : ISpawnable
		{
			action (obj);
			for (int i = 0; i < ObjectPool.instance.spawnedEntries.Count; i ++)
			{
				ObjectPool.SpawnedEntry spawnedEntry = ObjectPool.instance.spawnedEntries[i];
				T instance = spawnedEntry.go.GetComponent<T>();
				if (obj != null)
					action(obj);
			}
		}

		public enum MenuType
		{
			Map,
			Settings
		}

		[Serializable]
		public struct DifficultySettings
		{
			public string name;
			public float difficulty;
		}

		[Serializable]
		public struct CursorEntry
		{
			public Sprite sprite;
			public Vector2 localPosition;
			public Vector2 sizeDelta;
			public CursorLockMode lockMode;
			public Color tint;

			public void Apply ()
			{
				RectTransform cursorRectTrs = instance.cursorRectTrs;
				Image cursorImage = instance.cursorImage;
				cursorImage.enabled = false;
				cursorImage.color = tint;
				cursorImage.sprite = sprite;
				cursorRectTrs.localPosition = localPosition;
				cursorRectTrs.sizeDelta = sizeDelta;
				Cursor.lockState = lockMode;
				cursorImage.enabled = true;
			}
		}
	}
}