using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public class EventManager : SingletonUpdateWhileEnabled<EventManager>
	{
		public static List<Event> events = new List<Event>();
		public static int currentEventIndex;
		public static int previousEventCount;

		public override void Awake ()
		{
			base.Awake ();
			events.Clear();
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused || Level.Instance.rewindDurationRemaining > 0)
				return;
			previousEventCount = events.Count;
			for (currentEventIndex = 0; currentEventIndex < previousEventCount; currentEventIndex ++)
			{
				Event _event = events[currentEventIndex];
				_event.timeUntilEvent -= Time.deltaTime;
				if (_event.timeUntilEvent <= 0)
				{
					_event.onEvent (_event.arg);
					if (currentEventIndex < events.Count && currentEventIndex >= 0)
					{
						events.RemoveAt(currentEventIndex);
						currentEventIndex --;
						previousEventCount --;
					}
				}
			}
		}

		public static Event AddEvent (Action<object> action, float timeUntilEvent = 0, object arg = null)
		{
			Event _event = new Event(action, timeUntilEvent, arg);
			events.Add(_event);
			return _event;
		}

		public static bool RemoveEvent (Event _event)
		{
			int eventIndex = events.IndexOf(_event);
			if (eventIndex == -1)
				return false;
			events.RemoveAt(eventIndex);
			if (eventIndex <= currentEventIndex)
				currentEventIndex --;
			previousEventCount --;
			return true;
		}

		public class Event
		{
			public Action<object> onEvent;
			public float timeUntilEvent;
			public object arg;

			public Event (Action<object> onEvent, float timeUntilEvent, object arg)
			{
				this.onEvent = onEvent;
				this.timeUntilEvent = timeUntilEvent;
				this.arg = arg;
			}

			public Event (Event _event) : this (_event.onEvent, _event.timeUntilEvent, _event.arg)
			{
			}
		}
	}
}