﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[RequireComponent(typeof(Light))]
	public class _Light : UpdateWhileEnabled
	{
		public Transform trs;
		public new Light light;
		float range;
		float intensity;
		
		void Start ()
		{
			range = light.range;
			intensity = light.intensity;
		}
		
		public override void DoUpdate ()
		{
			light.range = range * trs.lossyScale.x;
			light.intensity = intensity * trs.lossyScale.x * trs.lossyScale.x;
		}

		public void Scale (float rangeMultiplier)
		{
			light.range = light.range * rangeMultiplier;
			light.intensity = light.intensity * rangeMultiplier * rangeMultiplier;
		}
	}
}