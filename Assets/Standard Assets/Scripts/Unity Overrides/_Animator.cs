using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(Animator))]
	public class _Animator : MonoBehaviour
	{
		public Animator animator;
		// public static List<_Animator> instances = new List<_Animator>();
		public static _Animator[] instances = new _Animator[0];

#if UNITY_EDITOR
		void Awake ()
		{
			if (!Application.isPlaying)
			{
				if (animator == null)
					animator = GetComponent<Animator>();
				return;
			}
		}
#endif

		// void OnEnable ()
		// {
		// 	instances.Add(this);
		// }

		// void OnDisable ()
		// {
		// 	instances.Remove(this);
		// }

		public void PauseAnimator (Func<bool> whenToUnpause)
		{
			animator.enabled = false;
			GameManager.updatables = GameManager.updatables.Add(new UnpauseUpdater(this, whenToUnpause));
		}

		public class UnpauseUpdater : IUpdatable
		{
			public _Animator animator;
			public Func<bool> shouldUnpause;

			public UnpauseUpdater (_Animator animator, Func<bool> shouldUnpause)
			{
				this.animator = animator;
				this.shouldUnpause = shouldUnpause;
			}
			
			public void DoUpdate ()
			{
				if (shouldUnpause())
				{
					animator.animator.enabled = true;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}