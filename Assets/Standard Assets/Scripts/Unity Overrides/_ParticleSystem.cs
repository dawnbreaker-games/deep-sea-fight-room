using FightRoom;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class _ParticleSystem : Spawnable
{
	public ParticleSystem particleSystem;
	ParticleSystem duplicateParticleSystem;

	public override void Start ()
	{
#if UNITY_EDITOR
		base.Start ();
		if (!Application.isPlaying)
		{
			if (particleSystem == null)
				particleSystem = GetComponent<ParticleSystem>();
			return;
		}
#endif
		if (Level.instance.type == Level.Type.Playback && duplicateParticleSystem == null)
		{
			gameObject.SetActive(false);
			duplicateParticleSystem = Instantiate(particleSystem);
			Destroy(duplicateParticleSystem.GetComponent<_ParticleSystem>());
			gameObject.SetActive(true);
			EventManager.AddEvent((object obj) => { duplicateParticleSystem.gameObject.SetActive(true); }, Level.instance.playbackTimeOffset);
		}
	}
}