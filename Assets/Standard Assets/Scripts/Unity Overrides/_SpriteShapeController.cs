﻿﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.U2D;
using System.Collections;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class _SpriteShapeController : SpriteShapeController
{
	public Transform trs;
#if UNITY_EDITOR
	public Vector2 pivotOffset;
	public bool recenter;
	public float snapInterval;

	void OnValidate ()
	{
		if (trs == null)
			trs = GetComponent<Transform>();
		Vector2 center = new Vector2();
		for (int i = 0; i < spline.GetPointCount(); i ++)
		{
			Vector2 point = spline.GetPosition(i);
			point = trs.InverseTransformPoint(trs.TransformPoint(point).Snap(Vector2.one * snapInterval));
			spline.SetPosition(i, point);
			center += (Vector2) trs.TransformPoint(point);
		}
		if (recenter)
		{
			recenter = false;
			center /= spline.GetPointCount();
			center += pivotOffset;
			Vector2 previousPosition = trs.position;
			trs.position = center.SetZ(trs.position.z);
			for (int i = 0; i < spline.GetPointCount(); i ++)
			{
				Vector2 point = spline.GetPosition(i);
				point += previousPosition - (Vector2) trs.position;
				spline.SetPosition(i, point);
			}
		}
	}
#endif
}