using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Button))]
public class _Button : _Selectable
{
	public ColorBlock colors;
	public RectTransform canvasRectTrs;
	public static _Button[] instances = new _Button[0];

	public override void OnEnable ()
	{
		base.OnEnable ();
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		instances = instances.Add(this);
		if (!selectable.IsInteractable())
			selectable.image.color = colors.disabledColor;
		else
			selectable.image.color = colors.normalColor;
	}

#if UNITY_EDITOR
    public override void OnValidate ()
    {
		base.OnValidate ();
		Transform trs = rectTrs;
		while (trs != null)
		{
            Canvas canvas = trs.GetComponent<Canvas>();
			if (canvas != null)
				canvasRectTrs = (RectTransform) trs;
			trs = trs.parent;
		}
		EditorUtility.SetDirty(this);
    }
#endif

	public override void OnDisable ()
	{
		base.OnDisable ();
		instances = instances.Remove(this);
	}

	public void OnPointerEnter ()
	{
		if (selectable.IsInteractable())
			selectable.image.color = colors.highlightedColor;
	}

	public void OnPointerExit ()
	{
		if (selectable.IsInteractable())
			selectable.image.color = colors.normalColor;
	}

	public void OnPointerDown ()
	{
		if (selectable.IsInteractable())
			selectable.image.color = colors.pressedColor;
	}

	public void OnPointerUp ()
	{
		if (selectable.IsInteractable())
			selectable.image.color = colors.normalColor;
	}
}