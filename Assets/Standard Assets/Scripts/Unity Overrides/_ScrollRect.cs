using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(ScrollRect))]
public class _ScrollRect : _Selectable
{
	public RectTransform spriteMaskTrs;
	public ScrollRect scrollRect;
#if UNITY_WEBGL || UNITY_EDITOR
	public float divideScrollSensitivityForWebGL;
#endif

	void Awake ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			if (scrollRect != null)
				scrollRect = GetComponent<ScrollRect>();
			return;
		}
#elif UNITY_WEBGL
		scrollRect.scrollSensitivity /= divideScrollSensitivityForWebGL;
#endif
		if (spriteMaskTrs != null)
			spriteMaskTrs.localScale = spriteMaskTrs.rect.size;
	}

	public void OnScrolled (Vector2 value)
	{
		Rect worldRect = rectTrs.GetWorldRect();
		spriteMaskTrs.position = spriteMaskTrs.position.SetY((worldRect.center + worldRect.size.Multiply(value * Time.deltaTime)).y);
	}
}
