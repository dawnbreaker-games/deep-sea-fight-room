using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
[RequireComponent(typeof(Selectable))]
public class _Selectable : MonoBehaviour
{
	public RectTransform rectTrs;
	public Selectable selectable;
	public static _Selectable[] instances = new _Selectable[0];

	public virtual void OnEnable ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		instances = instances.Add(this);
	}

#if UNITY_EDITOR
    public virtual void OnValidate ()
	{
        if (rectTrs == null)
            rectTrs = GetComponent<RectTransform>();
        if (selectable == null)
            selectable = GetComponent<Selectable>();
		EditorUtility.SetDirty(this);
    }
#endif

    public virtual void OnDisable ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		instances = instances.Remove(this);
	}
}
