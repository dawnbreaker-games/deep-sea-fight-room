#if UNITY_EDITOR
using TMPro;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using UnityEditor.Callbacks;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	[InitializeOnLoad]
	public class CopyFontGlyphMetrics : PreBuildScript
	{
		public TMP_FontAsset copyFromFontAsset;
		public TMP_FontAsset copyToFontAsset;

		public override void Do ()
		{
			for (int i = 0; i < copyFromFontAsset.glyphTable.Count; i ++)
			{
				Glyph glyph = copyFromFontAsset.glyphTable[i];
				Glyph glyph2;
				if (copyToFontAsset.glyphLookupTable.TryGetValue(glyph.index, out glyph2))
					glyph2.metrics = glyph.metrics;
			}
		}

		[DidReloadScripts]
		[MenuItem("Game/Copy font glyph metrics for all CopyFontGlyphMetrics %&c")]
		public static void _DoForAll ()
		{
			CopyFontGlyphMetrics[] instances = FindObjectsByType<CopyFontGlyphMetrics>(FindObjectsSortMode.None);
			for (int i = 0; i < instances.Length; i ++)
			{
				CopyFontGlyphMetrics instance = instances[i];
				instance.Do ();
			}
		}
	}
}
#else
namespace FightRoom
{
	public class CopyFontGlyphMetrics : EditorScript
	{
	}
}
#endif