#if UNITY_EDITOR
using TMPro;
using System;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	public class SetFontGlyphMetrics : EditorScript
	{
		public TMP_FontAsset fontAsset;
		public ChooseCharactersMode includeCharactersMode;
		public UnicodeCategories includeUnicodeCategories;
		public string includeCustomCharacters;
		public ChooseCharactersMode excludeCharactersMode;
		public UnicodeCategories excludeUnicodeCategories;
		public string excludeCustomCharacters;
		public OperationType operation;
		public GlyphMetricsValues glyphMetricsValues;
		public float value;

		public override void Do ()
		{
			for (int i = 0; i < fontAsset.characterTable.Count; i ++)
			{
				TMP_Character character = fontAsset.characterTable[i];
				bool operateOnGlyphMetrics = false;
				if (includeCharactersMode.HasFlag(ChooseCharactersMode.UnicodeCategories) && includeUnicodeCategories.HasFlag((UnicodeCategories) Mathf.Pow(2, 1 + Char.GetUnicodeCategory((char) character.unicode).GetHashCode())))
					operateOnGlyphMetrics = true;
				else if (includeCharactersMode.HasFlag(ChooseCharactersMode.CustomCharacters) && includeCustomCharacters.Contains(((char) character.unicode)))
					operateOnGlyphMetrics = true;
				if (operateOnGlyphMetrics)
				{
					if (excludeCharactersMode.HasFlag(ChooseCharactersMode.UnicodeCategories) && excludeUnicodeCategories.HasFlag((UnicodeCategories) Mathf.Pow(2, 1 + Char.GetUnicodeCategory((char) character.unicode).GetHashCode())))
						operateOnGlyphMetrics = false;
					else if (excludeCharactersMode.HasFlag(ChooseCharactersMode.CustomCharacters) && excludeCustomCharacters.Contains(((char) character.unicode)))
						operateOnGlyphMetrics = false;
				}
				else
					continue;
				Glyph glyph;
				if (operateOnGlyphMetrics && fontAsset.glyphLookupTable.TryGetValue(character.glyph.index, out glyph))
					glyph.metrics = OperateOnGlyphMetrics(glyph.metrics);
			}
		}

		GlyphMetrics OperateOnGlyphMetrics (GlyphMetrics metrics)
		{
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.Width))
				metrics = new GlyphMetrics(OperateOnGlyphMetricsValue(metrics.width), metrics.height, metrics.horizontalBearingX, metrics.horizontalBearingY, metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.Height))
				metrics = new GlyphMetrics(metrics.width, OperateOnGlyphMetricsValue(metrics.height), metrics.horizontalBearingX, metrics.horizontalBearingY, metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.HorizontalBearingX))
				metrics = new GlyphMetrics(metrics.width, metrics.height, OperateOnGlyphMetricsValue(metrics.horizontalBearingX), metrics.horizontalBearingY, metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.HorizontalBearingY))
				metrics = new GlyphMetrics(metrics.width, metrics.height, metrics.horizontalBearingX, OperateOnGlyphMetricsValue(metrics.horizontalBearingY), metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.HorizontalAdvance))
				metrics = new GlyphMetrics(metrics.width, metrics.height, metrics.horizontalBearingX, metrics.horizontalBearingY, OperateOnGlyphMetricsValue(metrics.horizontalAdvance));
			return metrics;
		}

		float OperateOnGlyphMetricsValue (float value)
		{
			if (operation == OperationType.Set)
				return this.value;
			else if (operation == OperationType.Add)
				return value + this.value;
			else if (operation == OperationType.Subtract)
				return value - this.value;
			else if (operation == OperationType.SubtractSwapped)
				return this.value - value;
			else if (operation == OperationType.Multiply)
				return value * this.value;
			else if (operation == OperationType.Divide)
				return value / this.value;
			else if (operation == OperationType.DivideSwapped)
				return this.value / value;
			else if (operation == OperationType.Power)
				return Mathf.Pow(value, this.value);
			else if (operation == OperationType.PowerSwapped)
				return Mathf.Pow(this.value, value);
			else if (operation == OperationType.Modulo)
				return value % this.value;
			else // if (operation == OperationType.ModuloSwapped)
				return this.value % value;
		}

		[Flags]
		public enum ChooseCharactersMode
		{
			UnicodeCategories = 2,
			CustomCharacters = 4
		}

		public enum OperationType
		{
			Set,
			Add,
			Subtract,
			SubtractSwapped,
			Multiply,
			Divide,
			DivideSwapped,
			Power,
			PowerSwapped,
			Modulo,
			ModuloSwapped
		}

		[Flags]
		public enum GlyphMetricsValues
		{
			Width = 2,
			Height = 4,
			HorizontalBearingX = 8,
			HorizontalBearingY = 16,
			HorizontalAdvance = 32
		}
	}
}
#else
namespace FightRoom
{
	public class SetFontGlyphMetrics : EditorScript
	{
	}
}
#endif