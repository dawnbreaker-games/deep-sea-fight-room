#if UNITY_EDITOR
using TMPro;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using UnityEditor.Callbacks;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	[InitializeOnLoad]
	public class ImportAsset : PreBuildScript
	{
		public string assetPath;
		public ImportAssetOptions importAssetOptions;

		public override void Do ()
		{
			// AssetDatabase.ImportAsset(assetPath, importAssetOptions);
		}

		[DidReloadScripts]
		[MenuItem("Game/Reimport assets for all ImportAssets %&c")]
		public static void _DoForAll ()
		{
			ImportAsset[] instances = FindObjectsByType<ImportAsset>(FindObjectsSortMode.None);
			for (int i = 0; i < instances.Length; i ++)
			{
				ImportAsset instance = instances[i];
				instance.Do ();
			}
		}
	}
}
#else
namespace FightRoom
{
	public class ImportAsset : EditorScript
	{
	}
}
#endif