#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class MakeWalls : EditorScript
	{
		public UIntRange wallCountRange;
		public UIntRange wallPointCountRange;
		public FloatRange wallPointSpacingRange;
		public FloatRange wallSpacingRange;
		public int maxRetries;
		public Collider2D boundsCollider;
		public _SpriteShapeController wallPrefab;
		Dictionary<_SpriteShapeController, Shape2D> wallsShapesDict = new Dictionary<_SpriteShapeController, Shape2D>();
		
		public override void Do ()
		{
			wallsShapesDict.Clear();
			Rect rect = boundsCollider.bounds.ToRect().Expand(-Vector2.one * wallSpacingRange.min * 2);
			for (int i = 0; i <= maxRetries; i ++)
			{
				bool failed = false;
				uint wallCount = wallCountRange.Get(Random.value);
				for (int i2 = 0; i2 < wallCount; i2 ++)
				{
					_SpriteShapeController wall = Instantiate(wallPrefab);
					uint pointCount = wallPointCountRange.Get(Random.value);
					Vector2[] points = new Vector2[pointCount];
					Shape2D shape;
					while (true)
					{
						for (int i3 = 0; i3 < pointCount; i3 ++)
						{
							Vector2 point;
							bool isValidPoint = true;
							while (true)
							{
								point = rect.RandomPoint().Snap(Vector2.one * wall.snapInterval);
								for (int i4 = 0; i4 < i3; i4 ++)
								{
									Vector2 point2 = points[i4];
									float distanceSqr = (point - point2).sqrMagnitude;
									if (distanceSqr < wallPointSpacingRange.min * wallPointSpacingRange.min || distanceSqr > wallPointSpacingRange.max * wallPointSpacingRange.max)
									{
										isValidPoint = false;
										break;
									}
									if (!isValidPoint)
										break;
								}
								if (isValidPoint)
									break;
							}
							if (isValidPoint)
							{
								for (int i5 = 0; i5 < wallsShapesDict.Count; i5 ++)
								{
									Shape2D shape2 = wallsShapesDict.Values.Get(i5);
									float spacingSqr = Mathf.Infinity;
									for (int i6 = i5 + 1; i6 < wallsShapesDict.Count; i6 ++)
									{
										Shape2D shape3 = wallsShapesDict.Values.Get(i6);
										spacingSqr = Mathf.Min(shape2.GetDistanceSqr(shape3), spacingSqr);
										if (spacingSqr < wallSpacingRange.min * wallSpacingRange.min || spacingSqr > wallSpacingRange.max * wallSpacingRange.max)
										{
											isValidPoint = false;
											break;
										}
									}
									if (!isValidPoint)
										break;
								}
							}
							else
								break;
						}
					}
					wallsShapesDict.Add(wall, shape);
				}
				foreach (KeyValuePair<_SpriteShapeController, Shape2D> keyValuePair in wallsShapesDict)
					keyValuePair.Value.ToSpline (keyValuePair.Key.spline);
				DebugExtensions.DrawRect (rect, Color.red, 1);
			}
		}
	}
}
#else
namespace FightRoom
{
	public class MakeWalls : EditorScript
	{
	}
}
#endif
