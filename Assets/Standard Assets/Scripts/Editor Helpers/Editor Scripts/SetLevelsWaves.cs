#if UNITY_EDITOR
using System;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using Wave = FightRoom.Level.Wave;
using EnemySpawnEntry = FightRoom.Level.EnemySpawnEntry;

namespace FightRoom
{
	public class SetLevelsWaves : EditorScript
	{
		public byte waveCountPerLevel = 30;
		public byte sameWavesInARow = 2;
        public Wave[] allWaves = new Wave[0];
		public bool clearLevelsWaves;
		public bool setToAllWaves;
		public bool setFromAllWaves;
		public CorrectEnemyPositions correctEnemyPositions;

		public override void OnValidate ()
		{
			base.OnValidate ();
			List<Level> levels = new List<Level>(FindObjectsByType<Level>(FindObjectsSortMode.None));
			levels = levels.RemoveEach(FindObjectsByType<BossLevel>(FindObjectsSortMode.None));
			for (int i = 0; i < levels.Count; i ++)
			{
				Level level = levels[i];
				if (level.enemySpawnEntries.Length == 0)
				{
					levels.RemoveAt(i);
					i --;
				}
			}
			if (clearLevelsWaves)
			{
				clearLevelsWaves = false;
				for (int i = 0; i < levels.Count; i ++)
				{
					Level level = levels[i];
					level.waves = new Wave[0];
					EditorUtility.SetDirty(level);
				}
			}
			if (setToAllWaves)
			{
				setToAllWaves = false;
				List<Wave> _allWaves = new List<Wave>();
				for (int i = 0; i < levels.Count; i ++)
				{
					Level level = levels[i];
					Wave[] waves = new Wave[level.waves.Length];
					level.waves.CopyTo(waves, 0);
					if (waves.Length == 0)
						waves = new Wave[] { new Wave(level.enemySpawnEntries) };
					_allWaves.AddRange(waves);
				}
				allWaves = _allWaves.ToArray();
			}
			else if (setFromAllWaves)
			{
				setFromAllWaves = false;
				int waveIndex = 0;
				Rule[] rules = new Rule[] { correctEnemyPositions };
				for (int i = 0; i < rules.Length; i ++)
				{
					Rule rule = rules[i];
					rule.Init (allWaves);
				}
				List<Wave> _waves = new List<Wave>();
				for (int i = 0; i < levels.Count; i ++)
				{
					Level level = levels[i];
					print(level);
					level.waves = new Wave[0];
					for (int i2 = 0; i2 < waveCountPerLevel; i2 ++)
					{
						//Wave wave = default(Wave);
						//if (waveIndex >= allWaves.Length || waveIndex >= _waves.Count)
						//{
							//waveIndex = 0;
							//bool canAddWave = true;
							//while (true)
							//{
								//_waves = new List<Wave>(allWaves);
								//_waves = _waves.Randomize();
								//wave = _waves[0];
							//	for (int i3 = 0; i3 < rules.Length; i3 ++)
							//	{
							//		Rule rule = rules[i3];
							//		if (!rule.CanAddWave(level, wave))
							//		{
							//			int totalWaveCount = levels.Count * (int) waveCountPerLevel;
							//			print("Failed at " + totalWaveIndex + "/" + totalWaveCount + ". " + ((float) totalWaveIndex / totalWaveCount) * 100);
							//			canAddWave = false;
							//			break;
							//		}
							//	}
							//	if (canAddWave)
							//		break;
							//}
						//}
						//else
						//	wave = _waves[waveIndex];
						Wave wave = allWaves[waveIndex % allWaves.Length];
						for (int i3 = 0; i3 < sameWavesInARow; i3 ++)
							level.waves = level.waves.Add(wave);
						waveIndex ++;
                    }
                    EditorUtility.SetDirty(level);
				}
			}
		}
	}

	[Serializable]
	public class Rule
	{
		public virtual void Init (Wave[] allWaves)
		{
		}
		
		public virtual bool CanAddWave (Level level, Wave wave)
		{
			throw new NotImplementedException();
		}
	}

	[Serializable]
	public class CorrectEnemyPositions : Rule
	{
		[Range(0, 1)]
		public float makeFirstEnemyChangeLaterAtStart;
		[Range(0, 1)]
		public float makeLastEnemyChangeEarlierAtEnd;
		public byte easierEnemyTypeAfterHardEnemyTypeCount;
		FloatRange levelDifficultyRange = new FloatRange();
		FloatRange enemyDifficultyRange = new FloatRange();
		byte enemyTypeCount;

		public override void Init (Wave[] allWaves)
		{
			Level.instances = MonoBehaviour.FindObjectsByType<Level>(FindObjectsSortMode.None);
			List<Enemy> enemies = new List<Enemy>();
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				levelDifficultyRange.min = Mathf.Min(level.divideDifficulty, levelDifficultyRange.min);
				levelDifficultyRange.max = Mathf.Max(level.divideDifficulty, levelDifficultyRange.max);
			}
			for (int i = 0; i < allWaves.Length; i ++)
			{
				Wave wave = allWaves[i];
				for (int i2 = 0; i2 < wave.enemySpawnEntries.Length; i2 ++)
				{
					EnemySpawnEntry enemySpawnEntry = wave.enemySpawnEntries[i2];
					Enemy enemyPrefab = enemySpawnEntry.enemyPrefab;
					if (!enemies.Contains(enemyPrefab))
						enemies.Add(enemyPrefab);
					float enemyPrefabDifficulty = enemyPrefab.difficulty;
					enemyDifficultyRange.min = Mathf.Min(enemyPrefabDifficulty, enemyDifficultyRange.min);
					enemyDifficultyRange.max = Mathf.Max(enemyPrefabDifficulty, enemyDifficultyRange.max);
				}
			}
			enemyTypeCount = (byte) enemies.Count;
		}

		public override bool CanAddWave (Level level, Wave wave)
		{
			float normalizedLevelDifficulty = levelDifficultyRange.InverseGet(level.divideDifficulty);
			for (int i2 = 0; i2 < wave.enemySpawnEntries.Length; i2 ++)
			{
				EnemySpawnEntry enemySpawnEntry = wave.enemySpawnEntries[i2];
				float normalizedEnemyDifficulty = enemyDifficultyRange.InverseGet(enemySpawnEntry.enemyPrefab.difficulty);
				float difficultyOffset = normalizedEnemyDifficulty - normalizedLevelDifficulty;
				if (difficultyOffset < makeFirstEnemyChangeLaterAtStart || difficultyOffset > 1f - makeLastEnemyChangeEarlierAtEnd || difficultyOffset > 1f / enemyTypeCount / (levelDifficultyRange.max - levelDifficultyRange.min))
					return false;
			}
			return true;
		}
	}
}
#else
namespace FightRoom
{
	public class SetLevelsWaves : EditorScript
	{
	}
}
#endif
