#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace FightRoom
{
	public class RecalculateUVs : EditorScript
	{
		public MeshFilter meshFilter;
		public bool useSharedMesh;

		public override void Do ()
		{
			if (meshFilter == null)
				meshFilter = GetComponent<MeshFilter>();
			Mesh mesh;
			if (useSharedMesh)
				mesh = meshFilter.sharedMesh;
			else
				mesh = meshFilter.mesh;
			mesh.RecalculateUVDistributionMetric(0);
		}
	}
}
#else
namespace FightRoom
{
	public class RecalculateUVs : EditorScript
	{
	}
}
#endif