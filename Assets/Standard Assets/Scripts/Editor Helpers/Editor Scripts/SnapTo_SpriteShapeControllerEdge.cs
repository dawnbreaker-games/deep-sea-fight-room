#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace FightRoom
{
	public class SnapTo_SpriteShapeControllerEdge : EditorScript
	{
		public Transform trs;
		public _SpriteShapeController spriteShapeController;
		
		public override void Do ()
		{
			_Do (trs, spriteShapeController);
		}

		static void _Do (Transform trs, _SpriteShapeController spriteShapeController)
		{
			Shape2D shape = Shape2D.FromSpline(spriteShapeController.spline);
			trs.position = spriteShapeController.trs.TransformPoint(shape.GetClosestPoint(spriteShapeController.trs.InverseTransformPoint(trs.position)));
		}

		[MenuItem("Tools/Snap other selected Transforms to _SpriteShapeController edge")]
		static void DoForSelected ()
		{
			List<Transform> transforms = new List<Transform>(SelectionExtensions.GetSelected<Transform>());
			_SpriteShapeController spriteShapeController = null;
			for (int i = 0; i < transforms.Count; i ++)
			{
				Transform trs = transforms[i];
				spriteShapeController = trs.GetComponent<_SpriteShapeController>();
				if (spriteShapeController != null)
				{
					transforms.RemoveAt(i);
					break;
				}
			}
			for (int i = 0; i < transforms.Count; i ++)
			{
				Transform trs = transforms[i];
				_Do (trs, spriteShapeController);
			}
		}
	}
}
#else
namespace FightRoom
{
	public class SnapTo_SpriteShapeControllerEdge : EditorScript
	{
	}
}
#endif
