#if UNITY_EDITOR
using System;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class SetTransformAspectRatioToSprite : EditorScript
	{
		public Transform trs;
		public Renderer renderer;
		
		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (renderer == null)
				renderer = GetComponent<Renderer>();
			Texture2D texture = (Texture2D) renderer.sharedMaterial.mainTexture;
			trs.localScale = trs.localScale.SetX((float) texture.width / texture.height * trs.localScale.y);
		}
	}
}
#else
namespace FightRoom
{
	public class SetTransformAspectRatioToSprite : EditorScript
	{
	}
}
#endif
