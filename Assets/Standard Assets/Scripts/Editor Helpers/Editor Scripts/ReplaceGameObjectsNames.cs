#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

using System.Collections.Generic;

namespace FightRoom
{
	public class ReplaceGameObjectsNames : EditorScript
	{
		public GameObject[] gos = new GameObject[0];
		public string whatToReplace;
		public string replaceWith;

		public override void Do ()
		{
			for (int i = 0; i < gos.Length; i ++)
			{
				GameObject go = gos[i];
				go.name = go.name.Replace(whatToReplace, replaceWith);
			}
		}
	}
}
#else
namespace FightRoom
{
	public class ReplaceGameObjectsNames : EditorScript
	{
	}
}
#endif
