#if UNITY_EDITOR
using System;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

namespace FightRoom
{
	public class MergeMeshObjects : EditorScript
	{
		public MeshFilter[] meshFilters = null;

		public override void Do ()
		{
			if (meshFilters.Length == null)
				meshFilters = FindObjectsByType<MeshFilter>(FindObjectsSortMode.None);
		}

		public static void _Do (MeshFilter[] meshFilters)
		{
			List<Material> materials = new List<Material>();
			Shape3D outputShape = new Shape3D();
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				MeshRenderer meshRenderer = meshFilter.GetComponent<MeshRenderer>();
				if (meshRenderer != null && !materials.Contains(meshRenderer.sharedMaterial))
					materials.Add(meshRenderer.sharedMaterial);
				MeshObject meshObject = new MeshObject(meshFilter.GetComponent<Transform>(), meshFilter, meshRenderer );
				Shape3D shape = meshObject.ToShape3D(true);
				outputShape = outputShape.MergeForPorEnlcosed(shape);
				GameManager.DestroyOnNextEditorUpdate (meshFilter.gameObject);
			}
			GameObject go = new GameObject();
			MeshFilter newMeshFilter = go.AddComponent<MeshFilter>();
			Mesh mesh = outputShape.ToMesh();
			mesh.RecalculateNormals();
			mesh.RecalculateTangents();
			newMeshFilter.mesh = mesh;
			if (materials.Count > 0)
			{
				MeshRenderer newMeshRenderer = go.AddComponent<MeshRenderer>();
				newMeshRenderer.sharedMaterials = materials.ToArray();
			}
		}

		[MenuItem("Tools/Merge selected MeshFilters")]
		public static void DoForSelected ()
		{
			_Do (SelectionExtensions.GetSelected<MeshFilter>());
		}
	}
}
#else
namespace FightRoom
{
	public class MergeMeshObjects : EditorScript
	{
	}
}
#endif
