#if UNITY_EDITOR
using System;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class SetSpriteBorder : EditorScript
	{
		public Sprite sprite;
		public uint border;
		
		public override void Do ()
		{
			if (sprite == null)
			{
				Sprite[] sprites = SelectionExtensions.GetSelected<Sprite>();
				if (sprites.Length > 0)
					sprite = sprites[0];
			}
			
		}
	}
}
#else
namespace FightRoom
{
	public class SetSpriteBorder : EditorScript
	{
	}
}
#endif
