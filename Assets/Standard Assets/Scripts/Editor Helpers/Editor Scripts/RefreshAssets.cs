#if UNITY_EDITOR
using TMPro;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using UnityEditor.Callbacks;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	[InitializeOnLoad]
	public class RefreshAssets : PreBuildScript
	{
		public ImportAssetOptions importAssetOptions;

		public override void Do ()
		{
			// AssetDatabase.Refresh(importAssetOptions);
		}

		[DidReloadScripts]
		[MenuItem("Game/Refresh assets for all RefreshAssetss %&c")]
		public static void _DoForAll ()
		{
			RefreshAssets[] instances = FindObjectsByType<RefreshAssets>(FindObjectsSortMode.None);
			for (int i = 0; i < instances.Length; i ++)
			{
				RefreshAssets instance = instances[i];
				instance.Do ();
			}
		}
	}
}
#else
namespace FightRoom
{
	public class RefreshAssets : EditorScript
	{
	}
}
#endif