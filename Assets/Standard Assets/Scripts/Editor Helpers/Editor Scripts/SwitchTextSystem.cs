#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

namespace FightRoom
{
	public class SwitchTextSystem : EditorScript
	{
		public GameObject go;
		public TextSystem targetTextSystem;

		public override void Do ()
		{
			if (go == null)
				go = gameObject;
			_Text text = go.GetComponent<_Text>();
			if (text != null)
			{
				if (targetTextSystem == TextSystem.UnityUI)
				{
					Graphic graphic = go.GetComponent<Graphic>();
					if (graphic != null)
					{
						GameManager.DestroyOnNextEditorUpdate (graphic);
						EditorApplication.update += Do;
						return;
					}
					Text newText = go.AddComponent<Text>();
					newText.font = text.fontAsset.sourceFontFile;
					string textAnchor = text.alignmentOptions.ToString();
					if (textAnchor == "Center")
						newText.alignment = TextAnchor.MiddleCenter;
					else if (textAnchor == "Right")
						newText.alignment = TextAnchor.MiddleRight;
					else if (textAnchor == "Left")
						newText.alignment = TextAnchor.MiddleLeft;
					else
					{
						textAnchor = textAnchor.Replace("Top", "Upper");
						newText.alignment = (TextAnchor) Enum.Parse(typeof(TextAnchor), textAnchor);
					}
					newText.resizeTextForBestFit = true;
					newText.resizeTextMinSize = 0;
					newText.resizeTextMaxSize = int.MaxValue;
					newText.text = text.text;
					newText.color = text.material.GetColor("_tint");
					GameManager.DestroyOnNextEditorUpdate (text);
				}
				else
				{

				}
			}
			else
			{
				Text text2 = go.GetComponent<Text>();
				if (text2 != null)
				{
					if (targetTextSystem == TextSystem.TMPro)
					{
						
					}
					else
					{

					}
				}
				else
				{
					
				}
			}
		}

		public enum TextSystem
		{
			UnityUI,
			TMPro,
			Custom
		}
	}
}
#else
namespace FightRoom
{
	public class SwitchTextSystem : EditorScript
	{
	}
}
#endif