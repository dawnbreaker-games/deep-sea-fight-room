#if UNITY_EDITOR
using System;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class UnfurlAnimation : EditorScript
	{
		public float value;
		public RotationSpeedVectorPath rotationSpeedVectorPath;
		public SerializableDictionary<int, TransformGroup> spawnPointsDict = new SerializableDictionary<int, TransformGroup>();
		public float cubeDepth;
		public Transform cubePrefabTrs;
		public Transform[] cubeTransforms = new Transform[0];
		
		public override void Do ()
		{
			spawnPointsDict.Init ();
			for (int i = 0; i < this.cubeTransforms.Length; i ++)
			{
				Transform cubeTransform = this.cubeTransforms[i];
				if (cubeTransform != null)
					GameManager.DestroyOnNextEditorUpdate (cubeTransform.gameObject);
			}
			string valueString = "" + value;
			Transform[] spawnPoints = spawnPointsDict[int.Parse("" + valueString[0])].transforms;
			float distance = rotationSpeedVectorPath.distances[0];
			Vector3 position = rotationSpeedVectorPath.Get(distance, cubeDepth);
			Vector3 previousPosition = Quaternion.LookRotation(rotationSpeedVectorPath.startDirection) * Vector3.back;
			float maxDistance = rotationSpeedVectorPath.distances[rotationSpeedVectorPath.distances.Length - 1];
			List<Transform> cubeTransforms = new List<Transform>();
			while (distance < maxDistance)
			{
				distance += cubeDepth;
				previousPosition = position;
				position = rotationSpeedVectorPath.Get(distance, cubeDepth);
				char valueChar = valueString[Mathf.Clamp(Mathf.RoundToInt(distance / maxDistance * valueString.Length), 0, valueString.Length - 1)];
				int valueDigit = int.Parse("" + valueChar);
				for (int i = 0; i < spawnPoints.Length; i ++)
				{
					Transform spawnPoint = spawnPoints[i];
					for (int i2 = 0; i2 < valueDigit; i2 ++)
					{
						Quaternion rotation = Quaternion.AxisAngle(position - previousPosition, (float) i2 / valueDigit * 360);
						Transform cubeTrs = Instantiate(cubePrefabTrs, spawnPoint.TransformPoint(position), Quaternion.LookRotation(spawnPoint.TransformDirection(position - previousPosition)));
						cubeTrs.localScale = new Vector3(1, 1, cubeDepth);
						cubeTransforms.Add(cubeTrs);
					}
				}
			}
			this.cubeTransforms = cubeTransforms.ToArray();
		}

		[Serializable]
		public struct RotationSpeedVectorPath
		{
			public Quaternion[] rotations;
			public Vector3 startDirection;
			public float[] distances;

			public Vector3 Get (float distance, float checkInterval)
			{
				if (distance == 0)
					return Vector3.zero;
				Vector3 output = Vector3.zero;
				float maxDistance = distances[distances.Length - 1];
				Vector3 forward = startDirection;
				float normalizedDistance = 0;
				float previousDistance = distances[0];
				for (int i = 1; i < rotations.Length; i ++)
				{
					float pointDistance = distances[i];
					float distanceChange = pointDistance - previousDistance;
					distance -= checkInterval;
					if (distance < 0)
						distance = 0;
					normalizedDistance = distance / maxDistance;
					forward = Quaternion.Slerp(Quaternion.LookRotation(forward), rotations[i], normalizedDistance) * forward;
					forward = forward.normalized;
					output += forward * checkInterval;
					previousDistance = pointDistance;
				}
				return output;
			}
		}

		[Serializable]
		public struct TransformGroup
		{
			public Transform[] transforms;
		}
	}
}
#endif
