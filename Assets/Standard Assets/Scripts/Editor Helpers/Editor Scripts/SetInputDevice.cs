#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace FightRoom
{
	public class SetInputDevice : EditorScript
	{
		const string PATH_TO_INPUT_MANAGER = "Assets/Prefabs/Managers (Prefabs)/Input Manager.prefab";

		[MenuItem("Game/Use keyboard and mouse")]
		public static void SetToKeyboardAndMouse ()
		{
			Set (InputManager.InputDevice.KeyboardAndMouse);
		}

		[MenuItem("Game/Use VR")]
		public static void SetToPhone ()
		{
			Set (InputManager.InputDevice.Phone);
		}

		public static void Set (InputManager.InputDevice inputDevice)
		{
			InputManager inputManager = (InputManager) AssetDatabase.LoadAssetAtPath(PATH_TO_INPUT_MANAGER, typeof(InputManager));
			inputManager.inputDevice = inputDevice;
			PrefabUtility.SavePrefabAsset(inputManager.gameObject);
		}
	}
}
#else
namespace FightRoom
{
	public class SetInputDevice : EditorScript
	{
	}
}
#endif