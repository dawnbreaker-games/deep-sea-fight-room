#if UNITY_EDITOR
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class SwitchBetweenSurvivalAndKillingAchievementsBasedOnChance : EditorScript
	{
		public float enemiesKilledPerSecond;
		public Achievement achievement;
		public SurvivalAchievement survivalAchievementPrefab;
		public KillingAchievement killingAchievementPrefab;
		[Range(0, 1)]
		public float chanceOfSwitch;
		
		public override void Do ()
		{
			if (achievement == null)
				achievement = GetComponent<Achievement>();
			_Do (enemiesKilledPerSecond, achievement, survivalAchievementPrefab, killingAchievementPrefab, chanceOfSwitch);
		}

		public static void _Do (float enemiesKilledPerSecond, Achievement achievement, SurvivalAchievement survivalAchievementPrefab, KillingAchievement killingAchievementPrefab, float chanceOfSwitch)
		{
			if (Random.value > chanceOfSwitch)
				return;
			SurvivalAchievement survivalAchievement = achievement as SurvivalAchievement;
			Level levelIAmFoundOn = achievement.levelIAmFoundOn;
			Level[] levels = achievement.levels;
			Level unlockLevelOnComplete = achievement.unlockLevelOnComplete;
			Achievement newAchievement = null;
			Transform achievementTrs = achievement.GetComponent<Transform>();
			if (survivalAchievement != null)
			{
				KillingAchievement newKillingAchievement = (KillingAchievement) PrefabUtility.InstantiatePrefab(killingAchievementPrefab);
				Transform newKillingAchievementTrs = newKillingAchievement.GetComponent<Transform>();
				newKillingAchievementTrs.position = achievementTrs.position;
				newKillingAchievementTrs.eulerAngles = achievementTrs.eulerAngles;
				newKillingAchievementTrs.SetParent(achievementTrs.parent);
				newKillingAchievement.levelsThatNeedToReachEnemiesKilled = survivalAchievement.levelsThatNeedToReachTime;
				newKillingAchievement.enemiesKilled = (uint) ((float) survivalAchievement.time / enemiesKilledPerSecond);
				newAchievement = newKillingAchievement;
			}
			else
			{
				KillingAchievement killingAchievement = achievement as KillingAchievement;
				if (killingAchievement != null)
				{
					SurvivalAchievement newSurvivalAchievement = (SurvivalAchievement) PrefabUtility.InstantiatePrefab(survivalAchievementPrefab);
					Transform newSurvivalAchievementTrs = newSurvivalAchievement.GetComponent<Transform>();
					newSurvivalAchievementTrs.position = achievementTrs.position;
					newSurvivalAchievementTrs.eulerAngles = achievementTrs.eulerAngles;
					newSurvivalAchievementTrs.SetParent(achievementTrs.parent);
					newSurvivalAchievement.levelsThatNeedToReachTime = killingAchievement.levelsThatNeedToReachEnemiesKilled;
					newSurvivalAchievement.time = (uint) ((float) killingAchievement.enemiesKilled * enemiesKilledPerSecond);
					newAchievement = newSurvivalAchievement;
				}
			}
			if (newAchievement != null)
			{
				newAchievement.levelIAmFoundOn = levelIAmFoundOn;
				newAchievement.levels = levels;
				newAchievement.unlockLevelOnComplete = unlockLevelOnComplete;
				GameManager.DestroyOnNextEditorUpdate (achievement.gameObject);
			}
		}

		[MenuItem("Game/Switch between survival and killing achievements based on chance for selected")]
		static void DoForSelected ()
		{
			Achievement[] achievements = SelectionExtensions.GetSelected<Achievement>();
			SwitchBetweenSurvivalAndKillingAchievementsBasedOnChance switchBetweenSurvivalAndKillingAchievementsBasedOnChance = SelectionExtensions.GetSelected<SwitchBetweenSurvivalAndKillingAchievementsBasedOnChance>()[0];
			achievements = achievements.Randomize<Achievement>();
			uint achievementsToSwtich = (uint) (switchBetweenSurvivalAndKillingAchievementsBasedOnChance.chanceOfSwitch * achievements.Length);
			for (int i = 0; i < achievementsToSwtich; i ++)
			{
				Achievement achievement = achievements[i];
				_Do (switchBetweenSurvivalAndKillingAchievementsBasedOnChance.enemiesKilledPerSecond, achievement, switchBetweenSurvivalAndKillingAchievementsBasedOnChance.survivalAchievementPrefab, switchBetweenSurvivalAndKillingAchievementsBasedOnChance.killingAchievementPrefab, 1);
			}
		}
	}
}
#else
namespace FightRoom
{
	public class SwitchBetweenSurvivalAndKillingAchievementsBasedOnChance : EditorScript
	{
	}
}
#endif
