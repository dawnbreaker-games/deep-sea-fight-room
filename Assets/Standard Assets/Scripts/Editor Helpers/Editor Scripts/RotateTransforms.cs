#if UNITY_EDITOR
using UnityEngine;

namespace FightRoom
{
	public class RotateTransforms : EditorScript
	{
		public Transform[] transforms = new Transform[0];
		public Vector3 rotation;

		public override void Do ()
		{
			for (int i = 0; i < transforms.Length; i ++)
			{
				Transform trs = transforms[i];
				trs.eulerAngles += rotation;
			}
		}
	}
}
#else
namespace FightRoom
{
	public class RotateTransforms : EditorScript
	{
	}
}
#endif
