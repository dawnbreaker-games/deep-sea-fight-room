#if UNITY_EDITOR
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class MultiplySurvivalAndKillingAchievementsDifficulty : EditorScript
	{
		public float mulitplyDifficulty;
		public Achievement achievement;
		
		public override void Do ()
		{
			if (achievement == null)
				achievement = GetComponent<Achievement>();
			_Do (mulitplyDifficulty, achievement);
		}

		public static void _Do (float mulitplyDifficulty, Achievement achievement)
		{
			SurvivalAchievement survivalAchievement = achievement as SurvivalAchievement;
			uint previousRequiredValue = 0;
			if (survivalAchievement != null)
			{
				previousRequiredValue = survivalAchievement.time;
				survivalAchievement.time = (uint) (mulitplyDifficulty * previousRequiredValue);
			}
			else
			{
				KillingAchievement killingAchievement = achievement as KillingAchievement;
				if (killingAchievement != null)
				{
					previousRequiredValue = killingAchievement.enemiesKilled;
					killingAchievement.enemiesKilled = (uint) (mulitplyDifficulty * previousRequiredValue);
				}
			}
			if (previousRequiredValue > 0)
			{
				achievement.name = achievement.name.Replace("" + previousRequiredValue, "" + (uint) (mulitplyDifficulty * previousRequiredValue));
				achievement.displayName = achievement.displayName.Replace("" + previousRequiredValue, "" + (uint) (mulitplyDifficulty * previousRequiredValue));
				achievement.enabled = !achievement.enabled;
				achievement.enabled = !achievement.enabled;
			}
		}

		[MenuItem("Game/Multiply selected survival and killing achievements' difficulties")]
		static void DoForSelected ()
		{
			Achievement[] achievements = SelectionExtensions.GetSelected<Achievement>();
			MultiplySurvivalAndKillingAchievementsDifficulty[] multiplySurvivalAndKillingAchievementsDifficulties = SelectionExtensions.GetSelected<MultiplySurvivalAndKillingAchievementsDifficulty>();
			for (int i = 0; i < multiplySurvivalAndKillingAchievementsDifficulties.Length; i ++)
			{
				MultiplySurvivalAndKillingAchievementsDifficulty multiplySurvivalAndKillingAchievementsDifficulty = multiplySurvivalAndKillingAchievementsDifficulties[i];
				for (int i2 = 0; i2 < achievements.Length; i2 ++)
				{
					Achievement achievement = achievements[i2];
					_Do (multiplySurvivalAndKillingAchievementsDifficulty.mulitplyDifficulty, achievement);
				}
			}
		}

		[MenuItem("Game/Multiply all survival and killing achievements' difficulties")]
		static void DoForAll ()
		{
			Achievement[] achievements = FindObjectsByType<Achievement>(FindObjectsSortMode.None);
			MultiplySurvivalAndKillingAchievementsDifficulty[] multiplySurvivalAndKillingAchievementsDifficulties = FindObjectsByType<MultiplySurvivalAndKillingAchievementsDifficulty>(FindObjectsSortMode.None);
			for (int i = 0; i < multiplySurvivalAndKillingAchievementsDifficulties.Length; i ++)
			{
				MultiplySurvivalAndKillingAchievementsDifficulty multiplySurvivalAndKillingAchievementsDifficulty = multiplySurvivalAndKillingAchievementsDifficulties[i];
				for (int i2 = 0; i2 < achievements.Length; i2 ++)
				{
					Achievement achievement = achievements[i2];
					_Do (multiplySurvivalAndKillingAchievementsDifficulty.mulitplyDifficulty, achievement);
				}
			}
		}
	}
}
#else
namespace FightRoom
{
	public class MultiplySurvivalAndKillingAchievementsDifficulty : EditorScript
	{
	}
}
#endif
