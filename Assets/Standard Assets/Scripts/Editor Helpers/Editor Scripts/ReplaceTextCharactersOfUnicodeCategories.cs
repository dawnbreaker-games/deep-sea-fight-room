#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEditor;

namespace FightRoom
{
	public class ReplaceTextCharactersOfUnicodeCategories : EditorScript
	{
		[Multiline]
		public string input;
		[Multiline]
		public string output;
		public UnicodeCategories categories;
		[Multiline]
		public string replace;

		public override void Do ()
		{
			output = Do(input, categories, replace);
		}

		public static string Do (string input, UnicodeCategories categories, string replace)
		{
			string output = input;
			for (int i = 0; i < input.Length; i ++)
			{
				char c = input[i];
				if (categories.HasFlag((UnicodeCategories) Mathf.Pow(2, 1 + Char.GetUnicodeCategory(c).GetHashCode())))
				{
					output = output.Remove(i, 1);
					output = output.Insert(i, replace);
					i --;
					i += replace.Length;
				}
			}
			return output;
		}
	}
}
#else
namespace FightRoom
{
	public class ReplaceTextCharactersOfUnicodeCategories : EditorScript
	{
	}
}
#endif