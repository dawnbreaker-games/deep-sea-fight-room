#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[ExecuteInEditMode]
	public class SetAxisAngleRotation : EditorScript
	{
		public Transform trs;
		public Vector3 axis;
		public float angle;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			trs.rotation = Quaternion.AxisAngle(axis, angle);
		}
	}
}
#else
namespace FightRoom
{
	public class SetAxisAngleRotation : EditorScript
	{
	}
}
#endif