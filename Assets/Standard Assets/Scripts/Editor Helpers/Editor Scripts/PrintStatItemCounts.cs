#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class PrintStatItemCounts : EditorScript
	{
		public override void Do ()
		{
			Dictionary<string, byte> statItemCountsDict = new Dictionary<string, byte>();
			for (int i = 0; i < ShopMenu.Instance.allItems.Length; i ++)
			{
				Item item = ShopMenu.instance.allItems[i];
				for (int i2 = 0; i2 < item.combinedWith.Length; i2 ++)
				{
					Item item2 = item.combinedWith[i2];
					AddToStatItem addToStatItem = item2 as AddToStatItem;
					if (addToStatItem != null)
					{
						byte statItemCount;
						string key = addToStatItem.statName;
						if (addToStatItem.amount > 0)
							key = "+" + key;
						else
							key = "-" + key;
						if (statItemCountsDict.TryGetValue(key, out statItemCount))
							statItemCountsDict[key] = (byte) (statItemCount + 1);
						else
							statItemCountsDict[key] = 1;
					}
				}
			}
			foreach (KeyValuePair<string, byte> KeyValuePair in statItemCountsDict)
				print(KeyValuePair.Key + " " + KeyValuePair.Value);
		}
	}
}
#else
namespace FightRoom
{
	public class PrintStatItemCounts : EditorScript
	{
	}
}
#endif
