#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FightRoom
{
	public class AutoBuildProject : EditorScript
	{
		public uint buildAfterInactiveTime;
		public bool onlyBuildWithProjectClones = true;
		public _Regex clonedProjectNamesRegex;
		float timeSinceBuilt;
		
		public override void Do ()
		{
			if (UnityEditorInternal.InternalEditorUtility.isApplicationActive)
				timeSinceBuilt = Time.realtimeSinceStartup;
			else if (Time.realtimeSinceStartup - timeSinceBuilt > buildAfterInactiveTime)
			{
				if (!onlyBuildWithProjectClones || Regex.IsMatch(Application.dataPath, clonedProjectNamesRegex.pattern, clonedProjectNamesRegex.options))
					BuildManager.Instance._Build ();
				timeSinceBuilt = Time.realtimeSinceStartup;
			}
		}
	}
}
#else
namespace FightRoom
{
	public class AutoBuildProject : EditorScript
	{
	}
}
#endif
