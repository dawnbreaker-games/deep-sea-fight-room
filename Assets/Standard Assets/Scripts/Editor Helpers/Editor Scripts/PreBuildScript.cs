#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace FightRoom
{
	public class PreBuildScript : EditorScript
	{
		public static List<PreBuildScript> instances = new List<PreBuildScript>();

		public override void OnEnable ()
		{
			base.OnEnable ();
			instances.Add(this);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			instances.Remove(this);
		}
	}
}
#else
namespace FightRoom
{
	public class PreBuildScript : EditorScript
	{
	}
}
#endif
