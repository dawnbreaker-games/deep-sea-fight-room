#if UNITY_EDITOR
using Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace FightRoom
{
	public class OffsetRectTransformPositionByASizeDelta : EditorScript
	{
		public RectTransform getRectTrs;
		public RectTransform setRectTrs;
		
		public override void Do ()
		{
			if (getRectTrs == null)
				getRectTrs = GetComponent<RectTransform>();
			if (setRectTrs == null)
				setRectTrs = GetComponent<RectTransform>();
			_Do (getRectTrs, setRectTrs);
		}

		public static void _Do (RectTransform getRectTrs, RectTransform setRectTrs)
		{
			setRectTrs.position = getRectTrs.sizeDelta;
		}

		// [MenuItem("Game/Multiply selected survival and killing achievements' difficulties")]
		// static void DoForSelected ()
		// {
		// 	Achievement[] achievements = SelectionExtensions.GetSelected<Achievement>();
		// 	MultiplySurvivalAndKillingAchievementsDifficulty[] multiplySurvivalAndKillingAchievementsDifficulties = SelectionExtensions.GetSelected<MultiplySurvivalAndKillingAchievementsDifficulty>();
		// 	for (int i = 0; i < multiplySurvivalAndKillingAchievementsDifficulties.Length; i ++)
		// 	{
		// 		MultiplySurvivalAndKillingAchievementsDifficulty multiplySurvivalAndKillingAchievementsDifficulty = multiplySurvivalAndKillingAchievementsDifficulties[i];
		// 		for (int i2 = 0; i2 < achievements.Length; i2 ++)
		// 		{
		// 			Achievement achievement = achievements[i2];
		// 			_Do (multiplySurvivalAndKillingAchievementsDifficulty.mulitplyDifficulty, achievement);
		// 		}
		// 	}
		// }

		// [MenuItem("Game/Multiply all survival and killing achievements' difficulties")]
		// static void DoForAll ()
		// {
		// 	Achievement[] achievements = FindObjectsByType<Achievement>();
		// 	MultiplySurvivalAndKillingAchievementsDifficulty[] multiplySurvivalAndKillingAchievementsDifficulties = FindObjectsByType<MultiplySurvivalAndKillingAchievementsDifficulty>();
		// 	for (int i = 0; i < multiplySurvivalAndKillingAchievementsDifficulties.Length; i ++)
		// 	{
		// 		MultiplySurvivalAndKillingAchievementsDifficulty multiplySurvivalAndKillingAchievementsDifficulty = multiplySurvivalAndKillingAchievementsDifficulties[i];
		// 		for (int i2 = 0; i2 < achievements.Length; i2 ++)
		// 		{
		// 			Achievement achievement = achievements[i2];
		// 			_Do (multiplySurvivalAndKillingAchievementsDifficulty.mulitplyDifficulty, achievement);
		// 		}
		// 	}
		// }
	}
}
#else
namespace FightRoom
{
	public class OffsetRectTransformPositionByASizeDelta : EditorScript
	{
	}
}
#endif
