#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

namespace FightRoom
{
	public class SetLightIntensity : EditorScript
	{
		public Light light;
		public float multiplyIntensity;

		public override void Do ()
		{
			if (light == null)
				light = GetComponent<Light>();
			light.intensity *= multiplyIntensity;
		}
	}
}
#else
namespace FightRoom
{
	public class SetLightIntensity : EditorScript
	{
	}
}
#endif
